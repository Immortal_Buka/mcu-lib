#include <stdint.h>
//
typedef enum
{
	uroboros,
	other,
} cb_error_t;
//
class circular_buffer_t
{
	private:
		uint32_t size = 0;
		uint32_t write_index = 0;
		uint32_t read_index = 0;
		uint32_t user_index = 0;
		uint8_t* buffer = 0;
	public:
		circular_buffer_t(uint8_t* memory_ptr, uint32_t memory_size);
		void write(uint8_t* data_in_ptr, uint32_t data_in_size);
		uint32_t read(uint8_t* data_out_ptr, uint32_t data_out_size);
		void reset(void);
		uint32_t count_read(void);
		uint32_t count_write(void);
};

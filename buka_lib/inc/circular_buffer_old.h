/*
description: 	circular buffer
version:		190820
author:			Buka
history:
181127
- initial version
190603
- circle_buffer -> circle_buffer_t
190820
- add cb_error_t
191011
- add cb_ssum32
*/
#ifndef _CIRCULAR_BUFFER_H_
#define _CIRCULAR_BUFFER_H_
//
#include <stdint.h>
//
#define ALL	0xffffffff
//
typedef struct
{
	volatile uint32_t size;
	volatile uint32_t write_index;
	volatile uint32_t read_index;
	volatile uint32_t user_index;
	volatile uint8_t* buffer;
} circle_buffer_t;
typedef enum
{
	uroboros,
	other,
} cb_error_t;
//
void cb_write(circle_buffer_t* buffer, uint8_t* data_in_ptr, uint32_t data_in_size);
uint32_t cb_read(circle_buffer_t* buffer, uint8_t* data_out_ptr, uint32_t data_out_size);
uint32_t cb_read_to_void(circle_buffer_t* buffer, uint32_t data_out_inc);
void cb_reset(circle_buffer_t* buffer);
void cb_init(circle_buffer_t* buffer, uint8_t* memory_ptr, uint32_t memory_size);
uint32_t cb_calc_unread(circle_buffer_t* buffer);
uint32_t cb_read_ui(circle_buffer_t* buffer, uint8_t* data_out_ptr, uint32_t data_out_size);
uint32_t cb_calc_unread_ui(circle_buffer_t* buffer);
uint32_t cb_test_read(circle_buffer_t* buffer, uint8_t* data_out_ptr, uint32_t data_out_size);
int32_t cb_ssum32(circle_buffer_t* buffer);
//
#endif//_CIRCULAR_BUFFER_H_

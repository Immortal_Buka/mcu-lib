/*
description: 	color definitions & conversions
version:		190227
author:			Buka
history:
190227
- initial version
*/
#ifndef _COLOR_H_
#define _COLOR_H_
//
#include <stdint.h>
#include "arm_math.h"
//
typedef union
{
	struct
	{
		uint8_t g;
		uint8_t r;
		uint8_t b;
	} color;
	uint8_t raw[3];
} rgb_888_t;
typedef union
{
	struct
	{
		uint16_t b : 5;
		uint16_t g : 6;
		uint16_t r : 5;
	} color;
	uint16_t raw;
} rgb_565_t;
typedef union
{
	struct
	{
		uint16_t h;//hue       : 0..359
		uint8_t s;//saturation : 0..255 (white - color)
		uint8_t v;//value      : 0..255 (black - color)
	} color;
	uint32_t raw;
} hsv_t;
typedef union
{
	struct
	{
		uint16_t h;//hue       : 0..359
		uint8_t s;//saturation : 0..255
		uint8_t l;//lightness  : 0..255
	} color;
	uint32_t raw;
} hsl_t;
typedef struct
{
	uint8_t min;
	uint8_t max;
} rgb_mm_t;
typedef enum
{
	C_Aqua				= 0x07ff,
	C_Aquamarine		= 0x7ffa,
	C_Black				= 0x0000,
	C_Blue				= 0x001f,
	C_BlueViolet		= 0x895c,
	C_Brown 			= 0xa145,
	C_Chartreuse		= 0x7fe0,
	C_Chocolate 		= 0xd343,
	C_Coral				= 0xfbea,
	C_CornflowerBlue 	= 0x64bd,
	C_Crimson 			= 0xd8a7,
	C_Cyan				= 0x07ff,
	C_DarkBlue			= 0x0011,
	C_DarkCyan			= 0x0451,
	C_DarkGoldenRod		= 0xbc21,
	C_DarkGreen			= 0x0320,
	C_DarkGrey			= 0xad55,
	C_DarkMagenta 		= 0x8811,
	C_DarkOrange 		= 0xfc60,
	C_DarkOrchid 		= 0x9999,
	C_DarkRed			= 0x8800,
	C_DarkSalmon 		= 0xecaf,
	C_DarkSeaGreen		= 0x8df1,
	C_DarkSlateBlue 	= 0x49f1,
	C_DarkSlateGrey 	= 0x2a69,
	C_DarkTurquoise 	= 0x067a,
	C_DarkViolet 		= 0x901a,
	C_DeepPink			= 0xf8b2,
	C_DeepSkyBlue 		= 0x05ff,
	C_DimGrey 			= 0x6b4d,
	C_DodgerBlue 		= 0x1c9f,
	C_FireBrick			= 0xb104,
	C_ForestGreen		= 0x2444,
	C_Fuchsia			= 0xf81f,
	C_Gainsboro			= 0xdefb,
	C_Gold				= 0xfea0,
	C_GoldenRod			= 0xdd24,
	C_Green				= 0x0400,
	C_GreenYellow		= 0xafe5,
	C_Grey				= 0x8410,
	C_HotPink 			= 0xfb56,
	C_IndianRed 		= 0xcaeb,
	C_Indigo			= 0x4810,
	C_LawnGreen 		= 0x7fe0,
	C_LightBlue 		= 0xaedc,
	C_LightCoral 		= 0xf410,
	C_LightGreen 		= 0x9772,
	C_LightGrey 		= 0xd69a,
	C_LightPink 		= 0xfdb8,
	C_LightSalmon 		= 0xfd0f,
	C_LightSeaGreen 	= 0x2595,
	C_LightSkyBlue 		= 0x867f,
	C_LightSlateGrey 	= 0x7453,
	C_LightSteelBlue	= 0xb63b,
	C_Lime				= 0x07e0,
	C_LimeGreen 		= 0x3666,
	C_Magenta			= 0xf81f,
	C_Maroon			= 0x8000,
	C_MediumAquaMarine 	= 0x6675,
	C_MediumBlue 		= 0x0019,
	C_MediumOrchid 		= 0xbaba,
	C_MediumPurple 		= 0x939b,
	C_MediumSeaGreen 	= 0x3d8e,
	C_MediumSlateBlue 	= 0x7b5d,
	C_MediumSpringGreen = 0x07d3,
	C_MediumTurquoise 	= 0x4e99,
	C_MediumVioletRed 	= 0xc0b0,
	C_Navy				= 0x0010,
	C_Olive				= 0x8400,
	C_OliveDrab			= 0x6c64,
	C_Orange			= 0xfd20,
	C_OrangeRed			= 0xfa20,
	C_Orchid			= 0xdb9a,
	C_PaleGreen 		= 0x9fd3,
	C_PaleTurquoise 	= 0xaf7d,
	C_PaleVioletRed 	= 0xdb92,
	C_Peru				= 0xcc27,
	C_Pink				= 0xfe19,
	C_Plum				= 0xdd1b,
	C_PowderBlue 		= 0xb71c,
	C_Purple			= 0x8010,
	C_RebeccaPurple 	= 0x6193,
	C_Red				= 0xf800,
	C_RoyalBlue 		= 0x435c,
	C_SaddleBrown		= 0x8a22,
	C_Salmon			= 0xfc0e,
	C_SandyBrown 		= 0xf52c,
	C_SeaGreen 			= 0x2c4a,
	C_Sienna 			= 0xa285,
	C_Silver 			= 0xc618,
	C_SkyBlue			= 0x867d,
	C_SlateBlue 		= 0x6ad9,
	C_SlateGrey 		= 0x7412,
	C_SpringGreen		= 0x07ef,
	C_SteelBlue 		= 0x4416,
	C_Tan				= 0xd5b1,
	C_Teal				= 0x0410,
	C_Thistle 			= 0xddfb,
	C_Tomato 			= 0xfb08,
	C_Turquoise 		= 0x471a,
	C_Violet			= 0xec1d,
	C_Wheat 			= 0xf6f6,
	C_White				= 0xffff,
	C_Yellow			= 0xffe0,
	C_YellowGreen		= 0x9e66
} rgb_565_e;
typedef rgb_565_e colors;
//
void HSV_to_RGB_int(hsv_t* hsv, rgb_888_t* rgb);
void RGB_888_to_565(rgb_888_t* in, rgb_565_t* out);
void RGB_max_min(rgb_888_t* rgb, rgb_mm_t* mm);
uint16_t RGB_to_H(rgb_888_t* rgb, rgb_mm_t* mm);
void RGB_to_HSV(rgb_888_t* rgb, hsv_t* hsv);
void HSV_to_RGB(hsv_t* hsv, rgb_888_t* rgb);
void RGB_to_HSL(rgb_888_t* rgb, hsl_t* hsl);
void HSL_to_RGB(hsl_t* hsl, rgb_888_t* rgb);
void H_to_RGB(rgb_888_t* rgb, uint16_t* h, uint8_t v1, uint8_t v2, uint8_t v3, uint8_t v4);
void HSL_to_HSV(hsl_t* hsl, hsv_t* hsv);
void HSV_to_HSL(hsv_t* hsv, hsl_t* hsl);
//
#endif//_COLOR_H_

#include <stdint.h>
//
class console_t
{
	private:
		void* line_uart;
		uint8_t line[256];
		uint8_t line_counter = 0;
	public:
		console_t(void* uart) : line_uart(uart){};
		void in_char(uint8_t* data, uint8_t len);
};

#include <stdint.h>
//
template <typename T> class crc_base
{
	public:
		virtual T calc(uint8_t* data, uint32_t size) = 0;
};
template <typename T> class crc : crc_base<T>
{
	private:
		T polynom;
		T init;
		T end;
		T table[256];
	public:
		crc(T poly_value, T init_value, T end_value) : polynom(poly_value), init(init_value), end(end_value)
		{
			T temp_loc_1;
			constexpr uint8_t temp_loc_2 = sizeof(T);
			for(uint16_t i = 0; i < 256; i++)
			{
				temp_loc_1 = i<<((temp_loc_2-1)*8);
				for(uint8_t j = 0; j < 8; j++)
				{
					if((temp_loc_1 & (1<<(temp_loc_2*8-1))) != 0)
					{
						temp_loc_1 <<= 1;
						temp_loc_1 ^= polynom;
					}
					else temp_loc_1 <<= 1;
				}
				table[i] = temp_loc_1;
			}
		};
		T calc(uint8_t* data, uint32_t size)
		{
			T temp_loc_1 = init;
			constexpr uint8_t temp_loc_2 = (sizeof(T)-1)*8;
			for(uint32_t i=0; i<size; i++) temp_loc_1 = table[(temp_loc_1^(data[i]<<temp_loc_2))>>temp_loc_2] ^ (temp_loc_1 << 8);
			return (temp_loc_1 ^ end);
		}
};

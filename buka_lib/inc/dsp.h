#ifndef _DSP_H_
#define _DSP_H_
//
#include "arm_math.h"
//
#ifdef __cplusplus
extern "C" {
#endif
//
uint8_t linear_interpolation_f32(float32_t* xx, float32_t* yy, uint16_t n, float32_t x, float32_t* y);
uint8_t linear_interpolation_i32(int32_t* xx, int32_t* yy, uint16_t n, int32_t x, int32_t* y);
int32_t median_i32(int32_t new_value, int32_t* old_values, uint8_t lenght);
uint8_t bilinear_interpolation_f32(float32_t* a, uint16_t x_size, uint16_t y_size, float32_t x, float32_t y, float32_t* z);
uint8_t linear_interpolation_q31(q31_t* xx, q31_t* yy, uint16_t n, q31_t x, q31_t* y);
q31_t q31_div(q31_t a, q31_t b);
//
#ifdef __cplusplus
} // extern "C" block end
#endif
//
#endif//_DSP_H_

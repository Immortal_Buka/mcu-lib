/*
description: 	EEG file FATFS expansion
version:		180316
author:			Buka
history:
180316
- initial version
*/
#ifndef _EEG_H_
#define _EEG_H_
//
#include "ff.h"
#include "print.h"
#include "time.h"
//
#define EEG_VERSION_0		0xee000001
//
typedef struct __attribute__((packed, aligned(4)))
{
	uint32_t eeg_file_version;
	uint32_t patient_id;
	uint32_t record_id;
	datetime_t start_time;
	uint8_t number_of_channels;
	uint8_t text[31];
} file_header_t;
typedef struct __attribute__((packed, aligned(4)))
{
	uint8_t name[7];
	uint16_t sample_rate;
	uint8_t text[31];
	int32_t data_min;
	int32_t data_max;
	int32_t real_min;
	int32_t real_max;
	uint8_t dimension[4];
} channel_header_t;
typedef struct __attribute__((packed, aligned(4)))
{
	uint8_t channel;
	uint8_t reserved[3];
	int32_t data;
} data_record_t;
//
#endif//_EEG_H_







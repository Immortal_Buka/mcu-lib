#ifndef _JTAG_H_
#define _JTAG_H_
//
#include <stdint.h>
//
typedef struct
{
	int8_t ir_len;
	uint8_t ir_data_tx;
	uint8_t dr_len;
	uint64_t dr_data_tx;
	uint64_t dr_data_rx;
} jtag_device_t;
class jtag_pin_interface_t
{
	public:
		virtual void TMS(uint8_t tms) = 0;
		virtual void TCK(uint8_t tck) = 0;
		virtual void TDI(uint8_t tdi) = 0;
		virtual uint8_t TDO(void) = 0;
};
class jtag_chain_t
{
	private:
		jtag_pin_interface_t* pi;
		jtag_device_t* jtag;
		uint8_t size;
		void pulse_TCK(void);
	public:
		jtag_chain_t(jtag_pin_interface_t* pi_init, jtag_device_t* jtag_init, uint8_t size_init) : pi(pi_init), jtag(jtag_init), size(size_init){};
		static uint64_t jtag_swap(uint64_t in);
		void jtag_ir(void);
		void jtag_dr(void);
		void jtag_init(void);
};
#endif//_JTAG_H_

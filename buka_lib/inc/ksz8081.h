/*
description: 	KSZ8081 header
version:		190212
author:			Buka
history:
190212
- initial version
*/
#include <stdint.h>
//
#define PHY_BASICCONTROL_REG 		0x00	/*!< The PHY basic control register. */
#define PHY_BASICSTATUS_REG 		0x01	/*!< The PHY basic status register. */
#define PHY_ID1_REG 				0x02	/*!< The PHY ID one register. */
#define PHY_ID2_REG 				0x03	/*!< The PHY ID two register. */
#define PHY_AUTONEG_ADVERTISE_REG 	0x04	/*!< The PHY auto-negotiate advertise register. */
#define PHY_CONTROL1_REG 			0x1E	/*!< The PHY control one register. */
#define PHY_CONTROL2_REG 			0x1F	/*!< The PHY control two register. */
//
#define PHY_BCTL_DUPLEX_MASK 			0x0100U          /*!< The PHY duplex bit mask. */
#define PHY_BCTL_RESTART_AUTONEG_MASK 	0x0200U /*!< The PHY restart auto negotiation mask. */
#define PHY_BCTL_AUTONEG_MASK 			0x1000U         /*!< The PHY auto negotiation bit mask. */
#define PHY_BCTL_SPEED_MASK 			0x2000U           /*!< The PHY speed bit mask. */
#define PHY_BCTL_LOOP_MASK 				0x4000U            /*!< The PHY loop bit mask. */
#define PHY_BCTL_RESET_MASK 			0x8000U           /*!< The PHY reset bit mask. */
#define PHY_BCTL_SPEED_100M_MASK  		0x2000U     /*!< The PHY 100M speed mask. */
//
#define PHY_CTL2_REMOTELOOP_MASK 		0x0004U    /*!< The PHY remote loopback mask. */
#define PHY_CTL2_REFCLK_SELECT_MASK 	0x0080U /*!< The PHY RMII reference clock select. */
#define PHY_CTL1_10HALFDUPLEX_MASK 		0x0001U  /*!< The PHY 10M half duplex mask. */
#define PHY_CTL1_100HALFDUPLEX_MASK 	0x0002U /*!< The PHY 100M half duplex mask. */
#define PHY_CTL1_10FULLDUPLEX_MASK 		0x0005U  /*!< The PHY 10M full duplex mask. */
#define PHY_CTL1_100FULLDUPLEX_MASK 	0x0006U /*!< The PHY 100M full duplex mask. */
#define PHY_CTL1_SPEEDUPLX_MASK 		0x0007U     /*!< The PHY speed and duplex mask. */
#define PHY_CTL1_ENERGYDETECT_MASK 		0x0010U    /*!< The PHY signal present on rx differential pair. */
#define PHY_CTL1_LINKUP_MASK			0x0100U         /*!< The PHY link up. */
#define PHY_LINK_READY_MASK 			(PHY_CTL1_ENERGYDETECT_MASK | PHY_CTL1_LINKUP_MASK)
//
#define PHY_BSTATUS_LINKSTATUS_MASK 	0x0004U  /*!< The PHY link status mask. */
#define PHY_BSTATUS_AUTONEGABLE_MASK 	0x0008U /*!< The PHY auto-negotiation ability mask. */
#define PHY_BSTATUS_AUTONEGCOMP_MASK 	0x0020U /*!< The PHY auto-negotiation complete mask. */
//
#define PHY_100BaseT4_ABILITY_MASK 		0x0200U    /*!< The PHY have the T4 ability. */
#define PHY_100BASETX_FULLDUPLEX_MASK 	0x0100U /*!< The PHY has the 100M full duplex ability.*/
#define PHY_100BASETX_HALFDUPLEX_MASK 	0x0080U /*!< The PHY has the 100M full duplex ability.*/
#define PHY_10BASETX_FULLDUPLEX_MASK 	0x0040U  /*!< The PHY has the 10M full duplex ability.*/
#define PHY_10BASETX_HALFDUPLEX_MASK 	0x0020U  /*!< The PHY has the 10M full duplex ability.*/

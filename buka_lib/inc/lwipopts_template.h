//I) Debugging
//1) Assertion handling
#define LWIP_NOASSERT
//2) Statistics
#define LWIP_STATS
#define LWIP_STATS_DISPLAY
#define LINK_STATS
#define ETHARP_STATS
#define IP_STATS
#define IPFRAG_STATS
#define ICMP_STATS
#define IGMP_STATS
#define UDP_STATS
#define TCP_STATS
#define MEM_STATS
#define MEMP_STATS
#define SYS_STATS
#define IP6_STATS
#define ICMP6_STATS
#define IP6_FRAG_STATS
#define MLD6_STATS
#define ND6_STATS
#define MIB2_STATS
//3) Debug messages
#define LWIP_DBG_MIN_LEVEL   	LWIP_DBG_LEVEL_ALL
#define LWIP_DBG_TYPES_ON   	LWIP_DBG_ON
#define ETHARP_DEBUG   			LWIP_DBG_OFF
#define NETIF_DEBUG   			LWIP_DBG_OFF
#define PBUF_DEBUG   			LWIP_DBG_OFF
#define API_LIB_DEBUG   		LWIP_DBG_OFF
#define API_MSG_DEBUG   		LWIP_DBG_OFF
#define SOCKETS_DEBUG   		LWIP_DBG_OFF
#define ICMP_DEBUG   			LWIP_DBG_OFF
#define IGMP_DEBUG   			LWIP_DBG_OFF
#define INET_DEBUG   			LWIP_DBG_OFF
#define IP_DEBUG   				LWIP_DBG_OFF
#define IP_REASS_DEBUG   		LWIP_DBG_OFF
#define RAW_DEBUG   			LWIP_DBG_OFF
#define MEM_DEBUG   			LWIP_DBG_OFF
#define MEMP_DEBUG   			LWIP_DBG_OFF
#define SYS_DEBUG   			LWIP_DBG_OFF
#define TIMERS_DEBUG   			LWIP_DBG_OFF
#define TCP_DEBUG   			LWIP_DBG_OFF
#define TCP_INPUT_DEBUG   		LWIP_DBG_OFF
#define TCP_FR_DEBUG   			LWIP_DBG_OFF
#define TCP_RTO_DEBUG   		LWIP_DBG_OFF
#define TCP_CWND_DEBUG   		LWIP_DBG_OFF
#define TCP_WND_DEBUG   		LWIP_DBG_OFF
#define TCP_OUTPUT_DEBUG   		LWIP_DBG_OFF
#define TCP_RST_DEBUG   		LWIP_DBG_OFF
#define TCP_QLEN_DEBUG   		LWIP_DBG_OFF
#define UDP_DEBUG   			LWIP_DBG_OFF
#define TCPIP_DEBUG   			LWIP_DBG_OFF
#define SLIP_DEBUG   			LWIP_DBG_OFF
#define DHCP_DEBUG   			LWIP_DBG_OFF
#define AUTOIP_DEBUG   			LWIP_DBG_OFF
#define DNS_DEBUG   			LWIP_DBG_OFF
#define IP6_DEBUG   			LWIP_DBG_OFF
#define DHCP6_DEBUG   			LWIP_DBG_OFF
//4) Performance
#define LWIP_PERF
//II) Infrastructure
//1) NO_SYS
#define NO_SYS
//2) Timers
#define LWIP_TIMERS
#define LWIP_TIMERS_CUSTOM
//3) memcpy
#define MEMCPY(dst, src, len)
#define SMEMCPY(dst, src, len)
#define MEMMOVE(dst, src, len)
//4) Core locking and MPU
#define LWIP_MPU_COMPATIBLE
#define LWIP_TCPIP_CORE_LOCKING
#define LWIP_TCPIP_CORE_LOCKING_INPUT
#define SYS_LIGHTWEIGHT_PROT
#define LWIP_ASSERT_CORE_LOCKED()
#define LWIP_MARK_TCPIP_THREAD()
//5) Heap and memory pools
#define MEM_LIBC_MALLOC
#define MEMP_MEM_MALLOC
#define MEMP_MEM_INIT
#define MEM_ALIGNMENT
#define MEM_SIZE
#define MEMP_OVERFLOW_CHECK
#define MEMP_SANITY_CHECK
#define MEM_OVERFLOW_CHECK
#define MEM_SANITY_CHECK
#define MEM_USE_POOLS
#define MEM_USE_POOLS_TRY_BIGGER_POOL
#define MEMP_USE_CUSTOM_POOLS
#define LWIP_ALLOW_MEM_FREE_FROM_OTHER_CONTEXT
//6) Internal memory pools
#define MEMP_NUM_PBUF
#define MEMP_NUM_RAW_PCB
#define MEMP_NUM_UDP_PCB
#define MEMP_NUM_TCP_PCB
#define MEMP_NUM_TCP_PCB_LISTEN
#define MEMP_NUM_TCP_SEG
#define MEMP_NUM_ALTCP_PCB
#define MEMP_NUM_REASSDATA
#define MEMP_NUM_FRAG_PBUF
#define MEMP_NUM_ARP_QUEUE
#define MEMP_NUM_IGMP_GROUP
#define LWIP_NUM_SYS_TIMEOUT_INTERNAL
#define MEMP_NUM_SYS_TIMEOUT
#define MEMP_NUM_NETBUF
#define MEMP_NUM_NETCONN
#define MEMP_NUM_SELECT_CB
#define MEMP_NUM_TCPIP_MSG_API
#define MEMP_NUM_TCPIP_MSG_INPKT
#define MEMP_NUM_NETDB
#define MEMP_NUM_LOCALHOSTLIST
#define PBUF_POOL_SIZE
#define MEMP_NUM_API_MSG
#define MEMP_NUM_DNS_API_MSG
#define MEMP_NUM_SOCKET_SETGETSOCKOPT_DATA
#define MEMP_NUM_NETIFAPI_MSG
//7) SNMP MIB2 callbacks
#define LWIP_MIB2_CALLBACKS
//8) Multicast
#define LWIP_MULTICAST_TX_OPTIONS 
//9) Threading
#define TCPIP_THREAD_NAME
#define TCPIP_THREAD_STACKSIZE
#define TCPIP_THREAD_PRIO
#define TCPIP_MBOX_SIZE
#define LWIP_TCPIP_THREAD_ALIVE()
#define SLIPIF_THREAD_NAME
#define SLIPIF_THREAD_STACKSIZE
#define SLIPIF_THREAD_PRIO
#define DEFAULT_THREAD_NAME
#define DEFAULT_THREAD_STACKSIZE
#define DEFAULT_THREAD_PRIO
#define DEFAULT_RAW_RECVMBOX_SIZE
#define DEFAULT_UDP_RECVMBOX_SIZE
#define DEFAULT_TCP_RECVMBOX_SIZE
#define DEFAULT_ACCEPTMBOX_SIZE
//10) Checksum
#define LWIP_CHECKSUM_CTRL_PER_NETIF
#define CHECKSUM_GEN_IP
#define CHECKSUM_GEN_UDP
#define CHECKSUM_GEN_TCP
#define CHECKSUM_GEN_ICMP
#define CHECKSUM_GEN_ICMP6
#define CHECKSUM_CHECK_IP
#define CHECKSUM_CHECK_UDP
#define CHECKSUM_CHECK_TCP
#define CHECKSUM_CHECK_ICMP
#define CHECKSUM_CHECK_ICMP6
#define LWIP_CHECKSUM_ON_COPY
//11) Hooks
#define LWIP_HOOK_FILENAME
#define LWIP_HOOK_TCP_ISN(local_ip, local_port, remote_ip, remote_port)
#define LWIP_HOOK_TCP_INPACKET_PCB(pcb, hdr, optlen, opt1len, opt2, p)
#define LWIP_HOOK_TCP_OUT_TCPOPT_LENGTH(pcb, internal_len)
#define LWIP_HOOK_TCP_OUT_ADD_TCPOPTS(p, hdr, pcb, opts)
#define LWIP_HOOK_IP4_INPUT(pbuf, input_netif)
#define LWIP_HOOK_IP4_ROUTE()
#define LWIP_HOOK_IP4_ROUTE_SRC(src, dest)
#define LWIP_HOOK_IP4_CANFORWARD(src, dest)
#define LWIP_HOOK_ETHARP_GET_GW(netif, dest)
#define LWIP_HOOK_IP6_INPUT(pbuf, input_netif)
#define LWIP_HOOK_IP6_ROUTE(src, dest)
#define LWIP_HOOK_ND6_GET_GW(netif, dest)
#define LWIP_HOOK_VLAN_CHECK(netif, eth_hdr, vlan_hdr)
#define LWIP_HOOK_VLAN_SET(netif, p, src, dst, eth_type)
#define LWIP_HOOK_MEMP_AVAILABLE(memp_t_type)
#define LWIP_HOOK_UNKNOWN_ETH_PROTOCOL(pbuf, netif)
#define LWIP_HOOK_DHCP_APPEND_OPTIONS(netif, dhcp, state, msg, msg_type, options_len_ptr)
#define LWIP_HOOK_DHCP_PARSE_OPTION(netif, dhcp, state, msg, msg_type, option, len, pbuf, offset)
#define LWIP_HOOK_DHCP6_APPEND_OPTIONS(netif, dhcp6, state, msg, msg_type, options_len_ptr, max_len)
#define LWIP_HOOK_SOCKETS_SETSOCKOPT(s, sock, level, optname, optval, optlen, err)
#define LWIP_HOOK_SOCKETS_GETSOCKOPT(s, sock, level, optname, optval, optlen, err)
#define LWIP_HOOK_NETCONN_EXTERNAL_RESOLVE(name, addr, addrtype, err)
//III) Callback-style APIs
//1) RAW
#define LWIP_RAW
#define RAW_TTL   		IP_DEFAULT_TTL
//2) DNS
#define LWIP_DNS
#define DNS_TABLE_SIZE
#define DNS_MAX_NAME_LENGTH
#define DNS_MAX_SERVERS
#define DNS_MAX_RETRIES
#define DNS_DOES_NAME_CHECK
#define LWIP_DNS_SECURE
#define DNS_LOCAL_HOSTLIST
#define DNS_LOCAL_HOSTLIST_IS_DYNAMIC
#define LWIP_DNS_SUPPORT_MDNS_QUERIES
//3) UDP
#define LWIP_UDP
#define LWIP_UDPLITE
#define UDP_TTL   		IP_DEFAULT_TTL
#define LWIP_NETBUF_RECVINFO
//4) TCP
#define LWIP_TCP
#define TCP_TTL   		IP_DEFAULT_TTL
#define TCP_WND
#define TCP_MAXRTX
#define TCP_SYNMAXRTX
#define TCP_QUEUE_OOSEQ
#define LWIP_TCP_SACK_OUT
#define LWIP_TCP_MAX_SACK_NUM
#define TCP_MSS
#define TCP_CALCULATE_EFF_SEND_MSS
#define TCP_SND_BUF
#define TCP_SND_QUEUELEN
#define TCP_SNDLOWAT
#define TCP_SNDQUEUELOWAT
#define TCP_OOSEQ_MAX_BYTES
#define TCP_OOSEQ_MAX_PBUFS
#define TCP_LISTEN_BACKLOG
#define TCP_DEFAULT_LISTEN_BACKLOG
#define TCP_OVERSIZE
#define LWIP_TCP_TIMESTAMPS
#define TCP_WND_UPDATE_THRESHOLD
#define LWIP_EVENT_API
#define LWIP_WND_SCALE
#define LWIP_TCP_PCB_NUM_EXT_ARGS
#define LWIP_ALTCP
#define LWIP_ALTCP_TLS
//IV) Thread-safe APIs
//1) Netconn
#define LWIP_NETCONN
#define LWIP_TCPIP_TIMEOUT
#define LWIP_NETCONN_SEM_PER_THREAD
#define LWIP_NETCONN_FULLDUPLEX
//2) Sockets
#define LWIP_SOCKET
#define LWIP_COMPAT_SOCKETS
#define LWIP_POSIX_SOCKETS_IO_NAMES
#define LWIP_SOCKET_OFFSET
#define LWIP_TCP_KEEPALIVE
#define LWIP_SO_SNDTIMEO
#define LWIP_SO_RCVTIMEO
#define LWIP_SO_SNDRCVTIMEO_NONSTANDARD
#define LWIP_SO_RCVBUF
#define LWIP_SO_LINGER
#define RECV_BUFSIZE_DEFAULT
#define LWIP_TCP_CLOSE_TIMEOUT_MS_DEFAULT
#define SO_REUSE
#define SO_REUSE_RXTOALL
#define LWIP_FIONREAD_LINUXMODE
#define LWIP_SOCKET_SELECT
#define LWIP_SOCKET_POLL
//V) IPv4
//1) ARP
#define LWIP_ARP
#define ARP_TABLE_SIZE
#define ARP_MAXAGE
#define ARP_QUEUEING
#define ARP_QUEUE_LEN
#define ETHARP_SUPPORT_VLAN
#define LWIP_ETHERNET
#define ETH_PAD_SIZE
#define ETHARP_SUPPORT_STATIC_ENTRIES
#define ETHARP_TABLE_MATCH_NETIF
//2) ICMP
#define LWIP_ICMP
#define ICMP_TTL   IP_DEFAULT_TTL
#define LWIP_BROADCAST_PING
#define LWIP_MULTICAST_PING
//3) DHCP
#define LWIP_DHCP
#define DHCP_DOES_ARP_CHECK
#define LWIP_DHCP_BOOTP_FILE
#define LWIP_DHCP_GET_NTP_SRV
#define LWIP_DHCP_MAX_NTP_SERVERS
#define LWIP_DHCP_MAX_DNS_SERVERS
//4) AUTOIP
#define LWIP_AUTOIP
#define LWIP_DHCP_AUTOIP_COOP
#define LWIP_DHCP_AUTOIP_COOP_TRIES
//5) IGMP
#define LWIP_IGMP
//VI) PBUF
#define PBUF_LINK_HLEN
#define PBUF_LINK_ENCAPSULATION_HLEN
#define PBUF_POOL_BUFSIZE
#define LWIP_PBUF_REF_T
//VII) NETIF
#define LWIP_SINGLE_NETIF
#define LWIP_NETIF_HOSTNAME
#define LWIP_NETIF_API
#define LWIP_NETIF_STATUS_CALLBACK
#define LWIP_NETIF_EXT_STATUS_CALLBACK
#define LWIP_NETIF_LINK_CALLBACK
#define LWIP_NETIF_REMOVE_CALLBACK
#define LWIP_NETIF_HWADDRHINT
#define LWIP_NETIF_TX_SINGLE_PBUF
#define LWIP_NUM_NETIF_CLIENT_DATA
//VIII) IPv6
//1) ICMP6
#define LWIP_ICMP6
#define LWIP_ICMP6_DATASIZE
#define LWIP_ICMP6_HL
//2) Multicast listener discovery
#define LWIP_IPV6_MLD
#define MEMP_NUM_MLD6_GROUP
//3) Neighbor discovery
#define LWIP_ND6_QUEUEING
#define MEMP_NUM_ND6_QUEUE
#define LWIP_ND6_NUM_NEIGHBORS
#define LWIP_ND6_NUM_DESTINATIONS
#define LWIP_ND6_NUM_PREFIXES
#define LWIP_ND6_NUM_ROUTERS
#define LWIP_ND6_MAX_MULTICAST_SOLICIT
#define LWIP_ND6_MAX_UNICAST_SOLICIT
#define LWIP_ND6_MAX_ANYCAST_DELAY_TIME
#define LWIP_ND6_MAX_NEIGHBOR_ADVERTISEMENT
#define LWIP_ND6_REACHABLE_TIME
#define LWIP_ND6_RETRANS_TIMER
#define LWIP_ND6_DELAY_FIRST_PROBE_TIME
#define LWIP_ND6_ALLOW_RA_UPDATES
#define LWIP_ND6_TCP_REACHABILITY_HINTS
#define LWIP_ND6_RDNSS_MAX_DNS_SERVERS
//4) DHCPv6
#define LWIP_IPV6_DHCP6
#define LWIP_IPV6_DHCP6_STATEFUL
#define LWIP_IPV6_DHCP6_STATELESS
#define LWIP_DHCP6_GET_NTP_SRV
#define LWIP_DHCP6_MAX_NTP_SERVERS
#define LWIP_DHCP6_MAX_DNS_SERVERS
//end
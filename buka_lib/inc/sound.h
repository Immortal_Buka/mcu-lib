/*
description: 	pwm sound
version:		190112
author:			Buka
history:
190112
- initial version
*/
#ifndef _SOUND_H_
#define _SOUND_H_
//
#include <stdint.h>
//
/*
d - sharp/dièse/diesis
*/
//5th octave
#define E5		141
#define Dd5		150
#define D5		159
#define Cd5		168
#define C5		178
//4th octave
#define B4		189
#define Ad4		200
#define A4		217
#define Gd4		224
#define G4		238
#define Fd4		252
#define F4		267
#define E4		283
#define Dd4		300
#define D4		318
#define Cd4		337
#define C4		357
//3rd octave
#define B3		379
#define Ad3		401
#define A3		435
#define Gd3		450
#define G3		477
#define Fd3		506
#define F3		536
#define E3		568
#define Dd3		602
#define D3		638
#define Cd3		675
#define C3		716
//2nd octave
#define B2		758
#define Ad2		803
#define A2		851
#define Gd2		902
#define G2		956
#define Fd2		1013
#define F2		1073
#define E2		1137
#define Dd2		1204
#define D2		1276
#define Cd2		1352
#define C2		1432
//1st octave
#define B1		1518
#define Ad1		1608
#define A1		1704
#define Gd1		1805
#define G1		1912
#define Fd1		2026
#define F1		2147
#define E1		2274
#define Dd1		2410
#define D1		2553
#define Cd1		2705
#define C1		2866
//small octave
#define BS		3036
#define AdS		3217
#define AS		3408
#define GdS		3622
#define GS		3825
#define FdS		4053
#define FS		4294
#define ES		4550
#define DdS		4820
#define DS		5073
#define CdS		5411
#define CS		5732
//great octave
#define BG		6073
#define AdG		6435
#define AG		6817
#define GdG		7224
#define GG		7652
#define FdG		8107
#define FG		8589
#define EG		9100
#define DdG		9642
#define DG		10146
#define CdG		10822
#define CG		11465
//contra octave
#define BC		12147
#define AdC		12872
#define AC		13635
#define GdC		14450
#define GC		15305
#define FdC		16215
#define FC		17181
#define EC		18198
#define DdC		19289
#define DC		20297
#define CdC		21644
#define CC		22935
//sub contra octave
#define BSc		24294
#define AdSc	25746
#define ASc		27272
#define GdSc	28901
#define GSc		30611
#define FdSc	32438
#define FSc		34371
#define ESc		36389
//
typedef struct
{
    uint8_t repeat;
    uint8_t length;
    const uint16_t* pointer;//750k PWM
} music_descriptor_t;
//
uint8_t music_play(uint8_t idx);
void music_irq(void);
void music_init(const music_descriptor_t* table, uint8_t size);
//
#endif//_EPYH_H_

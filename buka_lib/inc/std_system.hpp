#ifndef _STD_SYSTEM_H_
#define _STD_SYSTEM_H_
#include <stdint.h>
#include "cmsis_compiler.h"
#include <stdbool.h>
//
typedef struct __attribute__((packed, aligned(4)))
{
	uint8_t number;
	uint8_t day;
	uint8_t month;
	uint8_t year;
} version_t;
typedef void(*func_ptr)(void);
//
//
void delay(uint32_t data);
void fpu_on(void);
uint32_t bb_periph_read(uint32_t addr, uint32_t bit);
void bb_periph_write(uint32_t addr, uint32_t bit, uint32_t data);
uint32_t bb_mem_read(uint32_t addr, uint32_t bit);
void bb_mem_write(uint32_t addr, uint32_t bit, uint32_t data);
void memset_word(uint32_t* ptr, uint32_t value, uint32_t size);
void arm8_stack_limit(void);
void delay_ms(uint32_t ms);
//
#endif//_STD_SYSTEM_H_

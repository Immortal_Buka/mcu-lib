/*
description: 	standart Cortex-M functions & startup
version:		190613
author:			Buka
history:
181015
- initial version
190613
- add memset_word
*/
#ifndef _STD_SYSTEM_H_
#define _STD_SYSTEM_H_
#include <stdint.h>
#include "cmsis_compiler.h"
#include "time.h"
#include <stdbool.h>
//
enum
{
	test = 't',
	release = 'r',
};
typedef struct __attribute__((packed, aligned(4)))
{
	uint8_t number;
	uint8_t day;
	uint8_t month;
	uint8_t year;
} version_t;
typedef struct __attribute__((packed, aligned(4)))
{
	uint32_t crc;
	uint32_t size;
	version_t version;
} fw_header_t;
/*typedef struct __attribute__((packed, aligned(4)))
{
	uint32_t crc;
	uint32_t size;
	version_v2_t version;
} fw_header_v2_t;*/
typedef void(*func_ptr)(void);
//
void Reset_Handler(void);
void Default_Handler(void);
void libc_init_array (void);
void delay(uint32_t data);
void fpu_on(void);
uint32_t BB_periph_read(uint32_t addr, uint32_t bit);
void BB_periph_write(uint32_t addr, uint32_t bit, uint32_t data);
uint32_t BB_mem_read(uint32_t addr, uint32_t bit);
void BB_mem_write(uint32_t addr, uint32_t bit, uint32_t data);
void memset_word(uint32_t* ptr, uint32_t value, uint32_t size);
//
#endif//_STD_SYSTEM_H_

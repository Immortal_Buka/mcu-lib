#ifndef __ARCH_SYS_ARCH_H__
#define __ARCH_SYS_ARCH_H__
//
#include "lwip/opt.h"
#include "arch/cc.h"
//
typedef unsigned long sys_prot_t;
//
void sys_assert(char *msg);
//
#endif /* __ARCH_SYS_ARCH_H__ */

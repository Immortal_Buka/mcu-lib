/*
description: 	systick delay
version:		180220
author:			Buka
history:
180220
- initial version
*/
#ifndef _SYSTICK_H_
#define _SYSTICK_H_
//
#include <stdint.h>
#include "system.h"
//
void SysTick_Handler(void);
void delay_systick_ms(uint32_t parametr);
void delay_systick_ms_init(uint32_t ticks_for_1ms, uint32_t priority);
//
#endif//_TIME_H_

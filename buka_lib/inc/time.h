/*
description: 	standart time
version:		180316
author:			Buka
history:
180316
- initial version
*/
#ifndef _TIME_H_
#define _TIME_H_
//
#include <stdint.h>
//
typedef struct __attribute__((packed, aligned(4)))
{
    int16_t year;			//-32k-32k
    uint8_t month;  		//1-12
    uint8_t day;    		//1-31
} my_date_t;
typedef struct __attribute__((packed, aligned(4)))
{
    uint8_t hour;   		//0-23
    uint8_t minute; 		//0-59
    uint8_t second; 		//0-59
    uint8_t second_cents; 	//0-99
} my_time_t;
typedef struct __attribute__((packed, aligned(4)))
{
	my_date_t date;
	my_time_t time;
} datetime_t;
//
#endif//_TIME_H_

/*
description: 	print logs throw UART
version:		180829
author:			Buka
history:
180829
- initial version
*/
#ifndef _UART_LOG_H_
#define _UART_LOG_H_
#ifdef __cplusplus
extern "C" {
#endif
#include "print.h"
//
typedef enum
{
	t_function,	//green/red
	t_irq,		//yellow
	t_info,		//blue
	t_fault,	//red
	t_data,		//cyan
	t_evt,		//magenta
} log_write_type;
//
void uart_log_init(void* uart);
void uart_log_add(log_write_type type, uint8_t* string_1, ...);
void print_ble_data(uint8_t* string, uint16_t size, uint8_t* data);
void print_address(uint8_t* addr);
//
#ifdef __cplusplus
}
#endif
#endif//_UART_LOG_H_

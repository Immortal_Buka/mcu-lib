/*
description: 	universal bootloader
version:		180205
author:			Buka
history:
180205
- initial version
*/
#include "system.h"
//
extern void boot_init(void);
extern void kill_jtag(void);
extern void crc32_init(void);
extern void crc_32_calc(uint32_t* crc, uint32_t* data);
extern void bootloader_error_signal(uint8_t reason);
extern void fw_copy(uint32_t* src, uint32_t* dst, uint32_t size);
extern void bootload_run_req(void);
extern void crc_32_reset(void);
//
void main(void);
void decrypt_fw_new(void);
void load_main(void);
uint8_t chk_fw_data(fw_header_t* old, fw_header_t* new);;
void bootloader_error(uint8_t reason);
/*
01 - no fw_main, no fw_copy, no fw_new
11 - fail to copy from fw_main to fw_copy
21 - fail to copy from fw_copy to fw_main
41 - broken fw_new
42 - fail to copy from fw_new to fw_main
43 - fail to copy to fw_copy
51 - fail to copy to fw_copy
52 - fail to copy to fw_main
53 - fail to copy from fw_main to fw_copy
61 - fail to copy to fw_main
62 - fail to copy from fw_copy to fw_main
81 - unknown state
91 - fail to load fw_main
*/
uint8_t chk_crc(uint32_t* fw_addr);
//
const fw_header_t* fwh_main = (fw_header_t*)__fw_main;
const fw_header_t* fwh_copy = (fw_header_t*)__fw_copy;
const fw_header_t* fwh_new = (fw_header_t*)__fw_new;;
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0x01,
	.day = 0x05,
	.month = 0x02,
	.year = 0x18,
};
uint8_t states = 0;
volatile uint8_t fw_temp[PAGE_SIZE];
//
void main (void)
{
	boot_init();
	kill_jtag();
	crc32_init();
	bootload_run_req();
	if(fwh_main->version.number != 0xff)
	{
		if(chk_crc((uint32_t*)__fw_main) == 0) states = 1;
	}
	if(fwh_copy->version.number != 0xff)
	{
		if(chk_crc((uint32_t*)__fw_copy) == 0) states |= 1<<1;
	}
	if(*((uint32_t*)__fw_new) != 0xffffffff)
	{
		states |= 1<<2;
		decrypt_fw_new();
	}
	switch(states)
	{
		case 0:
			bootloader_error(01);
			break;
		case 1:
			fw_copy((uint32_t*)__fw_main, (uint32_t*)__fw_copy, fwh_main->size);
			if(chk_crc((uint32_t*)__fw_copy)) bootloader_error(11);
			break;
		case 2:
			fw_copy((uint32_t*)__fw_copy, (uint32_t*)__fw_main, fwh_copy->size);
			if(chk_crc((uint32_t*)__fw_main)) bootloader_error(21);
			break;
		case 3: break;
		case 4:
			if((fwh_new->version.number == 0xff)||(chk_crc((uint32_t*)__fw_new))) bootloader_error(41);
			else
			{
				fw_copy((uint32_t*)__fw_new, (uint32_t*)__fw_main, fwh_new->size);
				if(chk_crc((uint32_t*)__fw_main)) bootloader_error(42);
				else
				{
					fw_copy((uint32_t*)__fw_new, (uint32_t*)__fw_copy, fwh_new->size);
					if(chk_crc((uint32_t*)__fw_copy))
					{
						fw_copy((uint32_t*)__fw_main, (uint32_t*)__fw_copy, fwh_main->size);
						if(chk_crc((uint32_t*)__fw_copy)) bootloader_error(43);
					}
				}
			}
			break;
		case 5:
		case 7:
			if((fwh_main->version.number == fwh_new->version.number)&&(chk_fw_data((fw_header_t*)fwh_main, (fw_header_t*)fwh_new))&&(chk_crc((uint32_t*)__fw_new) == 0))
			{
				fw_copy((uint32_t*)__fw_new, (uint32_t*)__fw_copy, fwh_new->size);
				if(chk_crc((uint32_t*)__fw_copy))
				{
					fw_copy((uint32_t*)__fw_main, (uint32_t*)__fw_copy, fwh_main->size);
					if(chk_crc((uint32_t*)__fw_copy)) bootloader_error(51);
				}
				fw_copy((uint32_t*)__fw_new, (uint32_t*)__fw_main, fwh_new->size);
				if(chk_crc((uint32_t*)__fw_main))
				{
					fw_copy((uint32_t*)__fw_copy, (uint32_t*)__fw_main, fwh_copy->size);
					if(chk_crc((uint32_t*)__fw_main)) bootloader_error(52);
				}
			}
			else
			{
				fw_copy((uint32_t*)__fw_main, (uint32_t*)__fw_copy, fwh_main->size);
				if(chk_crc((uint32_t*)__fw_copy)) bootloader_error(53);
			}
			break;
		case 6:
			if((fwh_copy->version.number == fwh_new->version.number)&&(chk_fw_data((fw_header_t*)fwh_copy, (fw_header_t*)fwh_new))&&(chk_crc((uint32_t*)__fw_new)))
			{
				fw_copy((uint32_t*)__fw_new, (uint32_t*)__fw_main, fwh_new->size);
				if(chk_crc((uint32_t*)__fw_main))
				{
					fw_copy((uint32_t*)__fw_copy, (uint32_t*)__fw_main, fwh_copy->size);
					if(chk_crc((uint32_t*)__fw_main)) bootloader_error(61);
				}
			}
			else
			{
				fw_copy((uint32_t*)__fw_copy, (uint32_t*)__fw_main, fwh_copy->size);
				if(chk_crc((uint32_t*)__fw_main)) bootloader_error(62);
			}
			break;
		default:
			bootloader_error(81);
			break;
	}
	load_main();
	bootloader_error(91);
}
void decrypt_fw_new(void)
{
	//TODO
}
void load_main(void)
{
	typedef void(*pFunc)(void);
	pFunc func = (pFunc)(*((uint32_t*)(__fw_main+132)));
	SCB->VTOR = __fw_main + 128;
	func();
}
uint8_t chk_fw_data(fw_header_t* old, fw_header_t* new)
{
	if(old->version.year < new->version.year) return 1;
	else
	{
		if(old->version.month < new->version.month) return 1;
		else
		{
			if(old->version.day < new->version.day) return 1;
			else return 0;
		}
	}
}
void bootloader_error(uint8_t reason)
{
	bootloader_error_signal(reason);
	while(1){};
}
uint8_t chk_crc(uint32_t* fw_addr)
{
	uint32_t crc_calc = 0;
	uint32_t size = fw_addr[1] - 8;
	uint32_t crc_ex = fw_addr[0];
	uint32_t l_temp = size >> 2;
	crc_32_reset();
	for(uint32_t i=0; i<l_temp; i++) crc_32_calc(&crc_calc, &fw_addr[i+2]);
	l_temp = (l_temp << 2);
	if(l_temp != size)
	{
		switch (size % l_temp)
		{
			case 1: l_temp = fw_addr[l_temp+2] & 0xff; break;
			case 2: l_temp = fw_addr[l_temp+2] & 0xffff;break;
			case 3: l_temp = fw_addr[l_temp+2] & 0xffffff; break;
		}
		crc_32_calc(&crc_calc, &l_temp);
	}
	if(crc_ex != crc_calc) return 1;
	else return 0;
}

#include "circular_buffer.hpp"
//
extern void cb_error_signal(circular_buffer_t* cb, cb_error_t error);
//
circular_buffer_t::circular_buffer_t(uint8_t* memory_ptr, uint32_t memory_size)
{
	buffer = memory_ptr;
	size = memory_size;
	for(uint32_t i = 0; i < size; i++) buffer[i] = 0;
	reset();
}
void circular_buffer_t::write(uint8_t* data_in_ptr, uint32_t data_in_size)
{
	for(uint32_t i = 0; i < data_in_size; i++)
	{
		buffer[write_index++] = data_in_ptr[i];
		if(write_index >= size) write_index = 0;
		if(write_index == read_index) cb_error_signal(this, uroboros);
	}
}
uint32_t circular_buffer_t::calc_unread(void)
{
	uint32_t loc_read, loc_write, loc_temp;
	loc_read = read_index;
	loc_write = write_index;
	if(loc_write > loc_read) loc_temp = loc_write - loc_read;
	else
	{
		if(loc_write == loc_read) loc_temp = 0;
		else
		{
			loc_temp = size - loc_read;
			loc_temp += loc_write;
		}
	}
	return loc_temp;
}
uint32_t circular_buffer_t::read(uint8_t* data_out_ptr, uint32_t data_out_size)
{
	uint32_t l_temp_1 = calc_unread();
	if(data_out_size < l_temp_1) l_temp_1 = data_out_size;
	for(uint32_t i = 0; i < l_temp_1; i++)
	{
		data_out_ptr[i] = buffer[read_index++];
		if(read_index >= size) read_index = 0;
	}
	return l_temp_1;
}
void circular_buffer_t::reset(void)
{
	write_index = 0;
	read_index = 0;
}
uint32_t circular_buffer_t::read_to_void(uint32_t data_out_size)
{
	uint32_t temp_1;
	uint32_t temp_2 = calc_unread();
	if(data_out_size > temp_2) temp_1 = temp_2;
	else temp_1 = data_out_size;
	read_index += temp_1;
	if(read_index >= size) read_index -= size;
	return temp_1;
}
uint32_t circular_buffer_t::calc_unread_ui(void)
{
	uint32_t loc_read, loc_write, loc_temp;
	loc_read = user_index;
	loc_write = write_index;
	if(loc_write > loc_read) loc_temp = loc_write - loc_read;
	else
	{
		if(loc_write == loc_read) loc_temp = 0;
		else
		{
			loc_temp = size - loc_read;
			loc_temp += loc_write;
		}
	}
	return loc_temp;
}
uint32_t circular_buffer_t::read_ui(uint8_t* data_out_ptr, uint32_t data_out_size)
{
	uint32_t l_temp_1 = calc_unread_ui();
	if(data_out_size < l_temp_1) l_temp_1 = data_out_size;
	for(uint32_t i = 0; i < l_temp_1; i++)
	{
		data_out_ptr[i] = buffer[user_index++];
		if(user_index >= size) user_index = 0;
	}
	return l_temp_1;
}
uint32_t circular_buffer_t::cb_test_read(uint8_t* data_out_ptr, uint32_t data_out_size)
{
	uint32_t loc_temp = read_index;
	uint32_t l_temp_1 = calc_unread();
	if(data_out_size < l_temp_1) l_temp_1 = data_out_size;
	for(uint32_t i = 0; i < l_temp_1; i++)
	{
		data_out_ptr[i] = buffer[loc_temp++];
		if(read_index >= size) loc_temp = 0;
	}
	return l_temp_1;
}

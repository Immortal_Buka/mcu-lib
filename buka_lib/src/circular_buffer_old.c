/*
description: 	circular buffer
version:		190820
author:			Buka
history:
181127
- initial version
190603
- circle_buffer -> circle_buffer_t
190820
- add cb_error_signal
191011
- add cb_ssum32
*/
#include "circular_buffer.h"
//
extern void cb_error_signal(circle_buffer_t* buffer, cb_error_t error);
//
void cb_write(circle_buffer_t* buffer, uint8_t* data_in_ptr, uint32_t data_in_size)
{
	for(uint32_t i=0; i<data_in_size; i++)
	{
		buffer->buffer[buffer->write_index++] = data_in_ptr[i];
		if(buffer->write_index >= buffer->size) buffer->write_index = 0;
		if(buffer->write_index == buffer->read_index)
		{
			cb_error_signal(buffer, uroboros);
		}
	}
}
uint32_t cb_read(circle_buffer_t* buffer, uint8_t* data_out_ptr, uint32_t data_out_size)
{
	uint32_t l_temp_1 = cb_calc_unread(buffer);
	if(data_out_size < l_temp_1) l_temp_1 = data_out_size;
	for(uint32_t i=0; i<l_temp_1; i++)
	{
		data_out_ptr[i] = buffer->buffer[buffer->read_index++];
		if(buffer->read_index >= buffer->size) buffer->read_index = 0;
	}
	return l_temp_1;
}
uint32_t cb_read_to_void(circle_buffer_t* buffer, uint32_t data_out_size)
{
	uint32_t temp_1;
	uint32_t temp_2 = cb_calc_unread(buffer);
	if(data_out_size > temp_2) temp_1 = temp_2;
	else temp_1 = data_out_size;
	buffer->read_index += temp_1;
	if(buffer->read_index >= buffer->size) buffer->read_index -= buffer->size;
	return temp_1;
}
void cb_reset(circle_buffer_t* buffer)
{
	buffer->write_index = 0;
	buffer->read_index = 0;
}
void cb_init(circle_buffer_t* buffer, uint8_t* memory_ptr, uint32_t memory_size)
{
	buffer->buffer = memory_ptr;
	buffer->size = memory_size;
	cb_reset(buffer);
}
uint32_t cb_calc_unread(circle_buffer_t* buffer)
{
	uint32_t loc_read, loc_write, loc_temp;
	loc_read = buffer->read_index;
	loc_write = buffer->write_index;
	if(loc_write > loc_read) loc_temp = loc_write - loc_read;
	else
	{
		if(loc_write == loc_read) loc_temp = 0;
		else
		{
			loc_temp = buffer->size - loc_read;
			loc_temp += loc_write;
		}
	}
	return loc_temp;
}
uint32_t cb_read_ui(circle_buffer_t* buffer, uint8_t* data_out_ptr, uint32_t data_out_size)
{
	uint32_t l_temp_1 = cb_calc_unread_ui(buffer);
	if(data_out_size < l_temp_1) l_temp_1 = data_out_size;
	for(uint32_t i=0; i<l_temp_1; i++)
	{
		data_out_ptr[i] = buffer->buffer[buffer->user_index++];
		if(buffer->user_index >= buffer->size) buffer->user_index = 0;
	}
	return l_temp_1;
}
uint32_t cb_calc_unread_ui(circle_buffer_t* buffer)
{
	uint32_t loc_read, loc_write, loc_temp;
	loc_read = buffer->user_index;
	loc_write = buffer->write_index;
	if(loc_write > loc_read) loc_temp = loc_write - loc_read;
	else
	{
		if(loc_write == loc_read) loc_temp = 0;
		else
		{
			loc_temp = buffer->size - loc_read;
			loc_temp += loc_write;
		}
	}
	return loc_temp;
}
uint32_t cb_test_read(circle_buffer_t* buffer, uint8_t* data_out_ptr, uint32_t data_out_size)
{
	uint32_t loc_temp = buffer->read_index;
	uint32_t l_temp_1 = cb_calc_unread(buffer);
	if(data_out_size < l_temp_1) l_temp_1 = data_out_size;
	for(uint32_t i=0; i<l_temp_1; i++)
	{
		data_out_ptr[i] = buffer->buffer[loc_temp++];
		if(buffer->read_index >= buffer->size) loc_temp = 0;
	}
	return l_temp_1;
}
int32_t cb_ssum32(circle_buffer_t* buffer)
{
	int32_t* temp_ptr = (int32_t*)buffer->buffer;
	int32_t temp_loc = 0;
	for(uint32_t i=0; i<(buffer->size >> 2); i++) temp_loc += temp_ptr[i];
	return temp_loc;
}

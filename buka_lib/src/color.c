/*
description: 	color definitions & conversions
version:		190227
author:			Buka
history:
190227
- initial version
*/
#include "color.h"
//
void HSV_to_RGB_int(hsv_t* hsv, rgb_888_t* rgb)
{
	int h = hsv->color.h;
	int s = hsv->color.s;
	int v = hsv->color.v;
    if(hsv->color.h > 359) hsv->color.h = 0;
	if(hsv->color.s == 0)
	{
		rgb->color.r = hsv->color.v;
		rgb->color.g = hsv->color.v;
		rgb->color.b = hsv->color.v;
	}
	else
    {
        int i = h/60;
        int p = (256*v - s*v)/256;
        if(i & 1) 
        {
            int q = (256*60*v - h*s*v + 60*s*v*i) / (256*60);
            switch(i) 
            {
                case 1:
                    rgb->color.r = q; 
                    rgb->color.g = v; 
                    rgb->color.b = p; 
                break;
                case 3:
                    rgb->color.r = p; 
                    rgb->color.g = q; 
                    rgb->color.b = v; 
                break;
                case 5: 
                    rgb->color.r = v; 
                    rgb->color.g = p; 
                    rgb->color.b = q; 
                break;
            }
        } 
        else 
        {
            int t = (256*60*v + h*s*v - 60*s*v*(i+1)) / (256*60);
            switch(i) 
            {
                case 0:   
                    rgb->color.r = v; 
                    rgb->color.g = t; 
                    rgb->color.b = p; 
                break;
                case 2:
                    rgb->color.r = p; 
                    rgb->color.g = v; 
                    rgb->color.b = t; 
                break;
                case 4:   
                    rgb->color.r = t; 
                    rgb->color.g = p; 
                    rgb->color.b = v; 
                break;
            }
        }
    }
}
void RGB_888_to_565(rgb_888_t* in, rgb_565_t* out)
{
	out->color.b = in->color.b >> 3;
	out->color.g = in->color.g >> 2;
	out->color.r = in->color.r >> 3;
}
void RGB_max_min(rgb_888_t* rgb, rgb_mm_t* mm)
{
	if(rgb->color.r > rgb->color.g)
	{
		if(rgb->color.r > rgb->color.b)
		{
			mm->max = rgb->color.r;
			if(rgb->color.g > rgb->color.b) mm->min = rgb->color.b;
			else mm->min = rgb->color.g;
		}
		else
		{
			mm->max = rgb->color.b;
			mm->min = rgb->color.g;
		}
	}
	else
	{
		if(rgb->color.g > rgb->color.b)
		{
			mm->max = rgb->color.g;
			if(rgb->color.r > rgb->color.b) mm->min = rgb->color.b;
			else mm->min = rgb->color.r;
		}
		else
		{
			mm->max = rgb->color.b;
			mm->min = rgb->color.r;
		}
	}
}
uint16_t RGB_to_H(rgb_888_t* rgb, rgb_mm_t* mm)
{
	float32_t loc_temp;
	if(mm->max == mm->min) return 0;
	else
	{
		loc_temp = (float32_t)(mm->max - mm->min);
		if(mm->max == rgb->color.r) loc_temp = 60.0*((float32_t)(rgb->color.g - rgb->color.b)/loc_temp);
		else
		{
			if(mm->max == rgb->color.g) loc_temp = 60.0*(2.0 + (float32_t)(rgb->color.b - rgb->color.r)/loc_temp);
			else loc_temp = 60.0*(4.0 + (float32_t)(rgb->color.r - rgb->color.g)/loc_temp);
		}
		if(loc_temp < 0.0) loc_temp = loc_temp + 360.0;
		return ((uint16_t)loc_temp);
	}
}
void RGB_to_HSV(rgb_888_t* rgb, hsv_t* hsv)
{
	float32_t loc_temp;
	rgb_mm_t mm;
	RGB_max_min(rgb, &mm);
	hsv->color.v = mm.max;
	if(mm.max == 0) hsv->color.s = 0;
	else
	{
		loc_temp = 255.0*(float32_t)(mm.max-mm.min)/(float32_t)mm.max;
		hsv->color.s = (uint8_t)loc_temp;
	}
	hsv->color.h = RGB_to_H(rgb, &mm);
}
void HSV_to_RGB(hsv_t* hsv, rgb_888_t* rgb)
{
	float32_t vmin = (float32_t)((255 - hsv->color.s) * hsv->color.v)/255.0;
	uint8_t vmin1 = (uint8_t)vmin;
	float32_t a = (float32_t)((hsv->color.v - vmin1)*(hsv->color.v % 60))/60.0;
	uint8_t a1 = (uint8_t)a;
	H_to_RGB(rgb, &hsv->color.h, hsv->color.v, vmin1 + a1, vmin1, hsv->color.v - a1);
}
void RGB_to_HSL(rgb_888_t* rgb, hsl_t* hsl)
{
	float32_t loc_temp_1;
	rgb_mm_t mm;
	RGB_max_min(rgb, &mm);
	hsl->color.l = (mm.max + mm.min) >> 1;
	if(hsl->color.l == 0) hsl->color.s = 0;
	else
	{
		if(hsl->color.l < (255-hsl->color.l)) loc_temp_1 = 255.0*(float32_t)(mm.max - hsl->color.l)/(float32_t)hsl->color.l;
		else loc_temp_1 = 255.0*(float32_t)(mm.max - hsl->color.l)/(float32_t)(255-hsl->color.l);
		hsl->color.s = (uint8_t)loc_temp_1;
	}
	hsl->color.h = RGB_to_H(rgb, &mm);
}
void HSL_to_RGB(hsl_t* hsl, rgb_888_t* rgb)
{
	if(hsl->color.s == 0)
	{
		rgb->color.r = hsl->color.l;
	    rgb->color.g = hsl->color.l;
	    rgb->color.b = hsl->color.l;
	}
	else
	{
		//c = ;
		//uint8_t x = ;
		//int8_t m = hsl->l - (c >> 1);
	    //uint8_t xm = x+m;
	    //H_to_RGB(rgb, &hsv->h, c+m, xm, m, xm);
	}
}
void H_to_RGB(rgb_888_t* rgb, uint16_t* h, uint8_t v1, uint8_t v2, uint8_t v3, uint8_t v4)
{
	if(*h > 359) *h = 0;
	switch(*h/60)
	{
		case 0:
			rgb->color.r = v1;
			rgb->color.g = v2;
			rgb->color.b = v3;
		break;
		case 1:
			rgb->color.r = v4;
			rgb->color.g = v1;
			rgb->color.b = v3;
		break;
		case 2:
			rgb->color.r = v3;
			rgb->color.g = v1;
			rgb->color.b = v2;
		break;
		case 3:
			rgb->color.r = v3;
			rgb->color.g = v4;
			rgb->color.b = v1;
		break;
		case 4:
			rgb->color.r = v2;
			rgb->color.g = v3;
			rgb->color.b = v1;
		break;
		case 5:
			rgb->color.r = v1;
			rgb->color.g = v3;
			rgb->color.b = v4;
		break;
	}
}
void HSL_to_HSV(hsl_t* hsl, hsv_t* hsv)
{
	float32_t loc_temp;
	hsv->color.h = hsl->color.h;
	if(hsl->color.l < (255 - hsl->color.l)) loc_temp = (float32_t)(hsl->color.s*hsl->color.l)/255.0;
	else loc_temp = (float32_t)(hsl->color.s*(255 - hsl->color.l))/255.0;
	hsv->color.v = hsl->color.l + (uint8_t)loc_temp;
	if(hsv->color.v == 0) hsv->color.s = 0;
	else
	{
		loc_temp = 510.0 - (float32_t)(510*hsl->color.l)/(float32_t)hsv->color.v;
		hsv->color.s = (uint8_t)loc_temp;
	}
}
void HSV_to_HSL(hsv_t* hsv, hsl_t* hsl)
{
	float32_t loc_temp;
	hsl->color.h = hsv->color.h;
	loc_temp = (float32_t)(hsv->color.v * hsv->color.s)/510.0;
	hsl->color.l = hsv->color.v - (uint8_t)loc_temp;
	if(hsl->color.l < (255 - hsl->color.l)) loc_temp = 255.0*(float32_t)(hsv->color.v - hsl->color.l)/(float32_t)hsl->color.l;
	else loc_temp = 255.0*(float32_t)(hsv->color.v - hsl->color.l)/(float32_t)(255-hsl->color.l);
	hsl->color.s = (uint8_t)loc_temp;
}

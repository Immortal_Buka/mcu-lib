#include "console.hpp"
#include "myprint.hpp"
//
void console_t::in_char(uint8_t* data, uint8_t len)
{
	extern void execute_cmd(uint8_t* in);
	uart_print_sized(this->line_uart, data, len);
	for(uint8_t i=0; i<len; i++)
	{
		if((data[i] == '\r')||(data[i] == '\n'))
		{
			this->line[this->line_counter] = 0;
			execute_cmd(this->line);
			this->line[0] = 0;
			this->line_counter = 0;
		}
		else this->line[this->line_counter++] = data[i];
	}
}

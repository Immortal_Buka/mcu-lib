#include "dsp.h"
//
q31_t dsp_func_1(q31_t x0, q31_t y0, q31_t x1, q31_t y1, q31_t x);
//
uint8_t linear_interpolation_f32(float32_t* xx, float32_t* yy, uint16_t n, float32_t x, float32_t* y)
{
	uint16_t counter = 0;
	//extrapolation
	if(xx[0] < xx[n-1])
	{
		if(x < xx[0])
		{
			*y = yy[0] + (x - xx[0]) * ((yy[1] - yy[0])/(xx[1]-xx[0]));
			return 0;
		}
		if(x > xx[n-1])
		{
			*y = yy[n-2] + (x - xx[n-2]) * ((yy[n-1] - yy[n-2])/(xx[n-1]-xx[n-2]));
			return 0;
		}
	}
	if(xx[0] > xx[n-1])
	{
		if(x > xx[0])
		{
			*y = yy[0] + (x - xx[0]) * ((yy[1] - yy[0])/(xx[1]-xx[0]));
			return 0;
		}
		if(x < xx[n-1])
		{
			*y = yy[n-2] + (x - xx[n-2]) * ((yy[n-1] - yy[n-2])/(xx[n-1]-xx[n-2]));
			return 0;
		}
	}
	//interpolation
	while(counter < n)
	{
		if(((xx[0] < xx[n-1])&&(x >= xx[counter])&&(x <= xx[counter+1]))||((x <= xx[counter])&&(x >= xx[counter+1])&&(xx[0] > xx[n-1])))
		{
			*y = yy[counter] + (x - xx[counter]) * ((yy[counter+1] - yy[counter])/(xx[counter+1]-xx[counter]));
			return 0;
		}
		counter++;
	}
	*y = 0.0;
	return 1;
}
uint8_t linear_interpolation_i32(int32_t* xx, int32_t* yy, uint16_t n, int32_t x, int32_t* y)
{
	uint16_t counter = 0;
	//extrapolation
	if(xx[0] < xx[n-1])
	{
		if(x < xx[0])
		{
			*y = yy[0] + (x - xx[0]) * ((yy[1] - yy[0])/(xx[1]-xx[0]));
			return 0;
		}
		if(x > xx[n-1])
		{
			*y = yy[n-2] + (x - xx[n-2]) * ((yy[n-1] - yy[n-2])/(xx[n-1]-xx[n-2]));
			return 0;
		}
	}
	if(xx[0] > xx[n-1])
	{
		if(x > xx[0])
		{
			*y = yy[0] + (x - xx[0]) * ((yy[1] - yy[0])/(xx[1]-xx[0]));
			return 0;
		}
		if(x < xx[n-1])
		{
			*y = yy[n-2] + (x - xx[n-2]) * ((yy[n-1] - yy[n-2])/(xx[n-1]-xx[n-2]));
			return 0;
		}
	}
	//interpolation
	while(counter < n)
	{
		if(((xx[0] < xx[n-1])&&(x >= xx[counter])&&(x <= xx[counter+1]))||((x <= xx[counter])&&(x >= xx[counter+1])&&(xx[0] > xx[n-1])))
		{
			*y = yy[counter] + (x - xx[counter]) * ((yy[counter+1] - yy[counter])/(xx[counter+1]-xx[counter]));
			return 0;
		}
		counter++;
	}
	*y = 0;
	return 1;
}
int32_t median_i32(int32_t new_value, int32_t* old_values, uint8_t lenght)
{
	int32_t temp_loc[20] = {0};
	int32_t return_value;
	for(uint8_t i=0; i<(lenght-1); i++) temp_loc[i] = old_values[i];
	temp_loc[lenght-1] = new_value;
	//sorting start
	for(uint8_t j=1; j<lenght; j++)
	{
		return_value = temp_loc[j];
		uint8_t i = j - 1;
		while((i > 0) && (temp_loc[i] > return_value))
		{
			temp_loc[i + 1] = temp_loc[i];
			i = i - 1;
		}
		temp_loc[i + 1] = return_value;
	}
	//sorting end
	return_value = temp_loc[(lenght-1)>>1];
	for(uint8_t i=0; i<(lenght-2); i++) old_values[i] = old_values[i+1];
	old_values[lenght-2] = new_value;
	return return_value;
}
uint8_t bilinear_interpolation_f32(float32_t* a, uint16_t x_size, uint16_t y_size, float32_t x, float32_t y, float32_t* z)
{
	uint16_t counter_x = 1, counter_y = 1, x1i = 0, x2i = 0, y1i = 0, y2i = 0;
	//extrapolation
	if(a[y_size + 1] < a[x_size*(y_size + 1)])
	{
		if(x < a[y_size + 1])
		{
			x1i = y_size + 1;
			x2i = 2*(y_size + 1);
		}
		if(x > a[x_size*(y_size + 1)])
		{
			x1i = (x_size-1)*(y_size + 1);
			x2i = x_size*(y_size + 1);
		}
	}
	if(a[y_size + 1] > a[x_size*(y_size + 1)])
	{
		if(x > a[y_size + 1])
		{
			x1i = y_size + 1;
			x2i = 2*(y_size + 1);
		}
		if(x < a[x_size*(y_size + 1)])
		{
			x1i = (x_size-1)*(y_size + 1);
			x2i = x_size*(y_size + 1);
		}
	}
	if(a[1] < a[y_size])
	{
		if(y < a[1])
		{
			y1i = 1;
			y2i = 2;
		}
		if(y > a[y_size])
		{
			y1i = y_size-1;
			y2i = y_size;
		}
	}
	if(a[1] > a[y_size])
	{
		if(y > a[1])
		{
			y1i = 1;
			y2i = 2;
		}
		if(y < a[y_size])
		{
			y1i = y_size-1;
			y2i = y_size;
		}
	}
	//interpolation
	if(x1i == 0) while(counter_x < x_size)
	{
		if(((a[y_size + 1] < a[x_size*(y_size + 1)])&&(x >= a[counter_x*(y_size+1)])&&(x <= a[(counter_x+1)*(y_size+1)]))||
				((x <= a[counter_x*(y_size+1)])&&(x >= a[(counter_x+1)*(y_size+1)])&&(a[y_size + 1] > a[x_size*(y_size + 1)])))
		{
			x1i = counter_x*(y_size+1);
			x2i = (counter_x+1)*(y_size+1);
			break;
		}
		counter_x++;
	}
	if(y1i == 0) while(counter_y < y_size)
	{
		if(((a[1] < a[y_size])&&(y >= a[counter_y])&&(y <= a[counter_y+1]))||
				((y <= a[counter_y])&&(y >= a[counter_y+1])&&(a[1] > a[y_size])))
		{
			y1i = counter_y;
			y2i = counter_y+1;
			break;
		}
		counter_y++;
	}
	*z = 0.0;
	if(counter_x == x_size)	return 1;
	if(counter_y == y_size)	return 2;
	*z = ((a[y2i]-y)*((a[x2i]-x)*a[x1i + y1i]+(x-a[x1i])*a[x2i + y1i])+(y-a[y1i])*((a[x2i]-x)*a[x1i + y2i]+(x-a[x1i])*a[x2i + y2i]))/((a[y2i]-a[y1i])*(a[x2i]-a[x1i]));
	return 0;
}
uint8_t linear_interpolation_q31(q31_t* xx, q31_t* yy, uint16_t n, q31_t x, q31_t* y)
{
	uint16_t counter = 0;
	//extrapolation
	if(xx[0] < xx[n-1])
	{
		if(x < xx[0])
		{
			*y = dsp_func_1(xx[0], yy[0], xx[1], yy[1], x);
			return 0;
		}
		if(x > xx[n-1])
		{
			*y = dsp_func_1(xx[n-2], yy[n-2], xx[n-1], yy[n-1], x);
			return 0;
		}
	}
	if(xx[0] > xx[n-1])
	{
		if(x > xx[0])
		{
			*y = dsp_func_1(xx[0], yy[0], xx[1], yy[1], x);
			return 0;
		}
		if(x < xx[n-1])
		{
			*y = dsp_func_1(xx[n-2], yy[n-2], xx[n-1], yy[n-1], x);
			return 0;
		}
	}
	//interpolation
	while(counter < n)
	{
		if(((xx[0] < xx[n-1])&&(x >= xx[counter])&&(x <= xx[counter+1]))||((x <= xx[counter])&&(x >= xx[counter+1])&&(xx[0] > xx[n-1])))
		{
			*y = dsp_func_1(xx[counter], yy[counter], xx[counter+1], yy[counter+1], x);
			return 0;
		}
		counter++;
	}
	*y = 0;
	return 1;
}
q31_t dsp_func_1(q31_t x0, q31_t y0, q31_t x1, q31_t y1, q31_t x)
{
	uint8_t shift = 0;
	q31_t temp_loc_1 = y1-y0;
	q31_t temp_loc_2 = x1-x0;
	q31_t temp_loc_3;
	while(((temp_loc_1 & 0x7fffffff) >> shift) > (temp_loc_2 & 0x7fffffff))
	{
		shift++;
	}
	temp_loc_1 = temp_loc_1 >> shift;
	temp_loc_3 = q31_div(temp_loc_1, temp_loc_2);
	temp_loc_1 = x-x0;
	arm_mult_q31(&temp_loc_1, &temp_loc_3, &temp_loc_2, 1);
	temp_loc_1 = temp_loc_2 << shift;
	temp_loc_2 = temp_loc_1 + y0;
	return temp_loc_2;
}
q31_t q31_div(q31_t a, q31_t b)
{
	if(b != 0)
	{
		q63_t temp_loc_1 = (q63_t)a << 31;
		q31_t temp_loc_2 = temp_loc_1/b;
		return (temp_loc_2);
	}
	return 0;
}

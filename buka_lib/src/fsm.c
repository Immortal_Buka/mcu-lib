typedef enum
{
    state_0 = 0,
    state_1,
    state_2,
} states_t;
typedef enum
{
    signal_0,
    signal_1,
    signal_2.
} signals_t;
typedef void (*transition_callback)(states_t state, signals_t signal);
struct transition
{
    enum states new_state;
    transition_callback cb;
} transition_t;
//
void func_0(states_t state, signals_t signal);
void func_1(states_t state, signals_t signal);
void func_2(states_t state, signals_t signal);
//
transition_t fsm_table[3][3] = 
{
    [state_0][signal_0] = {state_1, func_0},
    [state_0][signal_1] = {state_0, 0},
    [state_0][signal_2] = {state_1, func_1},
    [state_1][signal_0] = {state_0, func_2},
    [state_1][signal_1] = {state_1, func_2},
    [state_1][signal_2] = {state_2, 0},
    [state_2][signal_0] = {state_0, func_1},
    [state_2][signal_1] = {state_0, 0},
    [state_2][signal_2] = {state_0, func_2},
};
void fsm_process(voif)
{
    states_t current_state = initial;
	signals_t current_signal;
	states_t new_state;
	transition_callback cb;
    while (1)
    {
        current_signal = get_signal();
        new_state = fsm_table[current_state][current_signal].new_state;
        cb = fsm_table[current_state][current_signal].cb;
        if (worker != 0)
        {
            cb(current_state, current_signal);
        }
        current_state = new_state;
    }
}
#include "jtag.hpp"
//
void jtag_chain_t::pulse_TCK(void)
{
	this->pi->TCK(1);
	this->pi->TCK(0);
}
void jtag_chain_t::jtag_ir(void)
{
	uint16_t complete_len = 0, counter = 0;
	for(uint8_t i=0; i<(this->size); i++) complete_len += this->jtag[i].ir_len;
	this->pi->TMS(0);	// exit reset state
	this->pulse_TCK();
	this->pi->TMS(1);	// goto sel_dr
	this->pulse_TCK();
	this->pi->TMS(1);	// goto sel_ir
	this->pulse_TCK();
	this->pi->TMS(0);	// goto cap_ir
	this->pulse_TCK();
	this->pi->TMS(0);	// goto shift_ir
	this->pulse_TCK();
	this->pi->TMS(0);	// shift in command
	for(uint8_t i=0; i<size; i++)
	{
		for(uint8_t j=0; j<(this->jtag[i].ir_len); j++)
		{
			counter++;
			if((this->jtag[i].ir_data_tx & 1) != 0) this->pi->TDI(1);    // send bit
			else this->pi->TDI(0);
			this->jtag[i].ir_data_tx >>= 1;
			if(counter == complete_len) this->pi->TMS(1);
			this->pulse_TCK();
		}
	}
	this->pi->TMS(1);	// goto update_ir
	this->pulse_TCK();
	this->pi->TMS(0);	// goto idle state
	this->pulse_TCK();
}
void jtag_chain_t::jtag_dr(void)
{
  	uint16_t complete_len = 0, counter = 0;
	for(uint8_t i=0; i<(this->size); i++)
	{
		complete_len += this->jtag[i].dr_len;
		this->jtag[i].dr_data_rx = 0;
	}
	this->pi->TMS(0);	// exit reset state
	this->pulse_TCK();
	this->pi->TMS(1);	// goto sel_dr
	this->pulse_TCK();
	this->pi->TMS(0);	// goto cap_dr
	this->pulse_TCK();
	this->pi->TMS(0);	// goto shift_dr
	this->pulse_TCK();
	this->pi->TMS(0);	// shift in command
	for(uint8_t i=0; i<(this->size); i++)
	{
		for(uint8_t j=0; j<(this->jtag[i].dr_len); j++)
		{
			counter++;
			if((this->jtag[i].dr_data_tx & 1) != 0) this->pi->TDI(1);    // send bit
			else this->pi->TDI(0);
			this->jtag[i].dr_data_tx >>= 1;
			this->jtag[i].dr_data_rx >>= 1;
			if(this->pi->TDO()) this->jtag[i].dr_data_rx |= 0x8000000000000000;
			if (counter == complete_len) this->pi->TMS(1);
			this->pulse_TCK();
		}
		this->jtag[i].dr_data_rx >>= (64-(this->jtag[i].dr_len));
	}
	this->pi->TMS(1); // goto update_dr
	this->pulse_TCK();
	this->pi->TMS(0); // goto idle state
	this->pulse_TCK();
}
uint64_t jtag_chain_t::jtag_swap(uint64_t in)
{
	uint64_t temp_loc_1 = in;
	uint64_t temp_loc_2 = 0;
	for(uint8_t i = 0; i < 64; i++)
	{
		temp_loc_2 <<= 1;
		if(temp_loc_1 & 1) temp_loc_2 |= 1;
		temp_loc_1 >>= 1;
	}
	return temp_loc_2;
}
void jtag_chain_t::jtag_init(void)
{
	this->pi->TMS(1);
	this->pi->TDI(0);
	for(uint8_t i=0;i<6;i++) this->pulse_TCK();
	this->pi->TMS(0);
	this->pulse_TCK();
}

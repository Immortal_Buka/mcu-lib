#include "my_lwip.h"
#include "lwip/ip4_addr.h"
#include "lwip/init.h"
#include "netif/ethernet.h"
#include "lwip/timeouts.h"
#include "lwip/opt.h"
#include "netif/etharp.h"
#include "lwip/dhcp.h"
#include "lwip/prot/dhcp.h"
#include "print.h"
//
extern void low_level_init(struct netif* netif);
extern uint8_t* low_level_get_mac_ptr(void);
extern err_t low_level_output(struct netif *netif, struct pbuf *p);
err_t my_netif_init(struct netif *netif);
void my_tcp_init(uint16_t port);
err_t my_tcp_sent(void *arg, struct tcp_pcb *tpcb, u16_t len);
err_t my_tcp_accept(void *arg, struct tcp_pcb *newpcb, err_t err);
err_t my_tcp_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err);
void my_tcp_error(void *arg, err_t err);
void print_ip(ip_addr_t* addr);
//
//
struct tcp_pcb* tcp_pcb_inst;
extern uint32_t time_ms;
ip4_addr_t lwip_ipaddr, lwip_netmask, lwip_gw;
struct netif lwip_netif;
//
void my_lwip_connect(void)
{
	//dhcp_stop(&lwip_netif);
	my_tcp_init(12345);
}
void my_lwip_init(void)
{
	IP4_ADDR(&lwip_ipaddr, 0, 0, 0, 0);
	IP4_ADDR(&lwip_netmask, 0, 0, 0, 0);
	IP4_ADDR(&lwip_gw, 0, 0, 0, 0);
	lwip_init();
	netif_add(&lwip_netif, &lwip_ipaddr, &lwip_netmask, &lwip_gw, 0, my_netif_init, ethernet_input);
	netif_set_default(&lwip_netif);
	netif_set_up(&lwip_netif);
	dhcp_start(&lwip_netif);
	/*if (netif_is_link_up(&lwip_netif)) netif_set_up(&lwip_netif);
	else netif_set_down(&lwip_netif);
	netif_set_link_callback(&lwip_netif, ethernetif_update_config);*/
}
err_t my_netif_init(struct netif *netif)
{
	uint8_t* mac_ptr;
	netif->name[0] = 'a';
	netif->name[1] = 'b';
	netif->output = etharp_output;
	netif->linkoutput = low_level_output;
	netif->hwaddr_len = 6;
	mac_ptr = low_level_get_mac_ptr();
	for(uint8_t i=0; i<6; i++) netif->hwaddr[i] = mac_ptr[i];
	netif->mtu = 1500;
	//netif->flags |= NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP | NETIF_FLAG_LINK_UP;
#if LWIP_ARP
	netif->flags |= NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP;
#else
	netif->flags |= NETIF_FLAG_BROADCAST;
#endif /* LWIP_ARP */
	netif->flags |= NETIF_FLAG_LINK_UP;
	low_level_init(netif);
	return ERR_OK;
}
void my_tcp_init(uint16_t port)
{
	err_t err;
	tcp_pcb_inst = tcp_new();
	if (tcp_pcb_inst != 0)
	{
		err = tcp_bind(tcp_pcb_inst, IP_ADDR_ANY, port);
		if (err == ERR_OK)
		{
			tcp_pcb_inst = tcp_listen(tcp_pcb_inst);
			tcp_accept(tcp_pcb_inst, my_tcp_accept);
			tcp_arg(tcp_pcb_inst, 0);
		}
		else low_level_error("\r\ntcp_bind error");
	}
	else low_level_error("\r\ntcp_new error");
}
void my_lwip_loop(void)
{
	struct pbuf *p;
	while((p = low_level_input()) != NULL)
	{
		if(lwip_netif.input(p, &lwip_netif) != ERR_OK)
		{
			LWIP_DEBUGF(NETIF_DEBUG, ("ethernetif_input: IP input error\n"));
			pbuf_free(p);
			p = NULL;
		}
	}
	sys_check_timeouts();
}
sys_prot_t sys_arch_protect(void)
{
	sys_prot_t result = (sys_prot_t)__get_PRIMASK();
	__disable_irq();
	return result;
}
void sys_arch_unprotect(sys_prot_t xValue)
{
	__enable_irq();
	__set_PRIMASK((uint32_t)xValue);
}
void sys_assert(char* pcMessage)
{
	low_level_debug_msg(pcMessage);
}
u32_t sys_now(void)
{
	return (u32_t)time_ms;
}
err_t my_tcp_accept(void *arg, struct tcp_pcb *newpcb, err_t err)
{
	low_level_debug_msg("\r\nmy_tcp_accept");
	LWIP_UNUSED_ARG(arg);
	if ((err != ERR_OK) || (newpcb == NULL)) return ERR_VAL;
	tcp_pcb_inst = newpcb;
	tcp_accepted(newpcb);
	tcp_setprio(newpcb, TCP_PRIO_MIN);
	tcp_recv(newpcb, my_tcp_recv);
	tcp_err(newpcb, my_tcp_error);
	tcp_sent(newpcb, my_tcp_sent);
	return ERR_OK;
}
err_t my_tcp_sent(void *arg, struct tcp_pcb *tpcb, u16_t len)
{
	low_level_debug_msg("\r\nmy_tcp_sent");
	return ERR_OK;
}
err_t my_tcp_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err)
{
	low_level_debug_msg("\r\nmy_tcp_recv");
	low_level_data_in(p->len, (uint8_t*)p->payload);
	return ERR_OK;
}
void my_tcp_error(void *arg, err_t err)
{
	low_level_debug_msg("\r\nmy_tcp_error: ");
	switch(err)
	{
		case ERR_OK: low_level_debug_msg("No error, everything OK"); break;
		case ERR_MEM: low_level_debug_msg("Out of memory error"); break;
		case ERR_BUF: low_level_debug_msg("Buffer error"); break;
		case ERR_TIMEOUT: low_level_debug_msg("Timeout"); break;
		case ERR_RTE: low_level_debug_msg("Routing problem"); break;
		case ERR_INPROGRESS: low_level_debug_msg("Operation in progress"); break;
		case ERR_VAL: low_level_debug_msg("Illegal value"); break;
		case ERR_WOULDBLOCK: low_level_debug_msg("Operation would block"); break;
		case ERR_USE: low_level_debug_msg("Address in use"); break;
		case ERR_ALREADY: low_level_debug_msg("Already connecting"); break;
		case ERR_ISCONN: low_level_debug_msg("Conn already established"); break;
		case ERR_CONN: low_level_debug_msg("Not connected"); break;
		case ERR_IF: low_level_debug_msg("Low-level netif error"); break;
		case ERR_ABRT: low_level_debug_msg("Connection aborted"); break;
		case ERR_RST: low_level_debug_msg("Connection reset"); break;
		case ERR_CLSD: low_level_debug_msg("Connection closed"); break;
		case ERR_ARG: low_level_debug_msg("Illegal argument"); break;
		default: break;
	}
}
void print_dhcp_state(void)
{
	uint8_t string[4];
	static u8_t dhcp_last_state = DHCP_STATE_OFF;
	struct dhcp *dhcp = netif_dhcp_data(&lwip_netif);
	if (dhcp == NULL) dhcp_last_state = DHCP_STATE_OFF;
	else if (dhcp_last_state != dhcp->state)
	{
		dhcp_last_state = dhcp->state;
		low_level_debug_msg("\r\nDHCP state: ");
		switch (dhcp_last_state)
		{
			case DHCP_STATE_OFF: low_level_debug_msg("OFF"); break;
			case DHCP_STATE_REQUESTING: low_level_debug_msg("REQUESTING"); break;
			case DHCP_STATE_INIT: low_level_debug_msg("INIT"); break;
			case DHCP_STATE_REBOOTING: low_level_debug_msg("REBOOTING"); break;
			case DHCP_STATE_REBINDING: low_level_debug_msg("REBINDING"); break;
			case DHCP_STATE_RENEWING: low_level_debug_msg("RENEWING"); break;
			case DHCP_STATE_SELECTING: low_level_debug_msg("SELECTING"); break;
			case DHCP_STATE_INFORMING: low_level_debug_msg("INFORMING"); break;
			case DHCP_STATE_CHECKING: low_level_debug_msg("CHECKING"); break;
			case DHCP_STATE_BOUND: low_level_debug_msg("BOUND"); break;
			case DHCP_STATE_BACKING_OFF: low_level_debug_msg("BACKING_OFF"); break;
			default:
				uint8_to_string(dhcp_last_state, string);
				low_level_debug_msg(string);
			break;
		}
		if (dhcp_last_state == DHCP_STATE_BOUND)
		{
			low_level_debug_msg("\r\nIPv4 Address: ");
			print_ip(&lwip_netif.ip_addr);
			low_level_debug_msg("\r\nIPv4 Subnet mask: ");
			print_ip(&lwip_netif.netmask);
			low_level_debug_msg("\r\nIPv4 Gateway: ");
			print_ip(&lwip_netif.gw);
		}
	}
}
void print_ip(ip_addr_t* addr)
{
	uint8_t string[4];
	uint8_t temp_loc = ip4_addr1(addr);
	uint8_to_string(temp_loc, string);
	low_level_debug_msg(string);
	low_level_debug_msg(".");
	temp_loc = ip4_addr2(addr);
	uint8_to_string(temp_loc, string);
	low_level_debug_msg(string);
	low_level_debug_msg(".");
	temp_loc = ip4_addr3(addr);
	uint8_to_string(temp_loc, string);
	low_level_debug_msg(string);
	low_level_debug_msg(".");
	temp_loc = ip4_addr4(addr);
	uint8_to_string(temp_loc, string);
	low_level_debug_msg(string);
}

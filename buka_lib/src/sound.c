/*
description: 	pwm sound
version:		190112
author:			Buka
history:
190112
- initial version
*/
#include "sound.h"
//
extern void set_music_pwm(uint16_t pwm);
//
uint8_t music_number = 0;
music_descriptor_t* descriptors_table;
uint8_t descriptors_table_size = 0;
//const uint16_t in_taberna[] = {CS,Cd1,CS,Cd1,0,0,CS,Cd1,0,0};
//
uint8_t music_play(uint8_t idx)
{
	if(idx > descriptors_table_size) return 1;
	else music_number = idx;
	return 0;
}
void music_irq(void)
{
	static uint8_t music_work = 0;
	static uint8_t music_counter = 0;
    if(music_number > 0)
	{
		music_work = music_number;
		music_number = 0;
		music_counter = 0;
	}
    if(music_work > 0)
    {
    	set_music_pwm(descriptors_table[music_work-1].pointer[music_counter]);
        music_counter++;
        if(music_counter == descriptors_table[music_work-1].length)
        {
            if(descriptors_table[music_work-1].repeat) music_number = music_work;
            music_work = 0;
            set_music_pwm(0);
        }
    }
}
void music_init(const music_descriptor_t* table, uint8_t size)
{
	descriptors_table = (music_descriptor_t*)table;
	descriptors_table_size = size;
}

#include "std_system.hpp"
//
#define WA	__attribute__ ((weak, alias ("Default_Handler")))
//
void Reset_Handler(void);
void call_ctors(const func_ptr* start, const func_ptr* end);
extern "C"
{
	void Default_Handler(void);
}
void SysTick_Handler(void);
//
extern uint32_t __StackTop;
volatile uint32_t time_ms = 0;
//
#if defined __GNUC__
volatile const uint8_t compiler[4] __attribute__((section (".version"),used)) = {'g', __GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__};
#endif
#if __clang__
volatile const uint8_t compiler[4] __attribute__((section (".version"),used)) = {'c', __clang_major__, __clang_minor__, __clang_patchlevel__};
#endif
//
__attribute__((naked, noreturn)) void Reset_Handler(void)
{
	extern void sys_init(void);
	extern void main(void);
	extern uint32_t __bss_start__;
	extern uint32_t __bss_end__;
	extern func_ptr __cpp_begin[];
	extern func_ptr __cpp_end[];
	//
	volatile uint32_t *pSrc, *pDest;
	sys_init();
#if DEBUG_RAM == 0
	extern uint32_t __etext;
	extern uint32_t __data_start__;
	extern uint32_t __data_end__;
	pSrc = &__etext;
	pDest = &__data_start__;
	while(pDest < &__data_end__) *pDest++ = *pSrc++;
#endif	
	pDest = &__bss_start__;
	while(pDest < &__bss_end__) *pDest++ = 0;
	call_ctors(__cpp_begin, __cpp_end);
	main();
}
void arm8_stack_limit(void)
{
#if (defined (__ARM_FEATURE_CMSE))
	extern uint32_t __bss_end__;
	__set_MSPLIM(__bss_end__);
#endif
}
void call_ctors(const func_ptr* start, const func_ptr* end)
{
	while(start < end) 
	{
		if(*start) (*start)();
		start++;
	}
}
void SysTick_Handler(void)
{
	extern void project_systick_handler(void);
	time_ms++;
	project_systick_handler();
}
void delay(uint32_t data)
{
	volatile uint32_t temp1 = data;
	while (temp1--) __asm("NOP");
}
void delay_ms(uint32_t ms)
{
	volatile uint32_t* SYST_CVR = (volatile uint32_t*)0xe000e018;
	uint32_t temp_loc_1 = time_ms;
	uint32_t temp_loc_2 = *SYST_CVR;
	while((time_ms - temp_loc_1) < ms){};
	while(temp_loc_2 < (*SYST_CVR)){};
}
void fpu_on(void)
{
	*((volatile uint32_t*)0xE000ED88) |= (3 << 20)|(3 << 22);//SCB->CPACR
}
inline uint32_t bb_periph_read(uint32_t addr, uint32_t bit)
{
	return *((uint32_t*)(0x42000000 + (((uint32_t)addr - 0x40000000)*32) + (bit * 4)));
}
inline void bb_periph_write(uint32_t addr, uint32_t bit, uint32_t data)
{
	*((uint32_t*)(0x42000000 + (((uint32_t)addr - 0x40000000)*32) + (bit * 4))) = data;
}
inline uint32_t bb_mem_read(uint32_t addr, uint32_t bit)
{
	return *((uint32_t*)(0x22000000 + (((uint32_t)addr - 0x20000000)*32) + (bit * 4)));
}
inline void bb_mem_write(uint32_t addr, uint32_t bit, uint32_t data)
{
	*((uint32_t*)(0x22000000 + (((uint32_t)addr - 0x20000000)*32) + (bit * 4))) = data;
}
void memset_word(uint32_t* ptr, uint32_t value, uint32_t size)
{
	for(uint32_t i=0; i<(size>>2); i++) ptr[i] = value;
}
void Default_Handler(void)
{
	extern void project_def_handler(void);
	volatile uint32_t regs[8];
	/*
	0 - IPSR
	0 = Thread mode
	1 = Reset
	2 = NMI
	3 = HardFault
	4 = MemManage
	5 = BusFault
	6 = UsageFault
	7 = SecureFault
	8-10 = Reserved
	11 = SVCall
	12 = DebugMonitor
	13 = Reserved
	14 = PendSV
	15 = SysTick
	16 = IRQ0
	n+15 = IRQ(n-1)
	*/
	regs[0] = __get_IPSR();
	regs[1] = __get_MSP();
	regs[2] = *((uint32_t*)0xE000ED28);//SCS->CFSR
	regs[3] = *((uint32_t*)0xE000ED2C);//SCS->HFSR
	regs[4] = *((uint32_t*)0xE000ED30);//SCS->DFSR
	regs[5] = *((uint32_t*)0xE000ED3C);//SCS->AFSR
	regs[6] = *((uint32_t*)0xE000ED34);//SCS->MMFAR
	regs[7] = *((uint32_t*)0xE000ED38);//SCS->BFAR
	project_def_handler();
	while(1){};
}
extern "C" void __cxa_pure_virtual(void) WA;
//
void NMI_Handler(void) WA;
void HardFault_Handler(void) WA;
void MemManage_Handler(void) WA;
void BusFault_Handler(void) WA;
void UsageFault_Handler(void) WA;
void SVC_Handler(void) WA;
void DebugMon_Handler(void) WA;
void PendSV_Handler(void) WA;
void SecureFault_Handler(void) WA;
//
const func_ptr std_arm8mm_vectors[] __attribute__ ((section(".isr_vector_arm8mm"),used)) =
{
	(func_ptr)((uint32_t)&__StackTop),	//Initial Stack Pointer
	Reset_Handler,                 		//Reset Handler
	NMI_Handler,                   		//NMI Handler
	HardFault_Handler,             //Hard Fault Handler
	MemManage_Handler,             //MPU Fault Handler
	BusFault_Handler,              //Bus Fault Handler
	UsageFault_Handler,            //Usage Fault Handler
	SecureFault_Handler,           //Secure Fault Handler
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	SVC_Handler,                   //SVCall Handler
	DebugMon_Handler,              //Debug Monitor Handler
	0,                             //Reserved
	PendSV_Handler,                //PendSV Handler
	SysTick_Handler                //SysTick Handler
};
const func_ptr std_arm7m_vectors[] __attribute__ ((section(".isr_vector_arm7m"),used)) =
{
	(func_ptr) ((uint32_t) &__StackTop),            //Initial Stack Pointer
	Reset_Handler,                 //Reset Handler
	NMI_Handler,                   //NMI Handler
	HardFault_Handler,             //Hard Fault Handler
	MemManage_Handler,             //MPU Fault Handler
	BusFault_Handler,              //Bus Fault Handler
	UsageFault_Handler,            //Usage Fault Handler
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	SVC_Handler,                   //SVCall Handler
	DebugMon_Handler,              //Debug Monitor Handler
	0,                             //Reserved
	PendSV_Handler,                //PendSV Handler
	SysTick_Handler                //SysTick Handler
};
const func_ptr std_arm6m_8mb_vectors[] __attribute__ ((section(".isr_vector_arm6m"),used)) =
{
	(func_ptr)((uint32_t)&__StackTop),//Initial Stack Pointer
	Reset_Handler,                 //Reset Handler
	NMI_Handler,                   //NMI Handler
	HardFault_Handler,             //Hard Fault Handler
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	SVC_Handler,                   //SVCall Handler
	0,                             //Reserved
	0,                             //Reserved
	PendSV_Handler,                //PendSV Handler
	SysTick_Handler                //SysTick Handler
};
#include "device_vectors.h"

/*
description: 	standart Cortex-M functions & startup
version:		191115
author:			Buka
*/
#include "std_system.h"
//
#define WA	__attribute__ ((weak, alias ("Default_Handler")))
//
extern void main(void);
extern void sys_init(void);
extern void project_def_handler(void);
//
volatile const uint8_t gcc[4] __attribute__((section (".version"))) = {'g', __GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__};
//
void Reset_Handler(void)
{
	extern uint32_t __etext;
	extern uint32_t __data_start__;
	extern uint32_t __data_end__;
	extern uint32_t __bss_start__;
	extern uint32_t __bss_end__;
	volatile uint32_t *pSrc, *pDest;
	sys_init();
	pSrc = &__etext;
	pDest = &__data_start__;
	while(pDest < &__data_end__) *pDest++ = *pSrc++;
	pDest = &__bss_start__;
	while(pDest < &__bss_end__) *pDest++ = 0;
	main();
}
void Default_Handler(void)
{
	static volatile uint32_t regs[8] = {0};
/*
0 - IPSR
	0 = Thread mode
	1 = Reset
	2 = NMI
	3 = HardFault
	4 = MemManage
	5 = BusFault
	6 = UsageFault
	7 = SecureFault
	8-10 = Reserved
	11 = SVCall
	12 = DebugMonitor
	13 = Reserved
	14 = PendSV
	15 = SysTick
	16 = IRQ0
	n+15 = IRQ(n-1)
1 - MSP
2 - SCB->SFSR
3 - SCB->HFSR
4 - SCB->DFSR
5 - SCB->AFSR
6 - SCB->MMFAR
7 - SCB->BFAR
*/
	regs[0] = __get_xPSR();
	regs[1] = __get_MSP();
	regs[2] = *((uint32_t*)0xE000ED28);
	regs[3] = *((uint32_t*)0xE000ED2C);
	regs[4] = *((uint32_t*)0xE000ED30);
	regs[5] = *((uint32_t*)0xE000ED3C);
	regs[6] = *((uint32_t*)0xE000ED34);
	regs[7] = *((uint32_t*)0xE000ED38);
	project_def_handler();
	while(1){};
}
void delay(uint32_t data)
{
	volatile uint32_t temp1 = data;
	while (temp1--) __asm("NOP");
}
inline void fpu_on(void)
{
	*((uint32_t*)0xE000ED88) |= (3 << 20)|(3 << 22);//SCB->CPACR
}
inline uint32_t BB_periph_read(uint32_t addr, uint32_t bit)
{
	return *((uint32_t*)(0x42000000 + (((uint32_t)addr - 0x40000000)*32) + (bit * 4)));
}
inline void BB_periph_write(uint32_t addr, uint32_t bit, uint32_t data)
{
	*((uint32_t*)(0x42000000 + (((uint32_t)addr - 0x40000000)*32) + (bit * 4))) = data;
}
inline uint32_t BB_mem_read(uint32_t addr, uint32_t bit)
{
	return *((uint32_t*)(0x22000000 + (((uint32_t)addr - 0x20000000)*32) + (bit * 4)));
}
inline void BB_mem_write(uint32_t addr, uint32_t bit, uint32_t data)
{
	*((uint32_t*)(0x22000000 + (((uint32_t)addr - 0x20000000)*32) + (bit * 4))) = data;
}
void memset_word(uint32_t* ptr, uint32_t value, uint32_t size)
{
	for(uint32_t i=0; i<(size>>2); i++) ptr[i] = value;
}
//
void NMI_Handler(void) __attribute__ ((weak, alias ("Default_Handler")));
void HardFault_Handler(void) __attribute__ ((weak, alias("Default_Handler")));
void MemManage_Handler(void) __attribute__ ((weak, alias("Default_Handler")));
void BusFault_Handler(void) __attribute__ ((weak, alias("Default_Handler")));
void UsageFault_Handler(void) __attribute__ ((weak, alias("Default_Handler")));
void SVC_Handler(void) __attribute__ ((weak, alias("Default_Handler")));
void DebugMon_Handler(void) __attribute__ ((weak, alias("Default_Handler")));
void PendSV_Handler(void) __attribute__ ((weak, alias("Default_Handler")));
void SysTick_Handler(void) __attribute__ ((weak, alias("Default_Handler")));
void SecureFault_Handler(void) __attribute__ ((weak, alias("Default_Handler")));
//
extern uint32_t __StackTop;
volatile const func_ptr std_arm8mm_vectors[] __attribute__ ((section(".isr_vector_arm8mm"))) =
{
	(func_ptr)((uint32_t)&__StackTop),//Initial Stack Pointer
	Reset_Handler,                 //Reset Handler
	NMI_Handler,                   //NMI Handler
	HardFault_Handler,             //Hard Fault Handler
	MemManage_Handler,             //MPU Fault Handler
	BusFault_Handler,              //Bus Fault Handler
	UsageFault_Handler,            //Usage Fault Handler
	SecureFault_Handler,           //Secure Fault Handler
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	SVC_Handler,                   //SVCall Handler
	DebugMon_Handler,              //Debug Monitor Handler
	0,                             //Reserved
	PendSV_Handler,                //PendSV Handler
	SysTick_Handler                //SysTick Handler
};
volatile const func_ptr std_arm7m_vectors[] __attribute__ ((section(".isr_vector_arm7m"))) =
{
	(func_ptr) ((uint32_t) &__StackTop),            //Initial Stack Pointer
	Reset_Handler,                 //Reset Handler
	NMI_Handler,                   //NMI Handler
	HardFault_Handler,             //Hard Fault Handler
	MemManage_Handler,             //MPU Fault Handler
	BusFault_Handler,              //Bus Fault Handler
	UsageFault_Handler,            //Usage Fault Handler
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	SVC_Handler,                   //SVCall Handler
	DebugMon_Handler,              //Debug Monitor Handler
	0,                             //Reserved
	PendSV_Handler,                //PendSV Handler
	SysTick_Handler                //SysTick Handler
};
volatile const func_ptr std_arm6m_8mb_vectors[] __attribute__ ((section(".isr_vector_arm6m"))) =
{
	(func_ptr)((uint32_t)&__StackTop),//Initial Stack Pointer
	Reset_Handler,                 //Reset Handler
	NMI_Handler,                   //NMI Handler
	HardFault_Handler,             //Hard Fault Handler
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	0,                             //Reserved
	SVC_Handler,                   //SVCall Handler
	0,                             //Reserved
	0,                             //Reserved
	PendSV_Handler,                //PendSV Handler
	SysTick_Handler                //SysTick Handler
};
#include "device_vectors.h"

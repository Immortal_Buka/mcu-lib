//ver180215
/*
description: 	systick delay
version:		180215
author:			Buka
history:
180215
- initial version
*/
volatile uint32_t timer;
void SysTick_Handler(void)
{
    timer++;
}
void delay_systick_ms(uint32_t parametr)
{
	timer = 0;
	SysTick->VAL = 0;
	SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk|SysTick_CTRL_TICKINT_Msk|SysTick_CTRL_ENABLE_Msk;
	while(parametr != timer){};
	SysTick->CTRL = 0;
}
void delay_systick_ms_init(uint32_t ticks_for_1ms, uint32_t priority)
{
	SysTick->LOAD  = ticks_for_1ms - 1;
	NVIC_SetPriority(SysTick_IRQn, priority)
	NVIC_EnableIRQ(SysTick_IRQn);
}

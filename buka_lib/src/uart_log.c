/*
description: 	print logs throw UART
version:		180823
author:			Buka
history:
180823
- initial version
*/
#include "uart_log.h"
#include <stdarg.h>
//
#ifdef TIMESTAMP
	#include "time.h"
	extern void get_time(time_t* time);
#endif
//
void* log_uart = 0;
//
void uart_log_init(void* uart)
{
	log_uart = uart;
}
//
void uart_log_add(log_write_type type, uint8_t* string_1, ...)
{
	if(((uint32_t)log_uart) != 0)
	{
#ifdef TIMESTAMP
		time_t time;
		get_time(&time);
		uint8_t time_string[] = "\r\n\e[37m[00:00:00.00]";
		string[3] = time.hour/10 + 0x30;
		string[4] = time.hour%10 + 0x30;
		string[6] = time.minute/10 + 0x30;
		string[7] = time.minute%10 + 0x30;
		string[9] = time.second/10 + 0x30;
		string[10] = time.second%10 + 0x30;
		string[12] = time.second_cents/10 + 0x30;
		string[13] = time.second_cents%10 + 0x30;
		uart_print_string(log_uart, time_string);
#else
		uart_print_string(log_uart, "\r\n");
#endif
		va_list ap;
		uint8_t* message;
		va_start(ap, string_1);
		message = va_arg(ap, uint8_t*);
		switch(type)
		{
			case t_function:
				if((uint32_t)message == 0) uart_print_string(log_uart, "\e[32m[OK]\e[97m ");
				else uart_print_string(log_uart, "\e[31m[ERROR]\e[97m ");
				uart_print_string(log_uart, string_1);
				break;
			case t_irq:
				uart_print_string(log_uart, "\e[33m[IRQ]\e[97m ");
				uart_print_string(log_uart, string_1);
				break;
			case t_info:
				uart_print_string(log_uart, "\e[34m[INFO]\e[97m ");
				uart_print_string(log_uart, string_1);
				break;
			case t_fault:
				uart_print_string(log_uart, "\e[31m[FAULT]\e[97m ");
				uart_print_string(log_uart, string_1);
				break;
			case t_data:
				uart_print_string(log_uart, "\e[36m[DATA]\e[97m ");
				uart_print_string(log_uart, string_1);
				break;
			case t_evt:
				uart_print_string(log_uart, "\e[35m[");
				uart_print_string(log_uart, string_1);
				uart_print_string(log_uart, "]\e[97m ");
				break;
		}
		while(message)
		{
			uart_print_string(log_uart, " - ");
			uart_print_string(log_uart, message);
			message = va_arg(ap, uint8_t*);
		}
		va_end(ap);
	}
}
void print_ble_data(uint8_t* string, uint16_t size, uint8_t* data)
{
	if(((uint32_t)log_uart) != 0)
	{
		uint8_t string_1[7] = {0};
		uart_log_add(t_data, string, 0);
		for(uint16_t i=0; i<size;i++)
		{
			uint8_to_hex_string(data[i], string_1);
			uart_print_string(log_uart, string_1);
			uart_print_char(log_uart, 0x20);
		}
	}
}
void print_address(uint8_t* addr)
{
	if(((uint32_t)log_uart) != 0)
	{
		print_hex_uint8(log_uart, addr[5]);
		uart_print_char(log_uart, 0x3a);
		print_hex_uint8(log_uart, addr[4]);
		uart_print_char(log_uart, 0x3a);
		print_hex_uint8(log_uart, addr[3]);
		uart_print_char(log_uart, 0x3a);
		print_hex_uint8(log_uart, addr[2]);
		uart_print_char(log_uart, 0x3a);
		print_hex_uint8(log_uart, addr[1]);
		uart_print_char(log_uart, 0x3a);
		print_hex_uint8(log_uart, addr[0]);
	}
}

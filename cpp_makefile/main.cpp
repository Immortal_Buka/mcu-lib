#include "std_system.hpp"
#include "samg53n19.h"
#include "crc.hpp"
#include "circular_buffer.hpp"
//
class led
{
	protected:
		bool state;
	public:
		led(void) : state(0)
		{
		}
		virtual void toggle(void) = 0;
		virtual void on(void) = 0;
		virtual void off(void) = 0;
		bool get_state(void)
		{
			return state;
		}
		virtual ~led(void) = 0;
};
enum class Color
{
	Red,
	Green,
	Blue
};
//
void main(void);
void sys_init(void);
void project_def_handler(void);
void cb_error_signal(cb_error_t error);
void UART0_Handler(void);
//
class device_led : public virtual led
{
	public:
		void toggle(void)
		{
			if(state) off();
			else on();
		}
		void on(void)
		{
			state = true;
		}
		void off(void)
		{
			state = false;
		}
		device_led(void)
		{
			device_led::off();
		}
};
//
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x28,
	.month = 0x04,
	.year = 0x20,
};
crc<uint8_t> crc8_1(0x1d, 0xfa, 0x17);
volatile crc<uint16_t> crc16_1(0x1021, 0, 0);
uint8_t memory[1000];
circular_buffer as(memory, sizeof(memory));
uint8_t glob_loc = 1;
//
void main(void)
{
	Color col = Color::Red;
	device_led led_1;
	while(1)
	{
		glob_loc = crc8_1.calc(&glob_loc, 1);
		as.write(&glob_loc, 1);
		glob_loc++;
	}
}
void sys_init(void)
{
}
void project_def_handler(void)
{
}
void cb_error_signal(cb_error_t sign)
{
	uint8_t temp_loc = (uint8_t)sign;
	as.write(&temp_loc, 1);
	as.reset();
}
void UART0_Handler(void)
{
	glob_loc = crc8_1.calc(&glob_loc, 1);
	as.write(&glob_loc, 1);
	glob_loc++;
}

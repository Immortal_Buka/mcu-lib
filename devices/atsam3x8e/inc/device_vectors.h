//ver200205
void SUPC_Handler(void) WA;
void RSTC_Handler(void) WA;
void RTC_Handler(void) WA;
void RTT_Handler(void) WA;
void WDT_Handler(void) WA;
void PMC_Handler(void) WA;
void EFC0_Handler(void) WA;
void EFC1_Handler(void) WA;
void UART_Handler(void) WA;
void SMC_Handler(void) WA;
void SDRAMC_Handler(void) WA;
void PIOA_Handler(void) WA;
void PIOB_Handler(void) WA;
void PIOC_Handler(void) WA;
void PIOD_Handler(void) WA;
void PIOE_Handler(void) WA;
void PIOF_Handler(void) WA;
void USART0_Handler(void) WA;
void USART1_Handler(void) WA;
void USART2_Handler(void) WA;
void USART3_Handler(void) WA;
void HSMCI_Handler(void) WA;
void TWI0_Handler(void) WA;
void TWI1_Handler(void) WA;
void SPI0_Handler(void) WA;
void SPI1_Handler(void) WA;
void SSC_Handler(void) WA;
void TC0_Handler(void) WA;
void TC1_Handler(void) WA;
void TC2_Handler(void) WA;
void TC3_Handler(void) WA;
void TC4_Handler(void) WA;
void TC5_Handler(void) WA;
void TC6_Handler(void) WA;
void TC7_Handler(void) WA;
void TC8_Handler(void) WA;
void PWM_Handler(void) WA;
void ADC_Handler(void) WA;
void DACC_Handler(void) WA;
void DMAC_Handler(void) WA;
void UOTGHS_Handler(void) WA;
void TRNG_Handler(void) WA;
void EMAC_Handler(void) WA;
void CAN0_Handler(void) WA;
void CAN1_Handler(void) WA;
//
volatile const func_ptr device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	SUPC_Handler, /* 0  Supply Controller */
	RSTC_Handler, /* 1  Reset Controller */
	RTC_Handler, /* 2  Real Time Clock */
	RTT_Handler, /* 3  Real Time Timer */
	WDT_Handler, /* 4  Watchdog Timer */
	PMC_Handler, /* 5  Power Management Controller */
	EFC0_Handler, /* 6  Enhanced Flash Controller 0 */
	EFC1_Handler, /* 7  Enhanced Flash Controller 1 */
	UART_Handler, /* 8  Universal Asynchronous Receiver Transceiver */
	SMC_Handler, /* 9  Static Memory Controller */
	SDRAMC_Handler, /* 10 Synchronous Dynamic RAM Controller */
	PIOA_Handler, /* 11 Parallel I/O Controller A, */
	PIOB_Handler, /* 12 Parallel I/O Controller B */
	PIOC_Handler, /* 13 Parallel I/O Controller C */
	PIOD_Handler, /* 14 Parallel I/O Controller D */
	PIOE_Handler, /* 15 Parallel I/O Controller E */
	PIOF_Handler, /* 16 Parallel I/O Controller F */
	USART0_Handler, /* 17 USART 0 */
	USART1_Handler, /* 18 USART 1 */
	USART2_Handler, /* 19 USART 2 */
	USART3_Handler, /* 20 USART 3 */
	HSMCI_Handler, /* 21 Multimedia Card Interface */
	TWI0_Handler, /* 22 Two-Wire Interface 0 */
	TWI1_Handler, /* 23 Two-Wire Interface 1 */
	SPI0_Handler, /* 24 Serial Peripheral Interface */
	SPI1_Handler, /* 25 Serial Peripheral Interface */
	SSC_Handler, /* 26 Synchronous Serial Controller */
	TC0_Handler, /* 27 Timer Counter 0 */
	TC1_Handler, /* 28 Timer Counter 1 */
	TC2_Handler, /* 29 Timer Counter 2 */
	TC3_Handler, /* 30 Timer Counter 3 */
	TC4_Handler, /* 31 Timer Counter 4 */
	TC5_Handler, /* 32 Timer Counter 5 */
	TC6_Handler, /* 33 Timer Counter 6 */
	TC7_Handler, /* 34 Timer Counter 7 */
	TC8_Handler, /* 35 Timer Counter 8 */
	PWM_Handler, /* 36 Pulse Width Modulation Controller */
	ADC_Handler, /* 37 ADC Controller */
	DACC_Handler, /* 38 DAC Controller */
	DMAC_Handler, /* 39 DMA Controller */
	UOTGHS_Handler, /* 40 USB OTG High Speed */
	TRNG_Handler, /* 41 True Random Number Generator */
	EMAC_Handler, /* 42 Ethernet MAC */
	CAN0_Handler, /* 43 CAN Controller 0 */
	CAN1_Handler, /* 44 CAN Controller 1 */
};

//ver200205
#ifndef _SYSTEM_H_
#define _SYSTEM_H_
#define DONT_USE_CMSIS_INIT
//
#include "sam3x8e.h"
#include "std_system.h"
//
void sys_init(void);
void project_def_handler(void);
#endif

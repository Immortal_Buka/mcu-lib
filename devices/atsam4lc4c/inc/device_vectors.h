//ver200304
void HFLASHC_Handler(void) WA;
void PDCA_0_Handler(void) WA;
void PDCA_1_Handler(void) WA;
void PDCA_2_Handler(void) WA;
void PDCA_3_Handler(void) WA;
void PDCA_4_Handler(void) WA;
void PDCA_5_Handler(void) WA;
void PDCA_6_Handler(void) WA;
void PDCA_7_Handler(void) WA;
void PDCA_8_Handler(void) WA;
void PDCA_9_Handler(void) WA;
void PDCA_10_Handler(void) WA;
void PDCA_11_Handler(void) WA;
void PDCA_12_Handler(void) WA;
void PDCA_13_Handler(void) WA;
void PDCA_14_Handler(void) WA;
void PDCA_15_Handler(void) WA;
void CRCCU_Handler(void) WA;
void USBC_Handler(void) WA;
void PEVC_0_Handler(void) WA;
void PEVC_1_Handler(void) WA;
void AESA_Handler(void) WA;
void PM_Handler(void) WA;
void SCIF_Handler(void) WA;
void FREQM_Handler(void) WA;
void GPIO_0_Handler(void) WA;
void GPIO_1_Handler(void) WA;
void GPIO_2_Handler(void) WA;
void GPIO_3_Handler(void) WA;
void GPIO_4_Handler(void) WA;
void GPIO_5_Handler(void) WA;
void GPIO_6_Handler(void) WA;
void GPIO_7_Handler(void) WA;
void GPIO_8_Handler(void) WA;
void GPIO_9_Handler(void) WA;
void GPIO_10_Handler(void) WA;
void GPIO_11_Handler(void) WA;
void BPM_Handler(void) WA;
void BSCIF_Handler(void) WA;
void AST_0_Handler(void) WA;
void AST_1_Handler(void) WA;
void AST_2_Handler(void) WA;
void AST_3_Handler(void) WA;
void AST_4_Handler(void) WA;
void WDT_Handler(void) WA;
void EIC_0_Handler(void) WA;
void EIC_1_Handler(void) WA;
void EIC_2_Handler(void) WA;
void EIC_3_Handler(void) WA;
void EIC_4_Handler(void) WA;
void EIC_5_Handler(void) WA;
void EIC_6_Handler(void) WA;
void EIC_7_Handler(void) WA;
void IISC_Handler(void) WA;
void SPI_Handler(void) WA;
void TC0_0_Handler(void) WA;
void TC0_1_Handler(void) WA;
void TC0_2_Handler(void) WA;
void TC1_0_Handler(void) WA;
void TC1_1_Handler(void) WA;
void TC1_2_Handler(void) WA;
void TWIM0_Handler(void) WA;
void TWIS0_Handler(void) WA;
void TWIM1_Handler(void) WA;
void TWIS1_Handler(void) WA;
void USART0_Handler(void) WA;
void USART1_Handler(void) WA;
void USART2_Handler(void) WA;
void USART3_Handler(void) WA;
void ADCIFE_Handler(void) WA;
void DACC_Handler(void) WA;
void ACIFC_Handler(void) WA;
void ABDACB_Handler(void) WA;
void TRNG_Handler(void) WA;
void PARC_Handler(void) WA;
void CATB_Handler(void) WA;
void TWIM2_Handler(void) WA;
void TWIM3_Handler(void) WA;
void LCDCA_Handler(void) WA;
//
volatile const func_ptr device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	HFLASHC_Handler,        /*  0 Flash Controller */
    PDCA_0_Handler,         /*  1 PDCA_0 */
    PDCA_1_Handler,         /*  2 PDCA_1 */
    PDCA_2_Handler,         /*  3 PDCA_2 */
    PDCA_3_Handler,         /*  4 PDCA_3 */
    PDCA_4_Handler,         /*  5 PDCA_4 */
    PDCA_5_Handler,         /*  6 PDCA_5 */
    PDCA_6_Handler,         /*  7 PDCA_6 */
    PDCA_7_Handler,         /*  8 PDCA_7 */
    PDCA_8_Handler,         /*  9 PDCA_8 */
    PDCA_9_Handler,         /* 10 PDCA_9 */
    PDCA_10_Handler,        /* 11 PDCA_10 */
    PDCA_11_Handler,        /* 12 PDCA_11 */
    PDCA_12_Handler,        /* 13 PDCA_12 */
    PDCA_13_Handler,        /* 14 PDCA_13 */
    PDCA_14_Handler,        /* 15 PDCA_14 */
    PDCA_15_Handler,        /* 16 PDCA_15 */
    CRCCU_Handler,          /* 17 CRC Calculation Unit */
	USBC_Handler,           /* 18 USB 2.0 Interface */
	PEVC_0_Handler,         /* 19 PEVC_TR */
    PEVC_1_Handler,         /* 20 PEVC_OV */
	AESA_Handler,           /* 21 Advanced Encryption Standard */
	PM_Handler,             /* 22 Power Manager */
    SCIF_Handler,           /* 23 System Control Interface */
    FREQM_Handler,          /* 24 Frequency Meter */
    GPIO_0_Handler,         /* 25 GPIO_0 */
    GPIO_1_Handler,         /* 26 GPIO_1 */
    GPIO_2_Handler,         /* 27 GPIO_2 */
    GPIO_3_Handler,         /* 28 GPIO_3 */
    GPIO_4_Handler,         /* 29 GPIO_4 */
    GPIO_5_Handler,         /* 30 GPIO_5 */
    GPIO_6_Handler,         /* 31 GPIO_6 */
    GPIO_7_Handler,         /* 32 GPIO_7 */
    GPIO_8_Handler,         /* 33 GPIO_8 */
    GPIO_9_Handler,         /* 34 GPIO_9 */
    GPIO_10_Handler,        /* 35 GPIO_10 */
    GPIO_11_Handler,        /* 36 GPIO_11 */
    BPM_Handler,            /* 37 Backup Power Manager */
    BSCIF_Handler,          /* 38 Backup System Control Interface */
    AST_0_Handler,          /* 39 AST_ALARM */
    AST_1_Handler,          /* 40 AST_PER */
    AST_2_Handler,          /* 41 AST_OVF */
    AST_3_Handler,          /* 42 AST_READY */
    AST_4_Handler,          /* 43 AST_CLKREADY */
    WDT_Handler,            /* 44 Watchdog Timer */
    EIC_0_Handler,          /* 45 EIC_1 */
    EIC_1_Handler,          /* 46 EIC_2 */
    EIC_2_Handler,          /* 47 EIC_3 */
    EIC_3_Handler,          /* 48 EIC_4 */
    EIC_4_Handler,          /* 49 EIC_5 */
    EIC_5_Handler,          /* 50 EIC_6 */
    EIC_6_Handler,          /* 51 EIC_7 */
    EIC_7_Handler,          /* 52 EIC_8 */
    IISC_Handler,           /* 53 Inter-IC Sound (I2S) Controller */
    SPI_Handler,            /* 54 Serial Peripheral Interface */
    TC0_0_Handler,          /* 55 TC00 */
    TC0_1_Handler,          /* 56 TC01 */
    TC0_2_Handler,          /* 57 TC02 */
    TC1_0_Handler,          /* 58 TC10 */
    TC1_1_Handler,          /* 59 TC11 */
    TC1_2_Handler,          /* 60 TC12 */
    TWIM0_Handler,          /* 61 Two-wire Master Interface 0 */
    TWIS0_Handler,          /* 62 Two-wire Slave Interface 0 */
    TWIM1_Handler,          /* 63 Two-wire Master Interface 1 */
    TWIS1_Handler,          /* 64 Two-wire Slave Interface 1 */
    USART0_Handler,         /* 65 Universal Synchronous Asynchronous Receiver Transmitter 0 */
    USART1_Handler,         /* 66 Universal Synchronous Asynchronous Receiver Transmitter 1 */
    USART2_Handler,         /* 67 Universal Synchronous Asynchronous Receiver Transmitter 2 */
    USART3_Handler,         /* 68 Universal Synchronous Asynchronous Receiver Transmitter 3 */
    ADCIFE_Handler,         /* 69 ADC controller interface */
    DACC_Handler,           /* 70 DAC Controller */
    ACIFC_Handler,          /* 71 Analog Comparator Interface */
    ABDACB_Handler,         /* 72 Audio Bitstream DAC */
    TRNG_Handler,           /* 73 True Random Number Generator */
    PARC_Handler,           /* 74 Parallel Capture */
    CATB_Handler,           /* 75 Capacitive Touch Module B */
    0,                  	/* 76 Reserved */
    TWIM2_Handler,          /* 77 Two-wire Master Interface 2 */
    TWIM3_Handler,          /* 78 Two-wire Master Interface 3 */
	LCDCA_Handler,           /* 79 LCD Controller */
};
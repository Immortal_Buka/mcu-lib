/**
 * \file
 *
 * \brief Header file for SAM4LC4C
 *
 * Copyright (c) 2014-2018 Microchip Technology Inc. and its subsidiaries.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Subject to your compliance with these terms, you may use Microchip
 * software and any derivatives exclusively with Microchip products.
 * It is your responsibility to comply with third party license terms applicable
 * to your use of third party software (including open source software) that
 * may accompany Microchip software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY,
 * AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE
 * LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL
 * LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE
 * SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE
 * POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT
 * ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY
 * RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
 * THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="https://www.microchip.com/support/">Microchip Support</a>
 */

#ifndef _SAM4LC4C_
#define _SAM4LC4C_

/** \addtogroup SAM4LC4C_definitions SAM4LC4C definitions
  This file defines all structures and symbols for SAM4LC4C:
    - registers and bitfields
    - peripheral base address
    - peripheral ID
    - PIO definitions
*/
/*@{*/

#ifdef __cplusplus
 extern "C" {
#endif

#if !(defined(__ASSEMBLY__) || defined(__IAR_SYSTEMS_ASM__))
#include <stdint.h>
#ifndef __cplusplus
typedef volatile const uint32_t RoReg;   /**< Read only 32-bit register (volatile const unsigned int) */
typedef volatile const uint16_t RoReg16; /**< Read only 16-bit register (volatile const unsigned int) */
typedef volatile const uint8_t  RoReg8;  /**< Read only  8-bit register (volatile const unsigned int) */
#else
typedef volatile       uint32_t RoReg;   /**< Read only 32-bit register (volatile const unsigned int) */
typedef volatile       uint16_t RoReg16; /**< Read only 16-bit register (volatile const unsigned int) */
typedef volatile       uint8_t  RoReg8;  /**< Read only  8-bit register (volatile const unsigned int) */
#endif
typedef volatile       uint32_t WoReg;   /**< Write only 32-bit register (volatile unsigned int) */
typedef volatile       uint16_t WoReg16; /**< Write only 16-bit register (volatile unsigned int) */
typedef volatile       uint8_t  WoReg8;  /**< Write only  8-bit register (volatile unsigned int) */
typedef volatile       uint32_t RwReg;   /**< Read-Write 32-bit register (volatile unsigned int) */
typedef volatile       uint16_t RwReg16; /**< Read-Write 16-bit register (volatile unsigned int) */
typedef volatile       uint8_t  RwReg8;  /**< Read-Write  8-bit register (volatile unsigned int) */
#define CAST(type, value) ((type *)(value))
#define REG_ACCESS(type, address) (*(type*)(address)) /**< C code: Register value */
#else
#define CAST(type, value) (value)
#define REG_ACCESS(type, address) (address) /**< Assembly code: Register address */
#endif

/* ************************************************************************** */
/**  CMSIS DEFINITIONS FOR SAM4LC4C */
/* ************************************************************************** */
/** \addtogroup SAM4LC4C_cmsis CMSIS Definitions */
/*@{*/

/**< Interrupt Number Definition */
typedef enum IRQn
{
  /******  Cortex-M4 Processor Exceptions Numbers *******************************/
  NonMaskableInt_IRQn      = -14, /**<  2 Non Maskable Interrupt                */
  HardFault_IRQn           = -13, /**<  3 Cortex-M4 Hard Fault Interrupt        */
  MemoryManagement_IRQn    = -12, /**<  4 Cortex-M4 Memory Management Interrupt */
  BusFault_IRQn            = -11, /**<  5 Cortex-M4 Bus Fault Interrupt         */
  UsageFault_IRQn          = -10, /**<  6 Cortex-M4 Usage Fault Interrupt       */
  SVCall_IRQn              = -5,  /**< 11 Cortex-M4 SV Call Interrupt           */
  DebugMonitor_IRQn        = -4,  /**< 12 Cortex-M4 Debug Monitor Interrupt     */
  PendSV_IRQn              = -2,  /**< 14 Cortex-M4 Pend SV Interrupt           */
  SysTick_IRQn             = -1,  /**< 15 Cortex-M4 System Tick Interrupt       */
  /******  SAM4LC4C-specific Interrupt Numbers ***********************/
  HFLASHC_IRQn             =  0, /**<  0 SAM4LC4C Flash Controller (HFLASHC) */
  PDCA_0_IRQn              =  1, /**<  1 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_1_IRQn              =  2, /**<  2 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_2_IRQn              =  3, /**<  3 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_3_IRQn              =  4, /**<  4 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_4_IRQn              =  5, /**<  5 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_5_IRQn              =  6, /**<  6 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_6_IRQn              =  7, /**<  7 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_7_IRQn              =  8, /**<  8 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_8_IRQn              =  9, /**<  9 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_9_IRQn              = 10, /**< 10 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_10_IRQn             = 11, /**< 11 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_11_IRQn             = 12, /**< 12 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_12_IRQn             = 13, /**< 13 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_13_IRQn             = 14, /**< 14 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_14_IRQn             = 15, /**< 15 SAM4LC4C Peripheral DMA Controller (PDCA) */
  PDCA_15_IRQn             = 16, /**< 16 SAM4LC4C Peripheral DMA Controller (PDCA) */
  CRCCU_IRQn               = 17, /**< 17 SAM4LC4C CRC Calculation Unit (CRCCU) */
  USBC_IRQn                = 18, /**< 18 SAM4LC4C USB 2.0 Interface (USBC) */
  PEVC_TR_IRQn             = 19, /**< 19 SAM4LC4C Peripheral Event Controller (PEVC) */
  PEVC_OV_IRQn             = 20, /**< 20 SAM4LC4C Peripheral Event Controller (PEVC) */
  AESA_IRQn                = 21, /**< 21 SAM4LC4C Advanced Encryption Standard (AESA) */
  PM_IRQn                  = 22, /**< 22 SAM4LC4C Power Manager (PM) */
  SCIF_IRQn                = 23, /**< 23 SAM4LC4C System Control Interface (SCIF) */
  FREQM_IRQn               = 24, /**< 24 SAM4LC4C Frequency Meter (FREQM) */
  GPIO_0_IRQn              = 25, /**< 25 SAM4LC4C General-Purpose Input/Output Controller (GPIO) */
  GPIO_1_IRQn              = 26, /**< 26 SAM4LC4C General-Purpose Input/Output Controller (GPIO) */
  GPIO_2_IRQn              = 27, /**< 27 SAM4LC4C General-Purpose Input/Output Controller (GPIO) */
  GPIO_3_IRQn              = 28, /**< 28 SAM4LC4C General-Purpose Input/Output Controller (GPIO) */
  GPIO_4_IRQn              = 29, /**< 29 SAM4LC4C General-Purpose Input/Output Controller (GPIO) */
  GPIO_5_IRQn              = 30, /**< 30 SAM4LC4C General-Purpose Input/Output Controller (GPIO) */
  GPIO_6_IRQn              = 31, /**< 31 SAM4LC4C General-Purpose Input/Output Controller (GPIO) */
  GPIO_7_IRQn              = 32, /**< 32 SAM4LC4C General-Purpose Input/Output Controller (GPIO) */
  GPIO_8_IRQn              = 33, /**< 33 SAM4LC4C General-Purpose Input/Output Controller (GPIO) */
  GPIO_9_IRQn              = 34, /**< 34 SAM4LC4C General-Purpose Input/Output Controller (GPIO) */
  GPIO_10_IRQn             = 35, /**< 35 SAM4LC4C General-Purpose Input/Output Controller (GPIO) */
  GPIO_11_IRQn             = 36, /**< 36 SAM4LC4C General-Purpose Input/Output Controller (GPIO) */
  BPM_IRQn                 = 37, /**< 37 SAM4LC4C Backup Power Manager (BPM) */
  BSCIF_IRQn               = 38, /**< 38 SAM4LC4C Backup System Control Interface (BSCIF) */
  AST_ALARM_IRQn           = 39, /**< 39 SAM4LC4C Asynchronous Timer (AST) */
  AST_PER_IRQn             = 40, /**< 40 SAM4LC4C Asynchronous Timer (AST) */
  AST_OVF_IRQn             = 41, /**< 41 SAM4LC4C Asynchronous Timer (AST) */
  AST_READY_IRQn           = 42, /**< 42 SAM4LC4C Asynchronous Timer (AST) */
  AST_CLKREADY_IRQn        = 43, /**< 43 SAM4LC4C Asynchronous Timer (AST) */
  WDT_IRQn                 = 44, /**< 44 SAM4LC4C Watchdog Timer (WDT) */
  EIC_1_IRQn               = 45, /**< 45 SAM4LC4C External Interrupt Controller (EIC) */
  EIC_2_IRQn               = 46, /**< 46 SAM4LC4C External Interrupt Controller (EIC) */
  EIC_3_IRQn               = 47, /**< 47 SAM4LC4C External Interrupt Controller (EIC) */
  EIC_4_IRQn               = 48, /**< 48 SAM4LC4C External Interrupt Controller (EIC) */
  EIC_5_IRQn               = 49, /**< 49 SAM4LC4C External Interrupt Controller (EIC) */
  EIC_6_IRQn               = 50, /**< 50 SAM4LC4C External Interrupt Controller (EIC) */
  EIC_7_IRQn               = 51, /**< 51 SAM4LC4C External Interrupt Controller (EIC) */
  EIC_8_IRQn               = 52, /**< 52 SAM4LC4C External Interrupt Controller (EIC) */
  IISC_IRQn                = 53, /**< 53 SAM4LC4C Inter-IC Sound (I2S) Controller (IISC) */
  SPI_IRQn                 = 54, /**< 54 SAM4LC4C Serial Peripheral Interface (SPI) */
  TC00_IRQn                = 55, /**< 55 SAM4LC4C Timer/Counter 0 (TC0) */
  TC01_IRQn                = 56, /**< 56 SAM4LC4C Timer/Counter 0 (TC0) */
  TC02_IRQn                = 57, /**< 57 SAM4LC4C Timer/Counter 0 (TC0) */
  TC10_IRQn                = 58, /**< 58 SAM4LC4C Timer/Counter 1 (TC1) */
  TC11_IRQn                = 59, /**< 59 SAM4LC4C Timer/Counter 1 (TC1) */
  TC12_IRQn                = 60, /**< 60 SAM4LC4C Timer/Counter 1 (TC1) */
  TWIM0_IRQn               = 61, /**< 61 SAM4LC4C Two-wire Master Interface 0 (TWIM0) */
  TWIS0_IRQn               = 62, /**< 62 SAM4LC4C Two-wire Slave Interface 0 (TWIS0) */
  TWIM1_IRQn               = 63, /**< 63 SAM4LC4C Two-wire Master Interface 1 (TWIM1) */
  TWIS1_IRQn               = 64, /**< 64 SAM4LC4C Two-wire Slave Interface 1 (TWIS1) */
  USART0_IRQn              = 65, /**< 65 SAM4LC4C Universal Synchronous Asynchronous Receiver Transmitter 0 (USART0) */
  USART1_IRQn              = 66, /**< 66 SAM4LC4C Universal Synchronous Asynchronous Receiver Transmitter 1 (USART1) */
  USART2_IRQn              = 67, /**< 67 SAM4LC4C Universal Synchronous Asynchronous Receiver Transmitter 2 (USART2) */
  USART3_IRQn              = 68, /**< 68 SAM4LC4C Universal Synchronous Asynchronous Receiver Transmitter 3 (USART3) */
  ADCIFE_IRQn              = 69, /**< 69 SAM4LC4C ADC controller interface (ADCIFE) */
  DACC_IRQn                = 70, /**< 70 SAM4LC4C DAC Controller (DACC) */
  ACIFC_IRQn               = 71, /**< 71 SAM4LC4C Analog Comparator Interface (ACIFC) */
  ABDACB_IRQn              = 72, /**< 72 SAM4LC4C Audio Bitstream DAC (ABDACB) */
  TRNG_IRQn                = 73, /**< 73 SAM4LC4C True Random Number Generator (TRNG) */
  PARC_IRQn                = 74, /**< 74 SAM4LC4C Parallel Capture (PARC) */
  CATB_IRQn                = 75, /**< 75 SAM4LC4C Capacitive Touch Module B (CATB) */
  TWIM2_IRQn               = 77, /**< 77 SAM4LC4C Two-wire Master Interface 2 (TWIM2) */
  TWIM3_IRQn               = 78, /**< 78 SAM4LC4C Two-wire Master Interface 3 (TWIM3) */
  LCDCA_IRQn               = 79, /**< 79 SAM4LC4C LCD Controller (LCDCA) */

  PERIPH_COUNT_IRQn        = 80  /**< Number of peripheral IDs */
} IRQn_Type;

typedef struct _DeviceVectors
{
  /* Stack pointer */
  void* pvStack;

  /* Cortex-M handlers */
  void* pfnReset_Handler;
  void* pfnNMI_Handler;
  void* pfnHardFault_Handler;
  void* pfnMemManage_Handler;
  void* pfnBusFault_Handler;
  void* pfnUsageFault_Handler;
  void* pfnReservedM9;
  void* pfnReservedM8;
  void* pfnReservedM7;
  void* pfnReservedM6;
  void* pfnSVC_Handler;
  void* pfnDebugMon_Handler;
  void* pfnReservedM3;
  void* pfnPendSV_Handler;
  void* pfnSysTick_Handler;

  /* Peripheral handlers */
  void* pfnHFLASHC_Handler;               /*  0 Flash Controller */
  void* pfnPDCA_0_Handler;                /*  1 Peripheral DMA Controller */
  void* pfnPDCA_1_Handler;                /*  2 Peripheral DMA Controller */
  void* pfnPDCA_2_Handler;                /*  3 Peripheral DMA Controller */
  void* pfnPDCA_3_Handler;                /*  4 Peripheral DMA Controller */
  void* pfnPDCA_4_Handler;                /*  5 Peripheral DMA Controller */
  void* pfnPDCA_5_Handler;                /*  6 Peripheral DMA Controller */
  void* pfnPDCA_6_Handler;                /*  7 Peripheral DMA Controller */
  void* pfnPDCA_7_Handler;                /*  8 Peripheral DMA Controller */
  void* pfnPDCA_8_Handler;                /*  9 Peripheral DMA Controller */
  void* pfnPDCA_9_Handler;                /* 10 Peripheral DMA Controller */
  void* pfnPDCA_10_Handler;               /* 11 Peripheral DMA Controller */
  void* pfnPDCA_11_Handler;               /* 12 Peripheral DMA Controller */
  void* pfnPDCA_12_Handler;               /* 13 Peripheral DMA Controller */
  void* pfnPDCA_13_Handler;               /* 14 Peripheral DMA Controller */
  void* pfnPDCA_14_Handler;               /* 15 Peripheral DMA Controller */
  void* pfnPDCA_15_Handler;               /* 16 Peripheral DMA Controller */
  void* pfnCRCCU_Handler;                 /* 17 CRC Calculation Unit */
  void* pfnUSBC_Handler;                  /* 18 USB 2.0 Interface */
  void* pfnPEVC_TR_Handler;               /* 19 Peripheral Event Controller */
  void* pfnPEVC_OV_Handler;               /* 20 Peripheral Event Controller */
  void* pfnAESA_Handler;                  /* 21 Advanced Encryption Standard */
  void* pfnPM_Handler;                    /* 22 Power Manager */
  void* pfnSCIF_Handler;                  /* 23 System Control Interface */
  void* pfnFREQM_Handler;                 /* 24 Frequency Meter */
  void* pfnGPIO_0_Handler;                /* 25 General-Purpose Input/Output Controller */
  void* pfnGPIO_1_Handler;                /* 26 General-Purpose Input/Output Controller */
  void* pfnGPIO_2_Handler;                /* 27 General-Purpose Input/Output Controller */
  void* pfnGPIO_3_Handler;                /* 28 General-Purpose Input/Output Controller */
  void* pfnGPIO_4_Handler;                /* 29 General-Purpose Input/Output Controller */
  void* pfnGPIO_5_Handler;                /* 30 General-Purpose Input/Output Controller */
  void* pfnGPIO_6_Handler;                /* 31 General-Purpose Input/Output Controller */
  void* pfnGPIO_7_Handler;                /* 32 General-Purpose Input/Output Controller */
  void* pfnGPIO_8_Handler;                /* 33 General-Purpose Input/Output Controller */
  void* pfnGPIO_9_Handler;                /* 34 General-Purpose Input/Output Controller */
  void* pfnGPIO_10_Handler;               /* 35 General-Purpose Input/Output Controller */
  void* pfnGPIO_11_Handler;               /* 36 General-Purpose Input/Output Controller */
  void* pfnBPM_Handler;                   /* 37 Backup Power Manager */
  void* pfnBSCIF_Handler;                 /* 38 Backup System Control Interface */
  void* pfnAST_ALARM_Handler;             /* 39 Asynchronous Timer */
  void* pfnAST_PER_Handler;               /* 40 Asynchronous Timer */
  void* pfnAST_OVF_Handler;               /* 41 Asynchronous Timer */
  void* pfnAST_READY_Handler;             /* 42 Asynchronous Timer */
  void* pfnAST_CLKREADY_Handler;          /* 43 Asynchronous Timer */
  void* pfnWDT_Handler;                   /* 44 Watchdog Timer */
  void* pfnEIC_1_Handler;                 /* 45 External Interrupt Controller */
  void* pfnEIC_2_Handler;                 /* 46 External Interrupt Controller */
  void* pfnEIC_3_Handler;                 /* 47 External Interrupt Controller */
  void* pfnEIC_4_Handler;                 /* 48 External Interrupt Controller */
  void* pfnEIC_5_Handler;                 /* 49 External Interrupt Controller */
  void* pfnEIC_6_Handler;                 /* 50 External Interrupt Controller */
  void* pfnEIC_7_Handler;                 /* 51 External Interrupt Controller */
  void* pfnEIC_8_Handler;                 /* 52 External Interrupt Controller */
  void* pfnIISC_Handler;                  /* 53 Inter-IC Sound (I2S) Controller */
  void* pfnSPI_Handler;                   /* 54 Serial Peripheral Interface */
  void* pfnTC00_Handler;                  /* 55 Timer/Counter 0 */
  void* pfnTC01_Handler;                  /* 56 Timer/Counter 0 */
  void* pfnTC02_Handler;                  /* 57 Timer/Counter 0 */
  void* pfnTC10_Handler;                  /* 58 Timer/Counter 1 */
  void* pfnTC11_Handler;                  /* 59 Timer/Counter 1 */
  void* pfnTC12_Handler;                  /* 60 Timer/Counter 1 */
  void* pfnTWIM0_Handler;                 /* 61 Two-wire Master Interface 0 */
  void* pfnTWIS0_Handler;                 /* 62 Two-wire Slave Interface 0 */
  void* pfnTWIM1_Handler;                 /* 63 Two-wire Master Interface 1 */
  void* pfnTWIS1_Handler;                 /* 64 Two-wire Slave Interface 1 */
  void* pfnUSART0_Handler;                /* 65 Universal Synchronous Asynchronous Receiver Transmitter 0 */
  void* pfnUSART1_Handler;                /* 66 Universal Synchronous Asynchronous Receiver Transmitter 1 */
  void* pfnUSART2_Handler;                /* 67 Universal Synchronous Asynchronous Receiver Transmitter 2 */
  void* pfnUSART3_Handler;                /* 68 Universal Synchronous Asynchronous Receiver Transmitter 3 */
  void* pfnADCIFE_Handler;                /* 69 ADC controller interface */
  void* pfnDACC_Handler;                  /* 70 DAC Controller */
  void* pfnACIFC_Handler;                 /* 71 Analog Comparator Interface */
  void* pfnABDACB_Handler;                /* 72 Audio Bitstream DAC */
  void* pfnTRNG_Handler;                  /* 73 True Random Number Generator */
  void* pfnPARC_Handler;                  /* 74 Parallel Capture */
  void* pfnCATB_Handler;                  /* 75 Capacitive Touch Module B */
  void* pfnReserved76;
  void* pfnTWIM2_Handler;                 /* 77 Two-wire Master Interface 2 */
  void* pfnTWIM3_Handler;                 /* 78 Two-wire Master Interface 3 */
  void* pfnLCDCA_Handler;                 /* 79 LCD Controller */
} DeviceVectors;

/* Cortex-M4 processor handlers */
void Reset_Handler               ( void );
void NMI_Handler                 ( void );
void HardFault_Handler           ( void );
void MemManage_Handler           ( void );
void BusFault_Handler            ( void );
void UsageFault_Handler          ( void );
void SVC_Handler                 ( void );
void DebugMon_Handler            ( void );
void PendSV_Handler              ( void );
void SysTick_Handler             ( void );

/* Peripherals handlers */
void HFLASHC_Handler             ( void );
void PDCA_0_Handler              ( void );
void PDCA_1_Handler              ( void );
void PDCA_2_Handler              ( void );
void PDCA_3_Handler              ( void );
void PDCA_4_Handler              ( void );
void PDCA_5_Handler              ( void );
void PDCA_6_Handler              ( void );
void PDCA_7_Handler              ( void );
void PDCA_8_Handler              ( void );
void PDCA_9_Handler              ( void );
void PDCA_10_Handler             ( void );
void PDCA_11_Handler             ( void );
void PDCA_12_Handler             ( void );
void PDCA_13_Handler             ( void );
void PDCA_14_Handler             ( void );
void PDCA_15_Handler             ( void );
void CRCCU_Handler               ( void );
void USBC_Handler                ( void );
void PEVC_TR_Handler             ( void );
void PEVC_OV_Handler             ( void );
void AESA_Handler                ( void );
void PM_Handler                  ( void );
void SCIF_Handler                ( void );
void FREQM_Handler               ( void );
void GPIO_0_Handler              ( void );
void GPIO_1_Handler              ( void );
void GPIO_2_Handler              ( void );
void GPIO_3_Handler              ( void );
void GPIO_4_Handler              ( void );
void GPIO_5_Handler              ( void );
void GPIO_6_Handler              ( void );
void GPIO_7_Handler              ( void );
void GPIO_8_Handler              ( void );
void GPIO_9_Handler              ( void );
void GPIO_10_Handler             ( void );
void GPIO_11_Handler             ( void );
void BPM_Handler                 ( void );
void BSCIF_Handler               ( void );
void AST_ALARM_Handler           ( void );
void AST_PER_Handler             ( void );
void AST_OVF_Handler             ( void );
void AST_READY_Handler           ( void );
void AST_CLKREADY_Handler        ( void );
void WDT_Handler                 ( void );
void EIC_1_Handler               ( void );
void EIC_2_Handler               ( void );
void EIC_3_Handler               ( void );
void EIC_4_Handler               ( void );
void EIC_5_Handler               ( void );
void EIC_6_Handler               ( void );
void EIC_7_Handler               ( void );
void EIC_8_Handler               ( void );
void IISC_Handler                ( void );
void SPI_Handler                 ( void );
void TC00_Handler                ( void );
void TC01_Handler                ( void );
void TC02_Handler                ( void );
void TC10_Handler                ( void );
void TC11_Handler                ( void );
void TC12_Handler                ( void );
void TWIM0_Handler               ( void );
void TWIS0_Handler               ( void );
void TWIM1_Handler               ( void );
void TWIS1_Handler               ( void );
void USART0_Handler              ( void );
void USART1_Handler              ( void );
void USART2_Handler              ( void );
void USART3_Handler              ( void );
void ADCIFE_Handler              ( void );
void DACC_Handler                ( void );
void ACIFC_Handler               ( void );
void ABDACB_Handler              ( void );
void TRNG_Handler                ( void );
void PARC_Handler                ( void );
void CATB_Handler                ( void );
void TWIM2_Handler               ( void );
void TWIM3_Handler               ( void );
void LCDCA_Handler               ( void );

/*
 * \brief Configuration of the Cortex-M4 Processor and Core Peripherals
 */

#define LITTLE_ENDIAN          1
#define __BB_PRESENT           0         /*!< BIT_BANDING present or not */
#define __CLKGATE_PRESENT      1         /*!< CLKGATE present or not */
#define __CM4_REV              1         /*!< Core revision r0p1 */
#define __DEBUG_LVL            3         /*!< Full debug plus DWT data matching */
#define __FPU_PRESENT          0         /*!< FPU present or not */
#define __JTAG_PRESENT         1         /*!< JTAG present or not */
#define __MPU_PRESENT          1         /*!< MPU present or not */
#define __NVIC_PRIO_BITS       4         /*!< Number of bits used for Priority Levels */
#define __TRACE_LVL            1         /*!< Standard trace: ITM and DWT triggers and counters, but no ETM */
#define __Vendor_SysTickConfig 0         /*!< Set to 1 if different SysTick Config is used */
#define __WIC_PRESENT          0         /*!< WIC present or not */

/**
 * \brief CMSIS includes
 */

#include <core_cm4.h>
#if !defined DONT_USE_CMSIS_INIT
#include "system_sam4l.h"
#endif /* DONT_USE_CMSIS_INIT */

/*@}*/

/* ************************************************************************** */
/**  SOFTWARE PERIPHERAL API DEFINITION FOR SAM4LC4C */
/* ************************************************************************** */
/** \addtogroup SAM4LC4C_api Peripheral Software API */
/*@{*/

#include "component/component_abdacb.h"
#include "component/component_acifc.h"
#include "component/component_adcife.h"
#include "component/component_aesa.h"
#include "component/component_ast.h"
#include "component/component_bpm.h"
#include "component/component_bscif.h"
#include "component/component_catb.h"
#include "component/component_chipid.h"
#include "component/component_crccu.h"
#include "component/component_dacc.h"
#include "component/component_eic.h"
#include "component/component_flashcalw.h"
#include "component/component_freqm.h"
#include "component/component_gloc.h"
#include "component/component_gpio.h"
#include "component/component_hcache.h"
#include "component/component_hmatrixb.h"
#include "component/component_iisc.h"
#include "component/component_lcdca.h"
#include "component/component_parc.h"
#include "component/component_pdca.h"
#include "component/component_pevc.h"
#include "component/component_picouart.h"
#include "component/component_pm.h"
#include "component/component_scif.h"
#include "component/component_smap.h"
#include "component/component_spi.h"
#include "component/component_tc.h"
#include "component/component_trng.h"
#include "component/component_twim.h"
#include "component/component_twis.h"
#include "component/component_usart.h"
#include "component/component_usbc.h"
#include "component/component_wdt.h"
/*@}*/

/* ************************************************************************** */
/**  REGISTERS ACCESS DEFINITIONS FOR SAM4LC4C */
/* ************************************************************************** */
/** \addtogroup SAM4LC4C_reg Registers Access Definitions */
/*@{*/

#include "instance/instance_abdacb.h"
#include "instance/instance_acifc.h"
#include "instance/instance_adcife.h"
#include "instance/instance_aesa.h"
#include "instance/instance_ast.h"
#include "instance/instance_bpm.h"
#include "instance/instance_bscif.h"
#include "instance/instance_catb.h"
#include "instance/instance_chipid.h"
#include "instance/instance_crccu.h"
#include "instance/instance_dacc.h"
#include "instance/instance_eic.h"
#include "instance/instance_hflashc.h"
#include "instance/instance_freqm.h"
#include "instance/instance_gloc.h"
#include "instance/instance_gpio.h"
#include "instance/instance_hcache.h"
#include "instance/instance_hmatrix.h"
#include "instance/instance_iisc.h"
#include "instance/instance_lcdca.h"
#include "instance/instance_parc.h"
#include "instance/instance_pdca.h"
#include "instance/instance_pevc.h"
#include "instance/instance_picouart.h"
#include "instance/instance_pm.h"
#include "instance/instance_scif.h"
#include "instance/instance_smap.h"
#include "instance/instance_spi.h"
#include "instance/instance_tc0.h"
#include "instance/instance_tc1.h"
#include "instance/instance_trng.h"
#include "instance/instance_twim0.h"
#include "instance/instance_twim1.h"
#include "instance/instance_twim2.h"
#include "instance/instance_twim3.h"
#include "instance/instance_twis0.h"
#include "instance/instance_twis1.h"
#include "instance/instance_usart0.h"
#include "instance/instance_usart1.h"
#include "instance/instance_usart2.h"
#include "instance/instance_usart3.h"
#include "instance/instance_usbc.h"
#include "instance/instance_wdt.h"
/*@}*/

/* ************************************************************************** */
/**  PERIPHERAL ID DEFINITIONS FOR SAM4LC4C */
/* ************************************************************************** */
/** \addtogroup SAM4LC4C_id Peripheral Ids Definitions */
/*@{*/

// Peripheral instances on HTOP0 bridge
#define ID_IISC           0 /**< \brief Inter-IC Sound (I2S) Controller (IISC) */
#define ID_SPI            1 /**< \brief Serial Peripheral Interface (SPI) */
#define ID_TC0            2 /**< \brief Timer/Counter TC (TC0) */
#define ID_TC1            3 /**< \brief Timer/Counter TC (TC1) */
#define ID_TWIM0          4 /**< \brief Two-wire Master Interface TWIM (TWIM0) */
#define ID_TWIS0          5 /**< \brief Two-wire Slave Interface TWIS (TWIS0) */
#define ID_TWIM1          6 /**< \brief Two-wire Master Interface TWIM (TWIM1) */
#define ID_TWIS1          7 /**< \brief Two-wire Slave Interface TWIS (TWIS1) */
#define ID_USART0         8 /**< \brief Universal Synchronous Asynchronous Receiver Transmitter USART (USART0) */
#define ID_USART1         9 /**< \brief Universal Synchronous Asynchronous Receiver Transmitter USART (USART1) */
#define ID_USART2        10 /**< \brief Universal Synchronous Asynchronous Receiver Transmitter USART (USART2) */
#define ID_USART3        11 /**< \brief Universal Synchronous Asynchronous Receiver Transmitter USART (USART3) */
#define ID_ADCIFE        12 /**< \brief ADC controller interface (ADCIFE) */
#define ID_DACC          13 /**< \brief DAC Controller (DACC) */
#define ID_ACIFC         14 /**< \brief Analog Comparator Interface (ACIFC) */
#define ID_GLOC          15 /**< \brief Glue Logic Controller (GLOC) */
#define ID_ABDACB        16 /**< \brief Audio Bitstream DAC (ABDACB) */
#define ID_TRNG          17 /**< \brief True Random Number Generator (TRNG) */
#define ID_PARC          18 /**< \brief Parallel Capture (PARC) */
#define ID_CATB          19 /**< \brief Capacitive Touch Module B (CATB) */
#define ID_TWIM2         20 /**< \brief Two-wire Master Interface TWIM (TWIM2) */
#define ID_TWIM3         21 /**< \brief Two-wire Master Interface TWIM (TWIM3) */
#define ID_LCDCA         22 /**< \brief LCD Controller (LCDCA) */

// Peripheral instances on HTOP1 bridge
#define ID_HFLASHC       32 /**< \brief Flash Controller (HFLASHC) */
#define ID_HCACHE        33 /**< \brief Cortex M I&D Cache Controller (HCACHE) */
#define ID_HMATRIX       34 /**< \brief HSB Matrix (HMATRIX) */
#define ID_PDCA          35 /**< \brief Peripheral DMA Controller (PDCA) */
#define ID_SMAP          36 /**< \brief System Manager Access Port (SMAP) */
#define ID_CRCCU         37 /**< \brief CRC Calculation Unit (CRCCU) */
#define ID_USBC          38 /**< \brief USB 2.0 Interface (USBC) */
#define ID_PEVC          39 /**< \brief Peripheral Event Controller (PEVC) */

// Peripheral instances on HTOP2 bridge
#define ID_PM            64 /**< \brief Power Manager (PM) */
#define ID_CHIPID        65 /**< \brief Chip ID Registers (CHIPID) */
#define ID_SCIF          66 /**< \brief System Control Interface (SCIF) */
#define ID_FREQM         67 /**< \brief Frequency Meter (FREQM) */
#define ID_GPIO          68 /**< \brief General-Purpose Input/Output Controller (GPIO) */

// Peripheral instances on HTOP3 bridge
#define ID_BPM           96 /**< \brief Backup Power Manager (BPM) */
#define ID_BSCIF         97 /**< \brief Backup System Control Interface (BSCIF) */
#define ID_AST           98 /**< \brief Asynchronous Timer (AST) */
#define ID_WDT           99 /**< \brief Watchdog Timer (WDT) */
#define ID_EIC          100 /**< \brief External Interrupt Controller (EIC) */
#define ID_PICOUART     101 /**< \brief Pico UART (PICOUART) */

#define ID_PERIPH_COUNT 102 /**< \brief Number of peripheral IDs */
/*@}*/

/* ************************************************************************** */
/**  BASE ADDRESS DEFINITIONS FOR SAM4LC4C */
/* ************************************************************************** */
/** \addtogroup SAM4LC4C_base Peripheral Base Address Definitions */
/*@{*/

#if defined(__ASSEMBLY__) || defined(__IAR_SYSTEMS_ASM__)
#define ABDACB                        (0x40064000U) /**< \brief (ABDACB) APB Base Address */
#define ACIFC                         (0x40040000U) /**< \brief (ACIFC) APB Base Address */
#define ADCIFE                        (0x40038000U) /**< \brief (ADCIFE) APB Base Address */
#define AESA                          (0x400B0000U) /**< \brief (AESA) AHB Base Address */
#define AST                           (0x400F0800U) /**< \brief (AST) APB Base Address */
#define BPM                           (0x400F0000U) /**< \brief (BPM) APB Base Address */
#define BSCIF                         (0x400F0400U) /**< \brief (BSCIF) APB Base Address */
#define CATB                          (0x40070000U) /**< \brief (CATB) APB Base Address */
#define CHIPID                        (0x400E0400U) /**< \brief (CHIPID) APB Base Address */
#define CRCCU                         (0x400A4000U) /**< \brief (CRCCU) APB Base Address */
#define DACC                          (0x4003C000U) /**< \brief (DACC) APB Base Address */
#define EIC                           (0x400F1000U) /**< \brief (EIC) APB Base Address */
#define HFLASHC                       (0x400A0000U) /**< \brief (HFLASHC) APB Base Address */
#define HFLASHC_FROW                  (0x00800200U) /**< \brief (HFLASHC) FROW Base Address */
#define HFLASHC_USER                  (0x00800000U) /**< \brief (HFLASHC) USER Base Address */
#define FREQM                         (0x400E0C00U) /**< \brief (FREQM) APB Base Address */
#define GLOC                          (0x40060000U) /**< \brief (GLOC) APB Base Address */
#define GPIO                          (0x400E1000U) /**< \brief (GPIO) APB Base Address */
#define HCACHE                        (0x400A0400U) /**< \brief (HCACHE) APB Base Address */
#define HMATRIX                       (0x400A1000U) /**< \brief (HMATRIX) APB Base Address */
#define IISC                          (0x40004000U) /**< \brief (IISC) APB Base Address */
#define LCDCA                         (0x40080000U) /**< \brief (LCDCA) APB Base Address */
#define PARC                          (0x4006C000U) /**< \brief (PARC) APB Base Address */
#define PDCA                          (0x400A2000U) /**< \brief (PDCA) APB Base Address */
#define PEVC                          (0x400A6000U) /**< \brief (PEVC) APB Base Address */
#define PICOUART                      (0x400F1400U) /**< \brief (PICOUART) APB Base Address */
#define PM                            (0x400E0000U) /**< \brief (PM) APB Base Address */
#define SCIF                          (0x400E0800U) /**< \brief (SCIF) APB Base Address */
#define SMAP                          (0x400A3000U) /**< \brief (SMAP) APB Base Address */
#define SPI                           (0x40008000U) /**< \brief (SPI) APB Base Address */
#define TC0                           (0x40010000U) /**< \brief (TC0) APB Base Address */
#define TC1                           (0x40014000U) /**< \brief (TC1) APB Base Address */
#define TRNG                          (0x40068000U) /**< \brief (TRNG) APB Base Address */
#define TWIM0                         (0x40018000U) /**< \brief (TWIM0) APB Base Address */
#define TWIM1                         (0x4001C000U) /**< \brief (TWIM1) APB Base Address */
#define TWIM2                         (0x40078000U) /**< \brief (TWIM2) APB Base Address */
#define TWIM3                         (0x4007C000U) /**< \brief (TWIM3) APB Base Address */
#define TWIS0                         (0x40018400U) /**< \brief (TWIS0) APB Base Address */
#define TWIS1                         (0x4001C400U) /**< \brief (TWIS1) APB Base Address */
#define USART0                        (0x40024000U) /**< \brief (USART0) APB Base Address */
#define USART1                        (0x40028000U) /**< \brief (USART1) APB Base Address */
#define USART2                        (0x4002C000U) /**< \brief (USART2) APB Base Address */
#define USART3                        (0x40030000U) /**< \brief (USART3) APB Base Address */
#define USBC                          (0x400A5000U) /**< \brief (USBC) APB Base Address */
#define WDT                           (0x400F0C00U) /**< \brief (WDT) APB Base Address */
#else
#define ABDACB            ((Abdacb   *)0x40064000U) /**< \brief (ABDACB) APB Base Address */
#define ABDACB_ADDR                   (0x40064000U) /**< \brief (ABDACB) APB Base Address */
#define ABDACB_INST_NUM   1                         /**< \brief (ABDACB) Number of instances */
#define ABDACB_INSTS      { ABDACB }                /**< \brief (ABDACB) Instances List */

#define ACIFC             ((Acifc    *)0x40040000U) /**< \brief (ACIFC) APB Base Address */
#define ACIFC_ADDR                    (0x40040000U) /**< \brief (ACIFC) APB Base Address */
#define ACIFC_INST_NUM    1                         /**< \brief (ACIFC) Number of instances */
#define ACIFC_INSTS       { ACIFC }                 /**< \brief (ACIFC) Instances List */

#define ADCIFE            ((Adcife   *)0x40038000U) /**< \brief (ADCIFE) APB Base Address */
#define ADCIFE_ADDR                   (0x40038000U) /**< \brief (ADCIFE) APB Base Address */
#define ADCIFE_INST_NUM   1                         /**< \brief (ADCIFE) Number of instances */
#define ADCIFE_INSTS      { ADCIFE }                /**< \brief (ADCIFE) Instances List */

#define AESA              ((Aesa     *)0x400B0000U) /**< \brief (AESA) AHB Base Address */
#define AESA_ADDR                     (0x400B0000U) /**< \brief (AESA) AHB Base Address */
#define AESA_INST_NUM     1                         /**< \brief (AESA) Number of instances */
#define AESA_INSTS        { AESA }                  /**< \brief (AESA) Instances List */

#define AST               ((Ast      *)0x400F0800U) /**< \brief (AST) APB Base Address */
#define AST_ADDR                      (0x400F0800U) /**< \brief (AST) APB Base Address */
#define AST_INST_NUM      1                         /**< \brief (AST) Number of instances */
#define AST_INSTS         { AST }                   /**< \brief (AST) Instances List */

#define BPM               ((Bpm      *)0x400F0000U) /**< \brief (BPM) APB Base Address */
#define BPM_ADDR                      (0x400F0000U) /**< \brief (BPM) APB Base Address */
#define BPM_INST_NUM      1                         /**< \brief (BPM) Number of instances */
#define BPM_INSTS         { BPM }                   /**< \brief (BPM) Instances List */

#define BSCIF             ((Bscif    *)0x400F0400U) /**< \brief (BSCIF) APB Base Address */
#define BSCIF_ADDR                    (0x400F0400U) /**< \brief (BSCIF) APB Base Address */
#define BSCIF_INST_NUM    1                         /**< \brief (BSCIF) Number of instances */
#define BSCIF_INSTS       { BSCIF }                 /**< \brief (BSCIF) Instances List */

#define CATB              ((Catb     *)0x40070000U) /**< \brief (CATB) APB Base Address */
#define CATB_ADDR                     (0x40070000U) /**< \brief (CATB) APB Base Address */
#define CATB_INST_NUM     1                         /**< \brief (CATB) Number of instances */
#define CATB_INSTS        { CATB }                  /**< \brief (CATB) Instances List */

#define CHIPID            ((Chipid   *)0x400E0400U) /**< \brief (CHIPID) APB Base Address */
#define CHIPID_ADDR                   (0x400E0400U) /**< \brief (CHIPID) APB Base Address */
#define CHIPID_INST_NUM   1                         /**< \brief (CHIPID) Number of instances */
#define CHIPID_INSTS      { CHIPID }                /**< \brief (CHIPID) Instances List */

#define CRCCU             ((Crccu    *)0x400A4000U) /**< \brief (CRCCU) APB Base Address */
#define CRCCU_ADDR                    (0x400A4000U) /**< \brief (CRCCU) APB Base Address */
#define CRCCU_INST_NUM    1                         /**< \brief (CRCCU) Number of instances */
#define CRCCU_INSTS       { CRCCU }                 /**< \brief (CRCCU) Instances List */

#define DACC              ((Dacc     *)0x4003C000U) /**< \brief (DACC) APB Base Address */
#define DACC_ADDR                     (0x4003C000U) /**< \brief (DACC) APB Base Address */
#define DACC_INST_NUM     1                         /**< \brief (DACC) Number of instances */
#define DACC_INSTS        { DACC }                  /**< \brief (DACC) Instances List */

#define EIC               ((Eic      *)0x400F1000U) /**< \brief (EIC) APB Base Address */
#define EIC_ADDR                      (0x400F1000U) /**< \brief (EIC) APB Base Address */
#define EIC_INST_NUM      1                         /**< \brief (EIC) Number of instances */
#define EIC_INSTS         { EIC }                   /**< \brief (EIC) Instances List */

#define HFLASHC           ((Flashcalw *)0x400A0000U) /**< \brief (HFLASHC) APB Base Address */
#define HFLASHC_ADDR                  (0x400A0000U) /**< \brief (HFLASHC) APB Base Address */
#define HFLASHC_FROW      ((Flashcalw *)0x00800200U) /**< \brief (HFLASHC) FROW Base Address */
#define HFLASHC_FROW_ADDR             (0x00800200U) /**< \brief (HFLASHC) FROW Base Address */
#define HFLASHC_USER      ((Flashcalw *)0x00800000U) /**< \brief (HFLASHC) USER Base Address */
#define HFLASHC_USER_ADDR             (0x00800000U) /**< \brief (HFLASHC) USER Base Address */
#define FLASHCALW_INST_NUM 1                         /**< \brief (FLASHCALW) Number of instances */
#define FLASHCALW_INSTS   { HFLASHC }               /**< \brief (FLASHCALW) Instances List */

#define FREQM             ((Freqm    *)0x400E0C00U) /**< \brief (FREQM) APB Base Address */
#define FREQM_ADDR                    (0x400E0C00U) /**< \brief (FREQM) APB Base Address */
#define FREQM_INST_NUM    1                         /**< \brief (FREQM) Number of instances */
#define FREQM_INSTS       { FREQM }                 /**< \brief (FREQM) Instances List */

#define GLOC              ((Gloc     *)0x40060000U) /**< \brief (GLOC) APB Base Address */
#define GLOC_ADDR                     (0x40060000U) /**< \brief (GLOC) APB Base Address */
#define GLOC_INST_NUM     1                         /**< \brief (GLOC) Number of instances */
#define GLOC_INSTS        { GLOC }                  /**< \brief (GLOC) Instances List */

#define GPIO              ((Gpio     *)0x400E1000U) /**< \brief (GPIO) APB Base Address */
#define GPIO_ADDR                     (0x400E1000U) /**< \brief (GPIO) APB Base Address */
#define GPIO_INST_NUM     1                         /**< \brief (GPIO) Number of instances */
#define GPIO_INSTS        { GPIO }                  /**< \brief (GPIO) Instances List */

#define HCACHE            ((Hcache   *)0x400A0400U) /**< \brief (HCACHE) APB Base Address */
#define HCACHE_ADDR                   (0x400A0400U) /**< \brief (HCACHE) APB Base Address */
#define HCACHE_INST_NUM   1                         /**< \brief (HCACHE) Number of instances */
#define HCACHE_INSTS      { HCACHE }                /**< \brief (HCACHE) Instances List */

#define HMATRIX           ((Hmatrixb *)0x400A1000U) /**< \brief (HMATRIX) APB Base Address */
#define HMATRIX_ADDR                  (0x400A1000U) /**< \brief (HMATRIX) APB Base Address */
#define HMATRIXB_INST_NUM 1                         /**< \brief (HMATRIXB) Number of instances */
#define HMATRIXB_INSTS    { HMATRIX }               /**< \brief (HMATRIXB) Instances List */

#define IISC              ((Iisc     *)0x40004000U) /**< \brief (IISC) APB Base Address */
#define IISC_ADDR                     (0x40004000U) /**< \brief (IISC) APB Base Address */
#define IISC_INST_NUM     1                         /**< \brief (IISC) Number of instances */
#define IISC_INSTS        { IISC }                  /**< \brief (IISC) Instances List */

#define LCDCA             ((Lcdca    *)0x40080000U) /**< \brief (LCDCA) APB Base Address */
#define LCDCA_ADDR                    (0x40080000U) /**< \brief (LCDCA) APB Base Address */
#define LCDCA_INST_NUM    1                         /**< \brief (LCDCA) Number of instances */
#define LCDCA_INSTS       { LCDCA }                 /**< \brief (LCDCA) Instances List */

#define PARC              ((Parc     *)0x4006C000U) /**< \brief (PARC) APB Base Address */
#define PARC_ADDR                     (0x4006C000U) /**< \brief (PARC) APB Base Address */
#define PARC_INST_NUM     1                         /**< \brief (PARC) Number of instances */
#define PARC_INSTS        { PARC }                  /**< \brief (PARC) Instances List */

#define PDCA              ((Pdca     *)0x400A2000U) /**< \brief (PDCA) APB Base Address */
#define PDCA_ADDR                     (0x400A2000U) /**< \brief (PDCA) APB Base Address */
#define PDCA_INST_NUM     1                         /**< \brief (PDCA) Number of instances */
#define PDCA_INSTS        { PDCA }                  /**< \brief (PDCA) Instances List */

#define PEVC              ((Pevc     *)0x400A6000U) /**< \brief (PEVC) APB Base Address */
#define PEVC_ADDR                     (0x400A6000U) /**< \brief (PEVC) APB Base Address */
#define PEVC_INST_NUM     1                         /**< \brief (PEVC) Number of instances */
#define PEVC_INSTS        { PEVC }                  /**< \brief (PEVC) Instances List */

#define PICOUART          ((Picouart *)0x400F1400U) /**< \brief (PICOUART) APB Base Address */
#define PICOUART_ADDR                 (0x400F1400U) /**< \brief (PICOUART) APB Base Address */
#define PICOUART_INST_NUM 1                         /**< \brief (PICOUART) Number of instances */
#define PICOUART_INSTS    { PICOUART }              /**< \brief (PICOUART) Instances List */

#define PM                ((Pm       *)0x400E0000U) /**< \brief (PM) APB Base Address */
#define PM_ADDR                       (0x400E0000U) /**< \brief (PM) APB Base Address */
#define PM_INST_NUM       1                         /**< \brief (PM) Number of instances */
#define PM_INSTS          { PM }                    /**< \brief (PM) Instances List */

#define SCIF              ((Scif     *)0x400E0800U) /**< \brief (SCIF) APB Base Address */
#define SCIF_ADDR                     (0x400E0800U) /**< \brief (SCIF) APB Base Address */
#define SCIF_INST_NUM     1                         /**< \brief (SCIF) Number of instances */
#define SCIF_INSTS        { SCIF }                  /**< \brief (SCIF) Instances List */

#define SMAP              ((Smap     *)0x400A3000U) /**< \brief (SMAP) APB Base Address */
#define SMAP_ADDR                     (0x400A3000U) /**< \brief (SMAP) APB Base Address */
#define SMAP_INST_NUM     1                         /**< \brief (SMAP) Number of instances */
#define SMAP_INSTS        { SMAP }                  /**< \brief (SMAP) Instances List */

#define SPI               ((Spi      *)0x40008000U) /**< \brief (SPI) APB Base Address */
#define SPI_ADDR                      (0x40008000U) /**< \brief (SPI) APB Base Address */
#define SPI_INST_NUM      1                         /**< \brief (SPI) Number of instances */
#define SPI_INSTS         { SPI }                   /**< \brief (SPI) Instances List */

#define TC0               ((Tc       *)0x40010000U) /**< \brief (TC0) APB Base Address */
#define TC0_ADDR                      (0x40010000U) /**< \brief (TC0) APB Base Address */
#define TC1               ((Tc       *)0x40014000U) /**< \brief (TC1) APB Base Address */
#define TC1_ADDR                      (0x40014000U) /**< \brief (TC1) APB Base Address */
#define TC_INST_NUM       2                         /**< \brief (TC) Number of instances */
#define TC_INSTS          { TC0, TC1 }              /**< \brief (TC) Instances List */

#define TRNG              ((Trng     *)0x40068000U) /**< \brief (TRNG) APB Base Address */
#define TRNG_ADDR                     (0x40068000U) /**< \brief (TRNG) APB Base Address */
#define TRNG_INST_NUM     1                         /**< \brief (TRNG) Number of instances */
#define TRNG_INSTS        { TRNG }                  /**< \brief (TRNG) Instances List */

#define TWIM0             ((Twim     *)0x40018000U) /**< \brief (TWIM0) APB Base Address */
#define TWIM0_ADDR                    (0x40018000U) /**< \brief (TWIM0) APB Base Address */
#define TWIM1             ((Twim     *)0x4001C000U) /**< \brief (TWIM1) APB Base Address */
#define TWIM1_ADDR                    (0x4001C000U) /**< \brief (TWIM1) APB Base Address */
#define TWIM2             ((Twim     *)0x40078000U) /**< \brief (TWIM2) APB Base Address */
#define TWIM2_ADDR                    (0x40078000U) /**< \brief (TWIM2) APB Base Address */
#define TWIM3             ((Twim     *)0x4007C000U) /**< \brief (TWIM3) APB Base Address */
#define TWIM3_ADDR                    (0x4007C000U) /**< \brief (TWIM3) APB Base Address */
#define TWIM_INST_NUM     4                         /**< \brief (TWIM) Number of instances */
#define TWIM_INSTS        { TWIM0, TWIM1, TWIM2, TWIM3 } /**< \brief (TWIM) Instances List */

#define TWIS0             ((Twis     *)0x40018400U) /**< \brief (TWIS0) APB Base Address */
#define TWIS0_ADDR                    (0x40018400U) /**< \brief (TWIS0) APB Base Address */
#define TWIS1             ((Twis     *)0x4001C400U) /**< \brief (TWIS1) APB Base Address */
#define TWIS1_ADDR                    (0x4001C400U) /**< \brief (TWIS1) APB Base Address */
#define TWIS_INST_NUM     2                         /**< \brief (TWIS) Number of instances */
#define TWIS_INSTS        { TWIS0, TWIS1 }          /**< \brief (TWIS) Instances List */

#define USART0            ((Usart    *)0x40024000U) /**< \brief (USART0) APB Base Address */
#define USART0_ADDR                   (0x40024000U) /**< \brief (USART0) APB Base Address */
#define USART1            ((Usart    *)0x40028000U) /**< \brief (USART1) APB Base Address */
#define USART1_ADDR                   (0x40028000U) /**< \brief (USART1) APB Base Address */
#define USART2            ((Usart    *)0x4002C000U) /**< \brief (USART2) APB Base Address */
#define USART2_ADDR                   (0x4002C000U) /**< \brief (USART2) APB Base Address */
#define USART3            ((Usart    *)0x40030000U) /**< \brief (USART3) APB Base Address */
#define USART3_ADDR                   (0x40030000U) /**< \brief (USART3) APB Base Address */
#define USART_INST_NUM    4                         /**< \brief (USART) Number of instances */
#define USART_INSTS       { USART0, USART1, USART2, USART3 } /**< \brief (USART) Instances List */

#define USBC              ((Usbc     *)0x400A5000U) /**< \brief (USBC) APB Base Address */
#define USBC_ADDR                     (0x400A5000U) /**< \brief (USBC) APB Base Address */
#define USBC_INST_NUM     1                         /**< \brief (USBC) Number of instances */
#define USBC_INSTS        { USBC }                  /**< \brief (USBC) Instances List */

#define WDT               ((Wdt      *)0x400F0C00U) /**< \brief (WDT) APB Base Address */
#define WDT_ADDR                      (0x400F0C00U) /**< \brief (WDT) APB Base Address */
#define WDT_INST_NUM      1                         /**< \brief (WDT) Number of instances */
#define WDT_INSTS         { WDT }                   /**< \brief (WDT) Instances List */

#endif /* (defined(__ASSEMBLY__) || defined(__IAR_SYSTEMS_ASM__)) */
/*@}*/

/* ************************************************************************** */
/**  GPIO DEFINITIONS FOR SAM4LC4C */
/* ************************************************************************** */
/** \addtogroup SAM4LC4C_gpio GPIO Definitions */
/*@{*/

#define PIN_PA00                           0  /**< \brief Pin Number for PA00 */
#define GPIO_PA00                  (1u <<  0) /**< \brief GPIO Mask  for PA00 */
#define PIN_PA01                           1  /**< \brief Pin Number for PA01 */
#define GPIO_PA01                  (1u <<  1) /**< \brief GPIO Mask  for PA01 */
#define PIN_PA02                           2  /**< \brief Pin Number for PA02 */
#define GPIO_PA02                  (1u <<  2) /**< \brief GPIO Mask  for PA02 */
#define PIN_PA03                           3  /**< \brief Pin Number for PA03 */
#define GPIO_PA03                  (1u <<  3) /**< \brief GPIO Mask  for PA03 */
#define PIN_PA04                           4  /**< \brief Pin Number for PA04 */
#define GPIO_PA04                  (1u <<  4) /**< \brief GPIO Mask  for PA04 */
#define PIN_PA05                           5  /**< \brief Pin Number for PA05 */
#define GPIO_PA05                  (1u <<  5) /**< \brief GPIO Mask  for PA05 */
#define PIN_PA06                           6  /**< \brief Pin Number for PA06 */
#define GPIO_PA06                  (1u <<  6) /**< \brief GPIO Mask  for PA06 */
#define PIN_PA07                           7  /**< \brief Pin Number for PA07 */
#define GPIO_PA07                  (1u <<  7) /**< \brief GPIO Mask  for PA07 */
#define PIN_PA08                           8  /**< \brief Pin Number for PA08 */
#define GPIO_PA08                  (1u <<  8) /**< \brief GPIO Mask  for PA08 */
#define PIN_PA09                           9  /**< \brief Pin Number for PA09 */
#define GPIO_PA09                  (1u <<  9) /**< \brief GPIO Mask  for PA09 */
#define PIN_PA10                          10  /**< \brief Pin Number for PA10 */
#define GPIO_PA10                  (1u << 10) /**< \brief GPIO Mask  for PA10 */
#define PIN_PA11                          11  /**< \brief Pin Number for PA11 */
#define GPIO_PA11                  (1u << 11) /**< \brief GPIO Mask  for PA11 */
#define PIN_PA12                          12  /**< \brief Pin Number for PA12 */
#define GPIO_PA12                  (1u << 12) /**< \brief GPIO Mask  for PA12 */
#define PIN_PA13                          13  /**< \brief Pin Number for PA13 */
#define GPIO_PA13                  (1u << 13) /**< \brief GPIO Mask  for PA13 */
#define PIN_PA14                          14  /**< \brief Pin Number for PA14 */
#define GPIO_PA14                  (1u << 14) /**< \brief GPIO Mask  for PA14 */
#define PIN_PA15                          15  /**< \brief Pin Number for PA15 */
#define GPIO_PA15                  (1u << 15) /**< \brief GPIO Mask  for PA15 */
#define PIN_PA16                          16  /**< \brief Pin Number for PA16 */
#define GPIO_PA16                  (1u << 16) /**< \brief GPIO Mask  for PA16 */
#define PIN_PA17                          17  /**< \brief Pin Number for PA17 */
#define GPIO_PA17                  (1u << 17) /**< \brief GPIO Mask  for PA17 */
#define PIN_PA18                          18  /**< \brief Pin Number for PA18 */
#define GPIO_PA18                  (1u << 18) /**< \brief GPIO Mask  for PA18 */
#define PIN_PA19                          19  /**< \brief Pin Number for PA19 */
#define GPIO_PA19                  (1u << 19) /**< \brief GPIO Mask  for PA19 */
#define PIN_PA20                          20  /**< \brief Pin Number for PA20 */
#define GPIO_PA20                  (1u << 20) /**< \brief GPIO Mask  for PA20 */
#define PIN_PA21                          21  /**< \brief Pin Number for PA21 */
#define GPIO_PA21                  (1u << 21) /**< \brief GPIO Mask  for PA21 */
#define PIN_PA22                          22  /**< \brief Pin Number for PA22 */
#define GPIO_PA22                  (1u << 22) /**< \brief GPIO Mask  for PA22 */
#define PIN_PA23                          23  /**< \brief Pin Number for PA23 */
#define GPIO_PA23                  (1u << 23) /**< \brief GPIO Mask  for PA23 */
#define PIN_PA24                          24  /**< \brief Pin Number for PA24 */
#define GPIO_PA24                  (1u << 24) /**< \brief GPIO Mask  for PA24 */
#define PIN_PA25                          25  /**< \brief Pin Number for PA25 */
#define GPIO_PA25                  (1u << 25) /**< \brief GPIO Mask  for PA25 */
#define PIN_PA26                          26  /**< \brief Pin Number for PA26 */
#define GPIO_PA26                  (1u << 26) /**< \brief GPIO Mask  for PA26 */
/* ========== GPIO definition for TWIMS0 peripheral ========== */
#define PIN_PA24B_TWIMS0_TWCK             24  /**< \brief TWIMS0 signal: TWCK on PA24 mux B */
#define MUX_PA24B_TWIMS0_TWCK              1
#define PINMUX_PA24B_TWIMS0_TWCK   ((PIN_PA24B_TWIMS0_TWCK << 16) | MUX_PA24B_TWIMS0_TWCK)
#define GPIO_PA24B_TWIMS0_TWCK     (1u << 24)
#define PIN_PA23B_TWIMS0_TWD              23  /**< \brief TWIMS0 signal: TWD on PA23 mux B */
#define MUX_PA23B_TWIMS0_TWD               1
#define PINMUX_PA23B_TWIMS0_TWD    ((PIN_PA23B_TWIMS0_TWD << 16) | MUX_PA23B_TWIMS0_TWD)
#define GPIO_PA23B_TWIMS0_TWD      (1u << 23)
/* ========== GPIO definition for TWIMS2 peripheral ========== */
#define PIN_PA22E_TWIMS2_TWCK             22  /**< \brief TWIMS2 signal: TWCK on PA22 mux E */
#define MUX_PA22E_TWIMS2_TWCK              4
#define PINMUX_PA22E_TWIMS2_TWCK   ((PIN_PA22E_TWIMS2_TWCK << 16) | MUX_PA22E_TWIMS2_TWCK)
#define GPIO_PA22E_TWIMS2_TWCK     (1u << 22)
#define PIN_PA21E_TWIMS2_TWD              21  /**< \brief TWIMS2 signal: TWD on PA21 mux E */
#define MUX_PA21E_TWIMS2_TWD               4
#define PINMUX_PA21E_TWIMS2_TWD    ((PIN_PA21E_TWIMS2_TWD << 16) | MUX_PA21E_TWIMS2_TWD)
#define GPIO_PA21E_TWIMS2_TWD      (1u << 21)
/* ========== GPIO definition for SPI peripheral ========== */
#define PIN_PA03B_SPI_MISO                 3  /**< \brief SPI signal: MISO on PA03 mux B */
#define MUX_PA03B_SPI_MISO                 1
#define PINMUX_PA03B_SPI_MISO      ((PIN_PA03B_SPI_MISO << 16) | MUX_PA03B_SPI_MISO)
#define GPIO_PA03B_SPI_MISO        (1u <<  3)
#define PIN_PA21A_SPI_MISO                21  /**< \brief SPI signal: MISO on PA21 mux A */
#define MUX_PA21A_SPI_MISO                 0
#define PINMUX_PA21A_SPI_MISO      ((PIN_PA21A_SPI_MISO << 16) | MUX_PA21A_SPI_MISO)
#define GPIO_PA21A_SPI_MISO        (1u << 21)
#define PIN_PA22A_SPI_MOSI                22  /**< \brief SPI signal: MOSI on PA22 mux A */
#define MUX_PA22A_SPI_MOSI                 0
#define PINMUX_PA22A_SPI_MOSI      ((PIN_PA22A_SPI_MOSI << 16) | MUX_PA22A_SPI_MOSI)
#define GPIO_PA22A_SPI_MOSI        (1u << 22)
#define PIN_PA02B_SPI_NPCS0                2  /**< \brief SPI signal: NPCS0 on PA02 mux B */
#define MUX_PA02B_SPI_NPCS0                1
#define PINMUX_PA02B_SPI_NPCS0     ((PIN_PA02B_SPI_NPCS0 << 16) | MUX_PA02B_SPI_NPCS0)
#define GPIO_PA02B_SPI_NPCS0       (1u <<  2)
#define PIN_PA24A_SPI_NPCS0               24  /**< \brief SPI signal: NPCS0 on PA24 mux A */
#define MUX_PA24A_SPI_NPCS0                0
#define PINMUX_PA24A_SPI_NPCS0     ((PIN_PA24A_SPI_NPCS0 << 16) | MUX_PA24A_SPI_NPCS0)
#define GPIO_PA24A_SPI_NPCS0       (1u << 24)
#define PIN_PA13C_SPI_NPCS1               13  /**< \brief SPI signal: NPCS1 on PA13 mux C */
#define MUX_PA13C_SPI_NPCS1                2
#define PINMUX_PA13C_SPI_NPCS1     ((PIN_PA13C_SPI_NPCS1 << 16) | MUX_PA13C_SPI_NPCS1)
#define GPIO_PA13C_SPI_NPCS1       (1u << 13)
#define PIN_PA14C_SPI_NPCS2               14  /**< \brief SPI signal: NPCS2 on PA14 mux C */
#define MUX_PA14C_SPI_NPCS2                2
#define PINMUX_PA14C_SPI_NPCS2     ((PIN_PA14C_SPI_NPCS2 << 16) | MUX_PA14C_SPI_NPCS2)
#define GPIO_PA14C_SPI_NPCS2       (1u << 14)
#define PIN_PA15C_SPI_NPCS3               15  /**< \brief SPI signal: NPCS3 on PA15 mux C */
#define MUX_PA15C_SPI_NPCS3                2
#define PINMUX_PA15C_SPI_NPCS3     ((PIN_PA15C_SPI_NPCS3 << 16) | MUX_PA15C_SPI_NPCS3)
#define GPIO_PA15C_SPI_NPCS3       (1u << 15)
#define PIN_PA23A_SPI_SCK                 23  /**< \brief SPI signal: SCK on PA23 mux A */
#define MUX_PA23A_SPI_SCK                  0
#define PINMUX_PA23A_SPI_SCK       ((PIN_PA23A_SPI_SCK << 16) | MUX_PA23A_SPI_SCK)
#define GPIO_PA23A_SPI_SCK         (1u << 23)
/* ========== GPIO definition for TC0 peripheral ========== */
#define PIN_PA08B_TC0_A0                   8  /**< \brief TC0 signal: A0 on PA08 mux B */
#define MUX_PA08B_TC0_A0                   1
#define PINMUX_PA08B_TC0_A0        ((PIN_PA08B_TC0_A0 << 16) | MUX_PA08B_TC0_A0)
#define GPIO_PA08B_TC0_A0          (1u <<  8)
#define PIN_PA10B_TC0_A1                  10  /**< \brief TC0 signal: A1 on PA10 mux B */
#define MUX_PA10B_TC0_A1                   1
#define PINMUX_PA10B_TC0_A1        ((PIN_PA10B_TC0_A1 << 16) | MUX_PA10B_TC0_A1)
#define GPIO_PA10B_TC0_A1          (1u << 10)
#define PIN_PA12B_TC0_A2                  12  /**< \brief TC0 signal: A2 on PA12 mux B */
#define MUX_PA12B_TC0_A2                   1
#define PINMUX_PA12B_TC0_A2        ((PIN_PA12B_TC0_A2 << 16) | MUX_PA12B_TC0_A2)
#define GPIO_PA12B_TC0_A2          (1u << 12)
#define PIN_PA09B_TC0_B0                   9  /**< \brief TC0 signal: B0 on PA09 mux B */
#define MUX_PA09B_TC0_B0                   1
#define PINMUX_PA09B_TC0_B0        ((PIN_PA09B_TC0_B0 << 16) | MUX_PA09B_TC0_B0)
#define GPIO_PA09B_TC0_B0          (1u <<  9)
#define PIN_PA11B_TC0_B1                  11  /**< \brief TC0 signal: B1 on PA11 mux B */
#define MUX_PA11B_TC0_B1                   1
#define PINMUX_PA11B_TC0_B1        ((PIN_PA11B_TC0_B1 << 16) | MUX_PA11B_TC0_B1)
#define GPIO_PA11B_TC0_B1          (1u << 11)
#define PIN_PA13B_TC0_B2                  13  /**< \brief TC0 signal: B2 on PA13 mux B */
#define MUX_PA13B_TC0_B2                   1
#define PINMUX_PA13B_TC0_B2        ((PIN_PA13B_TC0_B2 << 16) | MUX_PA13B_TC0_B2)
#define GPIO_PA13B_TC0_B2          (1u << 13)
#define PIN_PA14B_TC0_CLK0                14  /**< \brief TC0 signal: CLK0 on PA14 mux B */
#define MUX_PA14B_TC0_CLK0                 1
#define PINMUX_PA14B_TC0_CLK0      ((PIN_PA14B_TC0_CLK0 << 16) | MUX_PA14B_TC0_CLK0)
#define GPIO_PA14B_TC0_CLK0        (1u << 14)
#define PIN_PA15B_TC0_CLK1                15  /**< \brief TC0 signal: CLK1 on PA15 mux B */
#define MUX_PA15B_TC0_CLK1                 1
#define PINMUX_PA15B_TC0_CLK1      ((PIN_PA15B_TC0_CLK1 << 16) | MUX_PA15B_TC0_CLK1)
#define GPIO_PA15B_TC0_CLK1        (1u << 15)
#define PIN_PA16B_TC0_CLK2                16  /**< \brief TC0 signal: CLK2 on PA16 mux B */
#define MUX_PA16B_TC0_CLK2                 1
#define PINMUX_PA16B_TC0_CLK2      ((PIN_PA16B_TC0_CLK2 << 16) | MUX_PA16B_TC0_CLK2)
#define GPIO_PA16B_TC0_CLK2        (1u << 16)
/* ========== GPIO definition for USART0 peripheral ========== */
#define PIN_PA04B_USART0_CLK               4  /**< \brief USART0 signal: CLK on PA04 mux B */
#define MUX_PA04B_USART0_CLK               1
#define PINMUX_PA04B_USART0_CLK    ((PIN_PA04B_USART0_CLK << 16) | MUX_PA04B_USART0_CLK)
#define GPIO_PA04B_USART0_CLK      (1u <<  4)
#define PIN_PA10A_USART0_CLK              10  /**< \brief USART0 signal: CLK on PA10 mux A */
#define MUX_PA10A_USART0_CLK               0
#define PINMUX_PA10A_USART0_CLK    ((PIN_PA10A_USART0_CLK << 16) | MUX_PA10A_USART0_CLK)
#define GPIO_PA10A_USART0_CLK      (1u << 10)
#define PIN_PA09A_USART0_CTS               9  /**< \brief USART0 signal: CTS on PA09 mux A */
#define MUX_PA09A_USART0_CTS               0
#define PINMUX_PA09A_USART0_CTS    ((PIN_PA09A_USART0_CTS << 16) | MUX_PA09A_USART0_CTS)
#define GPIO_PA09A_USART0_CTS      (1u <<  9)
#define PIN_PA06B_USART0_RTS               6  /**< \brief USART0 signal: RTS on PA06 mux B */
#define MUX_PA06B_USART0_RTS               1
#define PINMUX_PA06B_USART0_RTS    ((PIN_PA06B_USART0_RTS << 16) | MUX_PA06B_USART0_RTS)
#define GPIO_PA06B_USART0_RTS      (1u <<  6)
#define PIN_PA08A_USART0_RTS               8  /**< \brief USART0 signal: RTS on PA08 mux A */
#define MUX_PA08A_USART0_RTS               0
#define PINMUX_PA08A_USART0_RTS    ((PIN_PA08A_USART0_RTS << 16) | MUX_PA08A_USART0_RTS)
#define GPIO_PA08A_USART0_RTS      (1u <<  8)
#define PIN_PA05B_USART0_RXD               5  /**< \brief USART0 signal: RXD on PA05 mux B */
#define MUX_PA05B_USART0_RXD               1
#define PINMUX_PA05B_USART0_RXD    ((PIN_PA05B_USART0_RXD << 16) | MUX_PA05B_USART0_RXD)
#define GPIO_PA05B_USART0_RXD      (1u <<  5)
#define PIN_PA11A_USART0_RXD              11  /**< \brief USART0 signal: RXD on PA11 mux A */
#define MUX_PA11A_USART0_RXD               0
#define PINMUX_PA11A_USART0_RXD    ((PIN_PA11A_USART0_RXD << 16) | MUX_PA11A_USART0_RXD)
#define GPIO_PA11A_USART0_RXD      (1u << 11)
#define PIN_PA07B_USART0_TXD               7  /**< \brief USART0 signal: TXD on PA07 mux B */
#define MUX_PA07B_USART0_TXD               1
#define PINMUX_PA07B_USART0_TXD    ((PIN_PA07B_USART0_TXD << 16) | MUX_PA07B_USART0_TXD)
#define GPIO_PA07B_USART0_TXD      (1u <<  7)
#define PIN_PA12A_USART0_TXD              12  /**< \brief USART0 signal: TXD on PA12 mux A */
#define MUX_PA12A_USART0_TXD               0
#define PINMUX_PA12A_USART0_TXD    ((PIN_PA12A_USART0_TXD << 16) | MUX_PA12A_USART0_TXD)
#define GPIO_PA12A_USART0_TXD      (1u << 12)
/* ========== GPIO definition for USART1 peripheral ========== */
#define PIN_PA14A_USART1_CLK              14  /**< \brief USART1 signal: CLK on PA14 mux A */
#define MUX_PA14A_USART1_CLK               0
#define PINMUX_PA14A_USART1_CLK    ((PIN_PA14A_USART1_CLK << 16) | MUX_PA14A_USART1_CLK)
#define GPIO_PA14A_USART1_CLK      (1u << 14)
#define PIN_PA21B_USART1_CTS              21  /**< \brief USART1 signal: CTS on PA21 mux B */
#define MUX_PA21B_USART1_CTS               1
#define PINMUX_PA21B_USART1_CTS    ((PIN_PA21B_USART1_CTS << 16) | MUX_PA21B_USART1_CTS)
#define GPIO_PA21B_USART1_CTS      (1u << 21)
#define PIN_PA13A_USART1_RTS              13  /**< \brief USART1 signal: RTS on PA13 mux A */
#define MUX_PA13A_USART1_RTS               0
#define PINMUX_PA13A_USART1_RTS    ((PIN_PA13A_USART1_RTS << 16) | MUX_PA13A_USART1_RTS)
#define GPIO_PA13A_USART1_RTS      (1u << 13)
#define PIN_PA15A_USART1_RXD              15  /**< \brief USART1 signal: RXD on PA15 mux A */
#define MUX_PA15A_USART1_RXD               0
#define PINMUX_PA15A_USART1_RXD    ((PIN_PA15A_USART1_RXD << 16) | MUX_PA15A_USART1_RXD)
#define GPIO_PA15A_USART1_RXD      (1u << 15)
#define PIN_PA16A_USART1_TXD              16  /**< \brief USART1 signal: TXD on PA16 mux A */
#define MUX_PA16A_USART1_TXD               0
#define PINMUX_PA16A_USART1_TXD    ((PIN_PA16A_USART1_TXD << 16) | MUX_PA16A_USART1_TXD)
#define GPIO_PA16A_USART1_TXD      (1u << 16)
/* ========== GPIO definition for USART2 peripheral ========== */
#define PIN_PA18A_USART2_CLK              18  /**< \brief USART2 signal: CLK on PA18 mux A */
#define MUX_PA18A_USART2_CLK               0
#define PINMUX_PA18A_USART2_CLK    ((PIN_PA18A_USART2_CLK << 16) | MUX_PA18A_USART2_CLK)
#define GPIO_PA18A_USART2_CLK      (1u << 18)
#define PIN_PA22B_USART2_CTS              22  /**< \brief USART2 signal: CTS on PA22 mux B */
#define MUX_PA22B_USART2_CTS               1
#define PINMUX_PA22B_USART2_CTS    ((PIN_PA22B_USART2_CTS << 16) | MUX_PA22B_USART2_CTS)
#define GPIO_PA22B_USART2_CTS      (1u << 22)
#define PIN_PA17A_USART2_RTS              17  /**< \brief USART2 signal: RTS on PA17 mux A */
#define MUX_PA17A_USART2_RTS               0
#define PINMUX_PA17A_USART2_RTS    ((PIN_PA17A_USART2_RTS << 16) | MUX_PA17A_USART2_RTS)
#define GPIO_PA17A_USART2_RTS      (1u << 17)
#define PIN_PA25B_USART2_RXD              25  /**< \brief USART2 signal: RXD on PA25 mux B */
#define MUX_PA25B_USART2_RXD               1
#define PINMUX_PA25B_USART2_RXD    ((PIN_PA25B_USART2_RXD << 16) | MUX_PA25B_USART2_RXD)
#define GPIO_PA25B_USART2_RXD      (1u << 25)
#define PIN_PA19A_USART2_RXD              19  /**< \brief USART2 signal: RXD on PA19 mux A */
#define MUX_PA19A_USART2_RXD               0
#define PINMUX_PA19A_USART2_RXD    ((PIN_PA19A_USART2_RXD << 16) | MUX_PA19A_USART2_RXD)
#define GPIO_PA19A_USART2_RXD      (1u << 19)
#define PIN_PA26B_USART2_TXD              26  /**< \brief USART2 signal: TXD on PA26 mux B */
#define MUX_PA26B_USART2_TXD               1
#define PINMUX_PA26B_USART2_TXD    ((PIN_PA26B_USART2_TXD << 16) | MUX_PA26B_USART2_TXD)
#define GPIO_PA26B_USART2_TXD      (1u << 26)
#define PIN_PA20A_USART2_TXD              20  /**< \brief USART2 signal: TXD on PA20 mux A */
#define MUX_PA20A_USART2_TXD               0
#define PINMUX_PA20A_USART2_TXD    ((PIN_PA20A_USART2_TXD << 16) | MUX_PA20A_USART2_TXD)
#define GPIO_PA20A_USART2_TXD      (1u << 20)
/* ========== GPIO definition for ADCIFE peripheral ========== */
#define PIN_PA04A_ADCIFE_AD0               4  /**< \brief ADCIFE signal: AD0 on PA04 mux A */
#define MUX_PA04A_ADCIFE_AD0               0
#define PINMUX_PA04A_ADCIFE_AD0    ((PIN_PA04A_ADCIFE_AD0 << 16) | MUX_PA04A_ADCIFE_AD0)
#define GPIO_PA04A_ADCIFE_AD0      (1u <<  4)
#define PIN_PA05A_ADCIFE_AD1               5  /**< \brief ADCIFE signal: AD1 on PA05 mux A */
#define MUX_PA05A_ADCIFE_AD1               0
#define PINMUX_PA05A_ADCIFE_AD1    ((PIN_PA05A_ADCIFE_AD1 << 16) | MUX_PA05A_ADCIFE_AD1)
#define GPIO_PA05A_ADCIFE_AD1      (1u <<  5)
#define PIN_PA07A_ADCIFE_AD2               7  /**< \brief ADCIFE signal: AD2 on PA07 mux A */
#define MUX_PA07A_ADCIFE_AD2               0
#define PINMUX_PA07A_ADCIFE_AD2    ((PIN_PA07A_ADCIFE_AD2 << 16) | MUX_PA07A_ADCIFE_AD2)
#define GPIO_PA07A_ADCIFE_AD2      (1u <<  7)
#define PIN_PA05E_ADCIFE_TRIGGER           5  /**< \brief ADCIFE signal: TRIGGER on PA05 mux E */
#define MUX_PA05E_ADCIFE_TRIGGER           4
#define PINMUX_PA05E_ADCIFE_TRIGGER  ((PIN_PA05E_ADCIFE_TRIGGER << 16) | MUX_PA05E_ADCIFE_TRIGGER)
#define GPIO_PA05E_ADCIFE_TRIGGER  (1u <<  5)
/* ========== GPIO definition for DACC peripheral ========== */
#define PIN_PA06A_DACC_VOUT                6  /**< \brief DACC signal: VOUT on PA06 mux A */
#define MUX_PA06A_DACC_VOUT                0
#define PINMUX_PA06A_DACC_VOUT     ((PIN_PA06A_DACC_VOUT << 16) | MUX_PA06A_DACC_VOUT)
#define GPIO_PA06A_DACC_VOUT       (1u <<  6)
/* ========== GPIO definition for ACIFC peripheral ========== */
#define PIN_PA06E_ACIFC_ACAN0              6  /**< \brief ACIFC signal: ACAN0 on PA06 mux E */
#define MUX_PA06E_ACIFC_ACAN0              4
#define PINMUX_PA06E_ACIFC_ACAN0   ((PIN_PA06E_ACIFC_ACAN0 << 16) | MUX_PA06E_ACIFC_ACAN0)
#define GPIO_PA06E_ACIFC_ACAN0     (1u <<  6)
#define PIN_PA07E_ACIFC_ACAP0              7  /**< \brief ACIFC signal: ACAP0 on PA07 mux E */
#define MUX_PA07E_ACIFC_ACAP0              4
#define PINMUX_PA07E_ACIFC_ACAP0   ((PIN_PA07E_ACIFC_ACAP0 << 16) | MUX_PA07E_ACIFC_ACAP0)
#define GPIO_PA07E_ACIFC_ACAP0     (1u <<  7)
/* ========== GPIO definition for GLOC peripheral ========== */
#define PIN_PA06D_GLOC_IN0                 6  /**< \brief GLOC signal: IN0 on PA06 mux D */
#define MUX_PA06D_GLOC_IN0                 3
#define PINMUX_PA06D_GLOC_IN0      ((PIN_PA06D_GLOC_IN0 << 16) | MUX_PA06D_GLOC_IN0)
#define GPIO_PA06D_GLOC_IN0        (1u <<  6)
#define PIN_PA20D_GLOC_IN0                20  /**< \brief GLOC signal: IN0 on PA20 mux D */
#define MUX_PA20D_GLOC_IN0                 3
#define PINMUX_PA20D_GLOC_IN0      ((PIN_PA20D_GLOC_IN0 << 16) | MUX_PA20D_GLOC_IN0)
#define GPIO_PA20D_GLOC_IN0        (1u << 20)
#define PIN_PA04D_GLOC_IN1                 4  /**< \brief GLOC signal: IN1 on PA04 mux D */
#define MUX_PA04D_GLOC_IN1                 3
#define PINMUX_PA04D_GLOC_IN1      ((PIN_PA04D_GLOC_IN1 << 16) | MUX_PA04D_GLOC_IN1)
#define GPIO_PA04D_GLOC_IN1        (1u <<  4)
#define PIN_PA21D_GLOC_IN1                21  /**< \brief GLOC signal: IN1 on PA21 mux D */
#define MUX_PA21D_GLOC_IN1                 3
#define PINMUX_PA21D_GLOC_IN1      ((PIN_PA21D_GLOC_IN1 << 16) | MUX_PA21D_GLOC_IN1)
#define GPIO_PA21D_GLOC_IN1        (1u << 21)
#define PIN_PA05D_GLOC_IN2                 5  /**< \brief GLOC signal: IN2 on PA05 mux D */
#define MUX_PA05D_GLOC_IN2                 3
#define PINMUX_PA05D_GLOC_IN2      ((PIN_PA05D_GLOC_IN2 << 16) | MUX_PA05D_GLOC_IN2)
#define GPIO_PA05D_GLOC_IN2        (1u <<  5)
#define PIN_PA22D_GLOC_IN2                22  /**< \brief GLOC signal: IN2 on PA22 mux D */
#define MUX_PA22D_GLOC_IN2                 3
#define PINMUX_PA22D_GLOC_IN2      ((PIN_PA22D_GLOC_IN2 << 16) | MUX_PA22D_GLOC_IN2)
#define GPIO_PA22D_GLOC_IN2        (1u << 22)
#define PIN_PA07D_GLOC_IN3                 7  /**< \brief GLOC signal: IN3 on PA07 mux D */
#define MUX_PA07D_GLOC_IN3                 3
#define PINMUX_PA07D_GLOC_IN3      ((PIN_PA07D_GLOC_IN3 << 16) | MUX_PA07D_GLOC_IN3)
#define GPIO_PA07D_GLOC_IN3        (1u <<  7)
#define PIN_PA23D_GLOC_IN3                23  /**< \brief GLOC signal: IN3 on PA23 mux D */
#define MUX_PA23D_GLOC_IN3                 3
#define PINMUX_PA23D_GLOC_IN3      ((PIN_PA23D_GLOC_IN3 << 16) | MUX_PA23D_GLOC_IN3)
#define GPIO_PA23D_GLOC_IN3        (1u << 23)
#define PIN_PA08D_GLOC_OUT0                8  /**< \brief GLOC signal: OUT0 on PA08 mux D */
#define MUX_PA08D_GLOC_OUT0                3
#define PINMUX_PA08D_GLOC_OUT0     ((PIN_PA08D_GLOC_OUT0 << 16) | MUX_PA08D_GLOC_OUT0)
#define GPIO_PA08D_GLOC_OUT0       (1u <<  8)
#define PIN_PA24D_GLOC_OUT0               24  /**< \brief GLOC signal: OUT0 on PA24 mux D */
#define MUX_PA24D_GLOC_OUT0                3
#define PINMUX_PA24D_GLOC_OUT0     ((PIN_PA24D_GLOC_OUT0 << 16) | MUX_PA24D_GLOC_OUT0)
#define GPIO_PA24D_GLOC_OUT0       (1u << 24)
/* ========== GPIO definition for ABDACB peripheral ========== */
#define PIN_PA17B_ABDACB_DAC0             17  /**< \brief ABDACB signal: DAC0 on PA17 mux B */
#define MUX_PA17B_ABDACB_DAC0              1
#define PINMUX_PA17B_ABDACB_DAC0   ((PIN_PA17B_ABDACB_DAC0 << 16) | MUX_PA17B_ABDACB_DAC0)
#define GPIO_PA17B_ABDACB_DAC0     (1u << 17)
#define PIN_PA19B_ABDACB_DAC1             19  /**< \brief ABDACB signal: DAC1 on PA19 mux B */
#define MUX_PA19B_ABDACB_DAC1              1
#define PINMUX_PA19B_ABDACB_DAC1   ((PIN_PA19B_ABDACB_DAC1 << 16) | MUX_PA19B_ABDACB_DAC1)
#define GPIO_PA19B_ABDACB_DAC1     (1u << 19)
#define PIN_PA18B_ABDACB_DACN0            18  /**< \brief ABDACB signal: DACN0 on PA18 mux B */
#define MUX_PA18B_ABDACB_DACN0             1
#define PINMUX_PA18B_ABDACB_DACN0  ((PIN_PA18B_ABDACB_DACN0 << 16) | MUX_PA18B_ABDACB_DACN0)
#define GPIO_PA18B_ABDACB_DACN0    (1u << 18)
#define PIN_PA20B_ABDACB_DACN1            20  /**< \brief ABDACB signal: DACN1 on PA20 mux B */
#define MUX_PA20B_ABDACB_DACN1             1
#define PINMUX_PA20B_ABDACB_DACN1  ((PIN_PA20B_ABDACB_DACN1 << 16) | MUX_PA20B_ABDACB_DACN1)
#define GPIO_PA20B_ABDACB_DACN1    (1u << 20)
/* ========== GPIO definition for PARC peripheral ========== */
#define PIN_PA17D_PARC_PCCK               17  /**< \brief PARC signal: PCCK on PA17 mux D */
#define MUX_PA17D_PARC_PCCK                3
#define PINMUX_PA17D_PARC_PCCK     ((PIN_PA17D_PARC_PCCK << 16) | MUX_PA17D_PARC_PCCK)
#define GPIO_PA17D_PARC_PCCK       (1u << 17)
#define PIN_PA09D_PARC_PCDATA0             9  /**< \brief PARC signal: PCDATA0 on PA09 mux D */
#define MUX_PA09D_PARC_PCDATA0             3
#define PINMUX_PA09D_PARC_PCDATA0  ((PIN_PA09D_PARC_PCDATA0 << 16) | MUX_PA09D_PARC_PCDATA0)
#define GPIO_PA09D_PARC_PCDATA0    (1u <<  9)
#define PIN_PA10D_PARC_PCDATA1            10  /**< \brief PARC signal: PCDATA1 on PA10 mux D */
#define MUX_PA10D_PARC_PCDATA1             3
#define PINMUX_PA10D_PARC_PCDATA1  ((PIN_PA10D_PARC_PCDATA1 << 16) | MUX_PA10D_PARC_PCDATA1)
#define GPIO_PA10D_PARC_PCDATA1    (1u << 10)
#define PIN_PA11D_PARC_PCDATA2            11  /**< \brief PARC signal: PCDATA2 on PA11 mux D */
#define MUX_PA11D_PARC_PCDATA2             3
#define PINMUX_PA11D_PARC_PCDATA2  ((PIN_PA11D_PARC_PCDATA2 << 16) | MUX_PA11D_PARC_PCDATA2)
#define GPIO_PA11D_PARC_PCDATA2    (1u << 11)
#define PIN_PA12D_PARC_PCDATA3            12  /**< \brief PARC signal: PCDATA3 on PA12 mux D */
#define MUX_PA12D_PARC_PCDATA3             3
#define PINMUX_PA12D_PARC_PCDATA3  ((PIN_PA12D_PARC_PCDATA3 << 16) | MUX_PA12D_PARC_PCDATA3)
#define GPIO_PA12D_PARC_PCDATA3    (1u << 12)
#define PIN_PA13D_PARC_PCDATA4            13  /**< \brief PARC signal: PCDATA4 on PA13 mux D */
#define MUX_PA13D_PARC_PCDATA4             3
#define PINMUX_PA13D_PARC_PCDATA4  ((PIN_PA13D_PARC_PCDATA4 << 16) | MUX_PA13D_PARC_PCDATA4)
#define GPIO_PA13D_PARC_PCDATA4    (1u << 13)
#define PIN_PA14D_PARC_PCDATA5            14  /**< \brief PARC signal: PCDATA5 on PA14 mux D */
#define MUX_PA14D_PARC_PCDATA5             3
#define PINMUX_PA14D_PARC_PCDATA5  ((PIN_PA14D_PARC_PCDATA5 << 16) | MUX_PA14D_PARC_PCDATA5)
#define GPIO_PA14D_PARC_PCDATA5    (1u << 14)
#define PIN_PA15D_PARC_PCDATA6            15  /**< \brief PARC signal: PCDATA6 on PA15 mux D */
#define MUX_PA15D_PARC_PCDATA6             3
#define PINMUX_PA15D_PARC_PCDATA6  ((PIN_PA15D_PARC_PCDATA6 << 16) | MUX_PA15D_PARC_PCDATA6)
#define GPIO_PA15D_PARC_PCDATA6    (1u << 15)
#define PIN_PA16D_PARC_PCDATA7            16  /**< \brief PARC signal: PCDATA7 on PA16 mux D */
#define MUX_PA16D_PARC_PCDATA7             3
#define PINMUX_PA16D_PARC_PCDATA7  ((PIN_PA16D_PARC_PCDATA7 << 16) | MUX_PA16D_PARC_PCDATA7)
#define GPIO_PA16D_PARC_PCDATA7    (1u << 16)
#define PIN_PA18D_PARC_PCEN1              18  /**< \brief PARC signal: PCEN1 on PA18 mux D */
#define MUX_PA18D_PARC_PCEN1               3
#define PINMUX_PA18D_PARC_PCEN1    ((PIN_PA18D_PARC_PCEN1 << 16) | MUX_PA18D_PARC_PCEN1)
#define GPIO_PA18D_PARC_PCEN1      (1u << 18)
#define PIN_PA19D_PARC_PCEN2              19  /**< \brief PARC signal: PCEN2 on PA19 mux D */
#define MUX_PA19D_PARC_PCEN2               3
#define PINMUX_PA19D_PARC_PCEN2    ((PIN_PA19D_PARC_PCEN2 << 16) | MUX_PA19D_PARC_PCEN2)
#define GPIO_PA19D_PARC_PCEN2      (1u << 19)
/* ========== GPIO definition for CATB peripheral ========== */
#define PIN_PA02G_CATB_DIS                 2  /**< \brief CATB signal: DIS on PA02 mux G */
#define MUX_PA02G_CATB_DIS                 6
#define PINMUX_PA02G_CATB_DIS      ((PIN_PA02G_CATB_DIS << 16) | MUX_PA02G_CATB_DIS)
#define GPIO_PA02G_CATB_DIS        (1u <<  2)
#define PIN_PA12G_CATB_DIS                12  /**< \brief CATB signal: DIS on PA12 mux G */
#define MUX_PA12G_CATB_DIS                 6
#define PINMUX_PA12G_CATB_DIS      ((PIN_PA12G_CATB_DIS << 16) | MUX_PA12G_CATB_DIS)
#define GPIO_PA12G_CATB_DIS        (1u << 12)
#define PIN_PA23G_CATB_DIS                23  /**< \brief CATB signal: DIS on PA23 mux G */
#define MUX_PA23G_CATB_DIS                 6
#define PINMUX_PA23G_CATB_DIS      ((PIN_PA23G_CATB_DIS << 16) | MUX_PA23G_CATB_DIS)
#define GPIO_PA23G_CATB_DIS        (1u << 23)
#define PIN_PA04G_CATB_SENSE0              4  /**< \brief CATB signal: SENSE0 on PA04 mux G */
#define MUX_PA04G_CATB_SENSE0              6
#define PINMUX_PA04G_CATB_SENSE0   ((PIN_PA04G_CATB_SENSE0 << 16) | MUX_PA04G_CATB_SENSE0)
#define GPIO_PA04G_CATB_SENSE0     (1u <<  4)
#define PIN_PA05G_CATB_SENSE1              5  /**< \brief CATB signal: SENSE1 on PA05 mux G */
#define MUX_PA05G_CATB_SENSE1              6
#define PINMUX_PA05G_CATB_SENSE1   ((PIN_PA05G_CATB_SENSE1 << 16) | MUX_PA05G_CATB_SENSE1)
#define GPIO_PA05G_CATB_SENSE1     (1u <<  5)
#define PIN_PA06G_CATB_SENSE2              6  /**< \brief CATB signal: SENSE2 on PA06 mux G */
#define MUX_PA06G_CATB_SENSE2              6
#define PINMUX_PA06G_CATB_SENSE2   ((PIN_PA06G_CATB_SENSE2 << 16) | MUX_PA06G_CATB_SENSE2)
#define GPIO_PA06G_CATB_SENSE2     (1u <<  6)
#define PIN_PA07G_CATB_SENSE3              7  /**< \brief CATB signal: SENSE3 on PA07 mux G */
#define MUX_PA07G_CATB_SENSE3              6
#define PINMUX_PA07G_CATB_SENSE3   ((PIN_PA07G_CATB_SENSE3 << 16) | MUX_PA07G_CATB_SENSE3)
#define GPIO_PA07G_CATB_SENSE3     (1u <<  7)
#define PIN_PA08G_CATB_SENSE4              8  /**< \brief CATB signal: SENSE4 on PA08 mux G */
#define MUX_PA08G_CATB_SENSE4              6
#define PINMUX_PA08G_CATB_SENSE4   ((PIN_PA08G_CATB_SENSE4 << 16) | MUX_PA08G_CATB_SENSE4)
#define GPIO_PA08G_CATB_SENSE4     (1u <<  8)
#define PIN_PA09G_CATB_SENSE5              9  /**< \brief CATB signal: SENSE5 on PA09 mux G */
#define MUX_PA09G_CATB_SENSE5              6
#define PINMUX_PA09G_CATB_SENSE5   ((PIN_PA09G_CATB_SENSE5 << 16) | MUX_PA09G_CATB_SENSE5)
#define GPIO_PA09G_CATB_SENSE5     (1u <<  9)
#define PIN_PA10G_CATB_SENSE6             10  /**< \brief CATB signal: SENSE6 on PA10 mux G */
#define MUX_PA10G_CATB_SENSE6              6
#define PINMUX_PA10G_CATB_SENSE6   ((PIN_PA10G_CATB_SENSE6 << 16) | MUX_PA10G_CATB_SENSE6)
#define GPIO_PA10G_CATB_SENSE6     (1u << 10)
#define PIN_PA11G_CATB_SENSE7             11  /**< \brief CATB signal: SENSE7 on PA11 mux G */
#define MUX_PA11G_CATB_SENSE7              6
#define PINMUX_PA11G_CATB_SENSE7   ((PIN_PA11G_CATB_SENSE7 << 16) | MUX_PA11G_CATB_SENSE7)
#define GPIO_PA11G_CATB_SENSE7     (1u << 11)
#define PIN_PA13G_CATB_SENSE8             13  /**< \brief CATB signal: SENSE8 on PA13 mux G */
#define MUX_PA13G_CATB_SENSE8              6
#define PINMUX_PA13G_CATB_SENSE8   ((PIN_PA13G_CATB_SENSE8 << 16) | MUX_PA13G_CATB_SENSE8)
#define GPIO_PA13G_CATB_SENSE8     (1u << 13)
#define PIN_PA14G_CATB_SENSE9             14  /**< \brief CATB signal: SENSE9 on PA14 mux G */
#define MUX_PA14G_CATB_SENSE9              6
#define PINMUX_PA14G_CATB_SENSE9   ((PIN_PA14G_CATB_SENSE9 << 16) | MUX_PA14G_CATB_SENSE9)
#define GPIO_PA14G_CATB_SENSE9     (1u << 14)
#define PIN_PA15G_CATB_SENSE10            15  /**< \brief CATB signal: SENSE10 on PA15 mux G */
#define MUX_PA15G_CATB_SENSE10             6
#define PINMUX_PA15G_CATB_SENSE10  ((PIN_PA15G_CATB_SENSE10 << 16) | MUX_PA15G_CATB_SENSE10)
#define GPIO_PA15G_CATB_SENSE10    (1u << 15)
#define PIN_PA16G_CATB_SENSE11            16  /**< \brief CATB signal: SENSE11 on PA16 mux G */
#define MUX_PA16G_CATB_SENSE11             6
#define PINMUX_PA16G_CATB_SENSE11  ((PIN_PA16G_CATB_SENSE11 << 16) | MUX_PA16G_CATB_SENSE11)
#define GPIO_PA16G_CATB_SENSE11    (1u << 16)
#define PIN_PA17G_CATB_SENSE12            17  /**< \brief CATB signal: SENSE12 on PA17 mux G */
#define MUX_PA17G_CATB_SENSE12             6
#define PINMUX_PA17G_CATB_SENSE12  ((PIN_PA17G_CATB_SENSE12 << 16) | MUX_PA17G_CATB_SENSE12)
#define GPIO_PA17G_CATB_SENSE12    (1u << 17)
#define PIN_PA18G_CATB_SENSE13            18  /**< \brief CATB signal: SENSE13 on PA18 mux G */
#define MUX_PA18G_CATB_SENSE13             6
#define PINMUX_PA18G_CATB_SENSE13  ((PIN_PA18G_CATB_SENSE13 << 16) | MUX_PA18G_CATB_SENSE13)
#define GPIO_PA18G_CATB_SENSE13    (1u << 18)
#define PIN_PA19G_CATB_SENSE14            19  /**< \brief CATB signal: SENSE14 on PA19 mux G */
#define MUX_PA19G_CATB_SENSE14             6
#define PINMUX_PA19G_CATB_SENSE14  ((PIN_PA19G_CATB_SENSE14 << 16) | MUX_PA19G_CATB_SENSE14)
#define GPIO_PA19G_CATB_SENSE14    (1u << 19)
#define PIN_PA20G_CATB_SENSE15            20  /**< \brief CATB signal: SENSE15 on PA20 mux G */
#define MUX_PA20G_CATB_SENSE15             6
#define PINMUX_PA20G_CATB_SENSE15  ((PIN_PA20G_CATB_SENSE15 << 16) | MUX_PA20G_CATB_SENSE15)
#define GPIO_PA20G_CATB_SENSE15    (1u << 20)
#define PIN_PA21G_CATB_SENSE16            21  /**< \brief CATB signal: SENSE16 on PA21 mux G */
#define MUX_PA21G_CATB_SENSE16             6
#define PINMUX_PA21G_CATB_SENSE16  ((PIN_PA21G_CATB_SENSE16 << 16) | MUX_PA21G_CATB_SENSE16)
#define GPIO_PA21G_CATB_SENSE16    (1u << 21)
#define PIN_PA22G_CATB_SENSE17            22  /**< \brief CATB signal: SENSE17 on PA22 mux G */
#define MUX_PA22G_CATB_SENSE17             6
#define PINMUX_PA22G_CATB_SENSE17  ((PIN_PA22G_CATB_SENSE17 << 16) | MUX_PA22G_CATB_SENSE17)
#define GPIO_PA22G_CATB_SENSE17    (1u << 22)
#define PIN_PA24G_CATB_SENSE18            24  /**< \brief CATB signal: SENSE18 on PA24 mux G */
#define MUX_PA24G_CATB_SENSE18             6
#define PINMUX_PA24G_CATB_SENSE18  ((PIN_PA24G_CATB_SENSE18 << 16) | MUX_PA24G_CATB_SENSE18)
#define GPIO_PA24G_CATB_SENSE18    (1u << 24)
#define PIN_PA25G_CATB_SENSE19            25  /**< \brief CATB signal: SENSE19 on PA25 mux G */
#define MUX_PA25G_CATB_SENSE19             6
#define PINMUX_PA25G_CATB_SENSE19  ((PIN_PA25G_CATB_SENSE19 << 16) | MUX_PA25G_CATB_SENSE19)
#define GPIO_PA25G_CATB_SENSE19    (1u << 25)
#define PIN_PA26G_CATB_SENSE20            26  /**< \brief CATB signal: SENSE20 on PA26 mux G */
#define MUX_PA26G_CATB_SENSE20             6
#define PINMUX_PA26G_CATB_SENSE20  ((PIN_PA26G_CATB_SENSE20 << 16) | MUX_PA26G_CATB_SENSE20)
#define GPIO_PA26G_CATB_SENSE20    (1u << 26)
/* ========== GPIO definition for LCDCA peripheral ========== */
#define PIN_PA12F_LCDCA_COM0              12  /**< \brief LCDCA signal: COM0 on PA12 mux F */
#define MUX_PA12F_LCDCA_COM0               5
#define PINMUX_PA12F_LCDCA_COM0    ((PIN_PA12F_LCDCA_COM0 << 16) | MUX_PA12F_LCDCA_COM0)
#define GPIO_PA12F_LCDCA_COM0      (1u << 12)
#define PIN_PA11F_LCDCA_COM1              11  /**< \brief LCDCA signal: COM1 on PA11 mux F */
#define MUX_PA11F_LCDCA_COM1               5
#define PINMUX_PA11F_LCDCA_COM1    ((PIN_PA11F_LCDCA_COM1 << 16) | MUX_PA11F_LCDCA_COM1)
#define GPIO_PA11F_LCDCA_COM1      (1u << 11)
#define PIN_PA10F_LCDCA_COM2              10  /**< \brief LCDCA signal: COM2 on PA10 mux F */
#define MUX_PA10F_LCDCA_COM2               5
#define PINMUX_PA10F_LCDCA_COM2    ((PIN_PA10F_LCDCA_COM2 << 16) | MUX_PA10F_LCDCA_COM2)
#define GPIO_PA10F_LCDCA_COM2      (1u << 10)
#define PIN_PA09F_LCDCA_COM3               9  /**< \brief LCDCA signal: COM3 on PA09 mux F */
#define MUX_PA09F_LCDCA_COM3               5
#define PINMUX_PA09F_LCDCA_COM3    ((PIN_PA09F_LCDCA_COM3 << 16) | MUX_PA09F_LCDCA_COM3)
#define GPIO_PA09F_LCDCA_COM3      (1u <<  9)
#define PIN_PA13F_LCDCA_SEG5              13  /**< \brief LCDCA signal: SEG5 on PA13 mux F */
#define MUX_PA13F_LCDCA_SEG5               5
#define PINMUX_PA13F_LCDCA_SEG5    ((PIN_PA13F_LCDCA_SEG5 << 16) | MUX_PA13F_LCDCA_SEG5)
#define GPIO_PA13F_LCDCA_SEG5      (1u << 13)
#define PIN_PA14F_LCDCA_SEG6              14  /**< \brief LCDCA signal: SEG6 on PA14 mux F */
#define MUX_PA14F_LCDCA_SEG6               5
#define PINMUX_PA14F_LCDCA_SEG6    ((PIN_PA14F_LCDCA_SEG6 << 16) | MUX_PA14F_LCDCA_SEG6)
#define GPIO_PA14F_LCDCA_SEG6      (1u << 14)
#define PIN_PA15F_LCDCA_SEG7              15  /**< \brief LCDCA signal: SEG7 on PA15 mux F */
#define MUX_PA15F_LCDCA_SEG7               5
#define PINMUX_PA15F_LCDCA_SEG7    ((PIN_PA15F_LCDCA_SEG7 << 16) | MUX_PA15F_LCDCA_SEG7)
#define GPIO_PA15F_LCDCA_SEG7      (1u << 15)
#define PIN_PA16F_LCDCA_SEG8              16  /**< \brief LCDCA signal: SEG8 on PA16 mux F */
#define MUX_PA16F_LCDCA_SEG8               5
#define PINMUX_PA16F_LCDCA_SEG8    ((PIN_PA16F_LCDCA_SEG8 << 16) | MUX_PA16F_LCDCA_SEG8)
#define GPIO_PA16F_LCDCA_SEG8      (1u << 16)
#define PIN_PA17F_LCDCA_SEG9              17  /**< \brief LCDCA signal: SEG9 on PA17 mux F */
#define MUX_PA17F_LCDCA_SEG9               5
#define PINMUX_PA17F_LCDCA_SEG9    ((PIN_PA17F_LCDCA_SEG9 << 16) | MUX_PA17F_LCDCA_SEG9)
#define GPIO_PA17F_LCDCA_SEG9      (1u << 17)
#define PIN_PA18F_LCDCA_SEG18             18  /**< \brief LCDCA signal: SEG18 on PA18 mux F */
#define MUX_PA18F_LCDCA_SEG18              5
#define PINMUX_PA18F_LCDCA_SEG18   ((PIN_PA18F_LCDCA_SEG18 << 16) | MUX_PA18F_LCDCA_SEG18)
#define GPIO_PA18F_LCDCA_SEG18     (1u << 18)
#define PIN_PA19F_LCDCA_SEG19             19  /**< \brief LCDCA signal: SEG19 on PA19 mux F */
#define MUX_PA19F_LCDCA_SEG19              5
#define PINMUX_PA19F_LCDCA_SEG19   ((PIN_PA19F_LCDCA_SEG19 << 16) | MUX_PA19F_LCDCA_SEG19)
#define GPIO_PA19F_LCDCA_SEG19     (1u << 19)
#define PIN_PA20F_LCDCA_SEG20             20  /**< \brief LCDCA signal: SEG20 on PA20 mux F */
#define MUX_PA20F_LCDCA_SEG20              5
#define PINMUX_PA20F_LCDCA_SEG20   ((PIN_PA20F_LCDCA_SEG20 << 16) | MUX_PA20F_LCDCA_SEG20)
#define GPIO_PA20F_LCDCA_SEG20     (1u << 20)
#define PIN_PA08F_LCDCA_SEG23              8  /**< \brief LCDCA signal: SEG23 on PA08 mux F */
#define MUX_PA08F_LCDCA_SEG23              5
#define PINMUX_PA08F_LCDCA_SEG23   ((PIN_PA08F_LCDCA_SEG23 << 16) | MUX_PA08F_LCDCA_SEG23)
#define GPIO_PA08F_LCDCA_SEG23     (1u <<  8)
#define PIN_PA21F_LCDCA_SEG34             21  /**< \brief LCDCA signal: SEG34 on PA21 mux F */
#define MUX_PA21F_LCDCA_SEG34              5
#define PINMUX_PA21F_LCDCA_SEG34   ((PIN_PA21F_LCDCA_SEG34 << 16) | MUX_PA21F_LCDCA_SEG34)
#define GPIO_PA21F_LCDCA_SEG34     (1u << 21)
#define PIN_PA22F_LCDCA_SEG35             22  /**< \brief LCDCA signal: SEG35 on PA22 mux F */
#define MUX_PA22F_LCDCA_SEG35              5
#define PINMUX_PA22F_LCDCA_SEG35   ((PIN_PA22F_LCDCA_SEG35 << 16) | MUX_PA22F_LCDCA_SEG35)
#define GPIO_PA22F_LCDCA_SEG35     (1u << 22)
#define PIN_PA23F_LCDCA_SEG38             23  /**< \brief LCDCA signal: SEG38 on PA23 mux F */
#define MUX_PA23F_LCDCA_SEG38              5
#define PINMUX_PA23F_LCDCA_SEG38   ((PIN_PA23F_LCDCA_SEG38 << 16) | MUX_PA23F_LCDCA_SEG38)
#define GPIO_PA23F_LCDCA_SEG38     (1u << 23)
#define PIN_PA24F_LCDCA_SEG39             24  /**< \brief LCDCA signal: SEG39 on PA24 mux F */
#define MUX_PA24F_LCDCA_SEG39              5
#define PINMUX_PA24F_LCDCA_SEG39   ((PIN_PA24F_LCDCA_SEG39 << 16) | MUX_PA24F_LCDCA_SEG39)
#define GPIO_PA24F_LCDCA_SEG39     (1u << 24)
/* ========== GPIO definition for USBC peripheral ========== */
#define PIN_PA25A_USBC_DM                 25  /**< \brief USBC signal: DM on PA25 mux A */
#define MUX_PA25A_USBC_DM                  0
#define PINMUX_PA25A_USBC_DM       ((PIN_PA25A_USBC_DM << 16) | MUX_PA25A_USBC_DM)
#define GPIO_PA25A_USBC_DM         (1u << 25)
#define PIN_PA26A_USBC_DP                 26  /**< \brief USBC signal: DP on PA26 mux A */
#define MUX_PA26A_USBC_DP                  0
#define PINMUX_PA26A_USBC_DP       ((PIN_PA26A_USBC_DP << 16) | MUX_PA26A_USBC_DP)
#define GPIO_PA26A_USBC_DP         (1u << 26)
/* ========== GPIO definition for PEVC peripheral ========== */
#define PIN_PA08C_PEVC_PAD_EVT0            8  /**< \brief PEVC signal: PAD_EVT0 on PA08 mux C */
#define MUX_PA08C_PEVC_PAD_EVT0            2
#define PINMUX_PA08C_PEVC_PAD_EVT0  ((PIN_PA08C_PEVC_PAD_EVT0 << 16) | MUX_PA08C_PEVC_PAD_EVT0)
#define GPIO_PA08C_PEVC_PAD_EVT0   (1u <<  8)
#define PIN_PA09C_PEVC_PAD_EVT1            9  /**< \brief PEVC signal: PAD_EVT1 on PA09 mux C */
#define MUX_PA09C_PEVC_PAD_EVT1            2
#define PINMUX_PA09C_PEVC_PAD_EVT1  ((PIN_PA09C_PEVC_PAD_EVT1 << 16) | MUX_PA09C_PEVC_PAD_EVT1)
#define GPIO_PA09C_PEVC_PAD_EVT1   (1u <<  9)
#define PIN_PA10C_PEVC_PAD_EVT2           10  /**< \brief PEVC signal: PAD_EVT2 on PA10 mux C */
#define MUX_PA10C_PEVC_PAD_EVT2            2
#define PINMUX_PA10C_PEVC_PAD_EVT2  ((PIN_PA10C_PEVC_PAD_EVT2 << 16) | MUX_PA10C_PEVC_PAD_EVT2)
#define GPIO_PA10C_PEVC_PAD_EVT2   (1u << 10)
#define PIN_PA11C_PEVC_PAD_EVT3           11  /**< \brief PEVC signal: PAD_EVT3 on PA11 mux C */
#define MUX_PA11C_PEVC_PAD_EVT3            2
#define PINMUX_PA11C_PEVC_PAD_EVT3  ((PIN_PA11C_PEVC_PAD_EVT3 << 16) | MUX_PA11C_PEVC_PAD_EVT3)
#define GPIO_PA11C_PEVC_PAD_EVT3   (1u << 11)
/* ========== GPIO definition for SCIF peripheral ========== */
#define PIN_PA19E_SCIF_GCLK0              19  /**< \brief SCIF signal: GCLK0 on PA19 mux E */
#define MUX_PA19E_SCIF_GCLK0               4
#define PINMUX_PA19E_SCIF_GCLK0    ((PIN_PA19E_SCIF_GCLK0 << 16) | MUX_PA19E_SCIF_GCLK0)
#define GPIO_PA19E_SCIF_GCLK0      (1u << 19)
#define PIN_PA02A_SCIF_GCLK0               2  /**< \brief SCIF signal: GCLK0 on PA02 mux A */
#define MUX_PA02A_SCIF_GCLK0               0
#define PINMUX_PA02A_SCIF_GCLK0    ((PIN_PA02A_SCIF_GCLK0 << 16) | MUX_PA02A_SCIF_GCLK0)
#define GPIO_PA02A_SCIF_GCLK0      (1u <<  2)
#define PIN_PA20E_SCIF_GCLK1              20  /**< \brief SCIF signal: GCLK1 on PA20 mux E */
#define MUX_PA20E_SCIF_GCLK1               4
#define PINMUX_PA20E_SCIF_GCLK1    ((PIN_PA20E_SCIF_GCLK1 << 16) | MUX_PA20E_SCIF_GCLK1)
#define GPIO_PA20E_SCIF_GCLK1      (1u << 20)
#define PIN_PA23E_SCIF_GCLK_IN0           23  /**< \brief SCIF signal: GCLK_IN0 on PA23 mux E */
#define MUX_PA23E_SCIF_GCLK_IN0            4
#define PINMUX_PA23E_SCIF_GCLK_IN0  ((PIN_PA23E_SCIF_GCLK_IN0 << 16) | MUX_PA23E_SCIF_GCLK_IN0)
#define GPIO_PA23E_SCIF_GCLK_IN0   (1u << 23)
#define PIN_PA24E_SCIF_GCLK_IN1           24  /**< \brief SCIF signal: GCLK_IN1 on PA24 mux E */
#define MUX_PA24E_SCIF_GCLK_IN1            4
#define PINMUX_PA24E_SCIF_GCLK_IN1  ((PIN_PA24E_SCIF_GCLK_IN1 << 16) | MUX_PA24E_SCIF_GCLK_IN1)
#define GPIO_PA24E_SCIF_GCLK_IN1   (1u << 24)
/* ========== GPIO definition for EIC peripheral ========== */
#define PIN_PA06C_EIC_EXTINT1              6  /**< \brief EIC signal: EXTINT1 on PA06 mux C */
#define MUX_PA06C_EIC_EXTINT1              2
#define PINMUX_PA06C_EIC_EXTINT1   ((PIN_PA06C_EIC_EXTINT1 << 16) | MUX_PA06C_EIC_EXTINT1)
#define GPIO_PA06C_EIC_EXTINT1     (1u <<  6)
#define PIN_PA16C_EIC_EXTINT1             16  /**< \brief EIC signal: EXTINT1 on PA16 mux C */
#define MUX_PA16C_EIC_EXTINT1              2
#define PINMUX_PA16C_EIC_EXTINT1   ((PIN_PA16C_EIC_EXTINT1 << 16) | MUX_PA16C_EIC_EXTINT1)
#define GPIO_PA16C_EIC_EXTINT1     (1u << 16)
#define PIN_PA04C_EIC_EXTINT2              4  /**< \brief EIC signal: EXTINT2 on PA04 mux C */
#define MUX_PA04C_EIC_EXTINT2              2
#define PINMUX_PA04C_EIC_EXTINT2   ((PIN_PA04C_EIC_EXTINT2 << 16) | MUX_PA04C_EIC_EXTINT2)
#define GPIO_PA04C_EIC_EXTINT2     (1u <<  4)
#define PIN_PA17C_EIC_EXTINT2             17  /**< \brief EIC signal: EXTINT2 on PA17 mux C */
#define MUX_PA17C_EIC_EXTINT2              2
#define PINMUX_PA17C_EIC_EXTINT2   ((PIN_PA17C_EIC_EXTINT2 << 16) | MUX_PA17C_EIC_EXTINT2)
#define GPIO_PA17C_EIC_EXTINT2     (1u << 17)
#define PIN_PA05C_EIC_EXTINT3              5  /**< \brief EIC signal: EXTINT3 on PA05 mux C */
#define MUX_PA05C_EIC_EXTINT3              2
#define PINMUX_PA05C_EIC_EXTINT3   ((PIN_PA05C_EIC_EXTINT3 << 16) | MUX_PA05C_EIC_EXTINT3)
#define GPIO_PA05C_EIC_EXTINT3     (1u <<  5)
#define PIN_PA18C_EIC_EXTINT3             18  /**< \brief EIC signal: EXTINT3 on PA18 mux C */
#define MUX_PA18C_EIC_EXTINT3              2
#define PINMUX_PA18C_EIC_EXTINT3   ((PIN_PA18C_EIC_EXTINT3 << 16) | MUX_PA18C_EIC_EXTINT3)
#define GPIO_PA18C_EIC_EXTINT3     (1u << 18)
#define PIN_PA07C_EIC_EXTINT4              7  /**< \brief EIC signal: EXTINT4 on PA07 mux C */
#define MUX_PA07C_EIC_EXTINT4              2
#define PINMUX_PA07C_EIC_EXTINT4   ((PIN_PA07C_EIC_EXTINT4 << 16) | MUX_PA07C_EIC_EXTINT4)
#define GPIO_PA07C_EIC_EXTINT4     (1u <<  7)
#define PIN_PA19C_EIC_EXTINT4             19  /**< \brief EIC signal: EXTINT4 on PA19 mux C */
#define MUX_PA19C_EIC_EXTINT4              2
#define PINMUX_PA19C_EIC_EXTINT4   ((PIN_PA19C_EIC_EXTINT4 << 16) | MUX_PA19C_EIC_EXTINT4)
#define GPIO_PA19C_EIC_EXTINT4     (1u << 19)
#define PIN_PA20C_EIC_EXTINT5             20  /**< \brief EIC signal: EXTINT5 on PA20 mux C */
#define MUX_PA20C_EIC_EXTINT5              2
#define PINMUX_PA20C_EIC_EXTINT5   ((PIN_PA20C_EIC_EXTINT5 << 16) | MUX_PA20C_EIC_EXTINT5)
#define GPIO_PA20C_EIC_EXTINT5     (1u << 20)
#define PIN_PA21C_EIC_EXTINT6             21  /**< \brief EIC signal: EXTINT6 on PA21 mux C */
#define MUX_PA21C_EIC_EXTINT6              2
#define PINMUX_PA21C_EIC_EXTINT6   ((PIN_PA21C_EIC_EXTINT6 << 16) | MUX_PA21C_EIC_EXTINT6)
#define GPIO_PA21C_EIC_EXTINT6     (1u << 21)
#define PIN_PA22C_EIC_EXTINT7             22  /**< \brief EIC signal: EXTINT7 on PA22 mux C */
#define MUX_PA22C_EIC_EXTINT7              2
#define PINMUX_PA22C_EIC_EXTINT7   ((PIN_PA22C_EIC_EXTINT7 << 16) | MUX_PA22C_EIC_EXTINT7)
#define GPIO_PA22C_EIC_EXTINT7     (1u << 22)
#define PIN_PA23C_EIC_EXTINT8             23  /**< \brief EIC signal: EXTINT8 on PA23 mux C */
#define MUX_PA23C_EIC_EXTINT8              2
#define PINMUX_PA23C_EIC_EXTINT8   ((PIN_PA23C_EIC_EXTINT8 << 16) | MUX_PA23C_EIC_EXTINT8)
#define GPIO_PA23C_EIC_EXTINT8     (1u << 23)
/*@}*/

/* ************************************************************************** */
/**  ADDITIONAL DEFINITIONS FOR COMPATIBILITY */
/* ************************************************************************** */
/** \addtogroup SAM4LC4C_compat Definitions */
/*@{*/
// These defines are used to keep compatibility with existing
// sam/drivers/usart implementation from SAM3/4 products with SAM4L product.
#define US_MR_USART_MODE_HW_HANDSHAKING  US_MR_USART_MODE_HARDWARE
#define US_MR_USART_MODE_IS07816_T_0     US_MR_USART_MODE_ISO7816_T0
#define US_MR_USART_MODE_IS07816_T_1     US_MR_USART_MODE_ISO7816_T1
#define US_MR_NBSTOP_2_BIT    US_MR_NBSTOP_2
#define US_MR_NBSTOP_1_5_BIT  US_MR_NBSTOP_1_5
#define US_MR_NBSTOP_1_BIT    US_MR_NBSTOP_1
#define US_MR_CHRL_8_BIT      US_MR_CHRL_8
#define US_MR_PAR_NO          US_MR_PAR_NONE
#define US_MR_PAR_MULTIDROP   US_MR_PAR_MULTI
#define US_IF                 US_IFR
#define US_WPSR_WPVS          US_WPSR_WPV_1

#define USBC_UPCFG0_PBK_Msk   (0x1u << USBC_UPCFG0_PBK_Pos)

// These defines for homogeneity with other SAM header files.
#define CHIP_FREQ_FWS_0       (18000000UL) /**< \brief Maximum operating frequency when FWS is 0 */
#define CHIP_FREQ_FWS_1       (36000000UL) /**< \brief Maximum operating frequency when FWS is 1 */
// WARNING NOTE: these are preliminary values.
#define CHIP_FREQ_FLASH_HSEN_FWS_0 (18000000UL) /**< \brief Maximum operating frequency when FWS is 0 and the FLASH HS mode is enabled */
#define CHIP_FREQ_FLASH_HSEN_FWS_1 (36000000UL) /**< \brief Maximum operating frequency when FWS is 1 and the FLASH HS mode is enabled */

// These defines are used to keep compatibility with existing
// sam/drivers/tc implementation from SAM3/4 products with SAM4L product.
#define TC_CMR_LDRA_RISING    TC_CMR_LDRA_POS_EDGE_TIOA
#define TC_CMR_LDRB_FALLING   TC_CMR_LDRB_NEG_EDGE_TIOA
#define TC_CMR_ETRGEDG_FALLING TC_CMR_ETRGEDG_NEG_EDGE

// These defines are used to keep compatibility with existing
// sam/drivers/spi implementation from SAM3/4 products with SAM4L product.
#define SPI_CSR_BITS_8_BIT    SPI_CSR_BITS_8_BPT
#define SPI_WPCR_SPIWPKEY_VALUE SPI_WPCR_WPKEY_VALUE
#define SPI_WPCR_SPIWPEN      SPI_WPCR_WPEN

// These defines are used to keep compatibility with existing
// sam/drivers/crccu implementation from SAM3/4 products with SAM4L product.
#define CRCCU_DMA_EN          CRCCU_DMAEN
#define CRCCU_DMA_DIS         CRCCU_DMADIS
#define CRCCU_DMA_SR          CRCCU_DMASR
#define CRCCU_DMA_IER         CRCCU_DMAIER
#define CRCCU_DMA_IDR         CRCCU_DMAIDR
#define CRCCU_DMA_IMR         CRCCU_DMAIMR
#define CRCCU_DMA_ISR         CRCCU_DMAISR
#define CRCCU_DMA_EN_DMAEN    CRCCU_DMAEN_DMAEN
#define CRCCU_DMA_DIS_DMADIS  CRCCU_DMADIS_DMADIS
#define CRCCU_DMA_SR_DMASR    CRCCU_DMASR_DMASR
#define CRCCU_DMA_IER_DMAIER  CRCCU_DMAIER_DMAIER
#define CRCCU_DMA_IDR_DMAIDR  CRCCU_DMAIDR_DMAIDR
#define CRCCU_DMA_IMR_DMAIMR  CRCCU_DMAIMR_DMAIMR
#define CRCCU_DMA_ISR_DMAISR  CRCCU_DMAISR_DMAISR
/*@}*/

/* ************************************************************************** */
/**  MEMORY MAPPING DEFINITIONS FOR SAM4LC4C */
/* ************************************************************************** */

#define FLASH_SIZE            0x40000 /* 256 kB */
#define FLASH_PAGE_SIZE       512
#define FLASH_NB_OF_PAGES     512
#define FLASH_USER_PAGE_SIZE  512
#define HRAMC0_SIZE           0x8000 /* 32 kB */
#define HRAMC1_SIZE           0x800 /* 2 kB */
#define FLASH_ADDR            (0x00000000U) /**< FLASH base address */
#define FLASH_USER_PAGE_ADDR  (0x00800000U) /**< FLASH_USER_PAGE base address */
#define HRAMC0_ADDR           (0x20000000U) /**< HRAMC0 base address */
#define HRAMC1_ADDR           (0x21000000U) /**< HRAMC1 base address */

/* ************************************************************************** */
/**  ELECTRICAL DEFINITIONS FOR SAM4LC4C */
/* ************************************************************************** */


#ifdef __cplusplus
}
#endif

/*@}*/

#endif /* SAM4LC4C_H */

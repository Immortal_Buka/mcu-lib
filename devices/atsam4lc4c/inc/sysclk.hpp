#include "std_system.hpp"
#include "sam4lc4c.h"
//
#define PM_CLK_GRP_CPU               	0
#define PM_CLK_GRP_HSB               	1
#define PM_CLK_GRP_PBA               	2
#define PM_CLK_GRP_PBB               	3
#define PM_CLK_GRP_PBC               	4
#define PM_CLK_GRP_PBD               	5
#define SYSCLK_HRAMC1_DATA				2
#define SYSCLK_HRAMC1_REGS				1
#define SYSCLK_PBB_BRIDGE				6
#define SYSCLK_GPIO             		4
#define SYSCLK_LCDCA            		23
#define SYSCLK_PBA_BRIDGE       		5
//
void sysclk_priv_enable_module(uint32_t bus_id, uint32_t module_index);
void sysclk_enable_pbb_module(uint32_t module_index);
void flashcalw_issue_command(uint32_t command, int page_number);
void flashcalw_wait_until_ready(void);
void bpm_ps_no_halt_exec(uint32_t pmcon);
void sysclk_enable_pbc_module(uint32_t module_index);
void sysclk_enable_pba_module(uint32_t module_index);
void osc_priv_enable_osc32(void);

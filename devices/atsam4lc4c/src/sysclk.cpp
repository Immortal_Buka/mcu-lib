#include "sysclk.hpp"
//
void sysclk_priv_enable_module(uint32_t bus_id, uint32_t module_index)
{
	uint32_t mask = *(&PM->PM_CPUMASK + bus_id);
	mask |= 1U << module_index;
	PM->PM_UNLOCK = PM_UNLOCK_KEY(0xAAu)|BPM_UNLOCK_ADDR(((uint32_t)&PM->PM_CPUMASK - (uint32_t)PM) + (4 * bus_id));
	*(&PM->PM_CPUMASK + bus_id) = mask;
}
void sysclk_enable_pbb_module(uint32_t module_index)
{
	if (PM->PM_PBBMASK == 0) sysclk_priv_enable_module(PM_CLK_GRP_HSB, SYSCLK_PBB_BRIDGE);
	sysclk_priv_enable_module(PM_CLK_GRP_PBB, module_index);
}
void flashcalw_issue_command(uint32_t command, int page_number)
{
	uint32_t tempo;
	flashcalw_wait_until_ready();
	tempo = HFLASHC->FLASHCALW_FCMD;
	tempo &= ~FLASHCALW_FCMD_CMD_Msk;
	if(page_number >= 0) tempo = (FLASHCALW_FCMD_KEY_KEY|FLASHCALW_FCMD_PAGEN(page_number)|command);
	else tempo |= (FLASHCALW_FCMD_KEY_KEY | command);
	HFLASHC->FLASHCALW_FCMD = tempo;
	flashcalw_wait_until_ready();
}
void flashcalw_wait_until_ready(void)
{
	while ((HFLASHC->FLASHCALW_FSR & FLASHCALW_FSR_FRDY) == 0){};
}
void bpm_ps_no_halt_exec(uint32_t pmcon)
{
	BPM->BPM_UNLOCK = BPM_UNLOCK_KEY(0xAAu)|BPM_UNLOCK_ADDR((uint32_t)&BPM->BPM_PMCON - (uint32_t)BPM);
	BPM->BPM_PMCON = pmcon;
	while((BPM->BPM_SR & BPM_SR_PSOK) == 0){};
}
void sysclk_enable_pbc_module(uint32_t module_index)
{
	sysclk_priv_enable_module(PM_CLK_GRP_PBC, module_index);
}
void sysclk_enable_pba_module(uint32_t module_index)
{

	if(PM->PM_PBAMASK == 0) sysclk_priv_enable_module(PM_CLK_GRP_HSB, SYSCLK_PBA_BRIDGE);
	sysclk_priv_enable_module(PM_CLK_GRP_PBA, module_index);
}
void osc_priv_enable_osc32(void)
{
	BSCIF->BSCIF_UNLOCK = BSCIF_UNLOCK_KEY(0xAAu)|BSCIF_UNLOCK_ADDR((uint32_t)&BSCIF->BSCIF_OSCCTRL32 - (uint32_t)BSCIF);
	BSCIF->BSCIF_OSCCTRL32 = BSCIF_OSCCTRL32_STARTUP(5)|BSCIF_OSCCTRL32_SELCURR(10)|BSCIF_OSCCTRL32_MODE(1)|
		BSCIF_OSCCTRL32_EN1K|BSCIF_OSCCTRL32_EN32K|BSCIF_OSCCTRL32_OSC32EN;
}

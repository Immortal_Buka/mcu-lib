//ver190211
void SUPC_Handler(void) WA;
void RSTC_Handler(void) WA;
void RTC_Handler(void) WA;
void RTT_Handler(void) WA;
void WDT_Handler(void) WA;
void PMC_Handler(void) WA;
void EFC_Handler(void) WA;
void UART0_Handler(void) WA;
void UART1_Handler(void) WA;
void PIOA_Handler(void) WA;
void PIOB_Handler(void) WA;
void PDMIC0_Handler(void) WA;
void USART_Handler(void) WA;
void MEM2MEM_Handler(void) WA;
void I2SC0_Handler(void) WA;
void I2SC1_Handler(void) WA;
void PDMIC1_Handler(void) WA;
void TWI0_Handler(void) WA;
void TWI1_Handler(void) WA;
void SPI_Handler(void) WA;
void TWI2_Handler(void) WA;
void TC0_Handler(void) WA;
void TC1_Handler(void) WA;
void TC2_Handler(void) WA;
void TC3_Handler(void) WA;
void TC4_Handler(void) WA;
void TC5_Handler(void) WA;
void ADC_Handler(void) WA;
void ARM_Handler(void) WA;
void WKUP0_Handler(void) WA;
void WKUP1_Handler(void) WA;
void WKUP2_Handler(void) WA;
void WKUP3_Handler(void) WA;
void WKUP4_Handler(void) WA;
void WKUP5_Handler(void) WA;
void WKUP6_Handler(void) WA;
void WKUP7_Handler(void) WA;
void WKUP8_Handler(void) WA;
void WKUP9_Handler(void) WA;
void WKUP10_Handler(void) WA;
void WKUP11_Handler(void) WA;
void WKUP12_Handler(void) WA;
void WKUP13_Handler(void) WA;
void WKUP14_Handler(void) WA;
void WKUP15_Handler(void) WA;
//
volatile const func_ptr device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	SUPC_Handler,    /* 0  Supply Controller */
	RSTC_Handler,    /* 1  Reset Controller */
	RTC_Handler,     /* 2  Real Time Clock */
	RTT_Handler,     /* 3  Real Time Timer */
	WDT_Handler,     /* 4  Watchdog Timer */
	PMC_Handler,     /* 5  PMC */
	EFC_Handler,     /* 6  EFC0 */
	Default_Handler, /* 7  Reserved */
	UART0_Handler,   /* 8  UART0 */
	UART1_Handler,   /* 9  UART1 */
	Default_Handler, /* 10 Reserved */
	PIOA_Handler,    /* 11 Parallel IO Controller A */
	PIOB_Handler,    /* 12 Parallel IO Controller B */
	PDMIC0_Handler,  /* 13 PDM 0 */
	USART_Handler,   /* 14 USART */
	MEM2MEM_Handler, /* 15 MEM2MEM */
	I2SC0_Handler,   /* 16 I2SC0 */
	I2SC1_Handler,   /* 17 I2SC1 */
	PDMIC1_Handler,  /* 18 PDM 1 */
	TWI0_Handler,    /* 19 TWI 0 */
	TWI1_Handler,    /* 20 TWI 1 */
	SPI_Handler,     /* 21 SPI */
	TWI2_Handler,    /* 22 TWI2 */
	TC0_Handler,     /* 23 Timer Counter 0 */
	TC1_Handler,     /* 24 Timer Counter 1 */
	TC2_Handler,     /* 25 Timer Counter 2 */
	TC3_Handler,     /* 26 Timer Counter 3 */
	TC4_Handler,     /* 27 Timer Counter 4 */
	TC5_Handler,     /* 28 Timer Counter 5 */
	ADC_Handler,     /* 29 ADC controller */
	ARM_Handler,     /* 30 ARM */
	WKUP0_Handler,   /* 31 WKUP0 */
	WKUP1_Handler,   /* 32 WKUP1 */
	WKUP2_Handler,   /* 33 WKUP2 */
	WKUP3_Handler,   /* 34 WKUP3 */
	WKUP4_Handler,   /* 35 WKUP4 */
	WKUP5_Handler,   /* 36 WKUP5 */
	WKUP6_Handler,   /* 37 WKUP6 */
	WKUP7_Handler,   /* 38 WKUP7 */
	WKUP8_Handler,   /* 39 WKUP8 */
	WKUP9_Handler,   /* 40 WKUP9 */
	WKUP10_Handler,  /* 41 WKUP10 */
	WKUP11_Handler,  /* 42 WKUP11 */
	WKUP12_Handler,  /* 43 WKUP12 */
	WKUP13_Handler,  /* 44 WKUP13 */
	WKUP14_Handler,  /* 45 WKUP14 */
	WKUP15_Handler   /* 46 WKUP15 */
};

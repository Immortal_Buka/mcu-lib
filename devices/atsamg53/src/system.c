//ver191017
#include "system.h"
//
void sys_init(void)
{
	EFC->EEFC_FMR = (3<<8)|EEFC_FMR_CLOE;
	SUPC->SUPC_CR = SUPC_CR_KEY_PASSWD|SUPC_CR_XTALSEL;
	while((PMC->PMC_SR & PMC_SR_OSCSELS) == 0){};
	PMC->CKGR_PLLAR = 1|(1464<<16)|(0x3f<<8);//48005120Hz
	while ((PMC->PMC_SR & PMC_SR_LOCKA) == 0){};
	PMC->PMC_MCKR = PMC_MCKR_CSS_PLLA_CLK;
	while ((PMC->PMC_SR & PMC_SR_MCKRDY) == 0){};
	//disable wdog
	WDT->WDT_MR |= WDT_MR_WDDIS;
}
void project_def_handler(void)
{
}

//ver190404
void SYSTEM_Handler(void) WA;
void WDT_Handler(void) WA;
void RTC_Handler(void) WA;
void EIC_0_Handler(void) WA;
void EIC_1_Handler(void) WA;
void EIC_2_Handler(void) WA;
void EIC_3_Handler(void) WA;
void EIC_OTHER_Handler(void) WA;
void FREQM_Handler(void) WA;
void NVMCTRL_Handler(void) WA;
void PORT_Handler(void) WA;
void DMAC_0_Handler(void) WA;
void DMAC_1_Handler(void) WA;
void DMAC_2_Handler(void) WA;
void DMAC_3_Handler(void) WA;
void DMAC_OTHER_Handler(void) WA;
void EVSYS_0_Handler(void) WA;
void EVSYS_1_Handler(void) WA;
void EVSYS_2_Handler(void) WA;
void EVSYS_3_Handler(void) WA;
void EVSYS_NSCHK_Handler(void) WA;
void PAC_Handler(void) WA;
void SERCOM0_0_Handler(void) WA;
void SERCOM0_1_Handler(void) WA;
void SERCOM0_2_Handler(void) WA;
void SERCOM0_OTHER_Handler(void) WA;
void SERCOM1_0_Handler(void) WA;
void SERCOM1_1_Handler(void) WA;
void SERCOM1_2_Handler(void) WA;
void SERCOM1_OTHER_Handler(void) WA;
void TC0_Handler(void) WA;
void TC1_Handler(void) WA;
void TC2_Handler(void) WA;
void ADC_OTHER_Handler(void) WA;
void ADC_RESRDY_Handler(void) WA;
void AC_Handler(void) WA;
void DAC_UNDERRUN_A_Handler(void) WA;
void DAC_EMPTY_Handler(void) WA;
void PTC_Handler(void) WA;
void TRNG_Handler(void) WA;
void TRAM_Handler(void) WA;
//
volatile const func_ptr device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	SYSTEM_Handler, 		/* 0  Main Clock */
    WDT_Handler,    		/* 1  Watchdog Timer */
    RTC_Handler,    		/* 2  Real-Time Counter */
    EIC_0_Handler,  		/* 3  External Interrupt Controller */
    EIC_1_Handler,  		/* 4  External Interrupt Controller */
    EIC_2_Handler,  		/* 5  External Interrupt Controller */
    EIC_3_Handler,  		/* 6  External Interrupt Controller */
    EIC_OTHER_Handler, 		/* 7  External Interrupt Controller */
    FREQM_Handler,  		/* 8  Frequency Meter */
    NVMCTRL_Handler, 		/* 9  Non-Volatile Memory Controller */
    PORT_Handler,   		/* 10 Port Module */
    DMAC_0_Handler, 		/* 11 Direct Memory Access Controller */
    DMAC_1_Handler, 		/* 12 Direct Memory Access Controller */
    DMAC_2_Handler, 		/* 13 Direct Memory Access Controller */
    DMAC_3_Handler, 		/* 14 Direct Memory Access Controller */
    DMAC_OTHER_Handler, 	/* 15 Direct Memory Access Controller */
    EVSYS_0_Handler, 		/* 16 Event System Interface */
    EVSYS_1_Handler, 		/* 17 Event System Interface */
    EVSYS_2_Handler, 		/* 18 Event System Interface */
    EVSYS_3_Handler, 		/* 19 Event System Interface */
    EVSYS_NSCHK_Handler,	/* 20 Event System Interface */
    PAC_Handler,    		/* 21 Peripheral Access Controller */
    SERCOM0_0_Handler, 		/* 22 Serial Communication Interface */
    SERCOM0_1_Handler, 		/* 23 Serial Communication Interface */
    SERCOM0_2_Handler, 		/* 24 Serial Communication Interface */
    SERCOM0_OTHER_Handler,	/* 25 Serial Communication Interface */
    SERCOM1_0_Handler, 		/* 26 Serial Communication Interface */
    SERCOM1_1_Handler, 		/* 27 Serial Communication Interface */
    SERCOM1_2_Handler, 		/* 28 Serial Communication Interface */
    SERCOM1_OTHER_Handler,	/* 29 Serial Communication Interface */
    0,          			/* 30 Reserved */
    0,          			/* 31 Reserved */
    0,          			/* 32 Reserved */
    0,          			/* 33 Reserved */
    TC0_Handler,    		/* 34 Basic Timer Counter */
    TC1_Handler,    		/* 35 Basic Timer Counter */
    TC2_Handler,    		/* 36 Basic Timer Counter */
    ADC_OTHER_Handler, 		/* 37 Analog Digital Converter */
    ADC_RESRDY_Handler, 	/* 38 Analog Digital Converter */
    AC_Handler,     		/* 39 Analog Comparators */
    DAC_UNDERRUN_A_Handler,	/* 40 Digital Analog Converter */
    DAC_EMPTY_Handler, 		/* 41 Digital Analog Converter */
    PTC_Handler,    		/* 42 Peripheral Touch Controller */
    TRNG_Handler,   		/* 43 True Random Generator */
    TRAM_Handler,    		/* 44 TrustRAM */
};

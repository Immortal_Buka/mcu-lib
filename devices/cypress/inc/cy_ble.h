//ver180823
#ifndef _CY_BLE_H_
#define _CY_BLE_H_
//
#include "uart_log.h"
#include <ble_main.h>
//
extern void uart_print_char(void* uart, uint8_t data);
void ble_init(void);
void ble_event_handler(uint32_t event, void* eventParam);
CYBLE_GATT_ERR_CODE_T update_characteristic_byte(uint8_t* data, uint16_t attrHandle);
CYBLE_API_RESULT_T notify_characteristic_byte(uint8_t* data, uint16_t attrHandle);
void hid_event_handler(uint32_t event, void* eventParam);
void print_auth_info(CYBLE_GAP_AUTH_INFO_T* pointer);
void print_conn_param(CYBLE_GAP_CONN_PARAM_UPDATED_IN_CONTROLLER_T* pointer);
void print_pass(uint32_t data);
void print_hci_error(CYBLE_HCI_ERROR_T data);
void print_function_state(char* name, CYBLE_API_RESULT_T* result);
void print_address(uint8_t* addr);
void key_info_print(CYBLE_GAP_SMP_KEY_DIST_T* ptr);
void print_conn_handle(CYBLE_CONN_HANDLE_T* conn_handle);
void print_hid_char_handle(CYBLE_HIDS_CHAR_VALUE_T* hid);
void dis_event_handler(uint32 event, void* eventParam);
void bas_event_handler(uint32 event, void* eventParam);
void scps_event_handler(uint32 event, void* eventParam);
void print_bas_char(CYBLE_BAS_CHAR_VALUE_T* bas);
void print_scps_char(CYBLE_SCPS_CHAR_VALUE_T* scps);
//
#endif//_CY_BLE_H_

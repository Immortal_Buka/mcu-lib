//ver180823
#include <project.h>
#include "cy_ble.h"
//
CYBLE_CONN_HANDLE_T connectionHandle;
CYBLE_API_RESULT_T result;
uint8_t string[7] = {0};
//
void ble_init(void)
{
	CYBLE_STACK_LIB_VERSION_T ble_ver;
	result = CyBle_Start(ble_event_handler);
	print_function_state("CyBle_Start", &result);
	CyBle_HidsRegisterAttrCallback(hid_event_handler);
	CyBle_DisRegisterAttrCallback(dis_event_handler);
	CyBle_BasRegisterAttrCallback(bas_event_handler);
	CyBle_ScpsRegisterAttrCallback(scps_event_handler);
	uart_log_add(t_info, "BLE stack version", "", 0);
	if(CyBle_GetStackLibraryVersion(&ble_ver) == CYBLE_ERROR_OK)
	{
		print_hex_uint8(0, ble_ver.majorVersion);
		uart_print_char(0, 0x2e);
		print_hex_uint8(0, ble_ver.minorVersion);
	    uart_print_char(0, 0x2e);
	    print_hex_uint8(0, ble_ver.patch);
	}
	else uart_print_string(0, "\e[31mERROR\e[97m");
}
void ble_event_handler(uint32 event, void* eventParam)
{
	uart_log_add(t_evt, "BLE_EVT", "", 0);
	switch(event)
	{
/* Range for Generic events - 0x01 to 0x1F */
		case CYBLE_EVT_STACK_ON:
			uart_print_string(0, "bluetooth stack ON");
			//result = CyBle_GapFixAuthPassKey(1, passkey);
			//print_function_state("CyBle_GapFixAuthPassKey", &result);
			result = CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
			print_function_state("CyBle_GappStartAdvertisement", &result);
			break;
        case CYBLE_EVT_TIMEOUT:
        	uart_print_string(0, "is a timeout and application needs to handle the event");
        	uart_log_add(t_info, "Timeout reason", "", 0);
            switch(*(CYBLE_TO_REASON_CODE_T*)eventParam)
            {
                case CYBLE_GAP_ADV_MODE_TO: uart_print_string(0, "Advertisement time set by application has expired"); break;
                case CYBLE_GAP_SCAN_TO: uart_print_string(0, "Scan time set by application has expired"); break;
                case CYBLE_GATT_RSP_TO: uart_print_string(0, "GATT procedure timeout"); break;
                case CYBLE_GENERIC_TO: uart_print_string(0, "Generic timeout"); break;
                default: uart_print_string(0, "unknown"); break;
            }
            break;
        case CYBLE_EVT_HCI_STATUS:
        	uart_print_string(0, "hci \e[31mERROR\e[39m");
        	print_hci_error(*((CYBLE_HCI_ERROR_T*)eventParam));
            break;
/* Range for GAP events - 0x20 to 0x3F */
        case CYBLE_EVT_GAP_AUTH_REQ:
        	uart_print_string(0, "gap authentication request");
            //result = CyBle_GappAuthReqReply(connectionHandle.bdHandle,&cyBle_authInfo);
            //print_function_state("CyBle_GappAuthReqReply", &result);
            print_auth_info((CYBLE_GAP_AUTH_INFO_T*)eventParam);
            break;
        case CYBLE_EVT_GAP_PASSKEY_DISPLAY_REQUEST:
        	uart_print_string(0, "gap passkey display request");
        	print_pass(*((uint32_t*)eventParam));//pass
            break;
        case CYBLE_EVT_GAP_AUTH_COMPLETE:
        	uart_print_string(0, "gap authentication OK");
            print_auth_info((CYBLE_GAP_AUTH_INFO_T*)eventParam);
            break;
        case CYBLE_EVT_GAP_AUTH_FAILED:
        	uart_print_string(0, "gap authentication \e[31mFAIL\e[39m");
        	uart_log_add(t_info, "reason", "", 0);
            switch(*(CYBLE_GAP_AUTH_FAILED_REASON_T*)eventParam)
            {
            	case CYBLE_GAP_AUTH_ERROR_PASSKEY_ENTRY_FAILED:
            		uart_print_string(0, "user input of passkey failed");
                    break;
                case CYBLE_GAP_AUTH_ERROR_REPEATED_ATTEMPTS:
                	uart_print_string(0, "too little time has elapsed since last request");
                    break;
                case CYBLE_GAP_AUTH_ERROR_CONFIRM_VALUE_NOT_MATCH:
                	uart_print_string(0, "confirm value does not match the calculated compare value");
                    break;
                case CYBLE_GAP_AUTH_ERROR_LINK_DISCONNECTED:
                	uart_print_string(0, "link disconnected");
                    break;
				case CYBLE_GAP_AUTH_ERROR_AUTHENTICATION_TIMEOUT:
					uart_print_string(0, "authentication process timeout");
					break;
				case CYBLE_GAP_AUTH_ERROR_UNSPECIFIED_REASON:
					uart_print_string(0, "pairing failed due to an unspecified reason");
					break;
				case CYBLE_GAP_AUTH_ERROR_INSUFFICIENT_ENCRYPTION_KEY_SIZE:
					uart_print_string(0, "insufficient key size for the security requirements of this device or LTK is lost");
					break;
				case CYBLE_GAP_AUTH_ERROR_AUTHENTICATION_REQ_NOT_MET:
					uart_print_string(0, "pairing procedure cannot be performed as authentication requirements cannot be met due to IO capabilities of one or both devices");
					break;
				default:
					uint8_to_hex_string(*(uint8_t*)eventParam, string);
					uart_print_string(0, string);
					break;
            }
            result = CyBle_GapDisconnect(connectionHandle.bdHandle);
            print_function_state("CyBle_GapDisconnect", &result);
            break;
        case CYBLE_EVT_GAPP_ADVERTISEMENT_START_STOP:
        	if(CyBle_GetState() == CYBLE_STATE_ADVERTISING) uart_print_string(0, "advertising start");
            else uart_print_string(0, "advertising stop");
            break;
        case CYBLE_EVT_GAP_DEVICE_CONNECTED:
        	uart_print_string(0, "gap device connected");
            print_conn_param((CYBLE_GAP_CONN_PARAM_UPDATED_IN_CONTROLLER_T*)eventParam);
            /*result = CyBle_GapAuthReq(connectionHandle.bdHandle, &cyBle_authInfo);
            print_function_state("CyBle_GapAuthReq", &result);*/
            break;
        case CYBLE_EVT_GAP_DEVICE_DISCONNECTED:
        	uart_print_string(0, "gap device disconnected");
            print_hci_error(*((CYBLE_HCI_ERROR_T*)eventParam));
            connectionHandle.bdHandle=0;
            connectionHandle.attId=0;
            result = CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
            print_function_state("CyBle_GappStartAdvertisement", &result);
            break;
        case CYBLE_EVT_GAP_ENCRYPT_CHANGE:
        	uart_print_string(0, "change in encryption");
            switch(*(uint8_t*)eventParam)
            {
            	case 0: uart_print_string(0, "encrytpion OFF"); break;
                case 1: uart_print_string(0, "encrytpion ON"); break;
                default: uart_print_string(0, "encrytpion \e[31mERROR\e[39m"); break;
            }
            break;
		case CYBLE_EVT_GAP_CONNECTION_UPDATE_COMPLETE:
			uart_print_string(0, "gap connection update complete");
			print_conn_param((CYBLE_GAP_CONN_PARAM_UPDATED_IN_CONTROLLER_T*) eventParam);
			break;
        case CYBLE_EVT_GAP_KEYINFO_EXCHNGE_CMPLT:
        	uart_print_string(0, "SMP keys exchange with peer device is complete");
            key_info_print((CYBLE_GAP_SMP_KEY_DIST_T*)eventParam);
            break;
		case CYBLE_EVT_GAP_SMP_NEGOTIATED_AUTH_INFO:
			uart_print_string(0, "SMP has completed pairing properties negotiation");
			print_auth_info((CYBLE_GAP_AUTH_INFO_T*)eventParam);
		    break;
/* Range for GATT events - 0x40 to 0x6F */
		case CYBLE_EVT_GATT_CONNECT_IND:
			uart_print_string(0, "gatt connect");
		    connectionHandle.bdHandle = ((CYBLE_CONN_HANDLE_T*)eventParam)->bdHandle;
		    connectionHandle.attId = ((CYBLE_CONN_HANDLE_T*)eventParam)->attId;
		    print_conn_handle((CYBLE_CONN_HANDLE_T*)eventParam);
		    break;
		case CYBLE_EVT_GATT_DISCONNECT_IND:
			uart_print_string(0, "gatt disconnect");
		    break;
		case CYBLE_EVT_GATTS_XCNHG_MTU_REQ:
			uart_print_string(0, "gatt mtu exchange request");
			print_conn_handle(&(((CYBLE_GATT_XCHG_MTU_PARAM_T *)eventParam)->connHandle));
			uint16_to_string(((CYBLE_GATT_XCHG_MTU_PARAM_T *)eventParam)->mtu, string);
			uart_log_add(t_info, "MTU", string, 0);
		    break;
		case CYBLE_EVT_GATTS_WRITE_REQ:
			uart_print_string(0, "gatts write request");
			print_conn_handle(&(((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->connHandle));
			uint16_to_hex_string(((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.attrHandle, string);
			uart_log_add(t_info, "Attribute Handle", string, 0);
			print_size(&(((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.value));
			print_ble_data("", ((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.value.len, ((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.value.val);
			CyBle_GattsWriteRsp(connectionHandle);
		    break;
		case CYBLE_EVT_GATTS_WRITE_CMD_REQ:
			uart_print_string(0, "gatts write command");
			print_conn_handle(&(((CYBLE_GATTS_WRITE_CMD_REQ_PARAM_T*)eventParam)->connHandle));
			uint16_to_hex_string(((CYBLE_GATTS_WRITE_CMD_REQ_PARAM_T*)eventParam)->handleValPair.attrHandle, string);
			uart_log_add(t_info, "Attribute Handle", string, 0);
			print_size(&(((CYBLE_GATTS_WRITE_CMD_REQ_PARAM_T*)eventParam)->handleValPair.value));
			print_ble_data("", ((CYBLE_GATTS_WRITE_CMD_REQ_PARAM_T*)eventParam)->handleValPair.value.len, ((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.value.val);
			CyBle_GattsWriteRsp(connectionHandle);
			if(CyBle_GattsWriteAttributeValue
		    (
		    		&((CYBLE_GATTS_WRITE_CMD_REQ_PARAM_T*)eventParam)->handleValPair,
		       		0,
		       		&((CYBLE_GATTS_WRITE_CMD_REQ_PARAM_T*)eventParam)->connHandle,
		       		CYBLE_GATT_DB_PEER_INITIATED
		    )
		    == CYBLE_GATT_ERR_NONE) CyBle_GattsWriteRsp(connectionHandle);
		    else uart_print_string(0, "\e[31mGATT ERROR\e[39m");
		    break;
		case CYBLE_EVT_GATTC_HANDLE_VALUE_IND:
			uart_print_string(0, "Indication data received from server device");
		    result = CyBle_GapDisconnect(connectionHandle.bdHandle);
		    print_function_state("CyBle_GapDisconnect", &result);
		    break;
		case CYBLE_EVT_GATTS_READ_CHAR_VAL_ACCESS_REQ:
			uart_print_string(0, "read request");
			print_conn_handle(&(((CYBLE_GATTS_CHAR_VAL_READ_REQ_T*)eventParam)->connHandle));
			uint16_to_hex_string(((CYBLE_GATTS_CHAR_VAL_READ_REQ_T*)eventParam)->attrHandle, string);
			uart_log_add(t_info, "Attribute Handle", string, 0);
			uint8_to_string(((CYBLE_GATTS_CHAR_VAL_READ_REQ_T*)eventParam)->gattErrorCode, string);
			uart_log_add(t_info, "Profile/Service specific error code", string, 0);
			break;
/* Range for L2CAP events - 0x70 to 0x7F */
/* Range for for future use - 0x90 to 0xFF */
        case CYBLE_EVT_PENDING_FLASH_WRITE:
        	uart_print_string(0, "flash write is pending");
            break;
        case CYBLE_EVT_FLASH_CORRUPT:
        	uart_print_string(0, "bonding information stored in flash is corrupted");
            break;
/* GATT Service Events */
        case CYBLE_EVT_GATTS_INDICATION_ENABLED:
        	uart_print_string(0, "GATT Server - Indications for GATT Service's 'Service Changed' Characteristic were enabled");
			print_conn_handle(&(((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->connHandle));
			uint16_to_hex_string(((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.attrHandle, string);
			uart_log_add(t_info, "Attribute Handle", string, 0);
			print_size(&(((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.value));
			print_ble_data("", ((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.value.len, ((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.value.val);
			CyBle_GattsWriteRsp(connectionHandle);
            break;
        case CYBLE_EVT_GATTS_INDICATION_DISABLED:
        	uart_print_string(0, "GATT Server - Indications for GATT Service's 'Service Changed' Characteristic were disabled");
			print_conn_handle(&(((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->connHandle));
			uint16_to_hex_string(((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.attrHandle, string);
			uart_log_add(t_info, "Attribute Handle", string, 0);
			print_size(&(((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.value));
			print_ble_data("", ((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.value.len, ((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.value.val);
			CyBle_GattsWriteRsp(connectionHandle);
            break;
        default:
        	uint16_to_string(event, string);
        	uart_print_string(0, string);
            break;
	}
}
CYBLE_GATT_ERR_CODE_T update_characteristic_byte(uint8* data, uint16 attrHandle)
{
	CYBLE_GATT_HANDLE_VALUE_PAIR_T handle;
    CYBLE_GATT_ERR_CODE_T result;
	handle.attrHandle = attrHandle;
	handle.value.val = data;
	handle.value.len = 1;
	result = CyBle_GattsWriteAttributeValue(&handle, 0, &connectionHandle, CYBLE_GATT_DB_LOCALLY_INITIATED);
    return result;
}
CYBLE_API_RESULT_T notify_characteristic_byte(uint8* data, uint16 attrHandle)
{
    CYBLE_GATTS_HANDLE_VALUE_NTF_T handle;
    CYBLE_API_RESULT_T result;
    handle.value.val  = data;
    handle.value.len  = 1;
    handle.attrHandle = attrHandle;
    result = CyBle_GattsNotification(connectionHandle, &handle);
    return result;
}
void hid_event_handler(uint32 event, void* eventParam)
{
	uart_log_add(t_evt, "HID_EVT", "", 0);
    switch(event)
    {
        case CYBLE_EVT_HIDSS_NOTIFICATION_ENABLED:
        	uart_print_string(0, "Notifications for HID service were enabled");
        	print_hid_char((CYBLE_HIDS_CHAR_VALUE_T*)eventParam);
            break;
        case CYBLE_EVT_HIDSS_NOTIFICATION_DISABLED:
        	uart_print_string(0, "Notifications for HID service were disabled");
        	print_hid_char((CYBLE_HIDS_CHAR_VALUE_T*)eventParam);
            break;
        default:
            uint16_to_hex_string(event, string);
            uart_print_string(0, string);
            break;
    }
}
void print_auth_info(CYBLE_GAP_AUTH_INFO_T* pointer)
{
	uart_log_add(t_info, "security mode", "", 0);
	switch(pointer->security & CYBLE_GAP_SEC_MODE_MASK)
	{
		case CYBLE_GAP_SEC_MODE_1: uart_print_string(0, "1"); break;
	    case CYBLE_GAP_SEC_MODE_2: uart_print_string(0, "2"); break;
	    default: uart_print_string(0, "U"); break;
	}
	uart_log_add(t_info, "security level", "", 0);
	switch(pointer->security & CYBLE_GAP_SEC_LEVEL_MASK)
	{
		case CYBLE_GAP_SEC_LEVEL_1: uart_print_string(0, "1"); break;
		case CYBLE_GAP_SEC_LEVEL_2: uart_print_string(0, "2"); break;
		case CYBLE_GAP_SEC_LEVEL_3: uart_print_string(0, "3"); break;
		case CYBLE_GAP_SEC_LEVEL_4: uart_print_string(0, "4"); break;
	    default: uart_print_string(0, "U"); break;
	}
	uart_log_add(t_info, "bonding", "", 0);
	if(pointer->bonding == CYBLE_GAP_BONDING_NONE) uart_debug_UartPutString("NO");
	else uart_debug_UartPutString("YES");
	uint8_to_string(pointer->ekeySize, string);
	uart_log_add(t_info, "Encryption Key Size", string, 0);
	uint8_to_string(pointer->authErr, string);
	uart_log_add(t_info, "authentication is accepted or rejected with reason", string, 0);
	uart_log_add(t_info, "MITM", "", 0);
	if(pointer->pairingProperties & SMP_SC_PAIR_PROP_MITM_MASK) uart_print_string(0, "YES");
	else uart_print_string(0, "NO");
	uart_log_add(t_info, "Key press", "", 0);
	if(pointer->pairingProperties & SMP_SC_PAIR_PROP_KP_MASK) uart_print_string(0, "YES");
	else uart_print_string(0, "NO");
}
void print_conn_param(CYBLE_GAP_CONN_PARAM_UPDATED_IN_CONTROLLER_T* pointer)
{
	print_hci_error(pointer->status);
	uint16_to_string(pointer->connIntv, string);
	uart_log_add(t_info, "connection interval", string, 0);
	uint16_to_string(pointer->connLatency, string);
	uart_log_add(t_info, "slave latency for the connection", string, 0);
	uint16_to_string(pointer->supervisionTO, string);
	uart_log_add(t_info, "supervision timeout for the LE link", string, 0);
}
void print_pass(uint32_t data)
{
	uint8_t chars[7];
    chars[0] = data/100000 + 0x30;
    chars[1] = ((data/10000)%10) + 0x30;
    chars[2] = ((data/1000)%10) + 0x30;
    chars[3] = ((data/100)%10) + 0x30;
    chars[4] = ((data/10)%10) + 0x30;
    chars[5] = (data%10) + 0x30;
    chars[6] = 0;
    uart_print_string(0, chars);
}
void print_hci_error(CYBLE_HCI_ERROR_T data)
{
	uart_log_add(t_info, "HCI status", "", 0);
    switch(data)
    {
        case CYBLE_HCI_NO_CONNECTION_ERROR:
        	uart_print_string(0, "unknown connection identifier");
            break;
        case CYBLE_HCI_CONNECTION_TERMINATED_USER_ERROR:
        	uart_print_string(0, "remote user terminated connection");
            break;
        case CYBLE_HCI_CONNECTION_TERMINATED_LOCAL_HOST_ERROR:
        	uart_print_string(0, "connection terminated by local host");
            break;
        case CYBLE_HCI_COMMAND_SUCCEEDED:
        	uart_print_string(0, "command success");
            break;
        case CYBLE_HCI_CONNECTION_FAILED_TO_BE_ESTABLISHED:
        	uart_print_string(0, "connection failed to be established");
            break;
        case CYBLE_HCI_CONNECTION_TIMEOUT_ERROR:
        	uart_print_string(0, "connection timeout");
            break;
        case CYBLE_HCI_LL_RESPONSE_TEMEOUT_ERROR:
        	uart_print_string(0, "LL Response Timeout");
            break;
        case CYBLE_HCI_CONNECTION_TERMINATED_DUE_TO_MIC_FAILURE:
        	uart_print_string(0, "Connection Terminated due to MIC Failure");
            break;
        default:
        	uint16_to_string(data, string);
        	uart_print_string(0, string);
            break;
    }
}
void print_function_state(char* name, CYBLE_API_RESULT_T* result)
{
    if(*result == CYBLE_ERROR_OK) uart_log(0, t_function, name, 0);
    else
    {
    	uart_log(0, t_function, name, "");
        switch(*result)
        {
            case CYBLE_ERROR_INVALID_PARAMETER: uart_print_string(0, "At least one of the input parameters is invalid"); break;
            case CYBLE_ERROR_INVALID_OPERATION: uart_print_string(0, "Operation is not permitted"); break;
            case CYBLE_ERROR_MEMORY_ALLOCATION_FAILED: uart_print_string(0, "An internal error occurred in the stack"); break;
            case CYBLE_ERROR_INSUFFICIENT_RESOURCES: uart_print_string(0, "Insufficient resources to perform requested operation"); break;
            case CYBLE_ERROR_OOB_NOT_AVAILABLE: uart_print_string(0, "OOB data not available"); break;
            case CYBLE_ERROR_NO_CONNECTION: uart_print_string(0, "Connection is required to perform requested operation. Connection not present"); break;
            case CYBLE_ERROR_NO_DEVICE_ENTITY: uart_print_string(0, "No device entity to perform requested operation"); break;
            case CYBLE_ERROR_REPEATED_ATTEMPTS: uart_print_string(0, "Attempted repeat operation is not allowed"); break;
            case CYBLE_ERROR_GAP_ROLE: uart_print_string(0, "GAP role is incorrect"); break;
            case CYBLE_ERROR_TX_POWER_READ: uart_print_string(0, "Error reading TC power"); break;
            case CYBLE_ERROR_BT_ON_NOT_COMPLETED: uart_print_string(0, "BLE Initialization failed"); break;
            case CYBLE_ERROR_SEC_FAILED: uart_print_string(0, "Security operation failed"); break;
            case CYBLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE: uart_print_string(0, "Invalid attribute handle"); break;
            case CYBLE_ERROR_DEVICE_ALREADY_EXISTS: uart_print_string(0, "Device cannot be added to whitelist as it has already been added"); break;
            case CYBLE_ERROR_FLASH_WRITE_NOT_PERMITED: uart_print_string(0, "Write to flash is not permitted"); break;
            case CYBLE_ERROR_HARDWARE_FAILURE: uart_print_string(0, "Hardware Failure"); break;
            case CYBLE_ERROR_UNSUPPORTED_FEATURE_OR_PARAMETER_VALUE: uart_print_string(0, "Unsupported feature or parameter value"); break;
            case CYBLE_ERROR_FLASH_WRITE: uart_print_string(0, "Error in flash Write"); break;
            case CYBLE_ERROR_CONTROLLER_BUSY: uart_print_string(0, "Controller Busy"); break;
            case CYBLE_ERROR_MAX: uart_print_string(0, "All other errors not covered in the above list map to this error code"); break;
            default:
            	uint16_to_string(*result, string);
            	uart_print_string(0, string);
                break;
        }
    }
}
void key_info_print(CYBLE_GAP_SMP_KEY_DIST_T* ptr)
{
	uart_log_add(t_info, "long term key", "", 0);
    for(uint8_t i=0; i<CYBLE_GAP_SMP_LTK_SIZE; i++) print_hex_uint8(0, ptr->ltkInfo[i]);
    uart_log(0, t_info, "encrypted diversifier and random number", "");
    for(uint8_t i=0; i<CYBLE_GAP_SMP_MID_INFO_SIZE; i++) print_hex_uint8(0, ptr->midInfo[i]);
    uart_log(0, t_info, "identity resolving key", "");
    for(uint8_t i=0; i<CYBLE_GAP_SMP_IRK_SIZE; i++) print_hex_uint8(0, ptr->irkInfo[i]);
    uart_log(0, t_info, "address type", "");
    if(ptr->idAddrInfo[0] == 0) uart_print_string(0, "public device");
    else uart_print_string(0, "static random");
    uart_log(0, t_info, "address", "");
    print_address(&(ptr->idAddrInfo[1]));
    uart_log(0, t_info, "connection signature resolving key", "");
    for(uint8_t i=0; i<CYBLE_GAP_SMP_CSRK_SIZE; i++) print_hex_uint8(0, ptr->csrkInfo[i]);
}
void print_conn_handle(CYBLE_CONN_HANDLE_T* conn_handle)
{
	uint8_to_string(conn_handle->bdHandle, string);
	uart_log(0, t_info, "Identifies the peer device", string);
	uint8_to_string(conn_handle->attId, string);
	uart_log(0, t_info, "Identifies the ATT Instance", string);
}
void print_hid_char(CYBLE_HIDS_CHAR_VALUE_T* hid)
{
	print_conn_handle(&(hid->connHandle));
    uint8_to_hex_string(hid->serviceIndex, string);
    uart_log(0, t_info, "Index of HID Service", string);
    uart_log(0, t_info, "Index of HID Service Characteristic", "");
    switch(hid->charIndex)
    {
        case CYBLE_HIDS_PROTOCOL_MODE: uart_print_string(0, "Protocol Mode Characteristic index"); break;
        case CYBLE_HIDS_INFORMATION: uart_print_string(0, "HID Information Characteristic index"); break;
        case CYBLE_HIDS_CONTROL_POINT: uart_print_string(0, "HID Control Point Characteristic index"); break;
        case CYBLE_HIDS_REPORT_MAP: uart_print_string(0, "Report Map Characteristic index"); break;
        case CYBLE_HIDS_BOOT_KYBRD_IN_REP: uart_print_string(0, "Boot Keyboard Input Report Characteristic index"); break;
        case CYBLE_HIDS_BOOT_KYBRD_OUT_REP: uart_print_string(0, "Boot Keyboard Output Report Characteristic index"); break;
        case CYBLE_HIDS_BOOT_MOUSE_IN_REP: uart_print_string(0, "Boot Mouse Input Report Characteristic index"); break;
        case CYBLE_HIDS_REPORT: uart_print_string(0, "Report Characteristic index"); break;
        case CYBLE_HIDS_CHAR_COUNT: uart_print_string(0, "Total count of characteristics"); break;
    }
    print_size(hid->value);
}
void print_size(CYBLE_GATT_VALUE_T* ptr)
{
	uint16_to_hex_string(ptr->len, string);
	uart_log(0, t_data, "Length of Value to be packed", string);
	uint16_to_hex_string(ptr->actualLen, string);
	uart_log(0, t_data, "Out Parameter Indicating Actual Length Packed", string);
}
void dis_event_handler(uint32 event, void* eventParam)
{
	uart_log(0, t_evt, "DIS_EVT", 0);
    switch(event)
    {
        default:
            uint32_to_hex_string(event, string);
            uart_print_string(0, string);
            break;
    }
}
void bas_event_handler(uint32 event, void* eventParam)
{
	uart_log(0, t_evt, "BATT_EVT", 0);
    switch(event)
    {
    	case CYBLE_EVT_BASS_NOTIFICATION_ENABLED:
    		uart_print_string(0, "BAS Server - Notifications for Battery Level Characteristic were enabled");
    		print_bas_char((CYBLE_BAS_CHAR_VALUE_T*)eventParam);
    		break;
    	case CYBLE_EVT_BASS_NOTIFICATION_DISABLED:
    		uart_print_string(0, "BAS Server - Notifications for Battery Level Characteristic were disabled");
    		print_bas_char((CYBLE_BAS_CHAR_VALUE_T*)eventParam);
    		break;
        default:
            uint16_to_hex_string(event, string);
            uart_print_string(0, string);
            break;
    }
}
void scps_event_handler(uint32 event, void* eventParam)
{
	uart_log(0, t_evt, "SCAN_EVT", 0);
    switch(event)
    {
    	case CYBLE_EVT_SCPSS_NOTIFICATION_ENABLED:
    		uart_print_string(0, "ScPS Server - Notifications for Scan Refresh Characteristic were enabled");
    		print_scps_char((CYBLE_SCPS_CHAR_VALUE_T*)eventParam);
    		break;
    	case CYBLE_EVT_SCPSS_NOTIFICATION_DISABLED:
    		uart_print_string(0, "ScPS Server - Notifications for Scan Refresh Characteristic were disabled");
    		print_scps_char((CYBLE_SCPS_CHAR_VALUE_T*)eventParam);
    		break;
        default:
            uint16_to_hex_string(event, string);
            uart_print_string(0, string);
            break;
    }
}
void print_bas_char(CYBLE_BAS_CHAR_VALUE_T* bas)
{
	print_conn_handle(&(bas->connHandle));
    uint8_to_hex_string(bas->serviceIndex, string);
    uart_log(0, t_info, "Service instance", string);
    uart_log(0, t_info, "Index of a service characteristic", "");
    switch(bas->charIndex)
    {
        case CYBLE_BAS_BATTERY_LEVEL: uart_print_string(0, "Battery Level characteristic index"); break;
        case CYBLE_BAS_CHAR_COUNT: uart_print_string(0, "Total count of characteristics"); break;
    }
    print_size(bas->value);
}
void print_scps_char(CYBLE_SCPS_CHAR_VALUE_T* scps)
{
	print_conn_handle(&(scps->connHandle));
    uart_log(0, t_info, "Index of HID Service Characteristic", "");
    switch(scps->charIndex)
    {
        case CYBLE_SCPS_SCAN_INT_WIN: uart_print_string(0, "Scan Interval Window characteristic index"); break;
        case CYBLE_SCPS_SCAN_REFRESH: uart_print_string(0, "Scan Refresh characteristic index"); break;
        case CYBLE_SCPS_CHAR_COUNT: uart_print_string(0, "Total count of characteristics"); break;
    }
    print_size(scps->value);
}

//ver180921
void EMU_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));          /**< EMU IRQ Handler */
void FRC_PRI_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));      /**< FRC_PRI IRQ Handler */
void WDOG0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));        /**< WDOG0 IRQ Handler */
void WDOG1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));        /**< WDOG1 IRQ Handler */
void FRC_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));          /**< FRC IRQ Handler */
void MODEM_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));        /**< MODEM IRQ Handler */
void RAC_SEQ_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));      /**< RAC_SEQ IRQ Handler */
void RAC_RSM_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));      /**< RAC_RSM IRQ Handler */
void BUFC_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));         /**< BUFC IRQ Handler */
void LDMA_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));         /**< LDMA IRQ Handler */
void GPIO_EVEN_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));    /**< GPIO_EVEN IRQ Handler */
void TIMER0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));       /**< TIMER0 IRQ Handler */
void USART0_RX_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));    /**< USART0_RX IRQ Handler */
void USART0_TX_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));    /**< USART0_TX IRQ Handler */
void ACMP0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));        /**< ACMP0 IRQ Handler */
void ADC0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));         /**< ADC0 IRQ Handler */
void IDAC0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));        /**< IDAC0 IRQ Handler */
void I2C0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));         /**< I2C0 IRQ Handler */
void GPIO_ODD_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));     /**< GPIO_ODD IRQ Handler */
void TIMER1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));       /**< TIMER1 IRQ Handler */
void USART1_RX_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));    /**< USART1_RX IRQ Handler */
void USART1_TX_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));    /**< USART1_TX IRQ Handler */
void LEUART0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));      /**< LEUART0 IRQ Handler */
void PCNT0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));        /**< PCNT0 IRQ Handler */
void CMU_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));          /**< CMU IRQ Handler */
void MSC_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));          /**< MSC IRQ Handler */
void CRYPTO0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));      /**< CRYPTO IRQ Handler */
void LETIMER0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));     /**< LETIMER0 IRQ Handler */
void AGC_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));          /**< AGC IRQ Handler */
void PROTIMER_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));     /**< PROTIMER IRQ Handler */
void RTCC_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));         /**< RTCC IRQ Handler */
void SYNTH_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));        /**< SYNTH IRQ Handler */
void CRYOTIMER_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));    /**< CRYOTIMER IRQ Handler */
void RFSENSE_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));      /**< RFSENSE IRQ Handler */
void FPUEH_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));        /**< FPUEH IRQ Handler */
void SMU_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));          /**< SMU IRQ Handler */
void WTIMER0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));      /**< WTIMER0 IRQ Handler */
void WTIMER1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));      /**< WTIMER1 IRQ Handler */
void PCNT1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));        /**< PCNT1 IRQ Handler */
void PCNT2_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));        /**< PCNT2 IRQ Handler */
void USART2_RX_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));    /**< USART2_RX IRQ Handler */
void USART2_TX_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));    /**< USART2_TX IRQ Handler */
void I2C1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));         /**< I2C1 IRQ Handler */
void USART3_RX_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));    /**< USART3_RX IRQ Handler */
void USART3_TX_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));    /**< USART3_TX IRQ Handler */
void VDAC0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));        /**< VDAC0 IRQ Handler */
void CSEN_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));         /**< CSEN IRQ Handler */
void LESENSE_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));      /**< LESENSE IRQ Handler */
void CRYPTO1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));      /**< CRYPTO1 IRQ Handler */
void TRNG0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));        /**< TRNG0 IRQ Handler */
void SYSCFG_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));       /**< SYSCFG IRQ Handler */
//
const pFunc device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	EMU_IRQHandler,
	FRC_PRI_IRQHandler,
	WDOG0_IRQHandler,
	WDOG1_IRQHandler,
	FRC_IRQHandler,
	MODEM_IRQHandler,
	RAC_SEQ_IRQHandler,
	RAC_RSM_IRQHandler,
	BUFC_IRQHandler,
	LDMA_IRQHandler,
	GPIO_EVEN_IRQHandler,
	TIMER0_IRQHandler,
	USART0_RX_IRQHandler,
	USART0_TX_IRQHandler,
	ACMP0_IRQHandler,
	ADC0_IRQHandler,
	IDAC0_IRQHandler,
	I2C0_IRQHandler,
	GPIO_ODD_IRQHandler,
	TIMER1_IRQHandler,
	USART1_RX_IRQHandler,
	USART1_TX_IRQHandler,
	LEUART0_IRQHandler,
	PCNT0_IRQHandler,
	CMU_IRQHandler,
	MSC_IRQHandler,
	CRYPTO0_IRQHandler,
	LETIMER0_IRQHandler,
	AGC_IRQHandler,
	PROTIMER_IRQHandler,
	RTCC_IRQHandler,
	SYNTH_IRQHandler,
	CRYOTIMER_IRQHandler,
	RFSENSE_IRQHandler,
	FPUEH_IRQHandler,
	SMU_IRQHandler,
	WTIMER0_IRQHandler,
	WTIMER1_IRQHandler,
	PCNT1_IRQHandler,
	PCNT2_IRQHandler,
	USART2_RX_IRQHandler,
	USART2_TX_IRQHandler,
	I2C1_IRQHandler,
	USART3_RX_IRQHandler,
	USART3_TX_IRQHandler,
	VDAC0_IRQHandler,
	CSEN_IRQHandler,
	LESENSE_IRQHandler,
	CRYPTO1_IRQHandler,
	TRNG0_IRQHandler,
	SYSCFG_IRQHandler
};

//ver180921
#ifndef _SYSTEM_H_
#define _SYSTEM_H_
//
#include "std_system.h"
#include "efr32mg12p332f1024gl125.h"
//
void project_def_handler(uint32_t* ipsr);
void sys_init(void);
//
#endif//_SYSTEM_H_

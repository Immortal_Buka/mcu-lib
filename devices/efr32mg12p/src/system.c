//ver180921
#include "system.h"
#include "uart_log.h"
//
void project_def_handler(uint32_t* ipsr)
{
	uint8_t loc_string[11] = {0};
	uint32_to_hex_string(*ipsr, loc_string);
	uart_log_add(t_fault, "project_def_handler", loc_string, 0);
}
void sys_init(void)
{
}

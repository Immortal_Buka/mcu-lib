//ver181010
void MIL_STD_1553B2_IRQHandler(void) WA;
void MIL_STD_1553B1_IRQHandler(void) WA;
void USB_IRQHandler(void) WA;
void CAN1_IRQHandler(void) WA;
void CAN2_IRQHandler(void) WA;
void DMA_IRQHandler(void) WA;
void UART1_IRQHandler(void) WA;
void UART2_IRQHandler(void) WA;
void SSP1_IRQHandler(void) WA;
void BUSY_IRQHandler(void) WA;
void ARINC429R_IRQHandler(void) WA;
void POWER_IRQHandler(void) WA;
void WWDG_IRQHandler(void) WA;
void Timer4_IRQHandler(void) WA;
void Timer1_IRQHandler(void) WA;
void Timer2_IRQHandler(void) WA;
void Timer3_IRQHandler(void) WA;
void ADC_IRQHandler(void) WA;
void ETHERNET_IRQHandler(void) WA;
void SSP3_IRQHandler(void) WA;
void SSP2_IRQHandler(void) WA;
void ARINC429T1_IRQHandler(void) WA;
void ARINC429T2_IRQHandler(void) WA;
void ARINC429T3_IRQHandler(void) WA;
void ARINC429T4_IRQHandler(void) WA;
void BKP_IRQHandler(void) WA;
void EXT_INT1_IRQHandler(void) WA;
void EXT_INT2_IRQHandler(void) WA;
void EXT_INT3_IRQHandler(void) WA;
void EXT_INT4_IRQHandler(void) WA;
//
volatile const func_ptr device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	MIL_STD_1553B2_IRQHandler,
	MIL_STD_1553B1_IRQHandler,
	USB_IRQHandler,
	CAN1_IRQHandler,
	CAN2_IRQHandler,
	DMA_IRQHandler,
	UART1_IRQHandler,
	UART2_IRQHandler,
	SSP1_IRQHandler,
	BUSY_IRQHandler,
	ARINC429R_IRQHandler,
	POWER_IRQHandler,
	WWDG_IRQHandler,
	Timer4_IRQHandler,
	Timer1_IRQHandler,
	Timer2_IRQHandler,
	Timer3_IRQHandler,
	ADC_IRQHandler,
	ETHERNET_IRQHandler,
	SSP3_IRQHandler,
	SSP2_IRQHandler,
	ARINC429T1_IRQHandler,
	ARINC429T2_IRQHandler,
	ARINC429T3_IRQHandler,
	ARINC429T4_IRQHandler,
	0,//Reserved
	0,//Reserved
	BKP_IRQHandler,
	EXT_INT1_IRQHandler,
	EXT_INT2_IRQHandler,
	EXT_INT3_IRQHandler,
	EXT_INT4_IRQHandler
};

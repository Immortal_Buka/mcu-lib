//ver180815
#include "system.h"
#include "std_system.h"
//
void project_def_handler(uint32_t* ipsr);
void sys_init(void);
extern void pi906_init(void);
//
DMA_CtrlDataTypeDef DMA_ControlTable[32] __attribute__ ((section (".ibss"), aligned(1024))) = {0};
extern uint32_t __ibss_start__;
extern uint32_t __ibss_end__;
//
void MIL_STD_1553B2_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void MIL_STD_1553B1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void USB_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void CAN1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void CAN2_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void DMA_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void UART1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void UART2_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void SSP1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void BUSY_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void ARINC429R_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void POWER_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void WWDG_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void Timer4_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void Timer1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void Timer2_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void Timer3_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void ADC_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void ETHERNET_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void SSP3_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void SSP2_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void ARINC429T1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void ARINC429T2_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void ARINC429T3_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void ARINC429T4_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void BKP_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void EXT_INT1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void EXT_INT2_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void EXT_INT3_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void EXT_INT4_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
//
const pFunc m1986ve1t_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	MIL_STD_1553B2_IRQHandler,
	MIL_STD_1553B1_IRQHandler,
	USB_IRQHandler,
	CAN1_IRQHandler,
	CAN2_IRQHandler,
	DMA_IRQHandler,
	UART1_IRQHandler,
	UART2_IRQHandler,
	SSP1_IRQHandler,
	BUSY_IRQHandler,
	ARINC429R_IRQHandler,
	POWER_IRQHandler,
	WWDG_IRQHandler,
	Timer4_IRQHandler,
	Timer1_IRQHandler,
	Timer2_IRQHandler,
	Timer3_IRQHandler,
	ADC_IRQHandler,
	ETHERNET_IRQHandler,
	SSP3_IRQHandler,
	SSP2_IRQHandler,
	ARINC429T1_IRQHandler,
	ARINC429T2_IRQHandler,
	ARINC429T3_IRQHandler,
	ARINC429T4_IRQHandler,
	0,//Reserved
	0,//Reserved
	BKP_IRQHandler,
	EXT_INT1_IRQHandler,
	EXT_INT2_IRQHandler,
	EXT_INT3_IRQHandler,
	EXT_INT4_IRQHandler
};
void project_def_handler(uint32_t* ipsr)
{
}
void sys_init(void)
{
	uint32_t *pSrc, *pDest;
	ve1t_clk_init();
	pDest = &__ibss_start__;
	while(pDest < &__ibss_end__) *pDest++ = 0;
}
void ve1t_clk_init(void)
{
	uint32_t l_temp_1 = 0, l_temp_2 = 0;
	MDR_RST_CLK->PER_CLOCK |= RST_CLK_PCLK_EEPROM;
	MDR_EEPROM->CMD = EEPROM_Latency_5;
	MDR_RST_CLK->HS_CONTROL |= RST_CLK_HSE_ON|RST_CLK_HSE_Bypass;// remove |
	while ((MDR_RST_CLK->CLOCK_STATUS & 4) != 4){};
	MDR_RST_CLK->CPU_CLOCK = 3;
	MDR_RST_CLK->PLL_CONTROL = (11<<8)|RST_CLK_PLL_CONTROL_PLL_CPU_ON;//(11+1)*(24/2) = 144
	while ((MDR_RST_CLK->CLOCK_STATUS & 2) != 2){};
	MDR_RST_CLK->CPU_CLOCK = 3|(1<<8)|(1<<2);
}
void pi906_init(void)
{
	MDR_RST_CLK->PER_CLOCK |= (RST_CLK_PCLK_UART1|RST_CLK_PCLK_UART2|RST_CLK_PCLK_TIMER1|RST_CLK_PCLK_PORTA|RST_CLK_PCLK_TIMER3|
		RST_CLK_PCLK_PORTC|RST_CLK_PCLK_PORTD|RST_CLK_PCLK_PORTF|RST_CLK_PCLK_EBC|RST_CLK_PCLK_TIMER2|RST_CLK_PCLK_DMA|
		RST_CLK_PCLK_TIMER4);
	MDR_RST_CLK->UART_CLOCK |= RST_CLK_UART_CLOCK_UART1_CLK_EN|RST_CLK_UART_CLOCK_UART2_CLK_EN|RST_CLK_UART_CLOCK_TIM4_CLK_EN;
	MDR_RST_CLK->TIM_CLOCK = RST_CLK_TIM_CLOCK_TIM1_CLK_EN|RST_CLK_TIM_CLOCK_TIM2_CLK_EN|RST_CLK_TIM_CLOCK_TIM3_CLK_EN;
//pins
	//A0-15 - fpga D0-15
	MDR_PORTA->FUNC = 0x55555555;
	MDR_PORTA->ANALOG = 0xFFFF;
	MDR_PORTA->PWR = 0xFFFFFFFF;
	//C0-2 - fpga, C3-4 - uart1, C5-8 - extint1-4
	MDR_PORTC->FUNC = 0x00015565;
	MDR_PORTC->ANALOG = 0x01ff;
	MDR_PORTC->PWR = 0x0003ffff;
//ext_irq
	//D9 - rs485, D13-14 - uart2
	MDR_PORTD->FUNC = (1<<14*2)|(1<<13*2)|(0<<9*2);
	MDR_PORTD->ANALOG = (1<<14)|(1<<13)|(1<<9);
	MDR_PORTD->PWR = (3<<14*2)|(3<<13*2)|(3<<9*2);
	MDR_PORTD->OE = (1<<9);
	MDR_PORTD->CLRTX = (1<<9);
	//F5-12 - fpga A2-9
	MDR_PORTF->FUNC = 0x02aaa800;
	MDR_PORTF->ANALOG = 0x1FE0;
	MDR_PORTF->PWR = 0x03FFFC00;
//
	bus_init();
//fpga - ebc
	MDR_EBC->CONTROL = (0xd<<12)|2;
//dma-deinit
	MDR_DMA->CFG = 0;
	MDR_DMA->CTRL_BASE_PTR = 0;
	MDR_DMA->CHNL_SW_REQUEST = 0;
	MDR_DMA->CHNL_USEBURST_CLR = 0xFFFFFFFF;
	MDR_DMA->CHNL_REQ_MASK_CLR = 0xFFFFFFFF;
	MDR_DMA->CHNL_ENABLE_CLR = 0xFFFFFFFF;
	MDR_DMA->CHNL_PRI_ALT_CLR = 0xFFFFFFFF;
	MDR_DMA->CHNL_PRIORITY_CLR = 0xFFFFFFFF;
	MDR_DMA->ERR_CLR = 0x01;
//dma
	MDR_DMA->CFG |= DMA_CFG_MASTER_ENABLE;
	MDR_DMA->ERR_CLR = 1;
	MDR_DMA->CTRL_BASE_PTR = (uint32_t)DMA_ControlTable;
//work
	NVIC_SetPriority(TIMER2_IRQn,0);
	NVIC_SetPriority(UART1_IRQn,1);
	NVIC_SetPriority(EXT_INT2_IRQn,2);
	NVIC_EnableIRQ(TIMER2_IRQn);
	NVIC_EnableIRQ(UART1_IRQn);
	NVIC_EnableIRQ(EXT_INT2_IRQn);
	buk_bus_rx_on();
}
void bus_init(void)
{
	//uart1 - rs485, 5m
	MDR_UART1->IBRD = 1;//(144/(16*5)) = 1.8
	MDR_UART1->FBRD = 51;
	//timer2 - 125us
	MDR_TIMER2->ARR = 18000;//144*125
	MDR_UART1->LCR_H= UART_WordLength8b|UART_StopBits1|UART_Parity_No|UART_FIFO_OFF;
	MDR_UART1->CR = UART_HardwareFlowControl_RXE|UART_HardwareFlowControl_TXE|1;
	MDR_UART1->IMSC = UART_IT_RX|UART_IT_TX;
	MDR_TIMER2->IE = 2;
}
void buk_bus_tx_on(void)
{
	MDR_UART1->IMSC = UART_IT_TX;
	MDR_PORTD->SETTX = (1<<9);//1 - tx
	BUS_DELAY;
}
void buk_bus_rx_on(void)
{
	MDR_UART1->IMSC = UART_IT_RX;
	MDR_PORTD->CLRTX = (1<<9);//0 - rx
	BUS_DELAY;
}
void buk_bus_tr_off(void)
{
	MDR_UART1->IMSC = 0;
	MDR_PORTD->CLRTX = (1<<9);//0 - rx
	BUS_DELAY;
}
#define HWREG(x)        (*((uint32_t*)(x)))
#define CONTROL4		0x400D0014	//rv2
#define CONTROL5		0x400D0018
#define INTMASK			0x400D0068	//rv3
#define CONTROL8		0x400D0070	//rv4
#define CONTROL9		0x400D0074
#define BIT22_1			0x40049000	//rv5
#define BIT22_2			0x40051000
#define BIT23_1			0x40049000	//rv6
#define BIT23_2			0x40051000
#define CONTROL5NU		0x400E0024	//rv7
#define CONTROL10		0x400D0078
#define CONTROL11		0x400D007C
#define CONTROL12		0x400D0080
#define CONTROL13		0x400D0084
#define PERCLK			0x4002001C
uint32_t chk_revision(void)
{
	uint32_t ctrl4, ctrl5, intmsk, ctrl8, ctrl9, bt22_1, bt22_2, bt23_1, bt23_2;
	uint32_t ctrl5nu, ctrl10, ctrl11, ctrl12, ctrl13;
	uint32_t rev;
	HWREG(PERCLK) = 0xFFFFFFFF;
	rev = 1;
/***rev2***/
	HWREG(CONTROL4) = 0x1234BBBB;
	HWREG(CONTROL5) = 0xCCCC5678;
	ctrl4 = HWREG(CONTROL4);
	ctrl5 = HWREG(CONTROL5);
	if(!((ctrl4 == 0x1234BBBB) && (ctrl5 == 0xCCCC5678))) return 1;
/***rev3***/
	HWREG(INTMASK) = 0x55AA55AA;
	intmsk = HWREG(INTMASK);
	if(intmsk != 0x55AA55AA) return 2;
/***rev4***/
	HWREG(CONTROL8) = 0x88883333;
	HWREG(CONTROL9) = 0x11331133;
	ctrl8 = HWREG(CONTROL8);
	ctrl9 = HWREG(CONTROL9);
	if(!((ctrl8 == 0x88883333) && (ctrl9 == 0x11331133))) return 3;
/***rev5***/
	HWREG(BIT22_1) = 0x00700000;
	bt22_1 = HWREG(BIT22_1);
	if(bt22_1 != 0x00700000) return 4;
/***rev6***/
	HWREG(BIT22_1) = 0x00F00000;
	bt22_1 = HWREG(BIT22_1);
	if(bt22_1 != 0x00F00000) return 5;
/***rev7***/
	HWREG(CONTROL10) = 0x44442222;
	HWREG(CONTROL11) = 0x44442233;
	HWREG(CONTROL12) = 0x44442244;
	HWREG(CONTROL13) = 0x44442255;
	ctrl10 = HWREG(CONTROL10);
	ctrl11 = HWREG(CONTROL11);
	ctrl12 = HWREG(CONTROL12);
	ctrl13 = HWREG(CONTROL13);
	if(ctrl10 == 0x44442222) return 7;
	else return 6;
}

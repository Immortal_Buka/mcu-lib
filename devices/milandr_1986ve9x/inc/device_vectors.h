//ver190716
void CAN1_IRQHandler(void) WA;
void CAN2_IRQHandler(void) WA;
void USB_IRQHandler(void) WA;
void DMA_IRQHandler(void) WA;
void UART1_IRQHandler(void) WA;
void UART2_IRQHandler(void) WA;
void SSP1_IRQHandler(void) WA;
void I2C_IRQHandler(void) WA;
void POWER_IRQHandler(void) WA;
void WWDG_IRQHandler(void) WA;
void Timer1_IRQHandler(void) WA;
void Timer2_IRQHandler(void) WA;
void Timer3_IRQHandler(void) WA;
void ADC_IRQHandler(void) WA;
void COMPARATOR_IRQHandler(void) WA;
void SSP2_IRQHandler(void) WA;
void BACKUP_IRQHandler(void) WA;
void EXT_INT1_IRQHandler(void) WA;
void EXT_INT2_IRQHandler(void) WA;
void EXT_INT3_IRQHandler(void) WA;
void EXT_INT4_IRQHandler(void) WA;
//
volatile const func_ptr device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	CAN1_IRQHandler,
	CAN2_IRQHandler,
	USB_IRQHandler,
	0,//Reserved
	0,//Reserved
	DMA_IRQHandler,
	UART1_IRQHandler,
	UART2_IRQHandler,
	SSP1_IRQHandler,
	0,//Reserved
	I2C_IRQHandler,
	POWER_IRQHandler,
	WWDG_IRQHandler,
	0,//Reserved
	Timer1_IRQHandler,
	Timer2_IRQHandler,
	Timer3_IRQHandler,
	ADC_IRQHandler,
	0,//Reserved
	COMPARATOR_IRQHandler,
	SSP2_IRQHandler,
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	BACKUP_IRQHandler,
	EXT_INT1_IRQHandler,
	EXT_INT2_IRQHandler,
	EXT_INT3_IRQHandler,
	EXT_INT4_IRQHandler
};

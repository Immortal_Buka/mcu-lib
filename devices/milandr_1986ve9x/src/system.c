//ver190716
#include "system.h"
//
void ve9x_clk_init(void)
{
	MDR_RST_CLK->PER_CLOCK |= RST_CLK_PCLK_RST_CLK|RST_CLK_PCLK_BKP|RST_CLK_PCLK_EEPROM;
	MDR_EEPROM->CMD = (3<<3);
	MDR_RST_CLK->HS_CONTROL = 3;
	while((MDR_RST_CLK->CLOCK_STATUS & 4) == 0){};
	MDR_RST_CLK->CPU_CLOCK = 3;
	MDR_RST_CLK->PLL_CONTROL = (9<<8)|(1<<2);//(9+1)*(16/2) = 80;
	while((MDR_RST_CLK->CLOCK_STATUS & 2) == 0){};
	MDR_RST_CLK->CPU_CLOCK = 3|(1<<8)|(1<<2);
}
void mp901_init(void)
{
	MDR_RST_CLK->PER_CLOCK |= (RST_CLK_PCLK_PORTA|RST_CLK_PCLK_PORTB|RST_CLK_PCLK_PORTC|RST_CLK_PCLK_PORTD|
		RST_CLK_PCLK_PORTE|RST_CLK_PCLK_PORTF|RST_CLK_PCLK_EBC|RST_CLK_PCLK_SSP2|RST_CLK_PCLK_UART1);
//pins
	//a0-15 - fpga
	MDR_PORTA->FUNC = 0x55555555;
	MDR_PORTA->ANALOG = 0xFFFF;
	MDR_PORTA->PWR = 0xFFFFFFFF;
	//b11 - int1
	MDR_PORTB->FUNC = (2<<11*2);
	MDR_PORTB->ANALOG = (1<<11);
	MDR_PORTB->PWR = (3<<11*2);
	//c1-7 - fpga, 12 - ext2, 13 - ext4
	MDR_PORTC->FUNC = 0x00005554|(2<<12*2)|(2<<13*2);
	MDR_PORTC->ANALOG = 0x00fe|(1<<12)|(1<<13);
	MDR_PORTC->PWR = 0x0000fffc|(3<<12*2)|(3<<13*2);
	//d15 - ext3
	MDR_PORTD->FUNC |= (3<<15*2);
	MDR_PORTD->ANALOG |= (1<<15);
	MDR_PORTD->PWR |= (3<<15*2);
	//e7 - gpio mcu->fpga, 12,13 - uart
	MDR_PORTE->FUNC = (3<<12*2)|(3<<13*2);
	MDR_PORTE->ANALOG = (1<<12)|(1<<13)|(1<<7);
	MDR_PORTE->PWR = (3<<12*2)|(3<<13*2)|(3<<7*2);
	MDR_PORTE->OE = (1<<7);
	//f0-9 - fpga, 12-15 - ��4�
	MDR_PORTF->FUNC = 0xff055555;
	MDR_PORTF->ANALOG = 0xf3FF;
	MDR_PORTF->PWR = 0xff0FFFFF;
	//b0-4 - jtag-a, 5-10,12-15, c0,8-11,14,15, d0-4 - jtag_b, 5-14, e0-6,8-11,14,15, f10,11
//fpga - ebc
	MDR_EBC->CONTROL = (0xf<<12)|(1<<1);
//ext_irq
	NVIC->ICPR[0] = 0xf0000000;
//spi memory
	//MDR_SSP2->CR0 = 0;
//uart1 - 115200
	MDR_RST_CLK->UART_CLOCK = RST_CLK_UART_CLOCK_UART1_CLK_EN;
	MDR_UART1->IBRD = 43;
	MDR_UART1->FBRD = 26;
	MDR_UART1->LCR_H= UART_WordLength8b|UART_StopBits1|UART_Parity_No|UART_FIFO_OFF;
	MDR_UART1->CR = UART_HardwareFlowControl_RXE|UART_HardwareFlowControl_TXE|1;
	MDR_UART1->IMSC = UART_IT_RX;
	NVIC_EnableIRQ(UART1_IRQn);
}

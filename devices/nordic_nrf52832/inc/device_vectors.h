//ver190215
void POWER_CLOCK_IRQHandler(void) WA;
void RADIO_IRQHandler(void) WA;
void UARTE0_UART0_IRQHandler(void) WA;
void SPIM0_SPIS0_TWIM0_TWIS0_SPI0_TWI0_IRQHandler(void) WA;
void SPIM1_SPIS1_TWIM1_TWIS1_SPI1_TWI1_IRQHandler(void) WA;
void NFCT_IRQHandler(void) WA;
void GPIOTE_IRQHandler(void) WA;
void SAADC_IRQHandler(void) WA;
void TIMER0_IRQHandler(void) WA;
void TIMER1_IRQHandler(void) WA;
void TIMER2_IRQHandler(void) WA;
void RTC0_IRQHandler(void) WA;
void TEMP_IRQHandler(void) WA;
void RNG_IRQHandler(void) WA;
void ECB_IRQHandler(void) WA;
void CCM_AAR_IRQHandler(void) WA;
void WDT_IRQHandler(void) WA;
void RTC1_IRQHandler(void) WA;
void QDEC_IRQHandler(void) WA;
void COMP_LPCOMP_IRQHandler(void) WA;
void SWI0_EGU0_IRQHandler(void) WA;
void SWI1_EGU1_IRQHandler(void) WA;
void SWI2_EGU2_IRQHandler(void) WA;
void SWI3_EGU3_IRQHandler(void) WA;
void SWI4_EGU4_IRQHandler(void) WA;
void SWI5_EGU5_IRQHandler(void) WA;
void TIMER3_IRQHandler(void) WA;
void TIMER4_IRQHandler(void) WA;
void PWM0_IRQHandler(void) WA;
void PDM_IRQHandler(void) WA;
void MWU_IRQHandler(void) WA;
void PWM1_IRQHandler(void) WA;
void PWM2_IRQHandler(void) WA;
void SPIM2_SPIS2_SPI2_IRQHandler(void) WA;
void RTC2_IRQHandler(void) WA;
void I2S_IRQHandler(void) WA;
void FPU_IRQHandler(void) WA;
//
volatile const func_ptr device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	POWER_CLOCK_IRQHandler,
	RADIO_IRQHandler,
	UARTE0_UART0_IRQHandler,
	SPIM0_SPIS0_TWIM0_TWIS0_SPI0_TWI0_IRQHandler,
	SPIM1_SPIS1_TWIM1_TWIS1_SPI1_TWI1_IRQHandler,
	NFCT_IRQHandler,
	GPIOTE_IRQHandler,
	SAADC_IRQHandler,
	TIMER0_IRQHandler,
	TIMER1_IRQHandler,
	TIMER2_IRQHandler,
	RTC0_IRQHandler,
	TEMP_IRQHandler,
	RNG_IRQHandler,
	ECB_IRQHandler,
	CCM_AAR_IRQHandler,
	WDT_IRQHandler,
	RTC1_IRQHandler,
	QDEC_IRQHandler,
	COMP_LPCOMP_IRQHandler,
	SWI0_EGU0_IRQHandler,
	SWI1_EGU1_IRQHandler,
	SWI2_EGU2_IRQHandler,
	SWI3_EGU3_IRQHandler,
	SWI4_EGU4_IRQHandler,
	SWI5_EGU5_IRQHandler,
	TIMER3_IRQHandler,
	TIMER4_IRQHandler,
	PWM0_IRQHandler,
	PDM_IRQHandler,
	0,//Reserved
	0,//Reserved
	MWU_IRQHandler,
	PWM1_IRQHandler,
	PWM2_IRQHandler,
	SPIM2_SPIS2_SPI2_IRQHandler,
	RTC2_IRQHandler,
	I2S_IRQHandler,
	FPU_IRQHandler,
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0,//Reserved
	0//Reserved
};

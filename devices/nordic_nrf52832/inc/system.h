//ver180225
#ifndef _SYSTEM_H_
#define _SYSTEM_H_
//
#include "std_system.h"
#include "nrf52.h"
#include "nrf52_bitfields.h"
//
extern void delay(uint32_t data);
void project_def_handler(uint32_t* ipsr);
void sys_init(void);
void print_uint8(uint8_t data);
static uint8_t errata_12(void);
static uint8_t errata_16(void);
static uint8_t errata_31(void);
static uint8_t errata_32(void);
static uint8_t errata_36(void);
static uint8_t errata_37(void);
static uint8_t errata_57(void);
static uint8_t errata_66(void);
static uint8_t errata_108(void);
static uint8_t errata_136(void);
void uart_print_char(void* uart, uint8_t data);
//
#endif

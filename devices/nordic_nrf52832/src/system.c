//ver180214
#include "system.h"
#include "std_system.h"
#include "print.h"
//
void project_def_handler(uint32_t* ipsr)
{
	uart_print_string(NRF_UART0, "IRQ#0x");
	print_uint8(((*ipsr)&0xff));
	uart_print_string(NRF_UART0, "\r\n");
}
void sys_init(void)
{
//Enable SWO trace functionality. If ENABLE_SWO is not defined, SWO pin will be used as GPIO
	/*CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	NRF_CLOCK->TRACECONFIG |= CLOCK_TRACECONFIG_TRACEMUX_Serial << CLOCK_TRACECONFIG_TRACEMUX_Pos;
	NRF_P0->PIN_CNF[18] = (GPIO_PIN_CNF_DRIVE_H0H1 << GPIO_PIN_CNF_DRIVE_Pos)|(GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos)|
		(GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos);*/
//Enable Trace functionality. If ENABLE_TRACE is not defined, TRACE pins will be used as GPIOs
	/*CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	NRF_CLOCK->TRACECONFIG |= CLOCK_TRACECONFIG_TRACEMUX_Parallel << CLOCK_TRACECONFIG_TRACEMUX_Pos;
	NRF_P0->PIN_CNF[14] = (GPIO_PIN_CNF_DRIVE_H0H1 << GPIO_PIN_CNF_DRIVE_Pos)|(GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos)|
		(GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos);
	NRF_P0->PIN_CNF[15] = (GPIO_PIN_CNF_DRIVE_H0H1 << GPIO_PIN_CNF_DRIVE_Pos)|(GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos)|
		(GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos);
	NRF_P0->PIN_CNF[16] = (GPIO_PIN_CNF_DRIVE_H0H1 << GPIO_PIN_CNF_DRIVE_Pos)|(GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos)|
		(GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos);
	NRF_P0->PIN_CNF[18] = (GPIO_PIN_CNF_DRIVE_H0H1 << GPIO_PIN_CNF_DRIVE_Pos)|(GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos)|
		(GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos);
	NRF_P0->PIN_CNF[20] = (GPIO_PIN_CNF_DRIVE_H0H1 << GPIO_PIN_CNF_DRIVE_Pos)|(GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos)|
		(GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos);
	*/
	if (*(volatile uint32_t *)0x4006EC00 == 0) *(volatile uint32_t*)0x4006EC00 = 0x9375;
	*(volatile uint32_t*)0x4006EC14 = 0xC0;
	if (errata_12()) *(volatile uint32_t *)0x40013540 = (*(uint32_t *)0x10000324 & 0x00001F00) >> 8;
	if (errata_16()) *(volatile uint32_t *)0x4007C074 = 3131961357ul;
	if (errata_31()) *(volatile uint32_t *)0x4000053C = ((*(volatile uint32_t *)0x10000244) & 0x0000E000) >> 13;
	if (errata_32()) CoreDebug->DEMCR &= ~CoreDebug_DEMCR_TRCENA_Msk;
	if (errata_36())
	{
		NRF_CLOCK->EVENTS_DONE = 0;
	    NRF_CLOCK->EVENTS_CTTO = 0;
	    NRF_CLOCK->CTIV = 0;
	}
	if (errata_37()) *(volatile uint32_t *)0x400005A0 = 0x3;
	if (errata_57())
	{
		*(volatile uint32_t *)0x40005610 = 0x00000005;
	    *(volatile uint32_t *)0x40005688 = 0x00000001;
	    *(volatile uint32_t *)0x40005618 = 0x00000000;
	    *(volatile uint32_t *)0x40005614 = 0x0000003F;
	}
	if (errata_66())
	{
		NRF_TEMP->A0 = NRF_FICR->TEMP.A0;
	    NRF_TEMP->A1 = NRF_FICR->TEMP.A1;
	    NRF_TEMP->A2 = NRF_FICR->TEMP.A2;
	    NRF_TEMP->A3 = NRF_FICR->TEMP.A3;
	    NRF_TEMP->A4 = NRF_FICR->TEMP.A4;
	    NRF_TEMP->A5 = NRF_FICR->TEMP.A5;
	    NRF_TEMP->B0 = NRF_FICR->TEMP.B0;
	    NRF_TEMP->B1 = NRF_FICR->TEMP.B1;
	    NRF_TEMP->B2 = NRF_FICR->TEMP.B2;
	    NRF_TEMP->B3 = NRF_FICR->TEMP.B3;
	    NRF_TEMP->B4 = NRF_FICR->TEMP.B4;
	    NRF_TEMP->B5 = NRF_FICR->TEMP.B5;
	    NRF_TEMP->T0 = NRF_FICR->TEMP.T0;
	    NRF_TEMP->T1 = NRF_FICR->TEMP.T1;
	    NRF_TEMP->T2 = NRF_FICR->TEMP.T2;
	    NRF_TEMP->T3 = NRF_FICR->TEMP.T3;
	    NRF_TEMP->T4 = NRF_FICR->TEMP.T4;
	}
	if (errata_108()) *(volatile uint32_t *)0x40000EE4 = *(volatile uint32_t *)0x10000258 & 0x0000004F;
	if (errata_136())
	{
		if (NRF_POWER->RESETREAS & POWER_RESETREAS_RESETPIN_Msk) NRF_POWER->RESETREAS =  ~POWER_RESETREAS_RESETPIN_Msk;
	}
/*Configure NFCT pins as GPIOs if NFCT is not to be used in your code. If CONFIG_NFCT_PINS_AS_GPIOS is not defined,
two GPIOs will be reserved for NFC and will not be available as normal GPIOs. */
#ifdef CONFIG_NFCT_PINS_AS_GPIOS
	if ((NRF_UICR->NFCPINS & UICR_NFCPINS_PROTECT_Msk) == (UICR_NFCPINS_PROTECT_NFC << UICR_NFCPINS_PROTECT_Pos))
	{
		NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos;
	    while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	    NRF_UICR->NFCPINS &= ~UICR_NFCPINS_PROTECT_Msk;
	    while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos;
	    while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	    NVIC_SystemReset();
	}
#endif
//enable reset pin
	if ((NRF_UICR->PSELRESET[0] & UICR_PSELRESET_CONNECT_Msk)||((NRF_UICR->PSELRESET[1] & UICR_PSELRESET_CONNECT_Msk)))
	{
		NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen;
	    while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	    NRF_UICR->PSELRESET[0] = 21;
	    while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	    NRF_UICR->PSELRESET[1] = 21;
	    while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren;
	    while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	    //NVIC_SystemReset();
	}
}
void print_uint8(uint8_t data)
{
	uint8_t chars[5] = "0x00\0";
	uint8_to_hex_string(data, &chars[2]);
	uart_print_string(NRF_UART0, chars);
}
static uint8_t errata_12(void)
{
    if ((((*(uint32_t *)0xF0000FE0) & 0x000000FF) == 0x6) && (((*(uint32_t *)0xF0000FE4) & 0x0000000F) == 0x0))
    {
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x30) return 1;
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x40) return 1;
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x50) return 1;
    }
    return 0;
}
static uint8_t errata_16(void)
{
    if ((((*(uint32_t *)0xF0000FE0) & 0x000000FF) == 0x6) && (((*(uint32_t *)0xF0000FE4) & 0x0000000F) == 0x0))
    {
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x30) return 1;
    }
    return 0;
}
static uint8_t errata_31(void)
{
    if ((((*(uint32_t *)0xF0000FE0) & 0x000000FF) == 0x6) && (((*(uint32_t *)0xF0000FE4) & 0x0000000F) == 0x0))
    {
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x30) return 1;
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x40) return 1;
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x50) return 1;
    }
    return 0;
}
static uint8_t errata_32(void)
{
    if ((((*(uint32_t *)0xF0000FE0) & 0x000000FF) == 0x6) && (((*(uint32_t *)0xF0000FE4) & 0x0000000F) == 0x0))
    {
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x30) return 1;
    }
    return 0;
}
static uint8_t errata_36(void)
{
    if ((((*(uint32_t *)0xF0000FE0) & 0x000000FF) == 0x6) && (((*(uint32_t *)0xF0000FE4) & 0x0000000F) == 0x0))
    {
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x30) return 1;
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x40) return 1;
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x50) return 1;
    }
    return 0;
}
static uint8_t errata_37(void)
{
    if ((((*(uint32_t *)0xF0000FE0) & 0x000000FF) == 0x6) && (((*(uint32_t *)0xF0000FE4) & 0x0000000F) == 0x0))
    {
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x30) return 1;
    }
    return 0;
}
static uint8_t errata_57(void)
{
    if ((((*(uint32_t *)0xF0000FE0) & 0x000000FF) == 0x6) && (((*(uint32_t *)0xF0000FE4) & 0x0000000F) == 0x0))
    {
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x30) return 1;
    }
    return 0;
}
static uint8_t errata_66(void)
{
    if ((((*(uint32_t *)0xF0000FE0) & 0x000000FF) == 0x6) && (((*(uint32_t *)0xF0000FE4) & 0x0000000F) == 0x0))
    {
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x50) return 1;
    }
    return 0;
}
static uint8_t errata_108(void)
{
    if ((((*(uint32_t *)0xF0000FE0) & 0x000000FF) == 0x6) && (((*(uint32_t *)0xF0000FE4) & 0x0000000F) == 0x0))
    {
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x30) return 1;
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x40) return 1;
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x50) return 1;
    }
    return 0;
}
static uint8_t errata_136(void)
{
    if ((((*(uint32_t *)0xF0000FE0) & 0x000000FF) == 0x6) && (((*(uint32_t *)0xF0000FE4) & 0x0000000F) == 0x0))
    {
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x30) return 1;
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x40) return 1;
        if (((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x50) return 1;
    }
    return 0;
}
void uart_print_char(void* uart, uint8_t data)
{
	((NRF_UART_Type*)uart)->TXD = data;
	while(((NRF_UART_Type*)uart)->EVENTS_TXDRDY == 0){}
	((NRF_UART_Type*)uart)->EVENTS_TXDRDY = 0;
}

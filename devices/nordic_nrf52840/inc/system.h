//ver190226
#ifndef _SYSTEM_H_
#define _SYSTEM_H_
//
#include "nrf52840_peripherals.h"
#include "nrf52840.h"
#include "nrf52840_bitfields.h"
#include "std_system.h"
//
#define NRF_DRV_USBD_EPSIZE 		32
//
extern void delay(uint32_t data);
void project_def_handler(void);
void sys_init(void);
void uart_print_char(void* uart, uint8_t data);
uint8_t errata_36(void);
uint8_t errata_66(void);
uint8_t errata_gen1(void);
uint8_t errata_98(void);
uint8_t errata_gen2(void);
uint8_t errata_103(void);
uint8_t errata_115(void);
uint8_t errata_120(void);
uint8_t errata_136(void);
uint8_t nrf_drv_usbd_errata_type_52840_eng_a(void);
uint8_t nrf_drv_usbd_errata_type_52840_eng_b(void);
uint8_t nrf_drv_usbd_errata_type_52840_eng_c(void);
uint8_t nrf_drv_usbd_errata_type_52840_eng_d(void);
uint8_t nrf_drv_usbd_errata_104(void);
uint8_t nrf_drv_usbd_errata_154(void);
uint8_t nrf_drv_usbd_errata_166(void);
uint8_t nrf_drv_usbd_errata_171(void);
uint8_t nrf_drv_usbd_errata_187(void);
uint8_t nrf_drv_usb_errata_199(void);
uint8_t nrf_drv_usbd_errata_200(void);
//
#endif

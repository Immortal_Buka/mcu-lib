//ver190226
#include "system.h"
//
#define CONFIG_NFCT_PINS_AS_GPIOS	1
#define CONFIG_GPIO_AS_PINRESET		1
//
void project_def_handler(void)
{
}
void sys_init(void)
{
	/* Enable SWO trace functionality.
	If ENABLE_SWO is not defined, SWO pin will be used as GPIO (see Product Specification to see which one). */
#if defined (ENABLE_SWO)
	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	NRF_CLOCK->TRACECONFIG |= CLOCK_TRACECONFIG_TRACEMUX_Serial << CLOCK_TRACECONFIG_TRACEMUX_Pos;
	NRF_P1->PIN_CNF[0] = (GPIO_PIN_CNF_DRIVE_H0H1 << GPIO_PIN_CNF_DRIVE_Pos) | (GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos) | (GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos);
#endif
	/* Enable Trace functionality.
	If ENABLE_TRACE is not defined, TRACE pins will be used as GPIOs (see Product Specification to see which ones). */
#if defined (ENABLE_TRACE)
	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	NRF_CLOCK->TRACECONFIG |= CLOCK_TRACECONFIG_TRACEMUX_Parallel << CLOCK_TRACECONFIG_TRACEMUX_Pos;
	NRF_P0->PIN_CNF[7]  = (GPIO_PIN_CNF_DRIVE_H0H1 << GPIO_PIN_CNF_DRIVE_Pos) | (GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos) | (GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos);
	NRF_P1->PIN_CNF[0]  = (GPIO_PIN_CNF_DRIVE_H0H1 << GPIO_PIN_CNF_DRIVE_Pos) | (GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos) | (GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos);
	NRF_P0->PIN_CNF[12] = (GPIO_PIN_CNF_DRIVE_H0H1 << GPIO_PIN_CNF_DRIVE_Pos) | (GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos) | (GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos);
	NRF_P0->PIN_CNF[11] = (GPIO_PIN_CNF_DRIVE_H0H1 << GPIO_PIN_CNF_DRIVE_Pos) | (GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos) | (GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos);
	NRF_P1->PIN_CNF[9]  = (GPIO_PIN_CNF_DRIVE_H0H1 << GPIO_PIN_CNF_DRIVE_Pos) | (GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos) | (GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos);
#endif
	/* Workaround for Errata 36 "CLOCK: Some registers are not reset when expected"*/
	if (errata_36())
	{
		NRF_CLOCK->EVENTS_DONE = 0;
	    NRF_CLOCK->EVENTS_CTTO = 0;
	    NRF_CLOCK->CTIV = 0;
	}
	/* Workaround for Errata 66 "TEMP: Linearity specification not met with default settings"*/
	if (errata_66())
	{
		NRF_TEMP->A0 = NRF_FICR->TEMP.A0;
	    NRF_TEMP->A1 = NRF_FICR->TEMP.A1;
	    NRF_TEMP->A2 = NRF_FICR->TEMP.A2;
	    NRF_TEMP->A3 = NRF_FICR->TEMP.A3;
	    NRF_TEMP->A4 = NRF_FICR->TEMP.A4;
	    NRF_TEMP->A5 = NRF_FICR->TEMP.A5;
	    NRF_TEMP->B0 = NRF_FICR->TEMP.B0;
	    NRF_TEMP->B1 = NRF_FICR->TEMP.B1;
	    NRF_TEMP->B2 = NRF_FICR->TEMP.B2;
	    NRF_TEMP->B3 = NRF_FICR->TEMP.B3;
	    NRF_TEMP->B4 = NRF_FICR->TEMP.B4;
	    NRF_TEMP->B5 = NRF_FICR->TEMP.B5;
	    NRF_TEMP->T0 = NRF_FICR->TEMP.T0;
	    NRF_TEMP->T1 = NRF_FICR->TEMP.T1;
	    NRF_TEMP->T2 = NRF_FICR->TEMP.T2;
	    NRF_TEMP->T3 = NRF_FICR->TEMP.T3;
	    NRF_TEMP->T4 = NRF_FICR->TEMP.T4;
	}
	/* Workaround for Errata 98 "NFCT: Not able to communicate with the peer"*/
	if (errata_98()) *(volatile uint32_t *)0x4000568Cul = 0x00038148ul;
	/* Workaround for Errata 103 "CCM: Wrong reset value of CCM MAXPACKETSIZE"*/
	if (errata_103()) NRF_CCM->MAXPACKETSIZE = 0xFBul;
	/* Workaround for Errata 115 "RAM: RAM content cannot be trusted upon waking up from System ON Idle or System OFF mode"*/
	if (errata_115()) *(volatile uint32_t *)0x40000EE4 = (*(volatile uint32_t *)0x40000EE4 & 0xFFFFFFF0) | (*(uint32_t *)0x10000258 & 0x0000000F);
	/* Workaround for Errata 120 "QSPI: Data read or written is corrupted"*/
	if (errata_120()) *(volatile uint32_t *)0x40029640ul = 0x200ul;
	/* Workaround for Errata 136 "System: Bits in RESETREAS are set when they should not be"*/
	if (errata_136())
	{
		if (NRF_POWER->RESETREAS & POWER_RESETREAS_RESETPIN_Msk) NRF_POWER->RESETREAS =  ~POWER_RESETREAS_RESETPIN_Msk;
	}
	/* Configure NFCT pins as GPIOs if NFCT is not to be used in your code.
	If CONFIG_NFCT_PINS_AS_GPIOS is not defined,
	two GPIOs (see Product Specification to see which ones) will be reserved for NFC and will not be available as normal GPIOs. */
#if defined (CONFIG_NFCT_PINS_AS_GPIOS)
	if ((NRF_UICR->NFCPINS & UICR_NFCPINS_PROTECT_Msk) == (UICR_NFCPINS_PROTECT_NFC << UICR_NFCPINS_PROTECT_Pos))
	{
		NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos;
	    while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	    NRF_UICR->NFCPINS &= ~UICR_NFCPINS_PROTECT_Msk;
	    while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos;
	    while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	    NVIC_SystemReset();
	}
#endif
	/* Configure GPIO pads as pPin Reset pin if Pin Reset capabilities desired.
	If CONFIG_GPIO_AS_PINRESET is not defined, pin reset will not be available.
	One GPIO (see Product Specification to see which one) will then be reserved for PinReset and not available as normal GPIO. */
#if defined (CONFIG_GPIO_AS_PINRESET)
	if (((NRF_UICR->PSELRESET[0] & UICR_PSELRESET_CONNECT_Msk) != (UICR_PSELRESET_CONNECT_Connected << UICR_PSELRESET_CONNECT_Pos)) ||
			((NRF_UICR->PSELRESET[1] & UICR_PSELRESET_CONNECT_Msk) != (UICR_PSELRESET_CONNECT_Connected << UICR_PSELRESET_CONNECT_Pos)))
	{
		NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos;
		while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	    NRF_UICR->PSELRESET[0] = 18;
	    while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	    NRF_UICR->PSELRESET[1] = 18;
	    while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos;
	    while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	    NVIC_SystemReset();
	}
#endif
}
void uart_print_char(void* uart, uint8_t data)
{
}
uint8_t errata_gen1(void)
{
    return 1;
}
uint8_t errata_gen2(void)
{
    if (*(uint32_t *)0x10000130ul == 0x8ul)
    {
        if (*(uint32_t *)0x10000134ul == 0x0ul) return 1;
    }
    return 0;
}
uint8_t errata_36(void)
{
	return errata_gen1();
}
uint8_t errata_66(void)
{
	return errata_gen1();
}
uint8_t errata_98(void)
{
	return errata_gen2();
}
uint8_t errata_103(void)
{
	return errata_gen2();
}
uint8_t errata_115(void)
{
	return errata_gen2();
}
uint8_t errata_120(void)
{
	return errata_gen2();
}
uint8_t errata_136(void)
{
	return errata_gen1();
}
uint8_t nrf_drv_usbd_errata_type_52840_eng_a(void)
{
    return ((*(uint32_t*)0x10000130 == 8) && (*(uint32_t*)0x10000134 == 0));
}
uint8_t nrf_drv_usbd_errata_type_52840_eng_b(void)
{
    return ((*(uint32_t*)0x10000130 == 8) && (*(uint32_t*)0x10000134 == 1));
}
uint8_t nrf_drv_usbd_errata_type_52840_eng_c(void)
{
    return ((*(uint32_t*)0x10000130 == 8) && (*(uint32_t*)0x10000134 == 2));
}
uint8_t nrf_drv_usbd_errata_type_52840_eng_d(void)
{
    return ((*(uint32_t*)0x10000130 == 8) && (*(uint32_t*)0x10000134 == 3));
}
uint8_t nrf_drv_usbd_errata_104(void)
{
    return (nrf_drv_usbd_errata_type_52840_eng_a());
}
uint8_t nrf_drv_usbd_errata_154(void)
{
    return (nrf_drv_usbd_errata_type_52840_eng_a());
}
uint8_t nrf_drv_usbd_errata_166(void)
{
    return 1;
}
uint8_t nrf_drv_usbd_errata_171(void)
{
    return 1;
}
uint8_t nrf_drv_usbd_errata_187(void)
{
    return ((nrf_drv_usbd_errata_type_52840_eng_b()||nrf_drv_usbd_errata_type_52840_eng_c()||nrf_drv_usbd_errata_type_52840_eng_d()));
}
uint8_t nrf_drv_usb_errata_199(void)
{
    return 1;
}
uint8_t nrf_drv_usbd_errata_200(void)
{
    return (nrf_drv_usbd_errata_type_52840_eng_a());
}

//ver181114
void DMA0_IRQHandler(void) WA;
void DMA1_IRQHandler(void) WA;
void DMA2_IRQHandler(void) WA;
void DMA3_IRQHandler(void) WA;
void DMA4_IRQHandler(void) WA;
void DMA5_IRQHandler(void) WA;
void DMA6_IRQHandler(void) WA;
void DMA7_IRQHandler(void) WA;
void DMA8_IRQHandler(void) WA;
void DMA9_IRQHandler(void) WA;
void DMA10_IRQHandler(void) WA;
void DMA11_IRQHandler(void) WA;
void DMA12_IRQHandler(void) WA;
void DMA13_IRQHandler(void) WA;
void DMA14_IRQHandler(void) WA;
void DMA15_IRQHandler(void) WA;
void DMA_Error_IRQHandler(void) WA;
void MCM_IRQHandler(void) WA;
void FTFE_IRQHandler(void) WA;
void Read_Collision_IRQHandler(void) WA;
void LVD_LVW_IRQHandler(void) WA;
void LLWU_IRQHandler(void) WA;
void WDOG_EWM_IRQHandler(void) WA;
void RNG_IRQHandler(void) WA;
void I2C0_IRQHandler(void) WA;
void I2C1_IRQHandler(void) WA;
void SPI0_IRQHandler(void) WA;
void SPI1_IRQHandler(void) WA;
void I2S0_Tx_IRQHandler(void) WA;
void I2S0_Rx_IRQHandler(void) WA;
void UART0_LON_IRQHandler(void) WA;
void UART0_RX_TX_IRQHandler(void) WA;
void UART0_ERR_IRQHandler(void) WA;
void UART1_RX_TX_IRQHandler(void) WA;
void UART1_ERR_IRQHandler(void) WA;
void UART2_RX_TX_IRQHandler(void) WA;
void UART2_ERR_IRQHandler(void) WA;
void UART3_RX_TX_IRQHandler(void) WA;
void UART3_ERR_IRQHandler(void) WA;
void ADC0_IRQHandler(void) WA;
void CMP0_IRQHandler(void) WA;
void CMP1_IRQHandler(void) WA;
void FTM0_IRQHandler(void) WA;
void FTM1_IRQHandler(void) WA;
void FTM2_IRQHandler(void) WA;
void CMT_IRQHandler(void) WA;
void RTC_IRQHandler(void) WA;
void RTC_Seconds_IRQHandler(void) WA;
void PIT0_IRQHandler(void) WA;
void PIT1_IRQHandler(void) WA;
void PIT2_IRQHandler(void) WA;
void PIT3_IRQHandler(void) WA;
void PDB0_IRQHandler(void) WA;
void USB0_IRQHandler(void) WA;
void USBDCD_IRQHandler(void) WA;
void Reserved71_IRQHandler(void) WA;
void DAC0_IRQHandler(void) WA;
void MCG_IRQHandler(void) WA;
void LPTMR0_IRQHandler(void) WA;
void PORTA_IRQHandler(void) WA;
void PORTB_IRQHandler(void) WA;
void PORTC_IRQHandler(void) WA;
void PORTD_IRQHandler(void) WA;
void PORTE_IRQHandler(void) WA;
void SWI_IRQHandler(void) WA;
void SPI2_IRQHandler(void) WA;
void UART4_RX_TX_IRQHandler(void) WA;
void UART4_ERR_IRQHandler(void) WA;
void UART5_RX_TX_IRQHandler(void) WA;
void UART5_ERR_IRQHandler(void) WA;
void CMP2_IRQHandler(void) WA;
void FTM3_IRQHandler(void) WA;
void DAC1_IRQHandler(void) WA;
void ADC1_IRQHandler(void) WA;
void I2C2_IRQHandler(void) WA;
void CAN0_ORed_Message_buffer_IRQHandler(void) WA;
void CAN0_Bus_Off_IRQHandler(void) WA;
void CAN0_Error_IRQHandler(void) WA;
void CAN0_Tx_Warning_IRQHandler(void) WA;
void CAN0_Rx_Warning_IRQHandler(void) WA;
void CAN0_Wake_Up_IRQHandler(void) WA;
void SDHC_IRQHandler(void) WA;
void ENET_1588_Timer_IRQHandler(void) WA;
void ENET_Transmit_IRQHandler(void) WA;
void ENET_Receive_IRQHandler(void) WA;
void ENET_Error_IRQHandler(void) WA;
//
volatile const func_ptr device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	DMA0_IRQHandler,
	DMA1_IRQHandler,
	DMA2_IRQHandler,
	DMA3_IRQHandler,
	DMA4_IRQHandler,
	DMA5_IRQHandler,
	DMA6_IRQHandler,
	DMA7_IRQHandler,
	DMA8_IRQHandler,
	DMA9_IRQHandler,
	DMA10_IRQHandler,
	DMA11_IRQHandler,
	DMA12_IRQHandler,
	DMA13_IRQHandler,
	DMA14_IRQHandler,
	DMA15_IRQHandler,
	DMA_Error_IRQHandler,
	MCM_IRQHandler,
	FTFE_IRQHandler,
	Read_Collision_IRQHandler,
	LVD_LVW_IRQHandler,
	LLWU_IRQHandler,
	WDOG_EWM_IRQHandler,
	RNG_IRQHandler,
	I2C0_IRQHandler,
	I2C1_IRQHandler,
	SPI0_IRQHandler,
	SPI1_IRQHandler,
	I2S0_Tx_IRQHandler,
	I2S0_Rx_IRQHandler,
	UART0_LON_IRQHandler,
	UART0_RX_TX_IRQHandler,
	UART0_ERR_IRQHandler,
	UART1_RX_TX_IRQHandler,
	UART1_ERR_IRQHandler,
	UART2_RX_TX_IRQHandler,
	UART2_ERR_IRQHandler,
	UART3_RX_TX_IRQHandler,
	UART3_ERR_IRQHandler,
	ADC0_IRQHandler,
	CMP0_IRQHandler,
	CMP1_IRQHandler,
	FTM0_IRQHandler,
	FTM1_IRQHandler,
	FTM2_IRQHandler,
	CMT_IRQHandler,
	RTC_IRQHandler,
	RTC_Seconds_IRQHandler,
	PIT0_IRQHandler,
	PIT1_IRQHandler,
	PIT2_IRQHandler,
	PIT3_IRQHandler,
	PDB0_IRQHandler,
	USB0_IRQHandler,
	USBDCD_IRQHandler,
	Reserved71_IRQHandler,
	DAC0_IRQHandler,
	MCG_IRQHandler,
	LPTMR0_IRQHandler,
	PORTA_IRQHandler,
	PORTB_IRQHandler,
	PORTC_IRQHandler,
	PORTD_IRQHandler,
	PORTE_IRQHandler,
	SWI_IRQHandler,
	SPI2_IRQHandler,
	UART4_RX_TX_IRQHandler,
	UART4_ERR_IRQHandler,
	UART5_RX_TX_IRQHandler,
	UART5_ERR_IRQHandler,
	CMP2_IRQHandler,
	FTM3_IRQHandler,
	DAC1_IRQHandler,
	ADC1_IRQHandler,
	I2C2_IRQHandler,
	CAN0_ORed_Message_buffer_IRQHandler,
	CAN0_Bus_Off_IRQHandler,
	CAN0_Error_IRQHandler,
	CAN0_Tx_Warning_IRQHandler,
	CAN0_Rx_Warning_IRQHandler,
	CAN0_Wake_Up_IRQHandler,
	SDHC_IRQHandler,
	ENET_1588_Timer_IRQHandler,
	ENET_Transmit_IRQHandler,
	ENET_Receive_IRQHandler,
	ENET_Error_IRQHandler,
	Default_Handler,/* 102*/
	Default_Handler,/* 103*/
	Default_Handler,/* 104*/
	Default_Handler,/* 105*/
	Default_Handler,/* 106*/
	Default_Handler,/* 107*/
	Default_Handler,/* 108*/
	Default_Handler,/* 109*/
	Default_Handler,/* 110*/
	Default_Handler,/* 111*/
	Default_Handler,/* 112*/
	Default_Handler,/* 113*/
	Default_Handler,/* 114*/
	Default_Handler,/* 115*/
	Default_Handler,/* 116*/
	Default_Handler,/* 117*/
	Default_Handler,/* 118*/
	Default_Handler,/* 119*/
	Default_Handler,/* 120*/
	Default_Handler,/* 121*/
	Default_Handler,/* 122*/
	Default_Handler,/* 123*/
	Default_Handler,/* 124*/
	Default_Handler,/* 125*/
	Default_Handler,/* 126*/
	Default_Handler,/* 127*/
	Default_Handler,/* 128*/
	Default_Handler,/* 129*/
	Default_Handler,/* 130*/
	Default_Handler,/* 131*/
	Default_Handler,/* 132*/
	Default_Handler,/* 133*/
	Default_Handler,/* 134*/
	Default_Handler,/* 135*/
	Default_Handler,/* 136*/
	Default_Handler,/* 137*/
	Default_Handler,/* 138*/
	Default_Handler,/* 139*/
	Default_Handler,/* 140*/
	Default_Handler,/* 141*/
	Default_Handler,/* 142*/
	Default_Handler,/* 143*/
	Default_Handler,/* 144*/
	Default_Handler,/* 145*/
	Default_Handler,/* 146*/
	Default_Handler,/* 147*/
	Default_Handler,/* 148*/
	Default_Handler,/* 149*/
	Default_Handler,/* 150*/
	Default_Handler,/* 151*/
	Default_Handler,/* 152*/
	Default_Handler,/* 153*/
	Default_Handler,/* 154*/
	Default_Handler,/* 155*/
	Default_Handler,/* 156*/
	Default_Handler,/* 157*/
	Default_Handler,/* 158*/
	Default_Handler,/* 159*/
	Default_Handler,/* 160*/
	Default_Handler,/* 161*/
	Default_Handler,/* 162*/
	Default_Handler,/* 163*/
	Default_Handler,/* 164*/
	Default_Handler,/* 165*/
	Default_Handler,/* 166*/
	Default_Handler,/* 167*/
	Default_Handler,/* 168*/
	Default_Handler,/* 169*/
	Default_Handler,/* 170*/
	Default_Handler,/* 171*/
	Default_Handler,/* 172*/
	Default_Handler,/* 173*/
	Default_Handler,/* 174*/
	Default_Handler,/* 175*/
	Default_Handler,/* 176*/
	Default_Handler,/* 177*/
	Default_Handler,/* 178*/
	Default_Handler,/* 179*/
	Default_Handler,/* 180*/
	Default_Handler,/* 181*/
	Default_Handler,/* 182*/
	Default_Handler,/* 183*/
	Default_Handler,/* 184*/
	Default_Handler,/* 185*/
	Default_Handler,/* 186*/
	Default_Handler,/* 187*/
	Default_Handler,/* 188*/
	Default_Handler,/* 189*/
	Default_Handler,/* 190*/
	Default_Handler,/* 191*/
	Default_Handler,/* 192*/
	Default_Handler,/* 193*/
	Default_Handler,/* 194*/
	Default_Handler,/* 195*/
	Default_Handler,/* 196*/
	Default_Handler,/* 197*/
	Default_Handler,/* 198*/
	Default_Handler,/* 199*/
	Default_Handler,/* 200*/
	Default_Handler,/* 201*/
	Default_Handler,/* 202*/
	Default_Handler,/* 203*/
	Default_Handler,/* 204*/
	Default_Handler,/* 205*/
	Default_Handler,/* 206*/
	Default_Handler,/* 207*/
	Default_Handler,/* 208*/
	Default_Handler,/* 209*/
	Default_Handler,/* 210*/
	Default_Handler,/* 211*/
	Default_Handler,/* 212*/
	Default_Handler,/* 213*/
	Default_Handler,/* 214*/
	Default_Handler,/* 215*/
	Default_Handler,/* 216*/
	Default_Handler,/* 217*/
	Default_Handler,/* 218*/
	Default_Handler,/* 219*/
	Default_Handler,/* 220*/
	Default_Handler,/* 221*/
	Default_Handler,/* 222*/
	Default_Handler,/* 223*/
	Default_Handler,/* 224*/
	Default_Handler,/* 225*/
	Default_Handler,/* 226*/
	Default_Handler,/* 227*/
	Default_Handler,/* 228*/
	Default_Handler,/* 229*/
	Default_Handler,/* 230*/
	Default_Handler,/* 231*/
	Default_Handler,/* 232*/
	Default_Handler,/* 233*/
	Default_Handler,/* 234*/
	Default_Handler,/* 235*/
	Default_Handler,/* 236*/
	Default_Handler,/* 237*/
	Default_Handler,/* 238*/
	Default_Handler,/* 239*/
	Default_Handler,/* 240*/
	Default_Handler,/* 241*/
	Default_Handler,/* 242*/
	Default_Handler,/* 243*/
	Default_Handler,/* 244*/
	Default_Handler,/* 245*/
	Default_Handler,/* 246*/
	Default_Handler,/* 247*/
	Default_Handler,/* 248*/
	Default_Handler,/* 249*/
	Default_Handler,/* 250*/
	Default_Handler,/* 251*/
	Default_Handler,/* 252*/
	Default_Handler,/* 253*/
	Default_Handler,/* 254*/
    (func_ptr)0xFFFFFFFF//Reserved for user TRIM value
};

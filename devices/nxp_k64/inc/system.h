//ver190402
#ifndef _SYSTEM_H_
#define _SYSTEM_H_
//
#include "MK64F12.h"
#include "std_system.h"
#include "time.h"
//
typedef enum
{
  off,
  on,
  toggle
} led_state;
typedef enum
{
  red = 1,
  green = 2,
  blue = 4,
  all = 7
} led;
//
void sys_init(void);
void sda_uart_init(void);
void uart_print_char(void* uart, uint8_t data);
void uart_scan_string(UART_Type* uart, uint8_t *data, uint8_t rxSize);
void frdm_led_rgb(led name, led_state state);
void error_signal (void);
void project_def_handler(void);
void mpu_disabled(void);
uint32_t rtc_datetime_to_seconds(datetime_t *datetime);
void rtc_seconds_to_datetime(uint32_t seconds, datetime_t *datetime);
void rnga_init(void);
void rtc_set_time(uint32_t time);
void rtc_get_time(datetime_t* date_time);
uint8_t rtc_init(void);
uint32_t get_random(void);
void init_common_gpio(void);
uint8_t adc16_calibration(void);
#endif

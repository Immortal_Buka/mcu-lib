//ver180215
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "ffconf.h"
#include "diskio.h"
#include "system.h"
#include "time.h"
//
#define SDHC_MAX_DVS ((SDHC_SYSCTL_DVS_MASK >> SDHC_SYSCTL_DVS_SHIFT) + 1)
#define SDHC_MAX_CLKFS ((SDHC_SYSCTL_SDCLKFS_MASK >> SDHC_SYSCTL_SDCLKFS_SHIFT) + 1)
#define SWAP_WORD_BYTE_SEQUENCE(x) (__REV(x))
#define SDMMC_CLOCK_400KHZ (400000)
#define SD_CLOCK_25MHZ (25000000)
#define SD_CLOCK_50MHZ (50000000)
#define SDMMC_R1_CURRENT_STATE(x) (((x)&0x00001E00U) >> 9U)
#define SDHC_MAX_BLOCK_COUNT (SDHC_BLKATTR_BLKCNT_MASK >> SDHC_BLKATTR_BLKCNT_SHIFT)
//
enum _sdhc_status
{
    kStatus_SDHC_BusyTransferring = 1,            /*!< Transfer is on-going */
    kStatus_SDHC_PrepareAdmaDescriptorFailed = 2, /*!< Set DMA descriptor failed */
};
enum _sdhc_transfer_flag
{
    kSDHC_ResponseLength48Flag = SDHC_XFERTYP_RSPTYP(2U),     /*!< 48 bit response length */
    kSDHC_EnableCrcCheckFlag = SDHC_XFERTYP_CCCEN_MASK,   /*!< Enable CRC check */
    kSDHC_EnableIndexCheckFlag = SDHC_XFERTYP_CICEN_MASK, /*!< Enable index check */
};
enum _sdhc_interrupt_status_flag
{
    kSDHC_CommandErrorFlag = (SDHC_IRQSTAT_CTOE_MASK|SDHC_IRQSTAT_CCE_MASK|SDHC_IRQSTAT_CEBE_MASK|SDHC_IRQSTAT_CIE_MASK), /*!< Command error */
    kSDHC_DataErrorFlag = (SDHC_IRQSTAT_DTOE_MASK|SDHC_IRQSTAT_DCE_MASK|SDHC_IRQSTAT_DEBE_MASK|SDHC_IRQSTAT_AC12E_MASK),                                  /*!< Data error */
    kSDHC_DataFlag = (SDHC_IRQSTAT_TC_MASK|SDHC_IRQSTAT_DINT_MASK|SDHC_IRQSTAT_BWR_MASK|SDHC_IRQSTAT_BRR_MASK|kSDHC_DataErrorFlag|SDHC_IRQSTAT_DMAE_MASK), /*!< Data interrupts */              /*!< Command interrupts */
    kSDHC_AllInterruptFlags = (SDHC_IRQSTAT_BGE_MASK|SDHC_IRQSTAT_CINT_MASK|kSDHC_CommandErrorFlag|SDHC_IRQSTAT_CC_MASK|kSDHC_DataFlag|kSDHC_CommandErrorFlag|kSDHC_DataErrorFlag|SDHC_IRQSTAT_DMAE_MASK), /*!< All flags mask */
};
typedef enum _sdhc_response_type
{
    kSDHC_ResponseTypeNone = 0U, /*!< Response type: none */
    kSDHC_ResponseTypeR1 = 1U,   /*!< Response type: R1 */
    kSDHC_ResponseTypeR1b = 2U,  /*!< Response type: R1b */
    kSDHC_ResponseTypeR2 = 3U,   /*!< Response type: R2 */
    kSDHC_ResponseTypeR3 = 4U,   /*!< Response type: R3 */
    kSDHC_ResponseTypeR4 = 5U,   /*!< Response type: R4 */
    kSDHC_ResponseTypeR5 = 6U,   /*!< Response type: R5 */
    kSDHC_ResponseTypeR5b = 7U,  /*!< Response type: R5b */
    kSDHC_ResponseTypeR6 = 8U,   /*!< Response type: R6 */
    kSDHC_ResponseTypeR7 = 9U,   /*!< Response type: R7 */
} sdhc_response_type_t;
typedef struct _sdhc_adma2_descriptor
{
    uint32_t attribute;      /*!< The control and status field */
    const uint32_t *address; /*!< The address field */
} sdhc_adma2_descriptor_t;
typedef struct _sdhc_transfer_config
{
    size_t dataBlockSize;     /*!< Data block size */
    uint32_t dataBlockCount;  /*!< Data block count */
} sdhc_transfer_config_t;
typedef struct _sdhc_data
{
    bool enableAutoCommand12; /*!< Enable auto CMD12 */
    bool enableIgnoreError;   /*!< Enable to ignore error event to read/write all the data */
    size_t blockSize;         /*!< Block size */
    uint32_t blockCount;      /*!< Block count */
    uint32_t *rxData;         /*!< Buffer to save data read */
    const uint32_t *txData;   /*!< Data buffer to write */
} sdhc_data_t;
typedef struct _sdhc_command
{
    uint32_t index;                    /*!< Command index */
    uint32_t argument;                 /*!< Command argument */
    uint8_t type;          /*!< Command type */
    sdhc_response_type_t responseType; /*!< Command response type */
    uint32_t response[4U];             /*!< Response for this command */
} sdhc_command_t;
typedef struct _sdhc_transfer
{
    sdhc_data_t *data;       /*!< Data to transfer */
    sdhc_command_t *command; /*!< Command to send */
} sdhc_transfer_t;
typedef struct _sdhc_handle sdhc_handle_t;
struct _sdhc_handle
{
    sdhc_data_t *volatile data;       /*!< Data to transfer */
    sdhc_command_t *volatile command; /*!< Command to send */
    volatile uint32_t transferredWords; /*!< Words transferred by DATAPORT way */
};
typedef enum _sdmmc_command
{
    kSDMMC_GoIdleState = 0U,         /*!< Go Idle State */
    kSDMMC_AllSendCid = 2U,          /*!< All Send CID */
    kSDMMC_SetDsr = 4U,              /*!< Set DSR */
    kSDMMC_SelectCard = 7U,          /*!< Select Card */
    kSDMMC_SendCsd = 9U,             /*!< Send CSD */
    kSDMMC_SendCid = 10U,            /*!< Send CID */
    kSDMMC_StopTransmission = 12U,   /*!< Stop Transmission */
    kSDMMC_SendStatus = 13U,         /*!< Send Status */
    kSDMMC_GoInactiveState = 15U,    /*!< Go Inactive State */
    kSDMMC_SetBlockLength = 16U,     /*!< Set Block Length */
    kSDMMC_ReadSingleBlock = 17U,    /*!< Read Single Block */
    kSDMMC_ReadMultipleBlock = 18U,  /*!< Read Multiple Block */
    kSDMMC_SendTuningBlock = 19U,    /*!< Send Tuning Block */
    kSDMMC_SetBlockCount = 23U,      /*!< Set Block Count */
    kSDMMC_WriteSingleBlock = 24U,   /*!< Write Single Block */
    kSDMMC_WriteMultipleBlock = 25U, /*!< Write Multiple Block */
    kSDMMC_ProgramCsd = 27U,         /*!< Program CSD */
    kSDMMC_SetWriteProtect = 28U,    /*!< Set Write Protect */
    kSDMMC_ClearWriteProtect = 29U,  /*!< Clear Write Protect */
    kSDMMC_SendWriteProtect = 30U,   /*!< Send Write Protect */
    kSDMMC_Erase = 38U,              /*!< Erase */
    kSDMMC_LockUnlock = 42U,         /*!< Lock Unlock */
    kSDMMC_ApplicationCommand = 55U, /*!< Send Application Command */
    kSDMMC_GeneralCommand = 56U,     /*!< General Purpose Command */
    kSDMMC_ReadOcr = 58U,            /*!< Read OCR */
} sdmmc_command_t;
typedef enum _sd_ocr_flag
{
    kSD_OcrPowerUpBusyFlag = (1U << 31U),                            /*!< Power up busy status */
    kSD_OcrHostCapacitySupportFlag = (1 << 30),                    /*!< Card capacity status */
    kSD_OcrCardCapacitySupportFlag = (1 << 30), /*!< Card capacity status */
    kSD_OcrVdd32_33Flag = (1U << 20U),                               /*!< VDD 3.1-3.2 */
    kSD_OcrVdd33_34Flag = (1U << 21U),                               /*!< VDD 3.2-3.3 */
} sd_ocr_flag_t;
typedef enum _sd_specification_version
{
    kSD_SpecificationVersion1_0 = (1U << 0U), /*!< SD card version 1.0-1.01 */
    kSD_SpecificationVersion1_1 = (1U << 1U), /*!< SD card version 1.10 */
    kSD_SpecificationVersion2_0 = (1U << 2U), /*!< SD card version 2.00 */
    kSD_SpecificationVersion3_0 = (1U << 3U), /*!< SD card version 3.0 */
} sd_specification_version_t;
typedef enum _sd_data_bus_width
{
    kSD_DataBusWidth1Bit = (1U << 0U), /*!< SD data bus width 1-bit mode */
    kSD_DataBusWidth4Bit = (1U << 2U), /*!< SD data bus width 4-bit mode */
} sd_data_bus_width_t;
typedef enum _sd_csd_flag
{
    kSD_CsdReadBlockPartialFlag = (1U << 0U),         /*!< Partial blocks for read allowed [79:79] */
    kSD_CsdWriteBlockMisalignFlag = (1U << 1U),       /*!< Write block misalignment [78:78] */
    kSD_CsdReadBlockMisalignFlag = (1U << 2U),        /*!< Read block misalignment [77:77] */
    kSD_CsdDsrImplementedFlag = (1U << 3U),           /*!< DSR implemented [76:76] */
    kSD_CsdEraseBlockEnabledFlag = (1U << 4U),        /*!< Erase single block enabled [46:46] */
    kSD_CsdWriteProtectGroupEnabledFlag = (1U << 5U), /*!< Write protect group enabled [31:31] */
    kSD_CsdWriteBlockPartialFlag = (1U << 6U),        /*!< Partial blocks for write allowed [21:21] */
    kSD_CsdFileFormatGroupFlag = (1U << 7U),          /*!< File format group [15:15] */
    kSD_CsdCopyFlag = (1U << 8U),                     /*!< Copy flag [14:14] */
    kSD_CsdPermanentWriteProtectFlag = (1U << 9U),    /*!< Permanent write protection [13:13] */
    kSD_CsdTemporaryWriteProtectFlag = (1U << 10U),   /*!< Temporary write protection [12:12] */
} sd_csd_flag_t;
typedef enum _sd_scr_flag
{
    kSD_ScrDataStatusAfterErase = (1U << 0U), /*!< Data status after erases [55:55] */
    kSD_ScrSdSpecification3 = (1U << 1U),     /*!< Specification version 3.00 or higher [47:47]*/
} sd_scr_flag_t;
typedef struct _sd_cid
{
    uint8_t manufacturerID;                     /*!< Manufacturer ID [127:120] */
    uint16_t applicationID;                     /*!< OEM/Application ID [119:104] */
    uint8_t productName[5]; /*!< Product name [103:64] */
    uint8_t productVersion;                     /*!< Product revision [63:56] */
    uint32_t productSerialNumber;               /*!< Product serial number [55:24] */
    uint16_t manufacturerData;                  /*!< Manufacturing date [19:8] */
} sd_cid_t;
typedef struct _sd_csd
{
    uint8_t csdStructure;        /*!< CSD structure [127:126] */
    uint8_t dataReadAccessTime1; /*!< Data read access-time-1 [119:112] */
    uint8_t dataReadAccessTime2; /*!< Data read access-time-2 in clock cycles (NSAC*100) [111:104] */
    uint8_t transferSpeed;       /*!< Maximum data transfer rate [103:96] */
    uint16_t cardCommandClass;   /*!< Card command classes [95:84] */
    uint8_t readBlockLength;     /*!< Maximum read data block length [83:80] */
    uint16_t flags;              /*!< Flags in sd_csd_flag_t */
    uint32_t deviceSize;         /*!< Device size [73:62] */
    /* Following fields from 'readCurrentVddMin' to 'deviceSizeMultiplier' exist in CSD version 1 */
    uint8_t readCurrentVddMin;    /*!< Maximum read current at VDD min [61:59] */
    uint8_t readCurrentVddMax;    /*!< Maximum read current at VDD max [58:56] */
    uint8_t writeCurrentVddMin;   /*!< Maximum write current at VDD min [55:53] */
    uint8_t writeCurrentVddMax;   /*!< Maximum write current at VDD max [52:50] */
    uint8_t deviceSizeMultiplier; /*!< Device size multiplier [49:47] */
    uint8_t eraseSectorSize;       /*!< Erase sector size [45:39] */
    uint8_t writeProtectGroupSize; /*!< Write protect group size [38:32] */
    uint8_t writeSpeedFactor;      /*!< Write speed factor [28:26] */
    uint8_t writeBlockLength;      /*!< Maximum write data block length [25:22] */
    uint8_t fileFormat;            /*!< File format [11:10] */
} sd_csd_t;
typedef struct _sd_scr
{
    uint8_t scrStructure;             /*!< SCR Structure [63:60] */
    uint8_t sdSpecification;          /*!< SD memory card specification version [59:56] */
    uint16_t flags;                   /*!< SCR flags in sd_scr_flag_t */
    uint8_t sdSecurity;               /*!< Security specification supported [54:52] */
    uint8_t sdBusWidths;              /*!< Data bus widths supported [51:48] */
    uint8_t extendedSecurity;         /*!< Extended security support [46:43] */
    uint8_t commandSupport;           /*!< Command support bits [33:32] */
    uint32_t reservedForManufacturer; /*!< reserved for manufacturer usage [31:0] */
} sd_scr_t;
enum _sdmmc_status
{
    kStatus_SDMMC_NotSupportYet = 1,             /*!< Haven't supported */
    kStatus_SDMMC_TransferFailed = 2,            /*!< Send command failed */
    kStatus_SDMMC_SetCardBlockSizeFailed = 3,    /*!< Set block size failed */
    kStatus_SDMMC_HostNotSupport = 4,            /*!< Host doesn't support */
    kStatus_SDMMC_CardNotSupport = 5,            /*!< Card doesn't support */
    kStatus_SDMMC_AllSendCidFailed = 6,          /*!< Send CID failed */
    kStatus_SDMMC_SendRelativeAddressFailed = 7, /*!< Send relative address failed */
    kStatus_SDMMC_SendCsdFailed = 8,             /*!< Send CSD failed */
    kStatus_SDMMC_SelectCardFailed = 9,          /*!< Select card failed */
    kStatus_SDMMC_SendScrFailed = 10,             /*!< Send SCR failed */
    kStatus_SDMMC_SetDataBusWidthFailed = 11,    /*!< Set bus width failed */
    kStatus_SDMMC_GoIdleFailed = 12,             /*!< Go idle failed */
    kStatus_SDMMC_HandShakeOperationConditionFailed = 13, /*!< Send Operation Condition failed */
    kStatus_SDMMC_SendApplicationCommandFailed = 14,                                    /*!< Send application command failed */
    kStatus_SDMMC_SwitchFailed = 15,           /*!< Switch command failed */
    kStatus_SDMMC_StopTransmissionFailed = 16, /*!< Stop transmission failed */
    kStatus_SDMMC_WaitWriteCompleteFailed = 17,    /*!< Wait write complete failed */
    kStatus_SDMMC_SetBlockCountFailed = 18,        /*!< Set block count failed */
    kStatus_SDMMC_SetRelativeAddressFailed = 19,   /*!< Set relative address failed */
    kStatus_SDMMC_SwitchHighSpeedFailed = 20,      /*!< Switch high speed failed */
    kStatus_SDMMC_SendExtendedCsdFailed = 21,      /*!< Send EXT_CSD failed */
    kStatus_SDMMC_ConfigureBootFailed = 22,        /*!< Configure boot failed */
    kStatus_SDMMC_ConfigureExtendedCsdFailed = 23, /*!< Configure EXT_CSD failed */
    kStatus_SDMMC_EnableHighCapacityEraseFailed = 24, /*!< Enable high capacity erase failed */
    kStatus_SDMMC_SendTestPatternFailed = 25,    /*!< Send test pattern failed */
    kStatus_SDMMC_ReceiveTestPatternFailed = 26, /*!< Receive test pattern failed */
};
enum _generic_status
{
    kStatus_Success = 0,
    kStatus_Fail = 1,
    kStatus_ReadOnly = 2,
    kStatus_OutOfRange = 3,
    kStatus_InvalidArgument = 4,
    kStatus_Timeout = 5,
    kStatus_NoTransferInProgress = 6,
};
typedef enum _sd_card_flag
{
    kSD_SupportHighCapacityFlag = (1U << 1U), /*!< Support high capacity */
    kSD_Support4BitWidthFlag = (1U << 2U),    /*!< Support 4-bit data width */
    kSD_SupportSdhcFlag = (1U << 3U),         /*!< Card is SDHC */
    kSD_SupportSdxcFlag = (1U << 4U),         /*!< Card is SDXC */
} sd_card_flag_t;
typedef int32_t status_t;
typedef struct _sd_card
{
    uint32_t relativeAddress; /*!< Relative address of the card */
    uint32_t version;         /*!< Card version */
    uint32_t flags;           /*!< Flags in sd_card_flag_t */
    uint32_t rawCid[4U];      /*!< Raw CID content */
    uint32_t rawCsd[4U];      /*!< Raw CSD content */
    uint32_t rawScr[2U];      /*!< Raw CSD content */
    uint32_t ocr;             /*!< Raw OCR content */
    sd_cid_t cid;             /*!< CID */
    sd_csd_t csd;             /*!< CSD */
    sd_scr_t scr;             /*!< SCR */
    uint32_t blockCount;      /*!< Card total block number */
    uint32_t blockSize;       /*!< Card block size */
} sd_card_t;
//
status_t SD_Init(void);
static status_t inline SD_SelectCard(bool isSelected);
static status_t inline SD_WaitWriteComplete(void);
static status_t inline SD_SendApplicationCmd(void);
static status_t inline SD_GoIdle(void);
static status_t inline SD_StopTransmission(void);
static status_t inline SD_SetBlockSize(uint32_t blockSize);
static status_t SD_SendRca(void);
static status_t SD_SwitchFunction(uint32_t mode, uint32_t *status);
static void SD_DecodeScr(uint32_t *rawScr);
static status_t SD_SendScr(void);
static status_t SD_SwitchHighspeed(void);
static status_t SD_SetDataBusWidth(sd_data_bus_width_t width);
static void SD_DecodeCsd(uint32_t *rawCsd);
static status_t SD_SendCsd(void);
static void SD_DecodeCid(uint32_t *rawCid);
static status_t SD_AllSendCid(void);
static status_t SD_ApplicationSendOperationCondition(uint32_t argument);
static status_t sdhc_transfer_function(sdhc_transfer_t *content);//++
status_t SDHC_SetAdmaTableConfig(const uint32_t *data, uint32_t dataBytes);//++
void SDHC_SetSdClock(uint32_t busClock_Hz);//++
status_t SDHC_TransferNonBlocking(sdhc_transfer_t *transfer);//++
void SysTick_Handler(void);//++
void SDHC_DriverIRQHandler(void);//++
//
static volatile uint32_t g_eventTransferComplete;
static volatile uint32_t g_timeMilliseconds;
uint32_t g_sdhcAdmaTable[8];
sdhc_handle_t g_sdhcHandle;
static volatile uint32_t g_sdhcTransferFailedFlag = 0;
static sd_card_t g_sd;
//
DSTATUS disk_status(BYTE pdrv)
{
	if(pdrv != 1) return STA_NOINIT;
	else return 0;
}

DSTATUS disk_initialize(BYTE pdrv)
{
    if(pdrv != 1) return STA_NOINIT;
    else
    {
    	memset(&g_sd, 0U, sizeof(g_sd));
    	memset(&g_sdhcAdmaTable, 0U, sizeof(g_sdhcAdmaTable));
    	memset(&g_sdhcHandle, 0U, sizeof(g_sdhcHandle));
    	uint32_t temp1;
    	SIM->SCGC3 |= SIM_SCGC3_SDHC_MASK;
    	uint8_t i = 100;
    	SDHC->SYSCTL |= SDHC_SYSCTL_RSTA_MASK;
    	while ((SDHC->SYSCTL & SDHC_SYSCTL_RSTA_MASK))
    	{
    		if (!i) break;
    	    i--;
    	}
    	temp1 = SDHC->PROCTL & ~(SDHC_PROCTL_D3CD_MASK | SDHC_PROCTL_EMODE_MASK | SDHC_PROCTL_DMAS_MASK);
    	temp1 |= (SDHC_PROCTL_EMODE(2) | SDHC_PROCTL_DMAS(2));
    	SDHC->PROCTL = temp1;
    	temp1 = SDHC->WML & ~(SDHC_WML_RDWML_MASK | SDHC_WML_WRWML_MASK);
    	temp1 |= (SDHC_WML_RDWML(0x80)| SDHC_WML_WRWML(0x80));
    	SDHC->WML = temp1;
    	SDHC->SYSCTL |= (SDHC_SYSCTL_PEREN_MASK | SDHC_SYSCTL_HCKEN_MASK | SDHC_SYSCTL_IPGEN_MASK);
    	SysTick_Config(120000);
    	SDHC->IRQSTATEN &= ~((uint32_t)kSDHC_AllInterruptFlags);
    	SDHC->IRQSIGEN &= ~((uint32_t)kSDHC_AllInterruptFlags);
    	temp1 = (SDHC_IRQSTAT_CIE_MASK|SDHC_IRQSTAT_CCE_MASK|SDHC_IRQSTAT_CEBE_MASK|SDHC_IRQSTAT_CTOE_MASK|
    			SDHC_IRQSTAT_CC_MASK|SDHC_IRQSTAT_DTOE_MASK|SDHC_IRQSTAT_DCE_MASK|SDHC_IRQSTAT_DEBE_MASK|SDHC_IRQSTAT_TC_MASK|
				SDHC_IRQSTAT_AC12E_MASK|SDHC_IRQSTAT_DMAE_MASK|SDHC_IRQSTAT_DINT_MASK);
    	SDHC->IRQSTATEN |= temp1;
    	SDHC->IRQSIGEN |= temp1;
        NVIC_EnableIRQ(SDHC_IRQn);
        g_eventTransferComplete = 0;
    	if (kStatus_Success != SD_Init())
    	{
    		SD_SelectCard(false);
    		SIM->SCGC3 &= ~SIM_SCGC3_SDHC_MASK;
    	    memset(&g_sd, 0U, sizeof(g_sd));
    	    memset(&g_sdhcAdmaTable, 0U, sizeof(g_sdhcAdmaTable));
    	    memset(&g_sdhcHandle, 0U, sizeof(g_sdhcHandle));
    	    return STA_NOINIT;
    	}
    	g_eventTransferComplete = 0;
    	return 0;
    }
}

DRESULT disk_read(BYTE pdrv, BYTE *buff, DWORD sector, UINT count)
{
	if(pdrv != 1) return RES_PARERR;
	else
	{
	    uint32_t blockCountOneTime;
	    uint32_t blockLeft;
	    uint32_t blockDone;
	    uint8_t *nextBuffer;
	    if ((count + sector) > g_sd.blockCount) return RES_ERROR;
	    blockLeft = count;
	    blockDone = 0U;
	    while (blockLeft)
	    {
	        if (blockLeft > 0xFFFF)
	        {
	            blockLeft = (blockLeft - 0xFFFF);
	            blockCountOneTime = 0xFFFF;
	        }
	        else
	        {
	            blockCountOneTime = blockLeft;
	            blockLeft = 0U;
	        }
	        nextBuffer = (buff + blockDone * 512);
	        sdhc_transfer_t content = {0};
	        sdhc_command_t command = {0};
	        sdhc_data_t data = {0};
	        if (512 > g_sd.blockSize) return RES_ERROR;
	        if (kStatus_Success != SD_WaitWriteComplete()) return kStatus_SDMMC_WaitWriteCompleteFailed;
	        data.blockSize = 512;
	        data.blockCount = blockCountOneTime;
	        data.rxData = (uint32_t *)nextBuffer;
	        command.index = kSDMMC_ReadMultipleBlock;
	        if (data.blockCount == 1U) command.index = kSDMMC_ReadSingleBlock;
	        command.argument = (sector + blockDone);
	        if (!(g_sd.flags & kSD_SupportHighCapacityFlag)) command.argument *= data.blockSize;
	        command.responseType = kSDHC_ResponseTypeR1;
	        content.command = &command;
	        content.data = &data;
	        if ((kStatus_Success != sdhc_transfer_function(&content)) ||((command.response[0U]) & 0xFFF90008)) return RES_ERROR;
	        if ((data.blockCount > 1) && (!(data.enableAutoCommand12)) && (kStatus_Success != SD_StopTransmission())) return RES_ERROR;
	        blockDone += blockCountOneTime;
	    }
	    return RES_OK;
	}
}

#if _USE_WRITE
DRESULT disk_write(BYTE pdrv, const BYTE *buff, DWORD sector, UINT count)
{
	if(pdrv !=1) return RES_PARERR;
	else
	{
	    uint32_t blockCountOneTime; /* The block count can be wrote in one time sending WRITE_BLOCKS command. */
	    uint32_t blockLeft;         /* Left block count to be wrote. */
	    uint32_t blockDone = 0U;    /* The block count has been wrote. */
	    const uint8_t *nextBuffer;
	    if ((count + sector) > g_sd.blockCount) return RES_ERROR;
	    blockLeft = count;
	    while (blockLeft)
	    {
	        if (blockLeft > 0xFFFF)
	        {
	            blockLeft = (blockLeft - 0xFFFF);
	            blockCountOneTime = 0xFFFF;
	        }
	        else
	        {
	            blockCountOneTime = blockLeft;
	            blockLeft = 0U;
	        }
	        nextBuffer = (buff + blockDone * 512);
	        sdhc_transfer_t content = {0};
	        sdhc_command_t command = {0};
	        sdhc_data_t data = {0};
	        if (512 > g_sd.blockSize) return RES_ERROR;
	        while (!(SDHC->PRSSTAT & (1 << 24))){}
	        data.blockSize = 512;
	        data.blockCount = blockCountOneTime;
	        data.txData = (const uint32_t *)nextBuffer;
	        command.index = kSDMMC_WriteMultipleBlock;
	        if (data.blockCount == 1) command.index = kSDMMC_WriteSingleBlock;
	        command.argument = (sector + blockDone);
	        if (!(g_sd.flags & kSD_SupportHighCapacityFlag)) command.argument *= 512;
	        command.responseType = kSDHC_ResponseTypeR1;
	        content.command = &command;
	        content.data = &data;
	        if ((kStatus_Success != sdhc_transfer_function(&content))||((command.response[0U]) & 0xFFF90008)) return RES_ERROR;
	        if ((data.blockCount > 1U) && (!(data.enableAutoCommand12)) && (kStatus_Success != SD_StopTransmission())) return RES_ERROR;
	        blockDone += blockCountOneTime;
	    }
	    return RES_OK;
	}
}
#endif

#if _USE_IOCTL
DRESULT disk_ioctl(BYTE pdrv, BYTE cmd, void *buff)
{
	if(pdrv != 1) return RES_PARERR;
	else
	{
		DRESULT result = RES_OK;
		switch (cmd)
		{
			case GET_SECTOR_COUNT:
				if (buff) *(uint32_t *)buff = g_sd.blockCount;
		        else result = RES_PARERR;
		        break;
		    case GET_SECTOR_SIZE:
		        if (buff) *(uint32_t *)buff = g_sd.blockSize;
		        else result = RES_PARERR;
		        break;
		    case GET_BLOCK_SIZE:
		        if (buff) *(uint32_t *)buff = g_sd.csd.eraseSectorSize;
		        else result = RES_PARERR;
		        break;
		    case CTRL_SYNC:
		        result = RES_OK;
		        break;
		    default:
		        result = RES_PARERR;
		        break;
		}
		return result;
	}
}
#endif

status_t sdhc_transfer_function(sdhc_transfer_t *content)
{
    status_t error = kStatus_Success;
    do
    {
        error = SDHC_TransferNonBlocking(content);
    }
    while (error == kStatus_SDHC_BusyTransferring);
    uint32_t elapsedTime;
    uint32_t startTime = g_timeMilliseconds;
    do
    {
    	elapsedTime = (g_timeMilliseconds - startTime);
    }
    while ((g_eventTransferComplete == 0) && (elapsedTime < 1000));
    g_eventTransferComplete = 0;
    if (!(elapsedTime < 1000)) error = kStatus_Fail;
    if ((error != kStatus_Success)||(g_sdhcTransferFailedFlag)) error = kStatus_Fail;
    return error;
}

void SDHC_SetSdClock(uint32_t busClock_Hz)
{
    uint32_t divisor;
    uint32_t prescaler;
    uint32_t sysctl;
    divisor = 1;
    prescaler = 2;
    SDHC->SYSCTL &= ~SDHC_SYSCTL_SDCLKEN_MASK;
    while ((120000000 / prescaler / SDHC_MAX_DVS > busClock_Hz) && (prescaler < SDHC_MAX_CLKFS)) prescaler = (prescaler << 1);
    while ((120000000 / prescaler / divisor > busClock_Hz) && (divisor < SDHC_MAX_DVS)) divisor++;
    prescaler = (prescaler >> 1);
    divisor--;
    sysctl = SDHC->SYSCTL;
    sysctl &= ~(SDHC_SYSCTL_DVS_MASK | SDHC_SYSCTL_SDCLKFS_MASK | SDHC_SYSCTL_DTOCV_MASK);
    sysctl |= (SDHC_SYSCTL_DVS(divisor) | SDHC_SYSCTL_SDCLKFS(prescaler) | SDHC_SYSCTL_DTOCV(0xEU));
    SDHC->SYSCTL = sysctl;
    while (!(SDHC->PRSSTAT & SDHC_PRSSTAT_SDSTB_MASK)){}
    SDHC->SYSCTL |= SDHC_SYSCTL_SDCLKEN_MASK;
    return;
}

status_t SDHC_SetAdmaTableConfig(const uint32_t *data, uint32_t dataBytes)
{
    status_t error = kStatus_Success;
    const uint32_t *startAddress;
    uint32_t entries;
    uint32_t i;
    sdhc_adma2_descriptor_t *adma2EntryAddress;

    if ((!data)||(!dataBytes)) error = kStatus_InvalidArgument;
    else
    {
    	startAddress = data;
        entries = ((dataBytes / 0xFFFF) + 1U);
        if (entries > ((8 * sizeof(uint32_t)) / sizeof(sdhc_adma2_descriptor_t))) error = kStatus_OutOfRange;
        else
        {
        	adma2EntryAddress = (sdhc_adma2_descriptor_t *)g_sdhcAdmaTable;
            for (i = 0U; i < entries; i++)
            {
            	if ((dataBytes - sizeof(uint32_t) * (startAddress - data)) <= 0xFFFF)
                {
            		adma2EntryAddress[i].address = startAddress;
                    adma2EntryAddress[i].attribute = ((dataBytes - sizeof(uint32_t) * (startAddress - data)) << 16);
                    adma2EntryAddress[i].attribute |= (1 << 5)|1|(1<<1);
                }
                else
                {
                	adma2EntryAddress[i].address = startAddress;
                    adma2EntryAddress[i].attribute = (((0xFFFF / sizeof(uint32_t)) * sizeof(uint32_t)) << 16);
                    adma2EntryAddress[i].attribute |= ((1 << 5)|1);
                    startAddress += (0xFFFF / sizeof(uint32_t));
                }
            }
            SDHC->DSADDR = 0U;
            SDHC->ADSADDR = (uint32_t)g_sdhcAdmaTable;
        }
    }

    return error;
}

status_t SDHC_TransferNonBlocking(sdhc_transfer_t *transfer)
{
    status_t error = kStatus_Success;
    sdhc_command_t *command = transfer->command;
    sdhc_data_t *data = transfer->data;
    if ((!(transfer->command)) || ((transfer->data) && (transfer->data->blockSize % 4U))) error = kStatus_InvalidArgument;
    else
    {
        if ((SDHC->PRSSTAT & SDHC_PRSSTAT_CIHB_MASK)||(data && (SDHC->PRSSTAT & SDHC_PRSSTAT_CDIHB_MASK))) error = kStatus_SDHC_BusyTransferring;
        else
        {
            if (data && (kStatus_Success != SDHC_SetAdmaTableConfig((data->rxData ? data->rxData : data->txData), (data->blockCount * data->blockSize))))
            {
                error = kStatus_SDHC_PrepareAdmaDescriptorFailed;
            }
            else
            {
            	g_sdhcHandle.command = command;
            	g_sdhcHandle.data = data;
            	g_sdhcHandle.transferredWords = 0U;
                uint32_t flags = 0U;
                sdhc_transfer_config_t sdhcTransferConfig;
                switch (command->responseType)
                {
                	case kSDHC_ResponseTypeNone:
                		break;
                	case kSDHC_ResponseTypeR1: /* Response 1 */
                        flags |= (kSDHC_ResponseLength48Flag|kSDHC_EnableCrcCheckFlag|kSDHC_EnableIndexCheckFlag);
                        break;
                    case kSDHC_ResponseTypeR1b: /* Response 1 with busy */
                        flags |= (SDHC_XFERTYP_RSPTYP(3)| kSDHC_EnableCrcCheckFlag | kSDHC_EnableIndexCheckFlag);
                        break;
                    case kSDHC_ResponseTypeR2: /* Response 2 */
                        flags |= (SDHC_XFERTYP_RSPTYP(1)| kSDHC_EnableCrcCheckFlag);
                        break;
                    case kSDHC_ResponseTypeR3: /* Response 3 */
                        flags |= (kSDHC_ResponseLength48Flag);
                        break;
                    case kSDHC_ResponseTypeR4: /* Response 4 */
                        flags |= (kSDHC_ResponseLength48Flag);
                        break;
                    case kSDHC_ResponseTypeR5: /* Response 5 */
                        flags |= (kSDHC_ResponseLength48Flag | kSDHC_EnableCrcCheckFlag);
                        break;
                    case kSDHC_ResponseTypeR5b: /* Response 5 with busy */
                        flags |= (SDHC_XFERTYP_RSPTYP(3)| kSDHC_EnableCrcCheckFlag | kSDHC_EnableIndexCheckFlag);
                        break;
                    case kSDHC_ResponseTypeR6: /* Response 6 */
                        flags |= (kSDHC_ResponseLength48Flag | kSDHC_EnableCrcCheckFlag | kSDHC_EnableIndexCheckFlag);
                        break;
                    case kSDHC_ResponseTypeR7: /* Response 7 */
                        flags |= (kSDHC_ResponseLength48Flag | kSDHC_EnableCrcCheckFlag | kSDHC_EnableIndexCheckFlag);
                        break;
                     default:
                        break;
                }
                if (command->type == 3) flags |= SDHC_XFERTYP_CMDTYP(3);
                if (data)
                {
                	flags |= SDHC_XFERTYP_DPSEL_MASK|SDHC_XFERTYP_DMAEN_MASK;
                    if (data->rxData) flags |= SDHC_XFERTYP_DTDSEL_MASK;
                    if (data->blockCount > 1U)
                    {
                    	flags |= (SDHC_XFERTYP_MSBSEL_MASK|SDHC_XFERTYP_BCEN_MASK);
                        if (data->enableAutoCommand12) flags |= SDHC_XFERTYP_AC12EN_MASK;
                    }
                    if (data->blockCount > SDHC_MAX_BLOCK_COUNT)
                    {
                    	sdhcTransferConfig.dataBlockSize = data->blockSize;
                        sdhcTransferConfig.dataBlockCount = SDHC_MAX_BLOCK_COUNT;
                        flags &= ~(uint32_t)SDHC_XFERTYP_BCEN_MASK;
                    }
                    else
                    {
                    	sdhcTransferConfig.dataBlockSize = data->blockSize;
                        sdhcTransferConfig.dataBlockCount = data->blockCount;
                    }
                }
                else
                {
                    sdhcTransferConfig.dataBlockSize = 0U;
                    sdhcTransferConfig.dataBlockCount = 0U;
                }
                SDHC->BLKATTR = ((SDHC->BLKATTR & ~(SDHC_BLKATTR_BLKSIZE_MASK|SDHC_BLKATTR_BLKCNT_MASK))|
                		(SDHC_BLKATTR_BLKSIZE(sdhcTransferConfig.dataBlockSize)|SDHC_BLKATTR_BLKCNT(sdhcTransferConfig.dataBlockCount)));
                SDHC->CMDARG = command->argument;
                SDHC->XFERTYP = (((command->index << SDHC_XFERTYP_CMDINX_SHIFT) & SDHC_XFERTYP_CMDINX_MASK)|(flags & (SDHC_XFERTYP_DMAEN_MASK|
                		SDHC_XFERTYP_MSBSEL_MASK|SDHC_XFERTYP_DPSEL_MASK|SDHC_XFERTYP_CMDTYP_MASK|SDHC_XFERTYP_BCEN_MASK|SDHC_XFERTYP_CICEN_MASK|
                        SDHC_XFERTYP_CCCEN_MASK|SDHC_XFERTYP_RSPTYP_MASK|SDHC_XFERTYP_DTDSEL_MASK|SDHC_XFERTYP_AC12EN_MASK)));
            }
        }
    }
    return error;
}

void SDHC_IRQHandler(void)
{
	uint32_t interruptFlags;
	interruptFlags = SDHC->IRQSTAT;
	if (interruptFlags & (kSDHC_CommandErrorFlag|SDHC_IRQSTAT_CC_MASK))
	{
	    if ((interruptFlags & (kSDHC_CommandErrorFlag|SDHC_IRQSTAT_CC_MASK) & kSDHC_CommandErrorFlag) && (!(g_sdhcHandle.data)))
	    {
	    	g_sdhcTransferFailedFlag = 1;
	    	g_eventTransferComplete = 1;
	    }
	    else
	    {
	        uint32_t i;
	        if (g_sdhcHandle.command->responseType != kSDHC_ResponseTypeNone)
	        {
	        	g_sdhcHandle.command->response[0U] = SDHC->CMDRSP[0];
	            if (g_sdhcHandle.command->responseType == kSDHC_ResponseTypeR2)
	            {
	            	g_sdhcHandle.command->response[1U] = SDHC->CMDRSP[1];
	            	g_sdhcHandle.command->response[2U] = SDHC->CMDRSP[2];
	            	g_sdhcHandle.command->response[3U] = SDHC->CMDRSP[3];
	                i = 4U;
	                do
	                {
	                	g_sdhcHandle.command->response[i - 1U] <<= 8U;
	                    if (i > 1U) g_sdhcHandle.command->response[i - 1U] |= ((g_sdhcHandle.command->response[i - 2U] & 0xFF000000U) >> 24U);
	                }
	                while (i--);
	            }
	        }
	        if (!(g_sdhcHandle.data))
	        {
	        	g_sdhcTransferFailedFlag = 0;
	        	g_eventTransferComplete = 1;
	        }
	    }
	}
	if (interruptFlags & kSDHC_DataFlag)
	{
	    if ((!(g_sdhcHandle.data->enableIgnoreError)) && ((interruptFlags & kSDHC_DataFlag) & (kSDHC_DataErrorFlag | SDHC_IRQSTAT_DMAE_MASK)))
	    {
	    	g_sdhcTransferFailedFlag = 1;
	    	g_eventTransferComplete = 1;
	    }
	    else
	    {
	        if ((interruptFlags & kSDHC_DataFlag) & SDHC_IRQSTAT_BRR_MASK)
	        {
	        	uint32_t wordsCanBeRead; /* The words can be read at this time. */
	        	uint32_t totalWords = ((g_sdhcHandle.data->blockCount * g_sdhcHandle.data->blockSize) / sizeof(uint32_t));
	        	if (0x80 >= totalWords) wordsCanBeRead = totalWords;
	        	else if ((0x80 < totalWords) && ((totalWords - g_sdhcHandle.transferredWords) >= 0x80)) wordsCanBeRead = 0x80;
	        	else wordsCanBeRead = (totalWords - g_sdhcHandle.transferredWords);
	        	uint32_t i = 0;
	        	while (i < wordsCanBeRead)
	        	{
	        		g_sdhcHandle.data->rxData[g_sdhcHandle.transferredWords++] = SDHC->DATPORT;
	        	    i++;
	        	}
	        }
	        else if ((interruptFlags & kSDHC_DataFlag) & SDHC_IRQSTAT_BWR_MASK)
	        {
	        	uint32_t wordsCanBeWrote; /* Words can be wrote at this time. */
	        	uint32_t totalWords = ((g_sdhcHandle.data->blockCount * g_sdhcHandle.data->blockSize) / sizeof(uint32_t));
	        	if (0x80 >= totalWords) wordsCanBeWrote = totalWords;
	        	else if ((0x80 < totalWords) && ((totalWords - g_sdhcHandle.transferredWords) >= 0x80)) wordsCanBeWrote = 0x80;
	        	else wordsCanBeWrote = (totalWords - g_sdhcHandle.transferredWords);
	        	uint32_t i = 0;
	        	while (i < wordsCanBeWrote)
	        	{
	        		SDHC->DATPORT = g_sdhcHandle.data->txData[g_sdhcHandle.transferredWords++];
	        	    i++;
	        	}
	        }
	        else if ((interruptFlags & kSDHC_DataFlag) & SDHC_IRQSTAT_TC_MASK)
	        {
	        	g_sdhcTransferFailedFlag = 0;
	        	g_eventTransferComplete = 1;
	        }
	    }

	}
	SDHC->IRQSTAT = interruptFlags;
}

static status_t inline SD_SelectCard(bool isSelected)
{
    sdhc_transfer_t content = {0};
    sdhc_command_t command = {0};
    command.index = kSDMMC_SelectCard;
    if (isSelected)
    {
        command.argument = (g_sd.relativeAddress << 16);
        command.responseType = kSDHC_ResponseTypeR1;
    }
    else
    {
        command.argument = 0U;
        command.responseType = kSDHC_ResponseTypeNone;
    }
    content.command = &command;
    content.data = NULL;
    if ((kStatus_Success != sdhc_transfer_function(&content)) || (command.response[0U] & 0xFFF90008)) return kStatus_SDMMC_TransferFailed;
    return kStatus_Success;
}

static status_t inline SD_WaitWriteComplete(void)
{
    sdhc_transfer_t content = {0};
    sdhc_command_t command = {0};
    command.index = kSDMMC_SendStatus;
    command.argument = (g_sd.relativeAddress << 16);
    command.responseType = kSDHC_ResponseTypeR1;
    do
    {
        content.command = &command;
        content.data = 0U;
        if ((kStatus_Success != sdhc_transfer_function(&content)) || (command.response[0U] & 0xFFF90008)) return kStatus_SDMMC_TransferFailed;
        if ((command.response[0U] & (1 << 8))&&(SDMMC_R1_CURRENT_STATE(command.response[0U]) != 7)) break;
    }
    while (true);
    return kStatus_Success;
}

static status_t inline SD_SendApplicationCmd(void)
{
    sdhc_transfer_t content = {0};
    sdhc_command_t command = {0};
    command.index = kSDMMC_ApplicationCommand;
    command.argument = 0U;
    command.argument = (g_sd.relativeAddress << 16);
    command.responseType = kSDHC_ResponseTypeR1;
    content.command = &command;
    content.data = 0U;
    if ((kStatus_Success != sdhc_transfer_function(&content)) || (command.response[0U] & 0xFFF90008)) return kStatus_SDMMC_TransferFailed;
    if (!(command.response[0U] & (1 << 5))) return kStatus_SDMMC_CardNotSupport;
    return kStatus_Success;
}

static status_t inline SD_GoIdle(void)
{
    sdhc_transfer_t content = {0};
    sdhc_command_t command = {0};
    command.index = kSDMMC_GoIdleState;
    content.command = &command;
    content.data = 0U;
    if (kStatus_Success != sdhc_transfer_function(&content)) return kStatus_SDMMC_TransferFailed;
    return kStatus_Success;
}

static status_t inline SD_StopTransmission(void)
{
    sdhc_transfer_t content = {0};
    sdhc_command_t command = {0};
    command.index = kSDMMC_StopTransmission;
    command.argument = 0U;
    command.type = 3;
    command.responseType = kSDHC_ResponseTypeR1b;
    content.command = &command;
    content.data = 0U;
    if ((kStatus_Success != sdhc_transfer_function(&content)) || (command.response[0U] & 0xFFF90008)) return kStatus_SDMMC_TransferFailed;
    return kStatus_Success;
}

static status_t inline SD_SetBlockSize(uint32_t blockSize)
{
    sdhc_transfer_t content = {0};
    sdhc_command_t command = {0};
    command.index = kSDMMC_SetBlockLength;
    command.argument = blockSize;
    command.responseType = kSDHC_ResponseTypeR1;
    content.command = &command;
    content.data = 0U;
    if ((kStatus_Success != sdhc_transfer_function(&content)) || (command.response[0U] & 0xFFF90008)) return kStatus_SDMMC_TransferFailed;
    return kStatus_Success;
}

static status_t SD_SendRca(void)
{
    sdhc_transfer_t content = {0};
    sdhc_command_t command = {0};
    command.index = 3;
    command.argument = 0U;
    command.responseType = kSDHC_ResponseTypeR6;
    content.command = &command;
    content.data = NULL;
    if (kStatus_Success == sdhc_transfer_function(&content))
    {
    	g_sd.relativeAddress = (command.response[0U] >> 16U);
        return kStatus_Success;
    }
    return kStatus_SDMMC_TransferFailed;
}

static status_t SD_SwitchFunction(uint32_t mode, uint32_t *status)
{
    sdhc_transfer_t content = {0};
    sdhc_command_t command = {0};
    sdhc_data_t data = {0};
    command.index = 6;
    command.argument = (mode << 31U | 0x00FFFFFFU);
    command.argument &= ~((uint32_t)(0xFU));
    command.argument |= 1;
    command.responseType = kSDHC_ResponseTypeR1;
    data.blockSize = 64U;
    data.blockCount = 1U;
    data.rxData = status;
    if (kStatus_Success != SD_SetBlockSize(data.blockSize)) return kStatus_SDMMC_SetCardBlockSizeFailed;
    content.command = &command;
    content.data = &data;
    if ((kStatus_Success != sdhc_transfer_function(&content))||((command.response[0U]) & 0xFFF90008))
    	return kStatus_SDMMC_TransferFailed;
    return kStatus_Success;
}

static void SD_DecodeScr(uint32_t *rawScr)
{
    sd_scr_t *scr = &(g_sd.scr);
    scr->scrStructure = (uint8_t)((rawScr[0U] & 0xF0000000U) >> 28U);
    scr->sdSpecification = (uint8_t)((rawScr[0U] & 0xF000000U) >> 24U);
    if ((uint8_t)((rawScr[0U] & 0x800000U) >> 23U)) scr->flags |= kSD_ScrDataStatusAfterErase;
    scr->sdSecurity = (uint8_t)((rawScr[0U] & 0x700000U) >> 20U);
    scr->sdBusWidths = (uint8_t)((rawScr[0U] & 0xF0000U) >> 16U);
    if ((uint8_t)((rawScr[0U] & 0x8000U) >> 15U)) scr->flags |= kSD_ScrSdSpecification3;
    scr->extendedSecurity = (uint8_t)((rawScr[0U] & 0x7800U) >> 10U);
    scr->commandSupport = (uint8_t)(rawScr[0U] & 0x3U);
    scr->reservedForManufacturer = rawScr[1U];
    switch (scr->sdSpecification)
    {
        case 0U:
        	g_sd.version = kSD_SpecificationVersion1_0;
            break;
        case 1U:
        	g_sd.version = kSD_SpecificationVersion1_1;
            break;
        case 2U:
        	g_sd.version = kSD_SpecificationVersion2_0;
            if (g_sd.scr.flags & kSD_ScrSdSpecification3) g_sd.version = kSD_SpecificationVersion3_0;
            break;
        default:
            break;
    }
    if (g_sd.scr.sdBusWidths & kSD_DataBusWidth4Bit) g_sd.flags |= kSD_Support4BitWidthFlag;
}

static status_t SD_SendScr(void)
{
    sdhc_transfer_t content = {0};
    sdhc_command_t command = {0};
    sdhc_data_t data = {0};
    uint32_t rawScr[2U] = {0U};
    if (kStatus_Success != SD_SendApplicationCmd()) return kStatus_SDMMC_SendApplicationCommandFailed;
    command.index = 51;
    command.responseType = kSDHC_ResponseTypeR1;
    command.argument = 0U;
    data.blockSize = 8U;
    data.blockCount = 1U;
    data.rxData = rawScr;
    content.data = &data;
    content.command = &command;
    if ((kStatus_Success != sdhc_transfer_function(&content))||((command.response[0U]) & 0xFFF90008))
    	return kStatus_SDMMC_TransferFailed;
    rawScr[0U] = SWAP_WORD_BYTE_SEQUENCE(rawScr[0U]);
    rawScr[1U] = SWAP_WORD_BYTE_SEQUENCE(rawScr[1U]);
    memcpy(g_sd.rawScr, rawScr, sizeof(g_sd.rawScr));
    SD_DecodeScr(rawScr);
    return kStatus_Success;
}

static status_t SD_SwitchHighspeed(void)
{
    uint32_t functionStatus[16U] = {0U};
    if ((g_sd.version < kSD_SpecificationVersion1_0) || (!(g_sd.csd.cardCommandClass & (1 << 10)))) return kStatus_SDMMC_CardNotSupport;
    if (kStatus_Success != SD_SwitchFunction(0, functionStatus)) return kStatus_SDMMC_SwitchFailed;
    functionStatus[3U] = SWAP_WORD_BYTE_SEQUENCE(functionStatus[3U]);
    functionStatus[4U] = SWAP_WORD_BYTE_SEQUENCE(functionStatus[4U]);
    if ((!(functionStatus[3U] & 0x10000U)) || ((functionStatus[4U] & 0x0f000000U) == 0x0F000000U)) return kStatus_SDMMC_CardNotSupport;
    if (kStatus_Success != SD_SwitchFunction(1, functionStatus)) return kStatus_SDMMC_SwitchFailed;
    functionStatus[4U] = SWAP_WORD_BYTE_SEQUENCE(functionStatus[4U]);
    if ((functionStatus[4U] & 0x0f000000U) != 0x01000000U) return kStatus_Fail;
    return kStatus_Success;
}

static status_t SD_SetDataBusWidth(sd_data_bus_width_t width)
{
    sdhc_transfer_t content = {0};
    sdhc_command_t command = {0};
    if (kStatus_Success != SD_SendApplicationCmd())return kStatus_SDMMC_SendApplicationCommandFailed;
    command.index = 6;
    command.responseType = kSDHC_ResponseTypeR1;
    switch (width)
    {
        case kSD_DataBusWidth1Bit:
            command.argument = 0U;
            break;
        case kSD_DataBusWidth4Bit:
            command.argument = 2U;
            break;
        default:
            return kStatus_InvalidArgument;
    }
    content.command = &command;
    content.data = NULL;
    if ((kStatus_Success != sdhc_transfer_function(&content)) ||((command.response[0U]) & 0xFFF90008))
    	return kStatus_SDMMC_TransferFailed;
    return kStatus_Success;
}

static void SD_DecodeCsd(uint32_t *rawCsd)
{
    sd_csd_t *csd = &(g_sd.csd);
    csd->csdStructure = (uint8_t)((rawCsd[3U] & 0xC0000000U) >> 30U);
    csd->dataReadAccessTime1 = (uint8_t)((rawCsd[3U] & 0xFF0000U) >> 16U);
    csd->dataReadAccessTime2 = (uint8_t)((rawCsd[3U] & 0xFF00U) >> 8U);
    csd->transferSpeed = (uint8_t)(rawCsd[3U] & 0xFFU);
    csd->cardCommandClass = (uint16_t)((rawCsd[2U] & 0xFFF00000U) >> 20U);
    csd->readBlockLength = (uint8_t)((rawCsd[2U] & 0xF0000U) >> 16U);
    if (rawCsd[2U] & 0x8000U) csd->flags |= kSD_CsdReadBlockPartialFlag;
    if (rawCsd[2U] & 0x4000U) csd->flags |= kSD_CsdReadBlockPartialFlag;
    if (rawCsd[2U] & 0x2000U) csd->flags |= kSD_CsdReadBlockMisalignFlag;
    if (rawCsd[2U] & 0x1000U) csd->flags |= kSD_CsdDsrImplementedFlag;
    switch (csd->csdStructure)
    {
        case 0:
            csd->deviceSize = (uint32_t)((rawCsd[2U] & 0x3FFU) << 2U);
            csd->deviceSize |= (uint32_t)((rawCsd[1U] & 0xC0000000U) >> 30U);
            csd->readCurrentVddMin = (uint8_t)((rawCsd[1U] & 0x38000000U) >> 27U);
            csd->readCurrentVddMax = (uint8_t)((rawCsd[1U] & 0x7000000U) >> 24U);
            csd->writeCurrentVddMin = (uint8_t)((rawCsd[1U] & 0xE00000U) >> 20U);
            csd->writeCurrentVddMax = (uint8_t)((rawCsd[1U] & 0x1C0000U) >> 18U);
            csd->deviceSizeMultiplier = (uint8_t)((rawCsd[1U] & 0x38000U) >> 15U);
            g_sd.blockCount = ((csd->deviceSize + 1U) << (csd->deviceSizeMultiplier + 2U));
            g_sd.blockSize = (1U << (csd->readBlockLength));
            if (g_sd.blockSize != 512)
            {
            	g_sd.blockCount = (g_sd.blockCount * g_sd.blockSize);
            	g_sd.blockSize = 512;
            	g_sd.blockCount = (g_sd.blockCount / g_sd.blockSize);
            }
            break;
        case 1:
        	g_sd.blockSize = 512;
            csd->deviceSize = (uint32_t)((rawCsd[2U] & 0x3FU) << 16U);
            csd->deviceSize |= (uint32_t)((rawCsd[1U] & 0xFFFF0000U) >> 16U);
            if (csd->deviceSize >= 0xFFFFU) g_sd.flags |= kSD_SupportSdxcFlag;
            g_sd.blockCount = ((csd->deviceSize + 1U) * 1024U);
            break;
        default:
            break;
    }
    if ((uint8_t)((rawCsd[1U] & 0x4000U) >> 14U)) csd->flags |= kSD_CsdEraseBlockEnabledFlag;
    csd->eraseSectorSize = (uint8_t)((rawCsd[1U] & 0x3F80U) >> 7U);
    csd->writeProtectGroupSize = (uint8_t)(rawCsd[1U] & 0x7FU);
    if ((uint8_t)(rawCsd[0U] & 0x80000000U)) csd->flags |= kSD_CsdWriteProtectGroupEnabledFlag;
    csd->writeSpeedFactor = (uint8_t)((rawCsd[0U] & 0x1C000000U) >> 26U);
    csd->writeBlockLength = (uint8_t)((rawCsd[0U] & 0x3C00000U) >> 22U);
    if ((uint8_t)((rawCsd[0U] & 0x200000U) >> 21U)) csd->flags |= kSD_CsdWriteBlockPartialFlag;
    if ((uint8_t)((rawCsd[0U] & 0x8000U) >> 15U)) csd->flags |= kSD_CsdFileFormatGroupFlag;
    if ((uint8_t)((rawCsd[0U] & 0x4000U) >> 14U)) csd->flags |= kSD_CsdCopyFlag;
    if ((uint8_t)((rawCsd[0U] & 0x2000U) >> 13U)) csd->flags |= kSD_CsdPermanentWriteProtectFlag;
    if ((uint8_t)((rawCsd[0U] & 0x1000U) >> 12U)) csd->flags |= kSD_CsdTemporaryWriteProtectFlag;
    csd->fileFormat = (uint8_t)((rawCsd[0U] & 0xC00U) >> 10U);
}

static status_t SD_SendCsd(void)
{
    sdhc_transfer_t content = {0};
    sdhc_command_t command = {0};
    command.index = kSDMMC_SendCsd;
    command.argument = (g_sd.relativeAddress << 16U);
    command.responseType = kSDHC_ResponseTypeR2;
    content.command = &command;
    content.data = NULL;
    if (kStatus_Success == sdhc_transfer_function(&content))
    {
        memcpy(g_sd.rawCsd, command.response, sizeof(g_sd.rawCsd));
        SD_DecodeCsd(command.response);
        return kStatus_Success;
    }
    return kStatus_SDMMC_TransferFailed;
}

static void SD_DecodeCid(uint32_t *rawCid)
{
    sd_cid_t *cid = &(g_sd.cid);
    cid->manufacturerID = (uint8_t)((rawCid[3U] & 0xFF000000U) >> 24U);
    cid->applicationID = (uint16_t)((rawCid[3U] & 0xFFFF00U) >> 8U);
    cid->productName[0U] = (uint8_t)((rawCid[3U] & 0xFFU));
    cid->productName[1U] = (uint8_t)((rawCid[2U] & 0xFF000000U) >> 24U);
    cid->productName[2U] = (uint8_t)((rawCid[2U] & 0xFF0000U) >> 16U);
    cid->productName[3U] = (uint8_t)((rawCid[2U] & 0xFF00U) >> 8U);
    cid->productName[4U] = (uint8_t)((rawCid[2U] & 0xFFU));
    cid->productVersion = (uint8_t)((rawCid[1U] & 0xFF000000U) >> 24U);
    cid->productSerialNumber = (uint32_t)((rawCid[1U] & 0xFFFFFFU) << 8U);
    cid->productSerialNumber |= (uint32_t)((rawCid[0U] & 0xFF000000U) >> 24U);
    cid->manufacturerData = (uint16_t)((rawCid[0U] & 0xFFF00U) >> 8U);
}

static status_t SD_AllSendCid(void)
{
    sdhc_transfer_t content = {0};
    sdhc_command_t command = {0};
    command.index = kSDMMC_AllSendCid;
    command.argument = 0U;
    command.responseType = kSDHC_ResponseTypeR2;
    content.command = &command;
    content.data = NULL;
    if (kStatus_Success == sdhc_transfer_function(&content))
    {
        memcpy(g_sd.rawCid, command.response, sizeof(g_sd.rawCid));
        SD_DecodeCid(command.response);
        return kStatus_Success;
    }
    return kStatus_SDMMC_TransferFailed;
}

static status_t SD_ApplicationSendOperationCondition(uint32_t argument)
{
    sdhc_transfer_t content = {0};
    sdhc_command_t command = {0};
    status_t error;
    uint32_t i = 1000;
    command.index = 41;
    command.argument = argument;
    command.responseType = kSDHC_ResponseTypeR3;

    while (i--)
    {
        if (kStatus_Success != SD_SendApplicationCmd()) return kStatus_SDMMC_SendApplicationCommandFailed;
        content.command = &command;
        content.data = NULL;
        if (kStatus_Success != sdhc_transfer_function(&content)) return kStatus_SDMMC_TransferFailed;
        if (command.response[0U] & kSD_OcrPowerUpBusyFlag)
        {
            if (command.response[0U] & kSD_OcrCardCapacitySupportFlag) g_sd.flags |= kSD_SupportHighCapacityFlag;
            error = kStatus_Success;
            g_sd.ocr = command.response[0U];
            break;
        }
        error = kStatus_Timeout;
    }
    return error;
}

status_t SD_Init(void)
{
    uint32_t applicationCommand41Argument = 0;
    status_t error = kStatus_Success;
    SDHC_SetSdClock(SDMMC_CLOCK_400KHZ);
    uint8_t i = 100;
	SDHC->SYSCTL |= SDHC_SYSCTL_INITA_MASK;
    while (!(SDHC->SYSCTL & SDHC_SYSCTL_INITA_MASK))
    {
        if (!i) break;
        i--;
    }
    if (kStatus_Success != SD_GoIdle()) return kStatus_SDMMC_GoIdleFailed;
    applicationCommand41Argument = (kSD_OcrVdd32_33Flag |kSD_OcrVdd33_34Flag);
    sdhc_transfer_t content = {0};
    sdhc_command_t command = {0};
    command.index = 8;
    command.argument = 0x1AAU;
    command.responseType = kSDHC_ResponseTypeR7;
    content.command = &command;
    content.data = NULL;
    if (kStatus_Success != sdhc_transfer_function(&content))
    {
    	if (kStatus_Success != SD_GoIdle()) return kStatus_SDMMC_GoIdleFailed;
    }
    else
    {
    	if ((command.response[0U] & 0xFFU) != 0xAAU)
        {
    		if (kStatus_Success != SD_GoIdle()) return kStatus_SDMMC_GoIdleFailed;
        }
        else
        {
        	applicationCommand41Argument |= kSD_OcrHostCapacitySupportFlag;
        	g_sd.flags |= kSD_SupportSdhcFlag;
        }
    }
    error = SD_ApplicationSendOperationCondition(applicationCommand41Argument);
    if (error == kStatus_Timeout) return kStatus_SDMMC_NotSupportYet;
    else if (error != kStatus_Success) return kStatus_SDMMC_HandShakeOperationConditionFailed;
    else{}
    if (kStatus_Success != SD_AllSendCid()) return kStatus_SDMMC_AllSendCidFailed;
    if (kStatus_Success != SD_SendRca()) return kStatus_SDMMC_SendRelativeAddressFailed;
    if (kStatus_Success != SD_SendCsd()) return kStatus_SDMMC_SendCsdFailed;
    if (kStatus_Success != SD_SelectCard(true)) return kStatus_SDMMC_SelectCardFailed;
    if (kStatus_Success != SD_SendScr()) return kStatus_SDMMC_SendScrFailed;
    SDHC_SetSdClock(SD_CLOCK_25MHZ);
    if (g_sd.flags & kSD_Support4BitWidthFlag)
    {
        if (kStatus_Success != SD_SetDataBusWidth(kSD_DataBusWidth4Bit)) return kStatus_SDMMC_SetDataBusWidthFailed;
        SDHC->PROCTL = ((SDHC->PROCTL & ~SDHC_PROCTL_DTW_MASK) | SDHC_PROCTL_DTW(1));
    }
    error = SD_SwitchHighspeed();
    if ((error != kStatus_Success) && (kStatus_SDMMC_CardNotSupport != error)) return kStatus_SDMMC_SwitchHighSpeedFailed;
    else if (error == kStatus_Success) SDHC_SetSdClock(SD_CLOCK_50MHZ);
    else  error = kStatus_Success;
    if (SD_SetBlockSize(512)) error = kStatus_SDMMC_SetCardBlockSizeFailed;
    return kStatus_Success;
}
void SysTick_Handler(void)
{
    g_timeMilliseconds++;
}
DWORD get_fattime (void)
{
	uint32_t temp1 = RTC->TSR;
	datetime_t dtime;
	rtc_seconds_to_datetime(temp1, &dtime);
	return (((dtime.date.year - 1980)<<25)|(dtime.date.month<<21)|(dtime.date.day<<16)|(dtime.time.hour<<11)|(dtime.time.minute<<5)|(dtime.time.second>>1));
}

#include "arch/sys_arch.h"
#include "lwip/opt.h"
#include "lwip/debug.h"
#include "lwip/def.h"
#include "lwip/sys.h"
#include "lwip/mem.h"
#include "lwip/stats.h"
#include "lwip/init.h"
#include <stdarg.h>
//
#if LWIP_SOCKET_SET_ERRNO
	#ifndef errno
int errno = 0;
	#endif
#endif
static volatile uint32_t time_now = 0;
//
void sys_assert(char *pcMessage)
{
	uart_print_string(UART0, "\n\r");
	uart_print_string(UART0, pcMessage);
}
u32_t lwip_rand(void)
{
	return(get_random());
}
u32_t sys_now(void)
{
	return (u32_t)time_now;
}
sys_prot_t sys_arch_protect(void)
{
	__disable_irq;
	return 0;
}
void sys_arch_unprotect(sys_prot_t xValue)
{
	__enable_irq();
}

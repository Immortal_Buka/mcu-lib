//ver190402
#include "system.h"
#include "std_system.h"
//
const uint32_t flash_config[] __attribute__ ((section (".flash_config"))) =
{
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFE
};
//
void sys_init(void)
{
	__disable_irq();//cpsid   i
	//disadle watchdog
	WDOG->UNLOCK = 0xC520;//key1
	WDOG->UNLOCK = 0xD928;//Key2
	WDOG->STCTRLH = WDOG_STCTRLH_BYTESEL(0x00)|WDOG_STCTRLH_WAITEN_MASK|WDOG_STCTRLH_STOPEN_MASK|WDOG_STCTRLH_ALLOWUPDATE_MASK|
			WDOG_STCTRLH_CLKSRC_MASK|0x0100;
	//clk = 120MHz
	SIM->CLKDIV1 = 0x01140000;
	//Core/system clocks = 120MHz
	//Bus clock = 60MHz
	//FlexBus clock = 60MHz
	//Flash clock = 24MHz
	//pllclk = 120MHz
	OSC->CR = OSC_CR_ERCLKEN_MASK;
	MCG->C2 |= MCG_C2_RANGE(2);
	MCG->C1 = MCG_C1_CLKS(2);
	while ((MCG->S & (MCG_S_IREFST_MASK|MCG_S_CLKST_MASK)) != (MCG_S_IREFST(0)|MCG_S_CLKST(2))){};
	while (MCG->S & MCG_S_PLLST_MASK){};
	MCG->C5 = MCG_C5_PRDIV0(0x13);
	MCG->C6 = MCG_C6_VDIV0(0x18);
	MCG->C5 |= MCG_C5_PLLCLKEN0_MASK;
	while (!(MCG->S & MCG_S_LOCK0_MASK)){};
	MCG->C6 |= MCG_C6_PLLS_MASK;
	while (!(MCG->S & MCG_S_PLLST_MASK)){};
	MCG->C1 = MCG_C1_CLKS(0);
	while (((MCG->S & MCG_S_CLKST_MASK) >> MCG_S_CLKST_SHIFT) != 3){};
	MCG->SC = 0;
	while (((MCG->S & MCG_S_IRCST_MASK) >> MCG_S_IRCST_SHIFT) != 0){};
	//init_data_bss();
	__enable_irq();
	mpu_disabled();
}
void sda_uart_init(void)
{
//115200, 8-n-1
	SIM->SCGC4 |= SIM_SCGC4_UART0_MASK;
	//UART0->BDH = 0;
	UART0->BDL = 65;
	UART0->C4 = 4;//BRFA
	//UART0->C1 = 0;SetBitCountPerChar,SetParityMode
	//UART0->BDH = 0;SetStopBitCount
	UART0->C2 = (UART_C2_TE_MASK|UART_C2_RE_MASK);
}
void uart_print_char(void* uart, uint8_t data)
{
	while (!(((UART_Type*)uart)->S1 & UART_S1_TDRE_MASK));
	((UART_Type*)uart)->D = data;
}
void uart_scan_string(UART_Type* uart, uint8_t *data, uint8_t rxSize)
{
    for(uint8_t i=0; i<rxSize; i++)
    {
    	while (!(uart->S1 & UART_S1_RDRF_MASK)){};
    	data[i] = uart->D;
    }
}
void frdm_led_rgb(led name, led_state state)
{
	if(name & red) switch (state)
	{
		case off: PTB->PSOR = (1 << 22); break;
		case on: PTB->PCOR = (1 << 22); break;
		case toggle: PTB->PTOR = (1 << 22); break;
	}
	if(name & green) switch (state)
	{
		case off: PTE->PSOR = (1 << 26); break;
		case on: PTE->PCOR = (1 << 26); break;
		case toggle: PTE->PTOR = (1 << 26); break;
	}
	if(name & blue) switch (state)
	{
		case off: PTB->PSOR = (1 << 21); break;
		case on: PTB->PCOR = (1 << 21); break;
		case toggle: PTB->PTOR = (1 << 21); break;
	}
}
void error_signal (void)
{
	frdm_led_rgb(blue, on);
	//uart_print_string("\n\rError!");
	while(1){};
}
void project_def_handler(void)
{
	frdm_led_rgb(green, off);
	frdm_led_rgb(red, on);
	while(1){}
}
inline void mpu_disabled(void)
{
	SYSMPU->CESR &= ~SYSMPU_CESR_VLD_MASK;
}
uint32_t rtc_datetime_to_seconds(datetime_t* datetime)
{
    uint16_t monthDays[] = {0U, 0U, 31U, 59U, 90U, 120U, 151U, 181U, 212U, 243U, 273U, 304U, 334U};
    uint32_t seconds;
    seconds = (datetime->date.year - 1970U) * 365;
    seconds += ((datetime->date.year / 4) - (1970U / 4));
    seconds += monthDays[datetime->date.month];
    seconds += (datetime->date.day - 1);
    if ((!(datetime->date.year & 3U)) && (datetime->date.month <= 2U)) seconds--;
    seconds = (seconds * 86400) + (datetime->time.hour * 3600) + (datetime->time.minute * 60) + datetime->time.second;
    return seconds;
}
void rtc_seconds_to_datetime(uint32_t seconds, datetime_t* datetime)
{
    uint32_t x;
    uint32_t secondsRemaining, days;
    uint16_t daysInYear;
    uint8_t daysPerMonth[] = {0U, 31U, 28U, 31U, 30U, 31U, 30U, 31U, 31U, 30U, 31U, 30U, 31U};
    secondsRemaining = seconds;
    days = secondsRemaining / 86400 + 1;
    secondsRemaining = secondsRemaining % 86400;
    datetime->time.hour = secondsRemaining / 3600;
    secondsRemaining = secondsRemaining % 3600;
    datetime->time.minute = secondsRemaining / 60U;
    datetime->time.second = secondsRemaining % 60;
    daysInYear = 365;
    datetime->date.year = 1970;
    while (days > daysInYear)
    {
        days -= daysInYear;
        datetime->date.year++;
        if (datetime->date.year & 3U) daysInYear = 365;
        else daysInYear = 365 + 1;
    }
    if (!(datetime->date.year & 3U)) daysPerMonth[2] = 29U;
    for (x = 1U; x <= 12U; x++)
    {
        if (days <= daysPerMonth[x])
        {
            datetime->date.month = x;
            break;
        }
        else days -= daysPerMonth[x];
    }
    datetime->date.day = days;
}
void rnga_init(void)
{
	SIM->SCGC6 |= SIM_SCGC6_RNGA_MASK;
	RNG->CR = RNG_CR_GO_MASK;
}
uint8_t rtc_init(void)
{
	SIM->SCGC6 |= SIM_SCGC6_RTC_MASK;
	if((RTC->SR & RTC_SR_TCE_MASK) != RTC_SR_TCE_MASK)
	{
		RTC->TSR = 1;
		RTC->IER = 0;
		RTC->CR = RTC_CR_OSCE_MASK;
		delay(120000);
		RTC->SR = RTC_SR_TCE_MASK;
		return 0;
	}
	else return 1;
}
void rtc_set_time(uint32_t time)
{
	RTC->SR = 0;
	RTC->TSR = time;
	RTC->SR = RTC_SR_TCE_MASK;
}
void rtc_get_time(datetime_t* date_time)
{
	uint32_t temptime = RTC->TSR;
	rtc_seconds_to_datetime(temptime, date_time);
}
uint32_t get_random(void)
{
	while((RNG->SR & (1<<8)) != (1<<8)){};//OREG_LVL
	return (RNG->OR);
}
void init_common_gpio(void)
{
	SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK|SIM_SCGC5_PORTB_MASK|SIM_SCGC5_PORTC_MASK|SIM_SCGC5_PORTD_MASK|SIM_SCGC5_PORTE_MASK;
	PORTA->PCR[4] = (1<<8)|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK;//sw2
	PORTA->PCR[5] = 4<<8;//RMII0_RXER
	PORTA->PCR[12] = 4<<8;//RMII0_RXD1
	PORTA->PCR[13] = 4<<8;//RMII0_RXD0
	PORTA->PCR[14] = 4<<8;//RMII0_CRS_DV
	PORTA->PCR[15] = 4<<8;//RMII0_TXEN
	PORTA->PCR[16] = 4<<8;//RMII0_TXD0
	PORTA->PCR[17] = 4<<8;//RMII0_TXD1
	PORTB->PCR[0] = (4<<8)|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK|PORT_PCR_ODE_MASK;//RMII0_MDIO
	PORTB->PCR[1] = 4<<8;//RMII0_MDC
	PORTB->PCR[16] = 3<<8;//uart0_rx
	PORTB->PCR[17] = 3<<8;//uart0_tx
	PORTB->PCR[21] = 1<<8;//blue led
	PORTB->PCR[22] = 1<<8;//red led
	PORTC->PCR[6] = (1<<8)|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK;//sw3
	PORTC->PCR[16] = 4<<8;//ENET0_1588_TMR0
	PORTC->PCR[17] = 4<<8;//ENET0_1588_TMR1
	PORTC->PCR[18] = 4<<8;//ENET0_1588_TMR2
	PORTE->PCR[0] = (4<<8)|PORT_PCR_DSE_MASK|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK;//uSD_D1
	PORTE->PCR[1] = (4<<8)|PORT_PCR_DSE_MASK|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK;//uSD_D0
	PORTE->PCR[2] = (4<<8)|PORT_PCR_DSE_MASK|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK;//uSD_DCLK
	PORTE->PCR[3] = (4<<8)|PORT_PCR_DSE_MASK|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK;//uSD_CMD
	PORTE->PCR[4] = (4<<8)|PORT_PCR_DSE_MASK|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK;//uSD_D3
	PORTE->PCR[5] = (4<<8)|PORT_PCR_DSE_MASK|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK;//uSD_D2
	PORTE->PCR[6] = (1<<8)|PORT_PCR_PE_MASK;//uSD_detect
	PORTE->PCR[26] = 1<<8;//greeen led
	//io
	PTB->PDDR |= (1<<21)|(1<<22);
	PTE->PDDR |= 1<<26;
}
uint8_t adc16_calibration(void)
{
    uint8_t bHWTrigger = 0;
    if (0!= (ADC_SC2_ADTRG_MASK & ADC0->SC2))
    {
        bHWTrigger = 1;
        ADC0->SC2 &= ~ADC_SC2_ADTRG_MASK;
    }
    ADC0->SC3 |= ADC_SC3_CAL_MASK|ADC_SC3_CALF_MASK;
    while (!(ADC0->SC1[0] & ADC_SC1_COCO_MASK))
    	if (0 != (ADC0->SC3 & ADC_SC3_CALF_MASK)) return 1;
    if (bHWTrigger) ADC0->SC2 |= ADC_SC2_ADTRG_MASK;
    if (0 != (ADC0->SC3 & ADC_SC3_CALF_MASK)) return 1;
    ADC0->PG = 0x8000|((ADC0->CLP0 + ADC0->CLP1 + ADC0->CLP2 + ADC0->CLP3 + ADC0->CLP4 + ADC0->CLPS) >> 1);
    ADC0->MG = 0x8000|((ADC0->CLM0 + ADC0->CLM1 + ADC0->CLM2 + ADC0->CLM3 + ADC0->CLM4 + ADC0->CLMS) >> 1);
    return 0;
}

//ver190715
void DAC_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void M0APP_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void DMA_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void ETHERNET_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void SDIO_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void LCD_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void USB0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void USB1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void SCT_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void RITIMER_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void WKT_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER2_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER3_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void MCPWM_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void ADC0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void I2C0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void I2C1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void SPI_INT_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void ADC1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void SSP0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void SSP1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void USART0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void UART1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void USART2_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void USART3_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void I2S0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void I2S1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void SGPIO_INT_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void PIN_INT0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void PIN_INT1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void PIN_INT2_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void PIN_INT3_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void PIN_INT4_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void PIN_INT5_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void PIN_INT6_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void PIN_INT7_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void GINT0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void GINT1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void EVENTROUTER_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void C_CAN1_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void ADCHS_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void ATIMER_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void RTC_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void WWDT_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void M0SUB_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void C_CAN0_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void QEI_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void FLASHEEPROM_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
void SPIFI_IRQHandler(void) __attribute__ ((weak, alias("Default_Handler")));
//
const pFunc lpc3667_m4[] __attribute__ ((section(".isr_vector_dev"))) =
{
	DAC_IRQHandler,
	M0APP_IRQHandler,
	DMA_IRQHandler,
	0,
	FLASHEEPROM_IRQHandler,
	ETHERNET_IRQHandler,
	SDIO_IRQHandler,
	LCD_IRQHandler,
	USB0_IRQHandler,
	USB1_IRQHandler,
	SCT_IRQHandler,
	RITIMER_IRQHandler,
	TIMER0_IRQHandler,
	TIMER1_IRQHandler,
	TIMER2_IRQHandler,
	TIMER3_IRQHandler,
	MCPWM_IRQHandler,
	ADC0_IRQHandler,
	I2C0_IRQHandler,
	I2C1_IRQHandler,
	SPI_INT_IRQHandler,
	ADC1_IRQHandler,
	SSP0_IRQHandler, 
	SSP1_IRQHandler,
	USART0_IRQHandler,
	UART1_IRQHandler,
	USART2_IRQHandler,
	USART3_IRQHandler,
	I2S0_IRQHandler,
	I2S1_IRQHandler,
	SPIFI_IRQHandler,
	SGPIO_INT_IRQHandler,
	PIN_INT0_IRQHandler,
	PIN_INT1_IRQHandler,
	PIN_INT2_IRQHandler,
	PIN_INT3_IRQHandler,
	PIN_INT4_IRQHandler,
	PIN_INT5_IRQHandler,
	PIN_INT6_IRQHandler,
	PIN_INT7_IRQHandler,
	GINT0_IRQHandler,
	GINT1_IRQHandler,
	EVENTROUTER_IRQHandler,
	C_CAN1_IRQHandler,
	0,
	ADCHS_IRQHandler,
	ATIMER_IRQHandler,
	RTC_IRQHandler,
	0,
	WWDT_IRQHandler,
	M0SUB_IRQHandler,
	C_CAN0_IRQHandler,
	QEI_IRQHandler,
};

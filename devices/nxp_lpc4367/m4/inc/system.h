//ver190715
#ifndef _SYSTEM_H_
#define _SYSTEM_H_
//
#define CHIP_LPC43XX
#define CORE_M4
//
#include "std_system.h"
#include "cmsis_43xx.h"
#include "chip_lpc43xx.h"
#include "rgu_18xx_43xx.h"
#include "creg_18xx_43xx.h"
#include "cguccu_18xx_43xx.h"
//
typedef enum
{
	CPUID_M4,
	CPUID_M0APP,
	CPUID_M0SUB,
} CPUID_T;
//
void sys_init(void);
void project_def_handler(void);
void fpuInit(void);
uint32_t M0Image_Boot(CPUID_T cpuid, uint32_t m0_image_addr);
#endif

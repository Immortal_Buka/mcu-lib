//ver190715
#include "system.h"
//
#define LPC_CPACR           0xE000ED88
#define SCB_MVFR0           0xE000EF40
#define SCB_MVFR0_RESET     0x10110021
#define SCB_MVFR1           0xE000EF44
#define SCB_MVFR1_RESET     0x11000011
//
void fpuInit(void)
{
	volatile uint32_t *regCpacr = (uint32_t *) LPC_CPACR;
	volatile uint32_t *regMvfr0 = (uint32_t *) SCB_MVFR0;
	volatile uint32_t *regMvfr1 = (uint32_t *) SCB_MVFR1;
	volatile uint32_t Cpacr;
	volatile uint32_t Mvfr0;
	volatile uint32_t Mvfr1;
	char vfpPresent = 0;
	Mvfr0 = *regMvfr0;
	Mvfr1 = *regMvfr1;
	vfpPresent = ((SCB_MVFR0_RESET == Mvfr0) && (SCB_MVFR1_RESET == Mvfr1));
	if (vfpPresent)
	{
		Cpacr = *regCpacr;
		Cpacr |= (0xF << 20);
		*regCpacr = Cpacr;
	}
}
uint32_t M0Image_Boot(CPUID_T cpuid, uint32_t m0_image_addr)
{
	if (m0_image_addr & 0xFFF) return 1;
	if ((m0_image_addr & 0xFFF00000) != ((*(uint32_t*)(m0_image_addr + 4)) & 0xFFF00000)) return 2;
	if (cpuid == CPUID_M0APP)
	{
		LPC_RGU->RESET_CTRL[1] |= 1<<24;
		LPC_CCU1->CLKCCU[114].CFG |= 1;
		LPC_CREG->M0APPMEMMAP = m0_image_addr;
		LPC_RGU->RESET_CTRL[1] &= ~(1<<24);
	}
	if (cpuid == CPUID_M0SUB)
	{
		LPC_RGU->RESET_CTRL[0] |= 1<<12;
		LPC_CCU1->CLKCCU[194].CFG |= 1;
		LPC_CREG->M0SUBMEMMAP = m0_image_addr;
		LPC_RGU->RESET_CTRL[0] &= ~(1<<12);
	}
	return 0;
}

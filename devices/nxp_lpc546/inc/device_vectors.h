//ver190925
//
extern uint32_t __valid_user_code_checksum;
//
void WDT_BOD_IRQHandler(void) WA;
void DMA0_IRQHandler(void) WA;
void GINT0_IRQHandler(void) WA;
void GINT1_IRQHandler(void) WA;
void PIN_INT0_IRQHandler(void) WA;
void PIN_INT1_IRQHandler(void) WA;
void PIN_INT2_IRQHandler(void) WA;
void PIN_INT3_IRQHandler(void) WA;
void UTICK0_IRQHandler(void) WA;
void MRT0_IRQHandler(void) WA;
void CTIMER0_IRQHandler(void) WA;
void CTIMER1_IRQHandler(void) WA;
void SCT0_IRQHandler(void) WA;
void CTIMER3_IRQHandler(void) WA;
void FLEXCOMM0_IRQHandler(void) WA;
void FLEXCOMM1_IRQHandler(void) WA;
void FLEXCOMM2_IRQHandler(void) WA;
void FLEXCOMM3_IRQHandler(void) WA;
void FLEXCOMM4_IRQHandler(void) WA;
void FLEXCOMM5_IRQHandler(void) WA;
void FLEXCOMM6_IRQHandler(void) WA;
void FLEXCOMM7_IRQHandler(void) WA;
void ADC0_SEQA_IRQHandler(void) WA;
void ADC0_SEQB_IRQHandler(void) WA;
void ADC0_THCMP_IRQHandler(void) WA;
void DMIC0_IRQHandler(void) WA;
void HWVAD0_IRQHandler(void) WA;
void USB0_NEEDCLK_IRQHandler(void) WA;
void USB0_IRQHandler(void) WA;
void RTC_IRQHandler(void) WA;
void Reserved46_IRQHandler(void) WA;
void Reserved47_IRQHandler(void) WA;
void PIN_INT4_IRQHandler(void) WA;
void PIN_INT5_IRQHandler(void) WA;
void PIN_INT6_IRQHandler(void) WA;
void PIN_INT7_IRQHandler(void) WA;
void CTIMER2_IRQHandler(void) WA;
void CTIMER4_IRQHandler(void) WA;
void RIT_IRQHandler(void) WA;
void SPIFI0_IRQHandler(void) WA;
void FLEXCOMM8_IRQHandler(void) WA;
void FLEXCOMM9_IRQHandler(void) WA;
void SDIO_IRQHandler(void) WA;
void CAN0_IRQ0_IRQHandler(void) WA;
void CAN0_IRQ1_IRQHandler(void) WA;
void CAN1_IRQ0_IRQHandler(void) WA;
void CAN1_IRQ1_IRQHandler(void) WA;
void USB1_IRQHandler(void) WA;
void USB1_NEEDCLK_IRQHandler(void) WA;
void ETHERNET_IRQHandler(void) WA;
void ETHERNET_PMT_IRQHandler(void) WA;
void ETHERNET_MACLP_IRQHandler(void) WA;
void EEPROM_IRQHandler(void) WA;
void LCD_IRQHandler(void) WA;
void SHA_IRQHandler(void) WA;
void SMARTCARD0_IRQHandler(void) WA;
void SMARTCARD1_IRQHandler(void) WA;
//
volatile const func_ptr device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	(func_ptr)((uint32_t)&__StackTop),            //Initial Stack Pointer
	Reset_Handler,                 //Reset Handler
	NMI_Handler,                   //NMI Handler
	HardFault_Handler,             //Hard Fault Handler
	MemManage_Handler,             //MPU Fault Handler
	BusFault_Handler,              //Bus Fault Handler
	UsageFault_Handler,            //Usage Fault Handler
	(func_ptr)&__valid_user_code_checksum,    //Checksum of the first 7 words
	(func_ptr)0xFFFFFFFF,                    //ECRP
	0,                             //Enhanced image marker, set to 0x0 for legacy boot
	0,                             //Pointer to enhanced boot block, set to 0x0 for legacy boot
	SVC_Handler,                   //SVCall Handler
	DebugMon_Handler,              //Debug Monitor Handler
	0,                             //Reserved
	PendSV_Handler,                //PendSV Handler
	SysTick_Handler,               //SysTick Handler
    // Chip Level - LPC54608
    WDT_BOD_IRQHandler,         // 16: Windowed watchdog timer, Brownout detect
    DMA0_IRQHandler,            // 17: DMA controller
    GINT0_IRQHandler,           // 18: GPIO group 0
    GINT1_IRQHandler,           // 19: GPIO group 1
    PIN_INT0_IRQHandler,        // 20: Pin interrupt 0 or pattern match engine slice 0
    PIN_INT1_IRQHandler,        // 21: Pin interrupt 1or pattern match engine slice 1
    PIN_INT2_IRQHandler,        // 22: Pin interrupt 2 or pattern match engine slice 2
    PIN_INT3_IRQHandler,        // 23: Pin interrupt 3 or pattern match engine slice 3
    UTICK0_IRQHandler,          // 24: Micro-tick Timer
    MRT0_IRQHandler,            // 25: Multi-rate timer
    CTIMER0_IRQHandler,         // 26: Standard counter/timer CTIMER0
    CTIMER1_IRQHandler,         // 27: Standard counter/timer CTIMER1
    SCT0_IRQHandler,            // 28: SCTimer/PWM
    CTIMER3_IRQHandler,         // 29: Standard counter/timer CTIMER3
    FLEXCOMM0_IRQHandler,       // 30: Flexcomm Interface 0 (USART, SPI, I2C, FLEXCOMM)
    FLEXCOMM1_IRQHandler,       // 31: Flexcomm Interface 1 (USART, SPI, I2C, FLEXCOMM)
    FLEXCOMM2_IRQHandler,       // 32: Flexcomm Interface 2 (USART, SPI, I2C, FLEXCOMM)
    FLEXCOMM3_IRQHandler,       // 33: Flexcomm Interface 3 (USART, SPI, I2C, FLEXCOMM)
    FLEXCOMM4_IRQHandler,       // 34: Flexcomm Interface 4 (USART, SPI, I2C, FLEXCOMM)
    FLEXCOMM5_IRQHandler,       // 35: Flexcomm Interface 5 (USART, SPI, I2C,, FLEXCOMM)
    FLEXCOMM6_IRQHandler,       // 36: Flexcomm Interface 6 (USART, SPI, I2C, I2S,, FLEXCOMM)
    FLEXCOMM7_IRQHandler,       // 37: Flexcomm Interface 7 (USART, SPI, I2C, I2S,, FLEXCOMM)
    ADC0_SEQA_IRQHandler,       // 38: ADC0 sequence A completion.
    ADC0_SEQB_IRQHandler,       // 39: ADC0 sequence B completion.
    ADC0_THCMP_IRQHandler,      // 40: ADC0 threshold compare and error.
    DMIC0_IRQHandler,           // 41: Digital microphone and DMIC subsystem
    HWVAD0_IRQHandler,          // 42: Hardware Voice Activity Detector
    USB0_NEEDCLK_IRQHandler,    // 43: USB Activity Wake-up Interrupt
    USB0_IRQHandler,            // 44: USB device
    RTC_IRQHandler,             // 45: RTC alarm and wake-up interrupts
    Reserved46_IRQHandler,      // 46: Reserved interrupt
    Reserved47_IRQHandler,      // 47: Reserved interrupt
    PIN_INT4_IRQHandler,        // 48: Pin interrupt 4 or pattern match engine slice 4 int
    PIN_INT5_IRQHandler,        // 49: Pin interrupt 5 or pattern match engine slice 5 int
    PIN_INT6_IRQHandler,        // 50: Pin interrupt 6 or pattern match engine slice 6 int
    PIN_INT7_IRQHandler,        // 51: Pin interrupt 7 or pattern match engine slice 7 int
    CTIMER2_IRQHandler,         // 52: Standard counter/timer CTIMER2
    CTIMER4_IRQHandler,         // 53: Standard counter/timer CTIMER4
    RIT_IRQHandler,             // 54: Repetitive Interrupt Timer
    SPIFI0_IRQHandler,          // 55: SPI flash interface
    FLEXCOMM8_IRQHandler,       // 56: Flexcomm Interface 8 (USART, SPI, I2C, FLEXCOMM)
    FLEXCOMM9_IRQHandler,       // 57: Flexcomm Interface 9 (USART, SPI, I2C, FLEXCOMM)
    SDIO_IRQHandler,            // 58: SD/MMC
    CAN0_IRQ0_IRQHandler,       // 59: CAN0 interrupt0
    CAN0_IRQ1_IRQHandler,       // 60: CAN0 interrupt1
    CAN1_IRQ0_IRQHandler,       // 61: CAN1 interrupt0
    CAN1_IRQ1_IRQHandler,       // 62: CAN1 interrupt1
    USB1_IRQHandler,            // 63: USB1 interrupt
    USB1_NEEDCLK_IRQHandler,    // 64: USB1 activity
    ETHERNET_IRQHandler,        // 65: Ethernet
    ETHERNET_PMT_IRQHandler,    // 66: Ethernet power management interrupt
    ETHERNET_MACLP_IRQHandler,  // 67: Ethernet MAC interrupt
    EEPROM_IRQHandler,          // 68: EEPROM interrupt
    LCD_IRQHandler,             // 69: LCD interrupt
    SHA_IRQHandler,             // 70: SHA interrupt
    SMARTCARD0_IRQHandler,      // 71: Smart card 0 interrupt
    SMARTCARD1_IRQHandler,      // 72: Smart card 1 interrupt
};

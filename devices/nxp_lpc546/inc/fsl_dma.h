#ifndef _FSL_DMA_H_
#define _FSL_DMA_H_
//
#define FSL_DMA_DRIVER_VERSION 									MAKE_VERSION(2, 3, 0)) /*!< Version 2.3.0. */
#define DMA_MAX_TRANSFER_COUNT 									0x400
#if defined FSL_FEATURE_DMA_NUMBER_OF_CHANNELS
	#define FSL_FEATURE_DMA_NUMBER_OF_CHANNELSn(x) 				FSL_FEATURE_DMA_NUMBER_OF_CHANNELS
	#define FSL_FEATURE_DMA_MAX_CHANNELS 						FSL_FEATURE_DMA_NUMBER_OF_CHANNELS
	#define FSL_FEATURE_DMA_ALL_CHANNELS 						(FSL_FEATURE_DMA_NUMBER_OF_CHANNELS * FSL_FEATURE_SOC_DMA_COUNT)
#endif
#define FSL_FEATURE_DMA_LINK_DESCRIPTOR_ALIGN_SIZE 				(16U)
#define DMA_ALLOCATE_HEAD_DESCRIPTORS(name, number) 			SDK_ALIGN(dma_descriptor_t name[number], FSL_FEATURE_DMA_DESCRIPTOR_ALIGN_SIZE)
#define DMA_ALLOCATE_LINK_DESCRIPTORS(name, number) 			SDK_ALIGN(dma_descriptor_t name[number], FSL_FEATURE_DMA_LINK_DESCRIPTOR_ALIGN_SIZE)
#define DMA_CHANNEL_GROUP(channel) 								(((uint8_t)(channel)) >> 5U)
#define DMA_CHANNEL_INDEX(channel) 								(((uint8_t)(channel)) & 0x1F)
#define DMA_COMMON_REG_GET(base, channel, reg) 					(((volatile uint32_t *)(&((base)->COMMON[0].reg)))[DMA_CHANNEL_GROUP(channel)])
#define DMA_COMMON_CONST_REG_GET(base, channel, reg) 			(((volatile const uint32_t *)(&((base)->COMMON[0].reg)))[DMA_CHANNEL_GROUP(channel)])
#define DMA_COMMON_REG_SET(base, channel, reg, value) 			(((volatile uint32_t *)(&((base)->COMMON[0].reg)))[DMA_CHANNEL_GROUP(channel)] = (value))
#define DMA_DESCRIPTOR_END_ADDRESS(start, inc, bytes, width) 	((void *)((uint32_t)(start) + inc * bytes - inc * width))
#define DMA_CHANNEL_XFER(reload, clrTrig, intA, intB, width, srcInc, dstInc, bytes)                                 \
    DMA_CHANNEL_XFERCFG_CFGVALID_MASK | DMA_CHANNEL_XFERCFG_RELOAD(reload) | DMA_CHANNEL_XFERCFG_CLRTRIG(clrTrig) | \
        DMA_CHANNEL_XFERCFG_SETINTA(intA) | DMA_CHANNEL_XFERCFG_SETINTB(intB) |                                     \
        DMA_CHANNEL_XFERCFG_WIDTH(width == 4 ? 2 : (width - 1)) |                                                   \
        DMA_CHANNEL_XFERCFG_SRCINC(srcInc == 4 ? (srcInc - 1) : srcInc) |                                           \
        DMA_CHANNEL_XFERCFG_DSTINC(dstInc == 4 ? (dstInc - 1) : dstInc) |                                           \
        DMA_CHANNEL_XFERCFG_XFERCOUNT(bytes / width - 1)
//
enum _dma_addr_interleave_size
{
    kDMA_AddressInterleave0xWidth = 0U, /*!< dma source/destination address no interleave */
    kDMA_AddressInterleave1xWidth = 1U, /*!< dma source/destination address interleave 1xwidth */
    kDMA_AddressInterleave2xWidth = 2U, /*!< dma source/destination address interleave 2xwidth */
    kDMA_AddressInterleave4xWidth = 4U, /*!< dma source/destination address interleave 3xwidth */
};
enum _dma_transfer_width
{
    kDMA_Transfer8BitWidth  = 1U, /*!< dma channel transfer bit width is 8 bit */
    kDMA_Transfer16BitWidth = 2U, /*!< dma channel transfer bit width is 16 bit */
    kDMA_Transfer32BitWidth = 4U, /*!< dma channel transfer bit width is 32 bit */
};
typedef struct _dma_descriptor
{
    volatile uint32_t xfercfg; /*!< Transfer configuration */
    void *srcEndAddr;          /*!< Last source address of DMA transfer */
    void *dstEndAddr;          /*!< Last destination address of DMA transfer */
    void *linkToNextDesc;      /*!< Address of next DMA descriptor in chain */
} dma_descriptor_t;
typedef struct _dma_xfercfg
{
    bool valid;             /*!< Descriptor is ready to transfer */
    bool reload;            /*!< Reload channel configuration register after current descriptor is exhausted */
    bool swtrig;            /*!< Perform software trigger. Transfer if fired when 'valid' is set */
    bool clrtrig;           /*!< Clear trigger */
    bool intA;              /*!< Raises IRQ when transfer is done and set IRQA status register flag */
    bool intB;              /*!< Raises IRQ when transfer is done and set IRQB status register flag */
    uint8_t byteWidth;      /*!< Byte width of data to transfer */
    uint8_t srcInc;         /*!< Increment source address by 'srcInc' x 'byteWidth' */
    uint8_t dstInc;         /*!< Increment destination address by 'dstInc' x 'byteWidth' */
    uint16_t transferCount; /*!< Number of transfers */
} dma_xfercfg_t;
typedef enum _dma_priority
{
    kDMA_ChannelPriority0 = 0, /*!< Highest channel priority - priority 0 */
    kDMA_ChannelPriority1,     /*!< Channel priority 1 */
    kDMA_ChannelPriority2,     /*!< Channel priority 2 */
    kDMA_ChannelPriority3,     /*!< Channel priority 3 */
    kDMA_ChannelPriority4,     /*!< Channel priority 4 */
    kDMA_ChannelPriority5,     /*!< Channel priority 5 */
    kDMA_ChannelPriority6,     /*!< Channel priority 6 */
    kDMA_ChannelPriority7,     /*!< Lowest channel priority - priority 7 */
} dma_priority_t;
typedef enum _dma_int
{
    kDMA_IntA,     /*!< DMA interrupt flag A */
    kDMA_IntB,     /*!< DMA interrupt flag B */
    kDMA_IntError, /*!< DMA interrupt flag error */
} dma_irq_t;
typedef enum _dma_trigger_type
{
    kDMA_NoTrigger        = 0,                                                         /*!< Trigger is disabled */
    kDMA_LowLevelTrigger  = DMA_CHANNEL_CFG_HWTRIGEN(1) | DMA_CHANNEL_CFG_TRIGTYPE(1), /*!< Low level active trigger */
    kDMA_HighLevelTrigger = DMA_CHANNEL_CFG_HWTRIGEN(1) | DMA_CHANNEL_CFG_TRIGTYPE(1) |DMA_CHANNEL_CFG_TRIGPOL(1),    /*!< High level active trigger */
    kDMA_FallingEdgeTrigger = DMA_CHANNEL_CFG_HWTRIGEN(1), /*!< Falling edge active trigger */
    kDMA_RisingEdgeTrigger = DMA_CHANNEL_CFG_HWTRIGEN(1) | DMA_CHANNEL_CFG_TRIGPOL(1), /*!< Rising edge active trigger */
} dma_trigger_type_t;
enum _dma_burst_size
{
    kDMA_BurstSize1    = 0U,  /*!< burst size 1 transfer */
    kDMA_BurstSize2    = 1U,  /*!< burst size 2 transfer */
    kDMA_BurstSize4    = 2U,  /*!< burst size 4 transfer */
    kDMA_BurstSize8    = 3U,  /*!< burst size 8 transfer */
    kDMA_BurstSize16   = 4U,  /*!< burst size 16 transfer */
    kDMA_BurstSize32   = 5U,  /*!< burst size 32 transfer */
    kDMA_BurstSize64   = 6U,  /*!< burst size 64 transfer */
    kDMA_BurstSize128  = 7U,  /*!< burst size 128 transfer */
    kDMA_BurstSize256  = 8U,  /*!< burst size 256 transfer */
    kDMA_BurstSize512  = 9U,  /*!< burst size 512 transfer */
    kDMA_BurstSize1024 = 10U, /*!< burst size 1024 transfer */
};
typedef enum _dma_trigger_burst
{
    kDMA_SingleTransfer     = 0,                            /*!< Single transfer */
    kDMA_LevelBurstTransfer = DMA_CHANNEL_CFG_TRIGBURST(1), /*!< Burst transfer driven by level trigger */
    kDMA_EdgeBurstTransfer1 = DMA_CHANNEL_CFG_TRIGBURST(1), /*!< Perform 1 transfer by edge trigger */
    kDMA_EdgeBurstTransfer2 = DMA_CHANNEL_CFG_TRIGBURST(1) | DMA_CHANNEL_CFG_BURSTPOWER(1), /*!< Perform 2 transfers by edge trigger */
    kDMA_EdgeBurstTransfer4 = DMA_CHANNEL_CFG_TRIGBURST(1) | DMA_CHANNEL_CFG_BURSTPOWER(2), /*!< Perform 4 transfers by edge trigger */
    kDMA_EdgeBurstTransfer8 = DMA_CHANNEL_CFG_TRIGBURST(1) | DMA_CHANNEL_CFG_BURSTPOWER(3), /*!< Perform 8 transfers by edge trigger */
    kDMA_EdgeBurstTransfer16 = DMA_CHANNEL_CFG_TRIGBURST(1) | DMA_CHANNEL_CFG_BURSTPOWER(4), /*!< Perform 16 transfers by edge trigger */
    kDMA_EdgeBurstTransfer32 = DMA_CHANNEL_CFG_TRIGBURST(1) | DMA_CHANNEL_CFG_BURSTPOWER(5), /*!< Perform 32 transfers by edge trigger */
    kDMA_EdgeBurstTransfer64 = DMA_CHANNEL_CFG_TRIGBURST(1) | DMA_CHANNEL_CFG_BURSTPOWER(6), /*!< Perform 64 transfers by edge trigger */
    kDMA_EdgeBurstTransfer128 = DMA_CHANNEL_CFG_TRIGBURST(1) | DMA_CHANNEL_CFG_BURSTPOWER(7), /*!< Perform 128 transfers by edge trigger */
    kDMA_EdgeBurstTransfer256 = DMA_CHANNEL_CFG_TRIGBURST(1) | DMA_CHANNEL_CFG_BURSTPOWER(8), /*!< Perform 256 transfers by edge trigger */
    kDMA_EdgeBurstTransfer512 = DMA_CHANNEL_CFG_TRIGBURST(1) | DMA_CHANNEL_CFG_BURSTPOWER(9), /*!< Perform 512 transfers by edge trigger */
    kDMA_EdgeBurstTransfer1024 = DMA_CHANNEL_CFG_TRIGBURST(1) | DMA_CHANNEL_CFG_BURSTPOWER(10), /*!< Perform 1024 transfers by edge trigger */
} dma_trigger_burst_t;
typedef enum _dma_burst_wrap
{
    kDMA_NoWrap        = 0,                               /*!< Wrapping is disabled */
    kDMA_SrcWrap       = DMA_CHANNEL_CFG_SRCBURSTWRAP(1), /*!< Wrapping is enabled for source */
    kDMA_DstWrap       = DMA_CHANNEL_CFG_DSTBURSTWRAP(1), /*!< Wrapping is enabled for destination */
    kDMA_SrcAndDstWrap = DMA_CHANNEL_CFG_SRCBURSTWRAP(1)|DMA_CHANNEL_CFG_DSTBURSTWRAP(1), /*!< Wrapping is enabled for source and destination */
} dma_burst_wrap_t;
typedef enum _dma_transfer_type
{
    kDMA_MemoryToMemory = 0x0U, /*!< Transfer from memory to memory (increment source and destination) */
    kDMA_PeripheralToMemory,    /*!< Transfer from peripheral to memory (increment only destination) */
    kDMA_MemoryToPeripheral,    /*!< Transfer from memory to peripheral (increment only source)*/
    kDMA_StaticToStatic,        /*!< Peripheral to static memory (do not increment source or destination) */
} dma_transfer_type_t;
typedef struct _dma_channel_trigger
{
    dma_trigger_type_t type;   /*!< Select hardware trigger as edge triggered or level triggered. */
    dma_trigger_burst_t burst; /*!< Select whether hardware triggers cause a single or burst transfer. */
    dma_burst_wrap_t wrap;     /*!< Select wrap type, source wrap or dest wrap, or both. */
} dma_channel_trigger_t;
typedef struct _dma_channel_config
{
    void *srcStartAddr;             /*!< Source data address */
    void *dstStartAddr;             /*!< Destination data address */
    void *nextDesc;                 /*!< Chain custom descriptor */
    uint32_t xferCfg;               /*!< channel transfer configurations */
    dma_channel_trigger_t *trigger; /*!< DMA trigger type */
    bool isPeriph;                  /*!< select the request type */
} dma_channel_config_t;
typedef struct _dma_transfer_config
{
    uint8_t *srcAddr;      /*!< Source data address */
    uint8_t *dstAddr;      /*!< Destination data address */
    uint8_t *nextDesc;     /*!< Chain custom descriptor */
    dma_xfercfg_t xfercfg; /*!< Transfer options */
    bool isPeriph;         /*!< DMA transfer is driven by peripheral */
} dma_transfer_config_t;
struct _dma_handle;
typedef void (*dma_callback)(struct _dma_handle *handle, void *userData, bool transferDone, uint32_t intmode);
typedef struct _dma_handle
{
    dma_callback callback; /*!< Callback function. Invoked when transfer of descriptor with interrupt flag finishes */
    void *userData;        /*!< Callback function parameter */
    DMA_Type *base;        /*!< DMA peripheral base address */
    uint8_t channel;       /*!< DMA channel number */
} dma_handle_t;
//
#endif /*_FSL_DMA_H_*/

void SPI0_IRQHandler(void) WA;
void SPI1_IRQHandler(void) WA;
void UART0_IRQHandler(void) WA;
void UART1_IRQHandler(void) WA;
void UART2_IRQHandler(void) WA;
void I2C_IRQHandler(void) WA;
void SCT_IRQHandler(void) WA;
void MRT_IRQHandler(void) WA;
void CMP_IRQHandler(void) WA;
void WDT_IRQHandler(void) WA;
void BOD_IRQHandler(void) WA;
void WKT_IRQHandler(void) WA;
void PININT0_IRQHandler(void) WA;
void PININT1_IRQHandler(void) WA;
void PININT2_IRQHandler(void) WA;
void PININT3_IRQHandler(void) WA;
void PININT4_IRQHandler(void) WA;
void PININT5_IRQHandler(void) WA;
void PININT6_IRQHandler(void) WA;
void PININT7_IRQHandler(void) WA;
//
volatile const func_ptr device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	SPI0_IRQHandler,                         // SPI0 controller
	SPI1_IRQHandler,                         // SPI1 controller
	0,                                       // Reserved
	UART0_IRQHandler,                        // UART0
	UART1_IRQHandler,                        // UART1
	UART2_IRQHandler,                        // UART2
	0,                                       // Reserved
	0,                                       // Reserved
	I2C_IRQHandler,                          // I2C controller
	SCT_IRQHandler,                          // Smart Counter Timer
	MRT_IRQHandler,                          // Multi-Rate Timer
	CMP_IRQHandler,                          // Comparator
	WDT_IRQHandler,                          // Watchdog
	BOD_IRQHandler,                          // Brown Out Detect
	0,                                       // Reserved
	WKT_IRQHandler,                          // Wakeup timer
	0,                                       // Reserved
	0,                                       // Reserved
	0,                                       // Reserved
	0,                                       // Reserved
	0,                                       // Reserved
	0,                                       // Reserved
	0,                                       // Reserved
	0,                                       // Reserved
	PININT0_IRQHandler,                      // PIO INT0
	PININT1_IRQHandler,                      // PIO INT1
	PININT2_IRQHandler,                      // PIO INT2
	PININT3_IRQHandler,                      // PIO INT3
	PININT4_IRQHandler,                      // PIO INT4
	PININT5_IRQHandler,                      // PIO INT5
	PININT6_IRQHandler,                      // PIO INT6
	PININT7_IRQHandler,                      // PIO INT7
};

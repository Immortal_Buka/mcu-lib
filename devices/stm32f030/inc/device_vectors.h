void WWDG_IRQHandler(void) WA;
void RTC_IRQHandler(void) WA;
void FLASH_IRQHandler(void) WA;
void RCC_IRQHandler(void) WA;
void EXTI0_1_IRQHandler(void) WA;
void EXTI2_3_IRQHandler(void) WA;
void EXTI4_15_IRQHandler(void) WA;
void DMA1_Channel1_IRQHandler(void) WA;
void DMA1_Channel2_3_IRQHandler(void) WA;
void DMA1_Channel4_5_IRQHandler(void) WA;
void ADC1_IRQHandler(void) WA;
void TIM1_BRK_UP_TRG_COM_IRQHandler(void) WA;
void TIM1_CC_IRQHandler(void) WA;
void TIM3_IRQHandler(void) WA;
void TIM14_IRQHandler(void) WA;
void TIM16_IRQHandler(void) WA;
void TIM17_IRQHandler(void) WA;
void I2C1_IRQHandler(void) WA;
void SPI1_IRQHandler(void) WA;
void USART1_IRQHandler(void) WA;
//
volatile const func_ptr device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	WWDG_IRQHandler,                   /* Window WatchDog              */
	0,                                 /* Reserved                     */
	RTC_IRQHandler,                    /* RTC through the EXTI line    */
	FLASH_IRQHandler,                  /* FLASH                        */
	RCC_IRQHandler,                    /* RCC                          */
	EXTI0_1_IRQHandler,                /* EXTI Line 0 and 1            */
	EXTI2_3_IRQHandler,                /* EXTI Line 2 and 3            */
	EXTI4_15_IRQHandler,               /* EXTI Line 4 to 15            */
	0,                                 /* Reserved                     */
	DMA1_Channel1_IRQHandler,          /* DMA1 Channel 1               */
	DMA1_Channel2_3_IRQHandler,        /* DMA1 Channel 2 and Channel 3 */
	DMA1_Channel4_5_IRQHandler,        /* DMA1 Channel 4 and Channel 5 */
	ADC1_IRQHandler,                   /* ADC1                         */
	TIM1_BRK_UP_TRG_COM_IRQHandler,    /* TIM1 Break, Update, Trigger and Commutation */
	TIM1_CC_IRQHandler,                /* TIM1 Capture Compare         */
	0,                                 /* Reserved                     */
	TIM3_IRQHandler,                   /* TIM3                         */
	0,                                 /* Reserved                     */
	0,                                 /* Reserved                     */
	TIM14_IRQHandler,                  /* TIM14                        */
	0,                                 /* Reserved                     */
	TIM16_IRQHandler,                  /* TIM16                        */
	TIM17_IRQHandler,                  /* TIM17                        */
	I2C1_IRQHandler,                   /* I2C1                         */
	0,                                 /* Reserved                     */
	SPI1_IRQHandler,                   /* SPI1                         */
	0,                                 /* Reserved                     */
	USART1_IRQHandler,                 /* USART1                       */
	0,                                 /* Reserved                     */
	0,                                 /* Reserved                     */
	0,                                 /* Reserved                     */
	0,                                 /* Reserved                     */
};

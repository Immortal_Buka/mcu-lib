void WWDG_IRQHandler(void) WA;
void PVD_IRQHandler(void) WA;
void TAMPER_IRQHandler(void) WA;
void RTC_IRQHandler(void) WA;
void FLASH_IRQHandler(void) WA;
void RCC_IRQHandler(void) WA;
void EXTI0_IRQHandler(void) WA;
void EXTI1_IRQHandler(void) WA;
void EXTI2_IRQHandler(void) WA;
void EXTI3_IRQHandler(void) WA;
void EXTI4_IRQHandler(void) WA;
void DMA1_Channel1_IRQHandler(void) WA;
void DMA1_Channel2_IRQHandler(void) WA;
void DMA1_Channel3_IRQHandler(void) WA;
void DMA1_Channel4_IRQHandler(void) WA;
void DMA1_Channel5_IRQHandler(void) WA;
void DMA1_Channel6_IRQHandler(void) WA;
void DMA1_Channel7_IRQHandler(void) WA;
void ADC1_2_IRQHandler(void) WA;
void USB_HP_CAN1_TX_IRQHandler(void) WA;
void USB_LP_CAN1_RX0_IRQHandler(void) WA;
void CAN1_RX1_IRQHandler(void) WA;
void CAN1_SCE_IRQHandler(void) WA;
void EXTI9_5_IRQHandler(void) WA;
void TIM1_BRK_IRQHandler(void) WA;
void TIM1_UP_IRQHandler(void) WA;
void TIM1_TRG_COM_IRQHandler(void) WA;
void TIM1_CC_IRQHandler(void) WA;
void TIM2_IRQHandler(void) WA;
void TIM3_IRQHandler(void) WA;
void TIM4_IRQHandler(void) WA;
void I2C1_EV_IRQHandler(void) WA;
void I2C1_ER_IRQHandler(void) WA;
void I2C2_EV_IRQHandler(void) WA;
void I2C2_ER_IRQHandler(void) WA;
void SPI1_IRQHandler(void) WA;
void SPI2_IRQHandler(void) WA;
void USART1_IRQHandler(void) WA;
void USART2_IRQHandler(void) WA;
void USART3_IRQHandler(void) WA;
void EXTI15_10_IRQHandler(void) WA;
void RTC_Alarm_IRQHandler(void) WA;
void USBWakeUp_IRQHandler(void) WA;
//
volatile const func_ptr device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	WWDG_IRQHandler,
	PVD_IRQHandler,
	TAMPER_IRQHandler,
	RTC_IRQHandler,
	FLASH_IRQHandler,
	RCC_IRQHandler,
	EXTI0_IRQHandler,
	EXTI1_IRQHandler,
	EXTI2_IRQHandler,
	EXTI3_IRQHandler,
	EXTI4_IRQHandler,
	DMA1_Channel1_IRQHandler,
	DMA1_Channel2_IRQHandler,
	DMA1_Channel3_IRQHandler,
	DMA1_Channel4_IRQHandler,
	DMA1_Channel5_IRQHandler,
	DMA1_Channel6_IRQHandler,
	DMA1_Channel7_IRQHandler,
	ADC1_2_IRQHandler,
	USB_HP_CAN1_TX_IRQHandler,
	USB_LP_CAN1_RX0_IRQHandler,
	CAN1_RX1_IRQHandler,
	CAN1_SCE_IRQHandler,
	EXTI9_5_IRQHandler,
	TIM1_BRK_IRQHandler,
	TIM1_UP_IRQHandler,
	TIM1_TRG_COM_IRQHandler,
	TIM1_CC_IRQHandler,
	TIM2_IRQHandler,
	TIM3_IRQHandler,
	TIM4_IRQHandler,
	I2C1_EV_IRQHandler,
	I2C1_ER_IRQHandler,
	I2C2_EV_IRQHandler,
	I2C2_ER_IRQHandler,
	SPI1_IRQHandler,
	SPI2_IRQHandler,
	USART1_IRQHandler,
	USART2_IRQHandler,
	USART3_IRQHandler,
	EXTI15_10_IRQHandler,
	RTC_Alarm_IRQHandler,
	USBWakeUp_IRQHandler,
};

#ifndef __DAP_CONFIG_H__
#define __DAP_CONFIG_H__
//
#include "system.h"
//
#define CPU_CLOCK               168000000
#define IO_PORT_WRITE_CYCLES    2
#define DAP_SWD                 0
#define DAP_JTAG                1
#define DAP_JTAG_DEV_CNT        8
#define DAP_DEFAULT_PORT        2
#define DAP_DEFAULT_SWJ_CLOCK   1000000U        ///< Default SWD/JTAG clock frequency in Hz.
#define DAP_PACKET_SIZE         512U            ///< Specifies Packet Size in bytes.
#define DAP_PACKET_COUNT        8U
#define SWO_UART                0
#define SWO_UART_MAX_BAUDRATE   10000000
#define SWO_MANCHESTER          0
#define SWO_BUFFER_SIZE         8192
#define SWO_STREAM              0
#define TIMESTAMP_CLOCK         168000000
#define TARGET_DEVICE_FIXED     0
#if TARGET_DEVICE_FIXED
	#define TARGET_DEVICE_VENDOR    ""
	#define TARGET_DEVICE_NAME      ""
#endif
//
__STATIC_INLINE uint8_t DAP_GetVendorString(char *str)
{
	(void)str;
	return (0U);
}
__STATIC_INLINE uint8_t DAP_GetProductString(char *str)
{
	(void)str;
	return (0U);
}
__STATIC_INLINE uint8_t DAP_GetSerNumString(char *str)
{
	(void)str;
	return (0U);
}
/** 
JTAG I/O Pin                 | CMSIS-DAP Hardware pin mode
---------------------------- || ---------------------------------------------
TCK: Test Clock              || Output Push/Pull
TMS: Test Mode Select        || Output Push/Pull; Input (for receiving data)
TDI: Test Data Input         || Output Push/Pull
TDO: Test Data Output        || Input
*/ 
__STATIC_INLINE void PORT_JTAG_SETUP(void)
{
	//LPC_GPIO_PORT->MASK[PIN_SWDIO_TMS_PORT] = 0U;
	//LPC_GPIO_PORT->MASK[PIN_TDI_PORT] = ~(1U << PIN_TDI_BIT);
}
__STATIC_INLINE void PORT_SWD_SETUP(void)
{
}
__STATIC_INLINE void PORT_OFF(void)
{
}
__STATIC_FORCEINLINE uint32_t PIN_SWCLK_TCK_IN(void)
{
	return 0;
}
__STATIC_FORCEINLINE void PIN_SWCLK_TCK_SET(void){}
__STATIC_FORCEINLINE void PIN_SWCLK_TCK_CLR(void){}
__STATIC_FORCEINLINE uint32_t PIN_SWDIO_TMS_IN(void)
{
	return 0;
}
__STATIC_FORCEINLINE void PIN_SWDIO_TMS_SET(void){}
__STATIC_FORCEINLINE void PIN_SWDIO_TMS_CLR(void){}
__STATIC_FORCEINLINE uint32_t PIN_SWDIO_IN(void)
{
	return 0;
}
__STATIC_FORCEINLINE void PIN_SWDIO_OUT(uint32_t bit){}
__STATIC_FORCEINLINE void PIN_SWDIO_OUT_ENABLE(void){}
__STATIC_FORCEINLINE void PIN_SWDIO_OUT_DISABLE(void){}
// TDI Pin I/O ---------------------------------------------
__STATIC_FORCEINLINE uint32_t PIN_TDI_IN(void)
{
	return ((GPIOA->IDR >> 12)&1);
}
__STATIC_FORCEINLINE void PIN_TDI_OUT(uint32_t bit)
{
	BB_periph_write(GPIOA->ODR, 12, bit);
}
// TDO Pin I/O ---------------------------------------------
__STATIC_FORCEINLINE uint32_t PIN_TDO_IN(void)
{
	return ((GPIOB->IDR >> 8)&1);
}
// nTRST Pin I/O -------------------------------------------
__STATIC_FORCEINLINE uint32_t PIN_nTRST_IN(void)
{
	return (0U);
}
__STATIC_FORCEINLINE void PIN_nTRST_OUT(uint32_t bit){}
// nRESET Pin I/O------------------------------------------
__STATIC_FORCEINLINE uint32_t PIN_nRESET_IN(void)
{
	return 0;
}
__STATIC_FORCEINLINE void PIN_nRESET_OUT(uint32_t bit){}
__STATIC_INLINE void LED_CONNECTED_OUT(uint32_t bit){}
__STATIC_INLINE void LED_RUNNING_OUT(uint32_t bit){}
__STATIC_INLINE uint32_t TIMESTAMP_GET(void)
{
	return (DWT->CYCCNT);
}
__STATIC_INLINE void DAP_SETUP(void)
{
  /* Enable clock and init GPIO outputs *//*
  LPC_CCU1->CLK_M4_GPIO_CFG = CCU_CLK_CFG_AUTO | CCU_CLK_CFG_RUN;
  while (!(LPC_CCU1->CLK_M4_GPIO_STAT & CCU_CLK_STAT_RUN));

  /* Configure I/O pins: function number, input buffer enabled,  */
  /*                     no pull-up/down except nRESET (pull-up) *//*
  LPC_SCU->SFSP1_17 = 0U | SCU_SFS_EPUN|SCU_SFS_EZI;
  LPC_SCU->SFSP1_6  = 0U | SCU_SFS_EPUN|SCU_SFS_EZI;
  LPC_SCU->SFSP1_5  = 0U | SCU_SFS_EPUN|SCU_SFS_EZI;
  LPC_SCU->SFSP1_18 = 0U | SCU_SFS_EPUN|SCU_SFS_EZI;
  LPC_SCU->SFSP1_14 = 0U | SCU_SFS_EPUN|SCU_SFS_EZI;
  LPC_SCU->SFSP2_5  = 4U |              SCU_SFS_EZI;
  LPC_SCU->SFSP2_6  = 4U | SCU_SFS_EPUN|SCU_SFS_EZI;
  LPC_SCU->SFSP1_1  = 0U | SCU_SFS_EPUN|SCU_SFS_EZI;

  /* Configure: SWCLK/TCK, SWDIO/TMS, SWDIO_OE, TDI as outputs (high level)   */
  /*            TDO as input                                                  */
  /*            nRESET as input with output latch set to low level            */
  /*            nRESET_OE as output (low level)                               *//*
  LPC_GPIO_PORT->SET[PIN_SWCLK_TCK_PORT]  =  (1U << PIN_SWCLK_TCK_BIT);
  LPC_GPIO_PORT->SET[PIN_SWDIO_TMS_PORT]  =  (1U << PIN_SWDIO_TMS_BIT);
  LPC_GPIO_PORT->SET[PIN_SWDIO_OE_PORT]   =  (1U << PIN_SWDIO_OE_BIT);
  LPC_GPIO_PORT->SET[PIN_TDI_PORT]        =  (1U << PIN_TDI_BIT);
  LPC_GPIO_PORT->CLR[PIN_nRESET_PORT]     =  (1U << PIN_nRESET_BIT);
  LPC_GPIO_PORT->CLR[PIN_nRESET_OE_PORT]  =  (1U << PIN_nRESET_OE_BIT);
  LPC_GPIO_PORT->DIR[PIN_SWCLK_TCK_PORT] |=  (1U << PIN_SWCLK_TCK_BIT);
  LPC_GPIO_PORT->DIR[PIN_SWDIO_TMS_PORT] |=  (1U << PIN_SWDIO_TMS_BIT);
  LPC_GPIO_PORT->DIR[PIN_SWDIO_OE_PORT]  |=  (1U << PIN_SWDIO_OE_BIT);
  LPC_GPIO_PORT->DIR[PIN_TDI_PORT]       |=  (1U << PIN_TDI_BIT);
  LPC_GPIO_PORT->DIR[PIN_TDO_PORT]       &= ~(1U << PIN_TDO_BIT);
  LPC_GPIO_PORT->DIR[PIN_nRESET_PORT]    &= ~(1U << PIN_nRESET_BIT);
  LPC_GPIO_PORT->DIR[PIN_nRESET_OE_PORT] |=  (1U << PIN_nRESET_OE_BIT);

  /* Configure: LED as output (turned off) *//*
  LPC_GPIO_PORT->CLR[LED_CONNECTED_PORT]  =  (1U << LED_CONNECTED_BIT);
  LPC_GPIO_PORT->DIR[LED_CONNECTED_PORT] |=  (1U << LED_CONNECTED_BIT);
  NVIC_SetPriority(USB0_IRQn, 1U);*/
}

/** Reset Target Device with custom specific I/O pin or command sequence.
This function allows the optional implementation of a device specific reset sequence.
It is called when the command \ref DAP_ResetTarget and is for example required 
when a device needs a time-critical unlock sequence that enables the debug port.
\return 0 = no device specific reset sequence is implemented.\n
        1 = a device specific reset sequence is implemented.
*/
__STATIC_INLINE uint8_t RESET_TARGET(void)
{
	return (0U);
}
#endif /* __DAP_CONFIG_H__ */

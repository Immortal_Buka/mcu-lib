//ver190515
#include "system.h"
//
void project_def_handler(void)
{
}
void uart_print_char(void* uart, uint8_t data)
{
	while(!(((USART_TypeDef*)uart)->SR & USART_SR_TXE));
	((USART_TypeDef*)uart)->DR=data;
}
void delay_us(uint32_t us)
{
	uint32_t temp1 = us*40;//168MHz
	while (temp1--) __asm("NOP");
}
void delay_ms(uint32_t ms)
{
	delay_us(1000*ms);
}
uint32_t mem_test_byte(volatile uint8_t* memory_ptr, uint32_t size)
{
	uint32_t temp_loc = 0;
	for(uint32_t i=0; i<size; i++) memory_ptr[i] = i&0xff;
	for(uint32_t i=0; i<size; i++)
	{
		if(memory_ptr[i] != (i&0xff)) temp_loc++;
	}
	return temp_loc;
}
void PVD_init(void)
{
	EXTI->IMR = (1<<16);
	EXTI->FTSR = (1<<16);
	NVIC_SetPriority(PVD_IRQn, NVIC_EncodePriority(0, 0, 0));
	NVIC_EnableIRQ(PVD_IRQn);
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	PWR->CR |= (7<<5)|PWR_CR_PVDE;
}

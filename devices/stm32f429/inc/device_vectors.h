void WWDG_IRQHandler(void) WA;
void PVD_IRQHandler(void) WA;
void TAMP_STAMP_IRQHandler(void) WA;
void RTC_WKUP_IRQHandler(void) WA;
void FLASH_IRQHandler(void) WA;
void RCC_IRQHandler(void) WA;
void EXTI0_IRQHandler(void) WA;
void EXTI1_IRQHandler(void) WA;
void EXTI2_IRQHandler(void) WA;
void EXTI3_IRQHandler(void) WA;
void EXTI4_IRQHandler(void) WA;
void DMA1_Stream0_IRQHandler(void) WA;
void DMA1_Stream1_IRQHandler(void) WA;
void DMA1_Stream2_IRQHandler(void) WA;
void DMA1_Stream3_IRQHandler(void) WA;
void DMA1_Stream4_IRQHandler(void) WA;
void DMA1_Stream5_IRQHandler(void) WA;
void DMA1_Stream6_IRQHandler(void) WA;
void ADC_IRQHandler(void) WA;
void CAN1_TX_IRQHandler(void) WA;
void CAN1_RX0_IRQHandler(void) WA;
void CAN1_RX1_IRQHandler(void) WA;
void CAN1_SCE_IRQHandler(void) WA;
void EXTI9_5_IRQHandler(void) WA;
void TIM1_BRK_TIM9_IRQHandler(void) WA;
void TIM1_UP_TIM10_IRQHandler(void) WA;
void TIM1_TRG_COM_TIM11_IRQHandler(void) WA;
void TIM1_CC_IRQHandler(void) WA;
void TIM2_IRQHandler(void) WA;
void TIM3_IRQHandler(void) WA;
void TIM4_IRQHandler(void) WA;
void I2C1_EV_IRQHandler(void) WA;
void I2C1_ER_IRQHandler(void) WA;
void I2C2_EV_IRQHandler(void) WA;
void I2C2_ER_IRQHandler(void) WA;
void SPI1_IRQHandler(void) WA;
void SPI2_IRQHandler(void) WA;
void USART1_IRQHandler(void) WA;
void USART2_IRQHandler(void) WA;
void USART3_IRQHandler(void) WA;
void EXTI15_10_IRQHandler(void) WA;
void RTC_Alarm_IRQHandler(void) WA;
void OTG_FS_WKUP_IRQHandler(void) WA;
void TIM8_BRK_TIM12_IRQHandler(void) WA;
void TIM8_UP_TIM13_IRQHandler(void) WA;
void TIM8_TRG_COM_TIM14_IRQHandler(void) WA;
void TIM8_CC_IRQHandler(void) WA;
void DMA1_Stream7_IRQHandler(void) WA;
void FMC_IRQHandler(void) WA;
void SDIO_IRQHandler(void) WA;
void TIM5_IRQHandler(void) WA;
void SPI3_IRQHandler(void) WA;
void UART4_IRQHandler(void) WA;
void UART5_IRQHandler(void) WA;
void TIM6_DAC_IRQHandler(void) WA;
void TIM7_IRQHandler(void) WA;
void DMA2_Stream0_IRQHandler(void) WA;
void DMA2_Stream1_IRQHandler(void) WA;
void DMA2_Stream2_IRQHandler(void) WA;
void DMA2_Stream3_IRQHandler(void) WA;
void DMA2_Stream4_IRQHandler(void) WA;
void ETH_IRQHandler(void) WA;
void ETH_WKUP_IRQHandler(void) WA;
void CAN2_TX_IRQHandler(void) WA;
void CAN2_RX0_IRQHandler(void) WA;
void CAN2_RX1_IRQHandler(void) WA;
void CAN2_SCE_IRQHandler(void) WA;
void OTG_FS_IRQHandler(void) WA;
void DMA2_Stream5_IRQHandler(void) WA;
void DMA2_Stream6_IRQHandler(void) WA;
void DMA2_Stream7_IRQHandler(void) WA;
void USART6_IRQHandler(void) WA;
void I2C3_EV_IRQHandler(void) WA;
void I2C3_ER_IRQHandler(void) WA;
void OTG_HS_EP1_OUT_IRQHandler(void) WA;
void OTG_HS_EP1_IN_IRQHandler(void) WA;
void OTG_HS_WKUP_IRQHandler(void) WA;
void OTG_HS_IRQHandler(void) WA;
void DCMI_IRQHandler(void) WA;
void HASH_RNG_IRQHandler(void) WA;
void FPU_IRQHandler(void) WA;
void UART7_IRQHandler(void) WA;
void UART8_IRQHandler(void) WA;
void SPI4_IRQHandler(void) WA;
void SPI5_IRQHandler(void) WA;
void SPI6_IRQHandler(void) WA;
void SAI1_IRQHandler(void) WA;
void LTDC_IRQHandler(void) WA;
void LTDC_ER_IRQHandler(void) WA;
void DMA2D_IRQHandler(void) WA;
//
volatile const func_ptr device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	WWDG_IRQHandler,
	PVD_IRQHandler,            //PVD through EXTI Line detection
	TAMP_STAMP_IRQHandler,     //Tamper and TimeStamps through the EXTI line
	RTC_WKUP_IRQHandler,       //RTCkeup through the EXTI line
	FLASH_IRQHandler,
	RCC_IRQHandler,
	EXTI0_IRQHandler,
	EXTI1_IRQHandler,
	EXTI2_IRQHandler,
	EXTI3_IRQHandler,
	EXTI4_IRQHandler,
	DMA1_Stream0_IRQHandler,
	DMA1_Stream1_IRQHandler,
	DMA1_Stream2_IRQHandler,
	DMA1_Stream3_IRQHandler,
	DMA1_Stream4_IRQHandler,
	DMA1_Stream5_IRQHandler,
	DMA1_Stream6_IRQHandler,
	ADC_IRQHandler,
	CAN1_TX_IRQHandler,
	CAN1_RX0_IRQHandler,
	CAN1_RX1_IRQHandler,
	CAN1_SCE_IRQHandler,
	EXTI9_5_IRQHandler,
	TIM1_BRK_TIM9_IRQHandler,
	TIM1_UP_TIM10_IRQHandler,
	TIM1_TRG_COM_TIM11_IRQHandler,
	TIM1_CC_IRQHandler,
	TIM2_IRQHandler,
	TIM3_IRQHandler,
	TIM4_IRQHandler,
	I2C1_EV_IRQHandler,
	I2C1_ER_IRQHandler,
	I2C2_EV_IRQHandler,
	I2C2_ER_IRQHandler,
	SPI1_IRQHandler,
	SPI2_IRQHandler,
	USART1_IRQHandler,
	USART2_IRQHandler,
	USART3_IRQHandler,
	EXTI15_10_IRQHandler,
	RTC_Alarm_IRQHandler,
	OTG_FS_WKUP_IRQHandler,
	TIM8_BRK_TIM12_IRQHandler,
	TIM8_UP_TIM13_IRQHandler,
	TIM8_TRG_COM_TIM14_IRQHandler,
	TIM8_CC_IRQHandler,
	DMA1_Stream7_IRQHandler,
	FMC_IRQHandler,
	SDIO_IRQHandler,
	TIM5_IRQHandler,
	SPI3_IRQHandler,
	UART4_IRQHandler,
	UART5_IRQHandler,
	TIM6_DAC_IRQHandler,
	TIM7_IRQHandler,
	DMA2_Stream0_IRQHandler,
	DMA2_Stream1_IRQHandler,
	DMA2_Stream2_IRQHandler,
	DMA2_Stream3_IRQHandler,
	DMA2_Stream4_IRQHandler,
	ETH_IRQHandler,
	ETH_WKUP_IRQHandler,
	CAN2_TX_IRQHandler,
	CAN2_RX0_IRQHandler,
	CAN2_RX1_IRQHandler,
	CAN2_SCE_IRQHandler,
	OTG_FS_IRQHandler,
	DMA2_Stream5_IRQHandler,
	DMA2_Stream6_IRQHandler,
  	DMA2_Stream7_IRQHandler,
	USART6_IRQHandler,
	I2C3_EV_IRQHandler,
	I2C3_ER_IRQHandler,
	OTG_HS_EP1_OUT_IRQHandler,
	OTG_HS_EP1_IN_IRQHandler,
	OTG_HS_WKUP_IRQHandler,
	OTG_HS_IRQHandler,
	DCMI_IRQHandler,
	0,//Reserved
	HASH_RNG_IRQHandler,
	FPU_IRQHandler,
	UART7_IRQHandler,
	UART8_IRQHandler,
	SPI4_IRQHandler,
	SPI5_IRQHandler,
	SPI6_IRQHandler,
	SAI1_IRQHandler,
	LTDC_IRQHandler,
	LTDC_ER_IRQHandler,
	DMA2D_IRQHandler
};

//ver190122
#ifndef _SYSTEM_H_
#define _SYSTEM_H_
//
#include "stm32f429xx.h"
#include "std_system.h"
//
#define USB_OTG_HS_PCGCCTL        	*(__IO uint32_t *)(0x40040E00)
#define USB_OTG_HS_DEVICE     		((USB_OTG_DeviceTypeDef *)(0x40040800))
#define USB_OTG_HS_INEP(i)    		((USB_OTG_INEndpointTypeDef *)(0x40040900 + i * 0x20))
#define USB_OTG_HS_OUTEP(i)  		((USB_OTG_OUTEndpointTypeDef *)(0x40040B00 + i * 0x20))
#define USB_DFIFO(i)   				*(__IO uint32_t *)(0x40041000 + (i * 0x1000))
#define __fw_main   				0x08004000
#define __fw_copy   				0x08008000
#define __fw_new   					0x0800c000
#define PAGE_SIZE					16384
#define BOOT_TIMEOUT				10000000
#define SDRAM_SIZE					0x800000
#define I2C_TIMEOUT					500000
//
typedef enum
{
  off = 0,
  on = 1
} led_state;
typedef enum
{
  red = 1,
  green = 2,
  both = 3
} led;
//
void sys_init(void);
void delay_us(uint32_t us);
void delay_ms(uint32_t ms);
void project_def_handler(uint32_t* ipsr);
void board_LED(led color, led_state state);
void uart1_init(void);
void uart_print_char(void* uart, uint8_t data);
void init_gpio(void);
void spi5_init(void);
void i2c3_init(void);
void sdram_init(void);
void spi5_wr(uint8_t data);
uint8_t spi5_wr_rd(uint8_t data);
void i2c3_wr(uint8_t addr, uint8_t reg, uint8_t size, uint8_t* tx_buffer);
uint8_t i2c3_rd(uint8_t addr, uint8_t reg, uint8_t size, uint8_t* rx_buffer);
void rng_init(void);
uint32_t rng_random_data(void);
void kill_jtag(void);
void crc32eth_init(void);
void crc32eth_calc(uint32_t* crc, uint32_t* data);
void bootloader_error_signal(uint8_t reason);
void flash_erase_page(uint32_t addr);
void flash_copy_data(uint32_t* dst, uint32_t* src, uint32_t size);
void boot_init(void);
void flash_unlock(void);
void flash_lock(void);
void fw_copy(uint32_t* src, uint32_t* dst, uint32_t size);
void crc32eth_reset(void);
void bootload_run_req(void);
uint16_t spi_wr_rd(SPI_TypeDef* spi, uint16_t data_wr);
void sdram_test(void);
#endif

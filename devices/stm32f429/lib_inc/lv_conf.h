#ifndef LV_CONF_H
#define LV_CONF_H
//
#define LV_HOR_RES_MAX          					(240)
#define LV_VER_RES_MAX          					(320)
#define LV_COLOR_DEPTH     							16
#define LV_COLOR_16_SWAP   							0
#define LV_COLOR_SCREEN_TRANSP    					0
#define LV_COLOR_TRANSP    							LV_COLOR_BLACK
#define LV_ANTIALIAS        						1
#define LV_DISP_DEF_REFR_PERIOD      				50/*[ms]*/
#define LV_DPI              						100
//
#define LV_MEM_CUSTOM      							0
#if LV_MEM_CUSTOM == 0
	#define LV_MEM_SIZE   	 						(64*1024)
	#define LV_MEM_ATTR								__attribute__ ((section (".ccmram")))
	#define LV_MEM_AUTO_DEFRAG  					1
#endif
#define LV_ENABLE_GC 								0
#define LV_INDEV_DEF_READ_PERIOD            		50
#define LV_INDEV_DEF_DRAG_LIMIT             		10
#define LV_INDEV_DEF_DRAG_THROW             		20
#define LV_INDEV_DEF_LONG_PRESS_TIME       		 	400
#define LV_INDEV_DEF_LONG_PRESS_REP_TIME    		100
#define LV_USE_ANIMATION        					0
#define LV_USE_SHADOW           					1
#define LV_USE_GROUP            					0
#define LV_USE_GPU              					0
#define LV_USE_FILESYSTEM       					0
#define LV_USE_USER_DATA        					0
#define LV_IMG_CF_INDEXED       					0
#define LV_IMG_CF_ALPHA         					0
#define LV_IMG_CACHE_DEF_SIZE       				1
#define LV_ATTRIBUTE_TICK_INC
#define LV_ATTRIBUTE_TASK_HANDLER
#define LV_ATTRIBUTE_MEM_ALIGN
#define LV_ATTRIBUTE_LARGE_CONST
#define LV_TICK_CUSTOM     							0
#define LV_USE_LOG      							0
#define LV_THEME_LIVE_UPDATE    					0
#define LV_USE_THEME_TEMPL      					0   /*Just for test*/
#define LV_USE_THEME_DEFAULT    					0   /*Built mainly from the built-in styles. Consumes very few RAM*/
#define LV_USE_THEME_ALIEN      					0   /*Dark futuristic theme*/
#define LV_USE_THEME_NIGHT      					1   /*Dark elegant theme*/
#define LV_USE_THEME_MONO       					0   /*Mono color theme for monochrome displays*/
#define LV_USE_THEME_MATERIAL   					0   /*Flat theme with bold colors and light shadows*/
#define LV_USE_THEME_ZEN        					0   /*Peaceful, mainly light theme */
#define LV_USE_THEME_NEMO       					0 	/*Water-like theme based on the movie "Finding Nemo"*/
#define LV_FONT_ROBOTO_12    						0
#define LV_FONT_ROBOTO_16    						1
#define LV_FONT_ROBOTO_22    						0
#define LV_FONT_ROBOTO_28    						0
#define LV_FONT_UNSCII_8     						0
#define LV_FONT_CUSTOM_DECLARE
#define LV_FONT_DEFAULT        						&lv_font_roboto_16
#define LV_TXT_ENC 									LV_TXT_ENC_UTF8
#define LV_TXT_BREAK_CHARS                  		" ,.;:-_"
#define LV_USE_OBJ_REALIGN          				0
#define LV_USE_EXT_CLICK_AREA  						LV_EXT_CLICK_AREA_OFF
#define LV_USE_ARC      							1
#define LV_USE_BAR      							1
#define LV_USE_BTN      							1
#if LV_USE_BTN != 0
	#define LV_BTN_INK_EFFECT   					0
#endif
#define LV_USE_BTNM     							1
#define LV_USE_CALENDAR 							0
#define LV_USE_CANVAS   							0
#define LV_USE_CB       							0
#define LV_USE_CHART    							1
#if LV_USE_CHART
	#define LV_CHART_AXIS_TICK_LABEL_MAX_LEN    	20
#endif
#define LV_USE_CONT     							1
#define LV_USE_DDLIST    							0
#define LV_USE_GAUGE 								0
#define LV_USE_IMG      							0
#define LV_USE_IMGBTN   							0
#define LV_USE_KB       							0
#define LV_USE_LABEL    							1
#if LV_USE_LABEL != 0
	#define LV_LABEL_DEF_SCROLL_SPEED       		25
	#define LV_LABEL_WAIT_CHAR_COUNT        		3
	#define LV_LABEL_TEXT_SEL               		0
	#define LV_LABEL_LONG_TXT_HINT          		0
#endif
#define LV_USE_LED									0
#define LV_USE_LINE     							0
#define LV_USE_LIST  								0
#define LV_USE_LMETER   							0
#define LV_USE_MBOX     							1
#define LV_USE_PAGE									0
#define LV_USE_PRELOAD								0
#define LV_USE_ROLLER								0
#define LV_USE_SLIDER    							1
#define LV_USE_SPINBOX       						0
#define LV_USE_SW       							1
#define LV_USE_TA 									0
#define LV_USE_TABLE    							1
#if LV_USE_TABLE
	#define LV_TABLE_COL_MAX    					12
#endif
#define LV_USE_TABVIEW     							0
#define LV_USE_TILEVIEW 							0
#define LV_USE_WIN      							0
//
#include "src/lv_conf_checker.h"
//
typedef int16_t lv_coord_t;
typedef void * lv_img_decoder_user_data_t;
typedef void * lv_disp_drv_user_data_t;
typedef void * lv_indev_drv_user_data_t;
typedef void * lv_font_user_data_t;
typedef void * lv_obj_user_data_t;
//
#endif /*LV_CONF_H*/

//ver190118
#include "lcd.h"
//
uint8_t const font[0x80][FONT_HEIGHT] = //10 - number of rows
{
	{0},// \0
	{0},//nothing
	{0},//nothing
	{0},//end of text
	{0},//END OF TRANSMISSION
	{0},//nothing
	{0},//nothing
	{0},//bell \a
	{0},//backspace \b
	{0},//hor. tab. \t
	{11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},//line feed \n
	{0},//vert. tab. \v
	{0},//form feed \f
	{11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},//carriage return \r
	{0},//nothing
	{0},//nothing
	{0},//nothing
	{0},//nothing
	{0},//nothing
	{0},//nothing
	{0},//nothing
	{0},//nothing
	{0},//nothing
	{0},//nothing
	{0},//nothing
	{0},//nothing
	{0},//nothing
	{0},//escape \e
	{0},//nothing
	{0},//nothing
	{0},//nothing
	{0},//nothing
	{4, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},//space
	{1, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x01},//!
	{3, 0x05, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},//"
	{6, 0x00, 0x00, 0x12, 0x3F, 0x12, 0x12, 0x3F, 0x12, 0x00},//#
	{5, 0x04, 0x0E, 0x15, 0x14, 0x0E, 0x05, 0x15, 0x0E, 0x04},//$
	{7, 0x24, 0x54, 0x24, 0x08, 0x08, 0x08, 0x12, 0x15, 0x12},//%
	{6, 0x0C, 0x12, 0x12, 0x14, 0x08, 0x14, 0x25, 0x22, 0x1D},//&
	{1, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},//'
	{3, 0x01, 0x02, 0x02, 0x04, 0x04, 0x04, 0x02, 0x02, 0x01},//(
	{3, 0x04, 0x02, 0x02, 0x01, 0x01, 0x01, 0x02, 0x02, 0x04},//)
	{5, 0x04, 0x04, 0x1F, 0x04, 0x0A, 0x0A, 0x00, 0x00, 0x00},//*
	{5, 0x00, 0x00, 0x04, 0x04, 0x1F, 0x04, 0x04, 0x00, 0x00},//+
	{2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x03, 0x01},//,
	{4, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x00, 0x00, 0x00},//-
	{2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x03},//.
	{3, 0x01, 0x01, 0x01, 0x02, 0x02, 0x02, 0x04, 0x04, 0x04},//slash
	{5, 0x0e, 0x11, 0x11, 0x13, 0x15, 0x19, 0x11, 0x11, 0x0e},//0
	{3, 0x01, 0x03, 0x05, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01},//1
	{5, 0x0e, 0x11, 0x01, 0x01, 0x01, 0x02, 0x04, 0x08, 0x1f},//2
	{5, 0x0e, 0x11, 0x01, 0x01, 0x06, 0x01, 0x01, 0x11, 0x0e},//3
	{5, 0x09, 0x09, 0x09, 0x11, 0x11, 0x1f, 0x01, 0x01, 0x01},//4
	{5, 0x1f, 0x10, 0x10, 0x10, 0x1e, 0x01, 0x01, 0x11, 0x0e},//5
	{5, 0x0e, 0x11, 0x10, 0x10, 0x1e, 0x11, 0x11, 0x11, 0x0e},//6
	{5, 0x1f, 0x01, 0x01, 0x01, 0x02, 0x02, 0x04, 0x04, 0x04},//7
	{5, 0x0e, 0x11, 0x11, 0x11, 0x0e, 0x11, 0x11, 0x11, 0x0e},//8
	{5, 0x0e, 0x11, 0x11, 0x11, 0x0f, 0x01, 0x01, 0x11, 0x0e},//9
	{2, 0x00, 0x03, 0x03, 0x00, 0x00, 0x00, 0x03, 0x03, 0x00},//:
	{2, 0x00, 0x03, 0x03, 0x00, 0x00, 0x00, 0x03, 0x03, 0x01},//;
	{4, 0x00, 0x01, 0x02, 0x04, 0x08, 0x04, 0x02, 0x01, 0x00},//<
	{4, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x0F, 0x00, 0x00, 0x00},//=
	{4, 0x00, 0x08, 0x04, 0x02, 0x01, 0x02, 0x04, 0x08, 0x00},//>
	{5, 0x0E, 0x11, 0x01, 0x02, 0x04, 0x04, 0x00, 0x00, 0x04},//?
	{8, 0x3C, 0x42, 0x99, 0xA5, 0xA5, 0xA5, 0x9E, 0x40, 0x3E},//@
	{6, 0x0c, 0x12, 0x12, 0x21, 0x21, 0x3f, 0x21, 0x21, 0x21},//A
	{6, 0x3c, 0x22, 0x22, 0x22, 0x3c, 0x22, 0x21, 0x21, 0x3e},//B
	{6, 0x0e, 0x11, 0x20, 0x20, 0x20, 0x20, 0x20, 0x11, 0x0e},//C
	{6, 0x3c, 0x12, 0x11, 0x11, 0x11, 0x11, 0x11, 0x12, 0x3c},//D
	{5, 0x1f, 0x10, 0x10, 0x10, 0x1f, 0x10, 0x10, 0x10, 0x1f},//E
	{5, 0x1f, 0x10, 0x10, 0x10, 0x1e, 0x10, 0x10, 0x10, 0x10},//F
	{6, 0x0e, 0x11, 0x20, 0x20, 0x20, 0x27, 0x21, 0x11, 0x0e},//G
	{5, 0x11, 0x11, 0x11, 0x11, 0x1f, 0x11, 0x11, 0x11, 0x11},//H
	{3, 0x07, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x07},//I
	{5, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x12, 0x0c},//J
	{5, 0x11, 0x11, 0x11, 0x12, 0x1c, 0x12, 0x11, 0x11, 0x11},//K
	{5, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x1f},//L
	{7, 0x41, 0x63, 0x55, 0x49, 0x41, 0x41, 0x41, 0x41, 0x41},//M
	{6, 0x21, 0x31, 0x29, 0x29, 0x25, 0x25, 0x23, 0x21, 0x21},//N
	{6, 0x0c, 0x12, 0x21, 0x21, 0x21, 0x21, 0x21, 0x12, 0x0c},//O
	{5, 0x1e, 0x11, 0x11, 0x11, 0x1e, 0x10, 0x10, 0x10, 0x10},//P
	{6, 0x0c, 0x12, 0x21, 0x21, 0x21, 0x21, 0x25, 0x12, 0x0d},//Q
	{5, 0x1e, 0x11, 0x11, 0x11, 0x12, 0x1c, 0x12, 0x11, 0x11},//R
	{5, 0x0e, 0x11, 0x10, 0x08, 0x04, 0x02, 0x01, 0x11, 0x0e},//S
	{7, 0x7f, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08},//T
	{6, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x12, 0x0c},//U
	{5, 0x11, 0x11, 0x11, 0x0a, 0x0a, 0x0a, 0x04, 0x04, 0x04},//V
	{7, 0x41, 0x41, 0x41, 0x41, 0x49, 0x49, 0x49, 0x2a, 0x14},//W
	{5, 0x11, 0x11, 0x0a, 0x0a, 0x04, 0x0a, 0x0a, 0x11, 0x11},//X
	{5, 0x11, 0x11, 0x0a, 0x0a, 0x04, 0x04, 0x04, 0x04, 0x04},//Y
	{5, 0x1f, 0x01, 0x02, 0x02, 0x04, 0x08, 0x08, 0x10, 0x1f},//Z
	{2, 0x03, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x03},//[
	{3, 0x04, 0x04, 0x04, 0x02, 0x02, 0x02, 0x01, 0x01, 0x01},//backslash
	{2, 0x03, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x03},//]
	{3, 0x02, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},//^
	{5, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F},//_
	{2, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},//`
	{4, 0x00, 0x00, 0x00, 0x00, 0x06, 0x09, 0x09, 0x09, 0x07},//a
	{4, 0x00, 0x08, 0x08, 0x08, 0x0E, 0x09, 0x09, 0x09, 0x0E},//b
	{3, 0x00, 0x00, 0x00, 0x00, 0x03, 0x04, 0x04, 0x04, 0x03},//c
	{4, 0x00, 0x01, 0x01, 0x01, 0x07, 0x09, 0x09, 0x09, 0x07},//d
	{4, 0x00, 0x00, 0x00, 0x00, 0x06, 0x09, 0x0F, 0x08, 0x06},//e
	{4, 0x00, 0x02, 0x05, 0x04, 0x04, 0x0E, 0x04, 0x04, 0x04},//f
	{4, 0x00, 0x00, 0x00, 0x06, 0x09, 0x09, 0x07, 0x01, 0x0E},//g
	{4, 0x00, 0x08, 0x08, 0x08, 0x0E, 0x09, 0x09, 0x09, 0x09},//h
	{1, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x01, 0x01},//i
	{3, 0x00, 0x01, 0x00, 0x01, 0x01, 0x01, 0x01, 0x05, 0x02},//j
	{4, 0x00, 0x08, 0x08, 0x08, 0x09, 0x0a, 0x0c, 0x0a, 0x09},//k
	{2, 0x00, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x01},//l
	{5, 0x00, 0x00, 0x00, 0x00, 0x1a, 0x15, 0x15, 0x15, 0x15},//m
	{4, 0x00, 0x00, 0x00, 0x00, 0x0e, 0x09, 0x09, 0x09, 0x09},//n
	{4, 0x00, 0x00, 0x00, 0x00, 0x06, 0x09, 0x09, 0x09, 0x06},//o
	{4, 0x00, 0x00, 0x00, 0x0e, 0x09, 0x09, 0x0e, 0x08, 0x08},//p
	{4, 0x00, 0x00, 0x00, 0x07, 0x09, 0x09, 0x07, 0x01, 0x01},//q
	{4, 0x00, 0x00, 0x00, 0x00, 0x05, 0x06, 0x04, 0x04, 0x04},//r
	{3, 0x00, 0x00, 0x00, 0x00, 0x03, 0x04, 0x02, 0x01, 0x06},//s
	{4, 0x00, 0x00, 0x00, 0x00, 0x04, 0x0e, 0x04, 0x05, 0x02},//t
	{4, 0x00, 0x00, 0x00, 0x00, 0x09, 0x09, 0x09, 0x09, 0x07},//u
	{4, 0x00, 0x00, 0x00, 0x00, 0x09, 0x09, 0x0a, 0x0c, 0x08},//v
	{5, 0x00, 0x00, 0x00, 0x00, 0x11, 0x11, 0x15, 0x15, 0x0a},//w
	{3, 0x00, 0x00, 0x00, 0x00, 0x05, 0x05, 0x02, 0x05, 0x05},//x
	{4, 0x00, 0x00, 0x00, 0x00, 0x09, 0x09, 0x07, 0x01, 0x06},//y
	{3, 0x00, 0x00, 0x00, 0x00, 0x07, 0x01, 0x02, 0x04, 0x07},//z
	{3, 0x01, 0x02, 0x02, 0x02, 0x04, 0x02, 0x02, 0x02, 0x01},//{
	{1, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01},//|
	{3, 0x04, 0x02, 0x02, 0x02, 0x01, 0x02, 0x02, 0x02, 0x04},//}
	{5, 0x00, 0x00, 0x00, 0x08, 0x15, 0x02, 0x00, 0x00, 0x00},//~
	{0}//delete
};
static uint8_t x_pos = 0;
static uint16_t y_pos = 0;
static colors default_color = C_Black;
union
{
	struct ts_calibration_data
	{
		float32_t xa,xb,ya,yb;
		uint32_t key;
	}data;
	uint32_t mem[5];
}ts_cal;
volatile push push_d = {0};
uint32_t* ts_cal_sram = (uint32_t*)BKPSRAM_BASE;
uint16_t screen_buffer[Y_RES][X_RES] __attribute__ ((section (".sdram")));
//
void lcd_clk(void)
{
	//LTDC clock
	RCC->PLLSAICFGR &= 0x8FFF803F;
	RCC->PLLSAICFGR |= 0x40003000;
	RCC->DCKCFGR |= 0x00020000;
	RCC->CR |= RCC_CR_PLLSAION;
    while((RCC->CR & RCC_CR_PLLSAIRDY) != RCC_CR_PLLSAIRDY){}
}
void lcd_init(colors color)
{
	sdram_init();
	lcd_clk();
	RCC->APB2ENR |= RCC_APB2ENR_LTDCEN;
	//LCD init
	LTDC->SSCR = ((9 << 16)|1);
	LTDC->BPCR = ((29 << 16)|3);
	LTDC->AWCR = ((269 << 16)|323);
	LTDC->TWCR = ((279 << 16)|327);
	//LTDC->IER |= LTDC_IER_TERRIE|LTDC_IER_FUIE;
	LTDC->GCR |= LTDC_GCR_LTDCEN;
	GPIOC->BSRR = 0x00040000;
	GPIOC->BSRR = 0x00000004;
	spi5_init();
	//LCD configure
	ili9341_WriteReg(0xCA);
	ili9341_WriteData(0xC3);
	ili9341_WriteData(0x08);
	ili9341_WriteData(0x50);
	ili9341_WriteReg(LCD_POWERB);
	ili9341_WriteData(0x00);
	ili9341_WriteData(0xC1);
	ili9341_WriteData(0x30);
	ili9341_WriteReg(LCD_POWER_SEQ);
	ili9341_WriteData(0x64);
	ili9341_WriteData(0x03);
	ili9341_WriteData(0x12);
	ili9341_WriteData(0x81);
	ili9341_WriteReg(LCD_DTCA);
	ili9341_WriteData(0x85);
	ili9341_WriteData(0x00);
	ili9341_WriteData(0x78);
	ili9341_WriteReg(LCD_POWERA);
	ili9341_WriteData(0x39);
	ili9341_WriteData(0x2C);
	ili9341_WriteData(0x00);
	ili9341_WriteData(0x34);
	ili9341_WriteData(0x02);
	ili9341_WriteReg(LCD_PRC);
	ili9341_WriteData(0x20);
	ili9341_WriteReg(LCD_DTCB);
	ili9341_WriteData(0x00);
	ili9341_WriteData(0x00);
	ili9341_WriteReg(LCD_FRMCTR1);
	ili9341_WriteData(0x00);
	ili9341_WriteData(0x1B);
	ili9341_WriteReg(LCD_DFC);
	ili9341_WriteData(0x0A);
	ili9341_WriteData(0xA2);
	ili9341_WriteReg(LCD_POWER1);
	ili9341_WriteData(0x10);
	ili9341_WriteReg(LCD_POWER2);
	ili9341_WriteData(0x10);
	ili9341_WriteReg(LCD_VCOM1);
	ili9341_WriteData(0x45);
	ili9341_WriteData(0x15);
	ili9341_WriteReg(LCD_VCOM2);
	ili9341_WriteData(0x90);
	ili9341_WriteReg(LCD_MAC);
	ili9341_WriteData(0xC8);
	ili9341_WriteReg(LCD_3GAMMA_EN);
	ili9341_WriteData(0x00);
	ili9341_WriteReg(LCD_RGB_INTERFACE);
	ili9341_WriteData(0xC2);
	ili9341_WriteReg(LCD_DFC);
	ili9341_WriteData(0x0A);
	ili9341_WriteData(0xA7);
	ili9341_WriteData(0x27);
	ili9341_WriteData(0x04);
	ili9341_WriteReg(LCD_COLUMN_ADDR);
	ili9341_WriteData(0x00);
	ili9341_WriteData(0x00);
	ili9341_WriteData(0x00);
	ili9341_WriteData(0xEF);
	ili9341_WriteReg(LCD_PAGE_ADDR);
	ili9341_WriteData(0x00);
	ili9341_WriteData(0x00);
	ili9341_WriteData(0x01);
	ili9341_WriteData(0x3F);
	ili9341_WriteReg(LCD_INTERFACE);
	ili9341_WriteData(0x01);
	ili9341_WriteData(0x00);
	ili9341_WriteData(0x06);
	ili9341_WriteReg(LCD_GRAM);
	delay_ms(200);
	ili9341_WriteReg(LCD_GAMMA);
	ili9341_WriteData(0x01);
	ili9341_WriteReg(LCD_PGAMMA);
	ili9341_WriteData(0x0F);
	ili9341_WriteData(0x29);
	ili9341_WriteData(0x24);
	ili9341_WriteData(0x0C);
	ili9341_WriteData(0x0E);
	ili9341_WriteData(0x09);
	ili9341_WriteData(0x4E);
	ili9341_WriteData(0x78);
	ili9341_WriteData(0x3C);
	ili9341_WriteData(0x09);
	ili9341_WriteData(0x13);
	ili9341_WriteData(0x05);
	ili9341_WriteData(0x17);
	ili9341_WriteData(0x11);
	ili9341_WriteData(0x00);
	ili9341_WriteReg(LCD_NGAMMA);
	ili9341_WriteData(0x00);
	ili9341_WriteData(0x16);
	ili9341_WriteData(0x1B);
	ili9341_WriteData(0x04);
	ili9341_WriteData(0x11);
	ili9341_WriteData(0x07);
	ili9341_WriteData(0x31);
	ili9341_WriteData(0x33);
	ili9341_WriteData(0x42);
	ili9341_WriteData(0x05);
	ili9341_WriteData(0x0C);
	ili9341_WriteData(0x0A);
	ili9341_WriteData(0x28);
	ili9341_WriteData(0x2F);
	ili9341_WriteData(0x0F);
	ili9341_WriteReg(LCD_SLEEP_OUT);
	delay_ms(200);
	ili9341_WriteReg(LCD_DISPLAY_ON);
	ili9341_WriteReg(LCD_GRAM);
	//layer 2
	LTDC_Layer2->WHPCR = ((((LTDC->BPCR & LTDC_BPCR_AHBP) >> 16) + 1)|((240 + ((LTDC->BPCR & LTDC_BPCR_AHBP) >> 16)) << 16));
	LTDC_Layer2->WVPCR = (((LTDC->BPCR & LTDC_BPCR_AVBP) + 1)|((320 + (LTDC->BPCR & LTDC_BPCR_AVBP)) << 16));
	LTDC_Layer2->PFCR = 0x00000002;//RGB565
	LTDC_Layer2->CACR = 255;
	LTDC_Layer2->BFCR = 0x00000607;
	LTDC_Layer2->CFBAR = (uint32_t)screen_buffer;
	LTDC_Layer2->CFBLR  = (((240 * 2) << 16)|((240 * 2)  + 3));
	LTDC_Layer2->CFBLNR  = 320;
	LTDC_Layer2->CR |= LTDC_LxCR_LEN;
	LTDC->SRCR = LTDC_SRCR_IMR;
	LTDC->GCR |= LTDC_GCR_DTEN;
	LTDC_Layer1->CACR = 0;
	LTDC->SRCR = LTDC_SRCR_IMR;
	screen_color(color);
	default_color = color;
}
void ili9341_WriteReg(uint8_t LCD_Reg)
{
	GPIOD->BSRR = 0x20000000;
	GPIOC->BSRR = 0x00040000;
	spi5_wr(LCD_Reg);
	GPIOC->BSRR = 0x00000004;
}
void ili9341_WriteData(uint8_t RegValue)
{
	GPIOD->BSRR = 0x00002000;
	GPIOC->BSRR = 0x00040000;
	spi5_wr(RegValue);
	GPIOC->BSRR = 0x00000004;
}
void lcd_print_char(uint8_t pc, colors color)
{
	uint8_t temp1;
	const uint8_t* letter;
	uint16_t *pixel_pointer;
	letter = font[pc];
	if(letter[0] != 0)
	{
		if(letter[0] < 10)
		{
			temp1 = x_pos + letter[0];
			if(temp1 > X_RES)
			{
				y_pos += (FONT_HEIGHT + 1);
				if(y_pos > (Y_RES - (FONT_HEIGHT - 1))) new_screen();
				else x_pos = 0;
			}
			pixel_pointer = (uint16_t*)((uint32_t)screen_buffer + (y_pos*240 + x_pos)*2);
			for(uint8_t a = 1; a < FONT_HEIGHT; a++)
			{
				for(uint8_t b = 1; b <= letter[0]; b++)
				{
					if (letter[a] >> (letter[0]-b) & 1) *(pixel_pointer + b - 1 + 0xf0*(a-1)) = color;
				}
			}
			x_pos = x_pos + 2 + letter[0];
		}
		else
		{
			switch(pc)
			{
				case 0x0a:
					y_pos += (FONT_HEIGHT + 1);
					if(y_pos > (Y_RES - (FONT_HEIGHT - 1))) new_screen();
				break;
				case 0x0d: x_pos = 0; break;
				default: break;
			}
		}
	}
}
void lcd_print_string(uint8_t* string, colors color)
{
	uint8_t i=0;
	while(string[i]) lcd_print_char(string[i++], color);
}
void new_line(void)
{
	y_pos += (FONT_HEIGHT + 1);
	if(y_pos > (Y_RES - (FONT_HEIGHT - 1))) new_screen();
	x_pos = 0;
}
void screen_color(colors color)
{
	for(uint16_t i=0; i<Y_RES; i++)
	{
		for(uint16_t j=0; j<X_RES; j++) screen_buffer[i][j] = color;
	}
}
void ts_init(colors color)
{
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	PWR->CR |= PWR_CR_DBP;
	RCC->AHB1ENR |= RCC_AHB1ENR_BKPSRAMEN;
	PWR->CSR |= PWR_CSR_BRE;
	i2c3_init();
	if(stmpe811_ReadID(TS_I2C_ADDRESS) == STMPE811_ID)
	{
		stmpe811_Reset(TS_I2C_ADDRESS);
		stmpe811_TS_Start(TS_I2C_ADDRESS);
		stmpe811_TS_EnableIT(TS_I2C_ADDRESS);
		for(uint8_t i=0; i<5; i++) ts_cal.mem[i] = ts_cal_sram[i];
		if(ts_cal.data.key != TS_KEY) ts_calibration(color);
	}
	else
	{
		lcd_print_char('E',C_Orange);
		while(1){};
	}
}
void lcd_draw_rectangle(my_point* p1, my_point* p2, uint16_t color)
{
	for (uint16_t j=p1->y; j < (p2->y+1); j++)
	{
		for (uint16_t i=p1->x; i < (p2->x+1); i++) screen_buffer[j][i] = color;
	}
}
void ts_calibration(colors color)
{
	my_point p1,p2;
	uint16_t xx1,xx2,xy1,xy2;
	//20%
	p1.x = 48-5;
	p1.y = 64-5;
	p2.x = 48+5;
	p2.y = 64+5;
	lcd_draw_rectangle(&p1, &p2, color);
	while(1)
	{
		if(push_d.flag)
		{
			xx1 = push_d.coord.x;
			xy1 = push_d.coord.y;
			break;
		}
	}
	new_screen();
	//80%
	p1.x = 198-5;
	p1.y = 256-5;
	p2.x = 198+5;
	p2.y = 256+5;
	lcd_draw_rectangle(&p1, &p2, color);
	delay_ms(300);
	push_d.flag = 0;
	while(1)
	{
		if(push_d.flag)
		{
			xx2 = push_d.coord.x;
			xy2 = push_d.coord.y;
			push_d.flag = 0;
			break;
		}
	}
	new_screen();
	ts_cal.data.xa = 150.0f/(float32_t)(xx2-xx1);//240*0.8-240*0.2=150
	ts_cal.data.xb = 198.0f-ts_cal.data.xa*(float32_t)xx2;
	ts_cal.data.ya = 192.0f/(float32_t)(xy2-xy1);//320*0.8-320*0.2=150
	ts_cal.data.yb = 256.0f-ts_cal.data.ya*(float32_t)xy2;
	ts_cal.data.key = TS_KEY;
	for(uint8_t i=0; i<5; i++) ts_cal_sram[i] = ts_cal.mem[i];
	/*lcd_print_string("Save?", C_White);
	ts_area areas[2];
	areas[0].lu.x = 10;
	areas[0].lu.y = 20;
	areas[0].rd.x = 50;
	areas[0].rd.y = 40;
	lcd_draw_rectangle(&areas[0].lu, &areas[0].rd, C_Green);
	areas[1].lu.x = 10;
	areas[1].lu.y = 60;
	areas[1].rd.x = 50;
	areas[1].rd.y = 80;
	lcd_draw_rectangle(&areas[1].lu, &areas[1].rd, C_Red);
	while(1)
	{
		if(push_d.flag)
		{
			push_d.flag = 0;
			if((push_d.coord.x > 10)&&(push_d.coord.x < 50))
			{
				if((push_d.coord.y > 60)&&(push_d.coord.y < 80)) return;
				else
				{
					if((push_d.coord.y > 20)&&(push_d.coord.y < 40))
					{
						uint32_t addr = (uint32_t)&ts_cal_rom.mem[0];
						FLASH->KEYR = 0x45670123;
						FLASH->KEYR = 0xCDEF89AB;
						FLASH->CR |= (FLASH_CR_PG|(2<<8));
						for(uint8_t i=0; i<5; i++)
						{
							while(FLASH->SR & FLASH_SR_BSY){};
							((uint32_t*)addr)[i] = ts_cal_ram.mem[i];
						}
						while(FLASH->SR & FLASH_SR_BSY){};
						FLASH->CR &= ~(FLASH_CR_PG);
						FLASH->CR |= FLASH_CR_LOCK;
						return;
					}
				}
			}
		}
	}*/
}
void new_screen(void)
{
	y_pos = 0;
	x_pos = 0;
	screen_color(default_color);
}
void test_font(colors font_color)
{
	new_screen();
	for (uint8_t i = 0x20; i < 0x7F; i++) lcd_print_char(i,font_color);
}
void stmpe811_Reset(uint16_t DeviceAddr)
{
	IOE_Write(DeviceAddr, STMPE811_REG_SYS_CTRL1, 2);
	IOE_Delay(10);
	IOE_Write(DeviceAddr, STMPE811_REG_SYS_CTRL1, 0);
	IOE_Delay(2);
}
uint16_t stmpe811_ReadID(uint16_t DeviceAddr)
{
	return ((IOE_Read(DeviceAddr, STMPE811_REG_CHP_ID_LSB) << 8)|(IOE_Read(DeviceAddr, STMPE811_REG_CHP_ID_MSB)));
}
void stmpe811_EnableGlobalIT(uint16_t DeviceAddr)
{
	uint8_t tmp = IOE_Read(DeviceAddr, STMPE811_REG_INT_CTRL);
	tmp |= (uint8_t)STMPE811_GIT_EN;
	IOE_Write(DeviceAddr, STMPE811_REG_INT_CTRL, tmp);
}
void stmpe811_DisableGlobalIT(uint16_t DeviceAddr)
{
	uint8_t tmp = IOE_Read(DeviceAddr, STMPE811_REG_INT_CTRL);
	tmp &= ~(uint8_t)STMPE811_GIT_EN;
	IOE_Write(DeviceAddr, STMPE811_REG_INT_CTRL, tmp);
}
void stmpe811_EnableITSource(uint16_t DeviceAddr, uint8_t Source)
{
	uint8_t tmp = IOE_Read(DeviceAddr, STMPE811_REG_INT_EN);
	tmp |= Source;
	IOE_Write(DeviceAddr, STMPE811_REG_INT_EN, tmp);
}
void stmpe811_DisableITSource(uint16_t DeviceAddr, uint8_t Source)
{
	uint8_t tmp = IOE_Read(DeviceAddr, STMPE811_REG_INT_EN);
	tmp &= ~Source;
  	IOE_Write(DeviceAddr, STMPE811_REG_INT_EN, tmp);
}
void stmpe811_SetITPolarity(uint16_t DeviceAddr, uint8_t Polarity)
{
	uint8_t tmp = IOE_Read(DeviceAddr, STMPE811_REG_INT_CTRL);
	tmp &= ~(uint8_t)0x04;
	tmp |= Polarity;
	IOE_Write(DeviceAddr, STMPE811_REG_INT_CTRL, tmp);
}
void stmpe811_SetITType(uint16_t DeviceAddr, uint8_t Type)
{
	uint8_t tmp = IOE_Read(DeviceAddr, STMPE811_REG_INT_CTRL);
	tmp &= ~(uint8_t)0x02;
	tmp |= Type;
	IOE_Write(DeviceAddr, STMPE811_REG_INT_CTRL, tmp);
}
uint8_t stmpe811_GlobalITStatus(uint16_t DeviceAddr, uint8_t Source)
{
	return((IOE_Read(DeviceAddr, STMPE811_REG_INT_STA) & Source) == Source);
}
uint8_t stmpe811_ReadGITStatus(uint16_t DeviceAddr, uint8_t Source)
{
	return((IOE_Read(DeviceAddr, STMPE811_REG_INT_STA) & Source));
}
void stmpe811_ClearGlobalIT(uint16_t DeviceAddr, uint8_t Source)
{
	IOE_Write(DeviceAddr, STMPE811_REG_INT_STA, Source);
}
void stmpe811_IO_Start(uint16_t DeviceAddr, uint32_t IO_Pin)
{
	uint8_t mode = IOE_Read(DeviceAddr, STMPE811_REG_SYS_CTRL2);
	mode &= ~(STMPE811_IO_FCT | STMPE811_ADC_FCT);
  	IOE_Write(DeviceAddr, STMPE811_REG_SYS_CTRL2, mode);
  	stmpe811_IO_DisableAF(DeviceAddr, (uint8_t)IO_Pin);
}
uint8_t stmpe811_IO_Config(uint16_t DeviceAddr, uint32_t IO_Pin, IO_ModeTypedef IO_Mode)
{
	uint8_t error_code = 0;
	switch(IO_Mode)
	{
		case IO_MODE_INPUT: /* Input mode */
			stmpe811_IO_InitPin(DeviceAddr, IO_Pin, STMPE811_DIRECTION_IN);
			break;
		case IO_MODE_OUTPUT: /* Output mode */
			stmpe811_IO_InitPin(DeviceAddr, IO_Pin, STMPE811_DIRECTION_OUT);
			break;
		case IO_MODE_IT_RISING_EDGE: /* Interrupt rising edge mode */
			stmpe811_IO_EnableIT(DeviceAddr);
			stmpe811_IO_EnablePinIT(DeviceAddr, IO_Pin);
			stmpe811_IO_InitPin(DeviceAddr, IO_Pin, STMPE811_DIRECTION_IN);
			stmpe811_SetITType(DeviceAddr, STMPE811_TYPE_EDGE);
			stmpe811_IO_SetEdgeMode(DeviceAddr, IO_Pin, STMPE811_EDGE_RISING);
			break;
		case IO_MODE_IT_FALLING_EDGE: /* Interrupt falling edge mode */
			stmpe811_IO_EnableIT(DeviceAddr);
			stmpe811_IO_EnablePinIT(DeviceAddr, IO_Pin);
			stmpe811_IO_InitPin(DeviceAddr, IO_Pin, STMPE811_DIRECTION_IN);
			stmpe811_SetITType(DeviceAddr, STMPE811_TYPE_EDGE);
			stmpe811_IO_SetEdgeMode(DeviceAddr, IO_Pin, STMPE811_EDGE_FALLING);
			break;
		case IO_MODE_IT_LOW_LEVEL: /* Low level interrupt mode */
			stmpe811_IO_EnableIT(DeviceAddr);
			stmpe811_IO_EnablePinIT(DeviceAddr, IO_Pin);
			stmpe811_IO_InitPin(DeviceAddr, IO_Pin, STMPE811_DIRECTION_IN);
			stmpe811_SetITType(DeviceAddr, STMPE811_TYPE_LEVEL);
			stmpe811_SetITPolarity(DeviceAddr, STMPE811_POLARITY_LOW);
			break;
		case IO_MODE_IT_HIGH_LEVEL: /* High level interrupt mode */
			stmpe811_IO_EnableIT(DeviceAddr);
			stmpe811_IO_EnablePinIT(DeviceAddr, IO_Pin);
			stmpe811_IO_InitPin(DeviceAddr, IO_Pin, STMPE811_DIRECTION_IN);
			stmpe811_SetITType(DeviceAddr, STMPE811_TYPE_LEVEL);
			stmpe811_SetITPolarity(DeviceAddr, STMPE811_POLARITY_HIGH);
			break;
		default:
			error_code = (uint8_t) IO_Mode;
			break;
	}
	return error_code;
}
void stmpe811_IO_InitPin(uint16_t DeviceAddr, uint32_t IO_Pin, uint8_t Direction)
{
	int8_t tmp = IOE_Read(DeviceAddr, STMPE811_REG_IO_DIR);
	if (Direction != STMPE811_DIRECTION_IN) tmp |= (uint8_t)IO_Pin;
	else tmp &= ~(uint8_t)IO_Pin;
	IOE_Write(DeviceAddr, STMPE811_REG_IO_DIR, tmp);
}
void stmpe811_IO_DisableAF(uint16_t DeviceAddr, uint32_t IO_Pin)
{
	uint8_t tmp = IOE_Read(DeviceAddr, STMPE811_REG_IO_AF);
	tmp |= (uint8_t)IO_Pin;
	IOE_Write(DeviceAddr, STMPE811_REG_IO_AF, tmp);
}
void stmpe811_IO_EnableAF(uint16_t DeviceAddr, uint32_t IO_Pin)
{
	uint8_t tmp = IOE_Read(DeviceAddr, STMPE811_REG_IO_AF);
	tmp &= ~(uint8_t)IO_Pin;
	IOE_Write(DeviceAddr, STMPE811_REG_IO_AF, tmp);
}
void stmpe811_IO_SetEdgeMode(uint16_t DeviceAddr, uint32_t IO_Pin, uint8_t Edge)
{
	uint8_t tmp1 = 0, tmp2 = 0;
	tmp1 = IOE_Read(DeviceAddr, STMPE811_REG_IO_FE);
	tmp2 = IOE_Read(DeviceAddr, STMPE811_REG_IO_RE);
	tmp1 &= ~(uint8_t)IO_Pin;
	tmp2 &= ~(uint8_t)IO_Pin;
	if (Edge & STMPE811_EDGE_FALLING) tmp1 |= (uint8_t)IO_Pin;
	if (Edge & STMPE811_EDGE_RISING) tmp2 |= (uint8_t)IO_Pin;
	IOE_Write(DeviceAddr, STMPE811_REG_IO_FE, tmp1);
	IOE_Write(DeviceAddr, STMPE811_REG_IO_RE, tmp2);
}
void stmpe811_IO_WritePin(uint16_t DeviceAddr, uint32_t IO_Pin, uint8_t PinState)
{
	if (PinState != 0) IOE_Write(DeviceAddr, STMPE811_REG_IO_SET_PIN, (uint8_t)IO_Pin);
	else IOE_Write(DeviceAddr, STMPE811_REG_IO_CLR_PIN, (uint8_t)IO_Pin);
}
uint32_t stmpe811_IO_ReadPin(uint16_t DeviceAddr, uint32_t IO_Pin)
{
	return((uint32_t)(IOE_Read(DeviceAddr, STMPE811_REG_IO_MP_STA) & (uint8_t)IO_Pin));
}
void stmpe811_IO_EnableIT(uint16_t DeviceAddr)
{
	IOE_ITConfig();
	stmpe811_EnableITSource(DeviceAddr, STMPE811_GIT_IO);
	stmpe811_EnableGlobalIT(DeviceAddr);
}
void stmpe811_IO_DisableIT(uint16_t DeviceAddr)
{
	stmpe811_DisableGlobalIT(DeviceAddr);
	stmpe811_DisableITSource(DeviceAddr, STMPE811_GIT_IO);
}
void stmpe811_IO_EnablePinIT(uint16_t DeviceAddr, uint32_t IO_Pin)
{
	uint8_t tmp = IOE_Read(DeviceAddr, STMPE811_REG_IO_INT_EN);
	tmp |= (uint8_t)IO_Pin;
	IOE_Write(DeviceAddr, STMPE811_REG_IO_INT_EN, tmp);
}
void stmpe811_IO_DisablePinIT(uint16_t DeviceAddr, uint32_t IO_Pin)
{
	uint8_t tmp = IOE_Read(DeviceAddr, STMPE811_REG_IO_INT_EN);
	tmp &= ~(uint8_t)IO_Pin;
	IOE_Write(DeviceAddr, STMPE811_REG_IO_INT_EN, tmp);
}
uint32_t stmpe811_IO_ITStatus(uint16_t DeviceAddr, uint32_t IO_Pin)
{
	return(IOE_Read(DeviceAddr, STMPE811_REG_IO_INT_STA) & (uint8_t)IO_Pin);
}
void stmpe811_IO_ClearIT(uint16_t DeviceAddr, uint32_t IO_Pin)
{
	stmpe811_ClearGlobalIT(DeviceAddr, STMPE811_GIT_IO);
	IOE_Write(DeviceAddr, STMPE811_REG_IO_INT_STA, (uint8_t)IO_Pin);
	IOE_Write(DeviceAddr, STMPE811_REG_IO_ED, (uint8_t)IO_Pin);
	IOE_Write(DeviceAddr, STMPE811_REG_IO_RE, (uint8_t)IO_Pin);
	IOE_Write(DeviceAddr, STMPE811_REG_IO_FE, (uint8_t)IO_Pin);
}
void stmpe811_TS_Start(uint16_t DeviceAddr)
{
	uint8_t mode = IOE_Read(DeviceAddr, STMPE811_REG_SYS_CTRL2);
	mode &= ~(STMPE811_IO_FCT);
	IOE_Write(DeviceAddr, STMPE811_REG_SYS_CTRL2, mode);
	stmpe811_IO_EnableAF(DeviceAddr, STMPE811_TOUCH_IO_ALL);
	mode &= ~(STMPE811_TS_FCT | STMPE811_ADC_FCT);
	IOE_Write(DeviceAddr, STMPE811_REG_SYS_CTRL2, mode);
	IOE_Write(DeviceAddr, STMPE811_REG_ADC_CTRL1, 0x49);
	IOE_Delay(2);
	IOE_Write(DeviceAddr, STMPE811_REG_ADC_CTRL2, 0x01);
	IOE_Write(DeviceAddr, STMPE811_REG_TSC_CFG, 0x9A);
	IOE_Write(DeviceAddr, STMPE811_REG_FIFO_TH, 0x01);
	IOE_Write(DeviceAddr, STMPE811_REG_FIFO_STA, 0x01);
	IOE_Write(DeviceAddr, STMPE811_REG_FIFO_STA, 0x00);
	IOE_Write(DeviceAddr, STMPE811_REG_TSC_FRACT_XYZ, 0x01);
	IOE_Write(DeviceAddr, STMPE811_REG_TSC_I_DRIVE, 0x01);
	IOE_Write(DeviceAddr, STMPE811_REG_TSC_CTRL, 0x01);
	IOE_Write(DeviceAddr, STMPE811_REG_INT_STA, 0xFF);
	IOE_Delay(2);
}
uint8_t stmpe811_TS_DetectTouch(uint16_t DeviceAddr)
{
	uint8_t state;
	uint8_t ret = 0;
	state = ((IOE_Read(DeviceAddr, STMPE811_REG_TSC_CTRL) & (uint8_t)STMPE811_TS_CTRL_STATUS) == (uint8_t)0x80);
	if(state > 0)
	{
		if(IOE_Read(DeviceAddr, STMPE811_REG_FIFO_SIZE) > 0) ret = 1;
	}
	else
	{
		IOE_Write(DeviceAddr, STMPE811_REG_FIFO_STA, 0x01);
		IOE_Write(DeviceAddr, STMPE811_REG_FIFO_STA, 0x00);
	}
	return ret;
}
void stmpe811_TS_GetXY(uint16_t DeviceAddr, uint16_t *X, uint16_t *Y)
{
	uint8_t  dataXYZ[4];
	uint32_t uldataXYZ;
	IOE_ReadMultiple(DeviceAddr, STMPE811_REG_TSC_DATA_NON_INC, dataXYZ, sizeof(dataXYZ)) ;
	uldataXYZ = (dataXYZ[0] << 24)|(dataXYZ[1] << 16)|(dataXYZ[2] << 8)|(dataXYZ[3] << 0);
	*X = (uldataXYZ >> 20) & 0x00000FFF;
	*Y = (uldataXYZ >>  8) & 0x00000FFF;
	IOE_Write(DeviceAddr, STMPE811_REG_FIFO_STA, 0x01);
	IOE_Write(DeviceAddr, STMPE811_REG_FIFO_STA, 0x00);
}
void stmpe811_TS_EnableIT(uint16_t DeviceAddr)
{
	IOE_ITConfig();
	stmpe811_EnableITSource(DeviceAddr, STMPE811_TS_IT);
	stmpe811_EnableGlobalIT(DeviceAddr);
}
void stmpe811_TS_DisableIT(uint16_t DeviceAddr)
{
	stmpe811_DisableGlobalIT(DeviceAddr);
	stmpe811_DisableITSource(DeviceAddr, STMPE811_TS_IT);
}
uint8_t stmpe811_TS_ITStatus(uint16_t DeviceAddr)
{
	return(stmpe811_ReadGITStatus(DeviceAddr, STMPE811_TS_IT));
}
void stmpe811_TS_ClearIT(uint16_t DeviceAddr)
{
	stmpe811_ClearGlobalIT(DeviceAddr, STMPE811_TS_IT);
}
void IOE_Write(uint8_t addr, uint8_t reg, uint8_t value)
{
	i2c3_wr(addr, reg, 1, &value);
}
void IOE_Delay(uint32_t delay)
{
	delay_ms(delay);
}
uint8_t IOE_Read(uint8_t addr, uint8_t reg)
{
	uint8_t temp = 0;
	i2c3_rd(addr, reg, 1, &temp);
	return temp;
}
void IOE_ReadMultiple(uint8_t addr, uint8_t reg, uint8_t *buffer, uint16_t length)
{
	i2c3_rd(addr, reg, length, buffer);
}
void IOE_ITConfig(void)
{
	EXTI->IMR = (1<<15);
	EXTI->FTSR = (1<<15);
	NVIC_EnableIRQ(EXTI15_10_IRQn);
}
void EXTI15_10_IRQHandler(void)
{
	uint16_t xt, yt;
	stmpe811_TS_GetXY(TS_I2C_ADDRESS, &xt, &yt);
	if((xt!=0)&&(yt!=0))
	{
		if(ts_cal.data.key == TS_KEY)
		{
			push_d.coord.x = ts_cal.data.xa*xt + ts_cal.data.xb;
			push_d.coord.y = ts_cal.data.ya*yt + ts_cal.data.yb;
		}
		else
		{
			push_d.coord.x = xt;
			push_d.coord.y = yt;
		}
		push_d.flag = 1;
	}
	stmpe811_TS_ClearIT(TS_I2C_ADDRESS);
	EXTI->PR = (1<<15);
}
void load_picture(uint16_t* ptr)
{
	for(uint16_t i=0; i<Y_RES; i++)
	{
		for(uint16_t j=0; j<X_RES; j++) screen_buffer[i][j] = *ptr;
		ptr++;
	}
	//for(uint32_t i=0; i < 76800; i++) *((uint16_t*)0xD0000000 + i) = ptr[i];
}

//ver190207
#include "system.h"
#include "print.h"
//
void sys_init(void)
{
 	FLASH->ACR = FLASH_ACR_ICEN|FLASH_ACR_DCEN|FLASH_ACR_PRFTEN|FLASH_ACR_LATENCY_5WS;
	RCC->CR |= RCC_CR_HSEON|RCC_CR_CSSON;
	RCC->PLLCFGR = RCC_PLLCFGR_PLLSRC_HSE|(7<<24)|4|(168<<6);
	//M = 4
	//N = 168
	//P = 2
	//Q = 7
	while((RCC->CR & RCC_CR_HSERDY) != RCC_CR_HSERDY){};
	RCC->CR |= RCC_CR_PLLON;
	while ((RCC->CR & RCC_CR_PLLRDY) != RCC_CR_PLLRDY){};
	RCC->CFGR = RCC_CFGR_SW_PLL|RCC_CFGR_PPRE1_DIV4|RCC_CFGR_PPRE2_DIV2;
	/*
	APB1 42MHz (PCLK1)
		x1 = 42M - USART2,3, UART4,5,7,8, SPI2,3
		x2 = 84M - TIM2,3,4,5,6,7,12,13,14
	APB2 84MHz (PCLK2)
		x1 = 84M - USART1,6, SPI1,4,5,6
		x2 = 168M - TIM1,8,9,10,11
	oth 168MHz
	*/
	while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL){};
	fpu_on();
	extern uint32_t __ccmram_start__;
	extern uint32_t __ccmram_end__;
	extern uint32_t __etext;
	extern uint32_t __data_start__;
	extern uint32_t __data_end__;
	uint32_t *pSrc, *pDest;
	pSrc = (uint32_t*)((uint32_t)&__etext + (uint32_t)&__data_end__ - (uint32_t)&__data_start__);
	pDest = &__ccmram_start__;
	while(pDest < &__ccmram_end__) *pDest++ = *pSrc++;
}
void delay_us(uint32_t us)
{
	uint32_t temp1 = us*40;//168
	while (temp1--) __NOP();
}
void delay_ms(uint32_t ms)
{
	delay_us(1000*ms);
}
void project_def_handler(uint32_t* ipsr)
{
	volatile uint32_t loc_temp;
	if(*ipsr == 3)
	{
		loc_temp = SCB->HFSR;
	}
	board_LED(red, on);
	board_LED(green, off);
	while(1){};
}
void board_LED(led color, led_state state)
{
	if(color & red)
	{
		if(state) GPIOG->BSRR = 1<<14;
		else GPIOG->BSRR = 1<<(14+16);
	}
	if(color & green)
	{
		if(state) GPIOG->BSRR = 1<<13;
		else GPIOG->BSRR = 1<<(13+16);
	}
}
void uart1_init(void)
{
	//PA9-TX, PA10-RX
	GPIOA->MODER |= (2<<(9*2))|(2<<(10*2));
	GPIOA->OSPEEDR |= (3<<(9*2))|(3<<(10*2));
	GPIOA->AFR[1] |= (7<<4)|(7<<8);
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
	USART1->BRR = (45<<4)|9;//115200
	USART1->CR1 = USART_CR1_UE|USART_CR1_TE|USART_CR1_RE;
}
void uart_print_char(void* uart, uint8_t data)
{
	while(!(((USART_TypeDef*)uart)->SR & USART_SR_TC));
	((USART_TypeDef*)uart)->DR=data;
}
void init_gpio(void)
{
	RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOAEN|RCC_AHB1ENR_GPIOBEN|RCC_AHB1ENR_GPIOCEN|RCC_AHB1ENR_GPIODEN|RCC_AHB1ENR_GPIOEEN|
		RCC_AHB1ENR_GPIOFEN|RCC_AHB1ENR_GPIOGEN);
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
	GPIOA->AFR[0] =
		(14<<(3*4))|//lcd_b5
		(14<<(4*4))|//lcd_vsync
		(14<<(6*4));//lcd_g2
	GPIOA->AFR[1] = (4<<((8-8)*4))|//i2c3_scl
		(14<<((11-8)*4))|//lcd_r4
		(14<<((12-8)*4))|//lcd_r5
		(0<<((13-8)*4))|//swdio
		(0<<((14-8)*4));//swclk
	GPIOA->MODER = (2<<(3*2))|(2<<(4*2))|(2<<(6*2))|(2<<(8*2))|(2<<(11*2))|(2<<(12*2))|(2<<(13*2))|(2<<(14*2));
	GPIOA->OSPEEDR = 0xffffffff;
	GPIOA->OTYPER = (1<<8);
	GPIOA->PUPDR = (1<<(13*2))|(2<<(14*2))|(1<<(15*2));
	GPIOB->AFR[0] = (9<<(0*4))|//lcd_r3
		(9<<(1*4))|//lcd_r6
		(12<<(5*4))|//fmc_sdcke1
		(12<<(6*4));//fmc_sdne1
	GPIOB->AFR[1] = (14<<((8-8)*4))|
		(14<<((9-8)*4))|//lcd_b7
		(14<<((10-8)*4))|//lcd_g4
		(14<<((11-8)*4))|//lcd_g5
		(12<<((12-8)*4))|//otg_hs_id
		(12<<((14-8)*4))|//otg_hs_dm
		(12<<((15-8)*4));//otg_hs_dp
	GPIOB->MODER = (2<<(0*2))|(2<<(1*2))|(2<<(5*2))|(2<<(6*2))|(2<<(8*2))|(2<<(9*2))|(2<<(10*2))|(2<<(11*2))|(2<<(12*2))|(2<<(14*2))|
		(2<<(15*2));
	GPIOB->OSPEEDR = 0xffffffff;
	GPIOB->OTYPER = 0;
	GPIOB->PUPDR = 0;
	GPIOC->AFR[0] = (12<<(0*4))|//fmc_sdnwe
		(14<<(6*4))|//lcd_hsync
		(14<<(7*4));//lcd_g6
	GPIOC->AFR[1] = (4<<((9-8)*4))|//i2c3_sda
		(14<<((10-8)*4));//lcd_r2
	GPIOC->MODER = (2<<(0*2))|(1<<(1*2))|(1<<(2*2))|(1<<(4*2))|(2<<(6*2))|(2<<(7*2))|(2<<(9*2))|(2<<(10*2));
	GPIOC->OSPEEDR = 0xffffffff;
	GPIOC->OTYPER = (1<<9);
	GPIOC->PUPDR = 0;
	GPIOD->AFR[0] = (12<<(0*4))|//fmc_d2
		(12<<(1*4))|//fmc_d3
		(14<<(3*4))|//lcd_g7
		(14<<(6*4));//lcd_b2
	GPIOD->AFR[1] = (12<<((8-8)*4))|//fmc_d13
		(12<<((9-8)*4))|//fmc_d14
		(12<<((10-8)*4))|//fmc_d15
		(12<<((14-8)*4))|//fmc_d0
		(12<<((15-8)*4));//fmc_d1
	GPIOD->MODER = (2<<(0*2))|(2<<(1*2))|(2<<(3*2))|(2<<(6*2))|(2<<(8*2))|(2<<(9*2))|(2<<(10*2))|(1<<(12*2))|(1<<(13*2))|
		(2<<(14*2))|(2<<(15*2));
	GPIOD->OSPEEDR = 0xffffffff;
	GPIOD->OTYPER = 0;
	GPIOD->PUPDR = 0;
	GPIOE->AFR[0] = (12<<(0*4))|//fmc_nbl0
		(12<<(1*4))|//fmc_nbl1
		(12<<(7*4));//fmc_d4
	GPIOE->AFR[1] = 0xCCCCCCCC;//fmc_d5 - fmc_d12
	GPIOE->MODER = 0xAAAA0000|(2<<(0*2))|(2<<(1*2))|(2<<(7*2));
	GPIOE->OSPEEDR = 0xffffffff;
	GPIOE->OTYPER = 0;
	GPIOE->PUPDR = 0;
	GPIOF->AFR[0] = 0x50CCCCCC;
	GPIOF->AFR[1] = 0xCCCCCe55;
	GPIOF->MODER = 0xaaaa8aaa;
	GPIOF->OSPEEDR = 0xffffffff;
	GPIOF->OTYPER = 0;
	GPIOF->PUPDR = 0x000a8000;
	GPIOG->AFR[0] = (12<<(0*4))|//fmc_a10
		(12<<(1*4))|//fmc_a11
		(12<<(4*4))|//fmc_a14/ba0
		(12<<(5*4))|//fmc_a15/ba1
		(14<<(6*4))|//lcd_r7
		(14<<(7*4));//lcd_clk
	GPIOG->AFR[1] = (12<<((8-8)*4))|//fmc_sdclk
		(9<<((10-8)*4))|//lcd_g3
		(14<<((11-8)*4))|//lcd_b3
		(9<<((12-8)*4))|//lcd_b4
		(12<<((15-8)*4));//fmc_sdncas
	GPIOG->MODER = (2<<(0*2))|(2<<(1*2))|(2<<(4*2))|(2<<(5*2))|(2<<(6*2))|(2<<(7*2))|(2<<(8*2))|(2<<(10*2))|(2<<(11*2))|(2<<(12*2))|
		(1<<(13*2))|(1<<(14*2))|(2<<(15*2));
	GPIOG->OSPEEDR = 0xffffffff;
	GPIOG->OTYPER = 0;
	GPIOG->PUPDR = 0x14000000;
}
void spi5_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_SPI5EN;
	SPI5->CR1 &=  ~SPI_CR1_SPE;
	SPI5->CR1 = (SPI_CR1_MSTR|SPI_CR1_SSI|SPI_CR1_SSM|0x18);
	SPI5->CR2 = ((SPI_CR1_SSM >> 16) & SPI_CR2_SSOE);
	SPI5->I2SCFGR &= ~SPI_I2SCFGR_I2SMOD;
	SPI5->CR1 |=  SPI_CR1_SPE;
}
void i2c3_init(void)
{
	//100kHz
	RCC->APB1ENR |= RCC_APB1ENR_I2C3EN;
	I2C3->CR2 = 42;
	I2C3->TRISE = 43;
	I2C3->CCR = 210;
	I2C3->CR1 = I2C_CR1_PE;
}
void sdram_init(void)
{
	RCC->AHB3ENR |= RCC_AHB3ENR_FMCEN;
	FMC_Bank5_6->SDCR[0] = (2<<10)|(1<<13);
	FMC_Bank5_6->SDCR[1] = (3<<7)|(1<<6)|(1<<4)|(1<<2);
	FMC_Bank5_6->SDTR[1] = (1<<24)|(2<<16)|(3<<8)|(6<<4)|1;
	FMC_Bank5_6->SDTR[0] = (1<<20)|(6<<12);
	FMC_Bank5_6->SDCMR = (0x00000001|FMC_SDCMR_CTB2);
	while((FMC_Bank5_6->SDSR & FMC_SDSR_BUSY) != 0){};
	delay_ms(1);
	FMC_Bank5_6->SDCMR = (0x00000002|FMC_SDCMR_CTB2);
	while((FMC_Bank5_6->SDSR & FMC_SDSR_BUSY) != 0){};
	FMC_Bank5_6->SDCMR = (0x00000003|FMC_SDCMR_CTB2|(3 << 5));
	while((FMC_Bank5_6->SDSR & FMC_SDSR_BUSY) != 0){};
	FMC_Bank5_6->SDCMR = (0x00000004|FMC_SDCMR_CTB2|(0x0230 << 9));
	while((FMC_Bank5_6->SDSR & FMC_SDSR_BUSY) != 0){};
	FMC_Bank5_6->SDRTR |= (1290<<1);
}
void spi5_wr(uint8_t data)
{
	spi_wr_rd(SPI5, data);
}
uint8_t spi5_wr_rd(uint8_t data)
{
	uint8_t temp1 = spi_wr_rd(SPI5, data);
	return temp1;
}
void i2c3_wr(uint8_t addr, uint8_t reg, uint8_t size, uint8_t* tx_buffer)
{
	I2C3->CR1 |= I2C_CR1_START;
	while((I2C3->SR1 & I2C_SR1_SB) == 0){};
	I2C3->DR = addr;
	while((I2C3->SR1 & I2C_SR1_ADDR) == 0){};
	(void)I2C3->SR2;
	I2C3->DR = reg;
	while((I2C3->SR1 & I2C_SR1_TXE) == 0){};
	while(size--)
	{
		I2C3->DR = *(tx_buffer++);
		while((I2C3->SR1 & I2C_SR1_TXE) == 0){};
	}
	I2C3->CR1 |= I2C_CR1_STOP;
}
uint8_t i2c3_rd(uint8_t addr, uint8_t reg, uint8_t size, uint8_t* rx_buffer)
{
	uint32_t loc_temp = 0;
	I2C3->CR1 |= I2C_CR1_START;
	while((I2C3->SR1 & I2C_SR1_SB) == 0){};
	I2C3->DR = addr;
	while((I2C3->SR1 & I2C_SR1_ADDR) == 0)
	{
		if(loc_temp++ > I2C_TIMEOUT) return 1;
	}
	(void)I2C3->SR2;
	I2C3->DR = reg;
	loc_temp = 0;
	while((I2C3->SR1 & I2C_SR1_TXE) == 0)
	{
		if(loc_temp++ > I2C_TIMEOUT) return 2;
	}//EV8
	I2C3->CR1 |= I2C_CR1_START;
	while((I2C3->SR1 & I2C_SR1_SB) == 0){};//EV5
	I2C3->DR = addr|1;
	loc_temp = 0;
	while((I2C3->SR1 & I2C_SR1_ADDR) == 0)
	{
		if(loc_temp++ > I2C_TIMEOUT) return 3;
	}//EV6
	(void)I2C3->SR2;
	while(size--)
	{
		if(size == 0)
		{
			I2C3->CR1 &= ~I2C_CR1_ACK;
			I2C3->CR1 |= I2C_CR1_STOP;
		}
		else I2C3->CR1 |= I2C_CR1_ACK;
		while((I2C3->SR1 & I2C_SR1_RXNE) == 0){};
		*(rx_buffer++) = I2C3->DR;
	}
	return 0;
}
void rng_init(void)
{
	RCC->AHB2ENR |= RCC_AHB2ENR_RNGEN;
	RNG->CR |= RNG_CR_RNGEN;
}
uint32_t rng_random_data(void)
{
	while((RNG->SR & RNG_SR_DRDY) == 0){};
	return RNG->DR;
}
void kill_jtag(void)
{
	//GPIOA->MODER = 0;
}
void crc32eth_init(void)
{
	RCC->AHB1ENR |= RCC_AHB1ENR_CRCEN;
}
void crc32eth_calc(uint32_t* crc, uint32_t* data)
{
	CRC->DR = *data;
	*crc = CRC->DR;
}
void crc32eth_reset(void)
{
	CRC->CR = 1;
}
void bootloader_error_signal(uint8_t reason)
{
	uint8_t string[4] = {0};
	uart_print_string(USART1, "\r\nError: ");
	uint8_to_string(reason, string);
	uart_print_string(USART1, string);
}
void flash_erase_page(uint32_t addr)
{
	uint8_t sector;
	addr &= 0x00ffffff;
	if(addr < 0x4000) sector = 0;
	else
	{
		if(addr < 0x8000) sector = 1;
		else
		{
			if(addr < 0xc000) sector = 2;
			else
			{
				if(addr < 0x10000) sector = 3;
				else
				{
					if(addr < 0x20000) sector = 4;
					else
					{

						if(addr < 0x100000) sector = (addr>>17) + 4;
						else return;
					}
				}
			}
		}
	}
	while (FLASH->SR & FLASH_SR_BSY){};
	FLASH->CR |= (2<<8)|FLASH_CR_SER|(sector<<3);
	FLASH->CR |= FLASH_CR_STRT;
	while (FLASH->SR & FLASH_SR_BSY){};
	FLASH->CR = 0;
}
void flash_copy_data(uint32_t* dst, uint32_t* src, uint32_t size)
{
	while (FLASH->SR & FLASH_SR_BSY){};
	FLASH->CR |= FLASH_CR_PG|(2<<8);
	for(uint16_t i=0; i<(size >> 2); i++)
	{
		dst[i] = src[i];
		while (FLASH->SR & FLASH_SR_BSY){};
	}
	FLASH->CR = 0;
}
void bootload_run_req(void)
{
	extern uint8_t fw_temp[PAGE_SIZE];
	uint8_t rx_char;
	uint32_t size, l_temp = 0;
	while(1)
	{
		uart_print_string(USART1, "\r\nCommand: ");
		while (!(USART1->SR & USART_SR_RXNE))
		{
			if(l_temp < BOOT_TIMEOUT)
			{
				l_temp++;
				delay(10);
			}
			else
			{
				uart_print_string(USART1, "\r\nTimeout");
				return;
			}
		}
		rx_char = USART1->DR;
		uart_print_char(USART1, rx_char);
		switch(rx_char)
		{
			case 'p':
				uart_print_string(USART1, "\r\nPut fw_new");
				for(uint16_t i=0;i<8;i++)
				{
					while (!(USART1->SR & USART_SR_RXNE)){};
					fw_temp[i] = USART1->DR;
				}
				size = *((uint32_t*)&fw_temp[4]);
				for(uint32_t i=0;i < (size - 8); i++)
				{
					while (!(USART1->SR & USART_SR_RXNE)){};
					fw_temp[i+8] = USART1->DR;
				}
				fw_copy((uint32_t*)fw_temp, (uint32_t*)__fw_new, size);
				uart_print_string(USART1, "\r\nfw_new has written to flash");
				return;
			case 'e':
				uart_print_string(USART1, "\r\nErase\r\nMatch sector (m,c,n): ");
				while (!(USART1->SR & USART_SR_RXNE)){};
				rx_char = USART1->DR;
				uart_print_char(USART1, rx_char);
				switch(rx_char)
				{
					case 'm':
						flash_unlock();
						flash_erase_page(0x08004000);
						flash_lock();
						uart_print_string(USART1, "\r\nfw_main erase");
					break;
					case 'c':
						flash_unlock();
						flash_erase_page(0x08008000);
						flash_lock();
						uart_print_string(USART1, "\r\nfw_copy erase");
					break;
					case 'n':
						flash_unlock();
						flash_erase_page(0x0800c000);
						flash_lock();
						uart_print_string(USART1, "\r\nfw_new erase");
					break;
					default:
						uart_print_string(USART1, "\r\nUnknown sector!");
					break;
				}
				break;
			case 'r':
				uart_print_string(USART1, "\r\nRun");
				return;
			default:
				uart_print_string(USART1, "\r\nUnknown command!");
			break;
		}
	}
}
void boot_init(void)
{
	extern version_t version;
	uint8_t string[4] = {0};
	sys_init();
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
	uart1_init();
	uart_print_string(USART1, "\r\nBootloader version: ");
	uint8_to_string(version.number, string);
	uart_print_string(USART1, string);
	uart_print_string(USART1, "\r\nDate: ");
	switch(version.month)
	{
		case 1: uart_print_string(USART1, "Jan "); break;
		case 2: uart_print_string(USART1, "Feb "); break;
		case 3: uart_print_string(USART1, "Mar "); break;
		case 4: uart_print_string(USART1, "Apr "); break;
		case 5: uart_print_string(USART1, "May "); break;
		case 6: uart_print_string(USART1, "Jun "); break;
		case 7: uart_print_string(USART1, "Jul "); break;
		case 8: uart_print_string(USART1, "Aug "); break;
		case 9: uart_print_string(USART1, "Sep "); break;
		case 0x10: uart_print_string(USART1, "Oct "); break;
		case 0x11: uart_print_string(USART1, "Nov "); break;
		case 0x12: uart_print_string(USART1, "Dec "); break;
	}
	uint8_to_string(version.day, string);
	uart_print_string(USART1, string);
}
void flash_unlock(void)
{
	if(FLASH->CR & FLASH_CR_LOCK)
	{
		FLASH->KEYR = 0x45670123;
		FLASH->KEYR = 0xCDEF89AB;
	}
}
void flash_lock(void)
{
	FLASH->CR |= FLASH_CR_LOCK;
}
void fw_copy(uint32_t* src, uint32_t* dst, uint32_t size)
{
	flash_unlock();
	flash_erase_page((uint32_t)dst);
	flash_copy_data(dst, src, size);
	flash_lock();
}
uint16_t spi_wr_rd(SPI_TypeDef* spi, uint16_t data_wr)
{
	uint16_t loc_temp;
	while((spi->SR & SPI_SR_TXE) == 0){};
	spi->DR = data_wr;
	while((spi->SR & SPI_SR_RXNE) == 0){};
	loc_temp = spi->DR;
	return loc_temp;
}
void sdram_test(void)
{
	uint8_t loc_temp;
	for(uint32_t i=0; i<SDRAM_SIZE; i++) *((uint8_t*)(0xd0000000 + i)) = i&0xff;
	for(uint32_t i=0; i<SDRAM_SIZE; i++)
	{
		loc_temp = *((uint8_t*)(0xd0000000 + i));
		if(loc_temp != (i&0xff))
		{
			//lcd_print_string("SDRAM check fail!", C_Red);
			while(1){};
		}
	}
}

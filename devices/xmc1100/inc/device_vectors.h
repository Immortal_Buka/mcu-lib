void SCU_0_IRQHandler(void) WA;
void SCU_1_IRQHandler(void) WA;
void SCU_2_IRQHandler(void) WA;
void ERU0_0_IRQHandler(void) WA;
void ERU0_1_IRQHandler(void) WA;
void ERU0_2_IRQHandler(void) WA;
void ERU0_3_IRQHandler(void) WA;
void USIC0_0_IRQHandler(void) WA;
void USIC0_1_IRQHandler(void) WA;
void USIC0_2_IRQHandler(void) WA;
void USIC0_3_IRQHandler(void) WA;
void USIC0_4_IRQHandler(void) WA;
void USIC0_5_IRQHandler(void) WA;
void VADC0_C0_0_IRQHandler(void) WA;
void VADC0_C0_1_IRQHandler(void) WA;
void CCU40_0_IRQHandler(void) WA;
void CCU40_1_IRQHandler(void) WA;
void CCU40_2_IRQHandler(void) WA;
void CCU40_3_IRQHandler(void) WA;
//
volatile const func_ptr device_vectors[] __attribute__ ((section(".isr_vector_dev"))) =
{
	(func_ptr)((uint32_t)&__StackTop),	//Initial Stack Pointer
	Reset_Handler,						//Reset Handler
	0,
	0,
	(func_ptr)0xffffffff,				//CLKVAL1_SSW
	(func_ptr)0xffffffff,				//CLKVAL2_SSW
};
volatile const __attribute__(( section(".isr_vector_ram"), naked)) void device_vectors_2(void)
{
	asm(" .long 0 ");							//Initial Stack Pointer
	asm(" .long 0 "); 							//Reset Handler
	asm(" .long 0 "); 							//NMI Handler
	asm(" ldr R0,=(_Z15Default_Handlerv) "); 	//Hard Fault Handler
	asm(" mov PC,R0");
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" ldr R0,=(_Z15Default_Handlerv) "); 	//SVCall Handler
	asm(" mov PC,R0 ");
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" ldr R0,=(_Z15Default_Handlerv) "); 	//PendSV Handler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15SysTick_Handlerv) "); 	//SysTick Handler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(SCU_0_IRQHandler) ");	//SCU_0_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//SCU_1_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//SCU_2_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//ERU0_0_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//ERU0_1_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//ERU0_2_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//ERU0_3_IRQHandler
	asm(" mov PC,R0 ");
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//USIC0_0_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//USIC0_1_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//USIC0_2_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//USIC0_3_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//USIC0_4_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//USIC0_5_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//VADC0_C0_0_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//VADC0_C0_1_IRQHandler
	asm(" mov PC,R0 ");
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//CCU40_0_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//CCU40_1_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//CCU40_2_IRQHandler
	asm(" mov PC,R0 ");
	asm(" ldr R0,=(_Z15Default_Handlerv) ");	//CCU40_3_IRQHandler
	asm(" mov PC,R0 ");
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
	asm(" .long 0 "); 							//Reserved
}


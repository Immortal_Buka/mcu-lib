case 0x002c: uart_print_string(NRF_UART0, "S110 v5.0.0"); break;
case 0x0035: uart_print_string(NRF_UART0, "S110 v6.2.1"); break;
case 0x0043: uart_print_string(NRF_UART0, "S110 v5.2.1"); break;
case 0x0049: uart_print_string(NRF_UART0, "S110 v6.0.0"); break;
case 0x004B: uart_print_string(NRF_UART0, "S210 v3.0.0"); break;
case 0x004D: uart_print_string(NRF_UART0, "S310 v1.0.0"); break;
case 0x004F: uart_print_string(NRF_UART0, "S110 v7.0.0"); break;
case 0x0055: uart_print_string(NRF_UART0, "S120 v1.0.0"); break;
case 0x0057: uart_print_string(NRF_UART0, "S210 v4.0.0 or S210 v4.0.1"); break;
case 0x0058: uart_print_string(NRF_UART0, "S120 v1.0.1"); break;
case 0x005A: uart_print_string(NRF_UART0, "S110 v7.1.0"); break;
case 0x005B: uart_print_string(NRF_UART0, "S120 v2.0.0-1.alpha"); break;
case 0x005D: uart_print_string(NRF_UART0, "S310 v2.0.0 & v2.0.1"); break;
case 0x005E: uart_print_string(NRF_UART0, "S130 v0.9.0-1.alpha"); break;
case 0x0060: uart_print_string(NRF_UART0, "S120 v2.0.0"); break;
case 0x0063: uart_print_string(NRF_UART0, "S110 v7.3.0"); break;
case 0x0064: uart_print_string(NRF_UART0, "S110 v8.0.0"); break;
case 0x0065: uart_print_string(NRF_UART0, "S310 v3.0.0"); break;
case 0x0066: uart_print_string(NRF_UART0, "S130 v1.0.0-3.alpha"); break;
case 0x0067: uart_print_string(NRF_UART0, "S130 v1.0.0"); break;
case 0x0069: uart_print_string(NRF_UART0, "S210 v5.0.0"); break;
case 0x006B: uart_print_string(NRF_UART0, "S120 v2.1.0"); break;
case 0x006D: uart_print_string(NRF_UART0, "S132 v1.0.0-3.alpha"); break;
case 0x0074: uart_print_string(NRF_UART0, "S132 v2.0.0-4.alpha"); break;
case 0x0078: uart_print_string(NRF_UART0, "S130 v2.0.0-7.alpha"); break;
case 0x0079: uart_print_string(NRF_UART0, "S132 v2.0.0-7.alpha"); break;
case 0x007E: uart_print_string(NRF_UART0, "S332 v0.6.0.alpha"); break;
case 0x007F: uart_print_string(NRF_UART0, "S212 v0.6.0.alpha"); break;
case 0x0080: uart_print_string(NRF_UART0, "S130 v2.0.0"); break;
case 0x0081: uart_print_string(NRF_UART0, "S132 v2.0.0"); break;
case 0x0082: uart_print_string(NRF_UART0, "S332 v0.9.1.alpha"); break;
case 0x0083: uart_print_string(NRF_UART0, "S212 v0.9.1.alpha"); break;
case 0x0087: uart_print_string(NRF_UART0, "S130 v2.0.1"); break;
case 0x0088: uart_print_string(NRF_UART0, "S132 v2.0.1"); break;
case 0x008C: uart_print_string(NRF_UART0, "S132 v3.0.0"); break;
case 0x008D: uart_print_string(NRF_UART0, "S212 v2.0.1"); break;
case 0x008E: uart_print_string(NRF_UART0, "S332 v2.0.1"); break;
case 0x008f: uart_print_string(NRF_UART0, "S140 v5.0.0-1.alpha"); break;
case 0x0090: uart_print_string(NRF_UART0, "S132 v5.0.0-1.alpha"); break;
case 0x0091: uart_print_string(NRF_UART0, "S132 v3.1.0"); break;
case 0x0092: uart_print_string(NRF_UART0, "S132 v4.0.0-1.alpha"); break;
case 0x0093: uart_print_string(NRF_UART0, "S212 v4.0.5"); break;
case 0x0094: uart_print_string(NRF_UART0, "S332 v4.0.5"); break;
case 0x0095: uart_print_string(NRF_UART0, "S132 v4.0.0"); break;
case 0x0096: uart_print_string(NRF_UART0, "S140 v5.0.0-2.alpha"); break;
case 0x0098: uart_print_string(NRF_UART0, "S132 v4.0.2"); break;
case 0x0099: uart_print_string(NRF_UART0, "S132 v4.0.3"); break;
case 0x009B: uart_print_string(NRF_UART0, "S332 v5.0.0"); break;
case 0x009C: uart_print_string(NRF_UART0, "S212 v5.0.0"); break;
case 0x009D: uart_print_string(NRF_UART0, "S132 v5.0.0"); break;
case 0x009e: uart_print_string(NRF_UART0, "S132 v4.0.4"); break;
case 0x009f: uart_print_string(NRF_UART0, "S132 v4.0.5"); break;
case 0x00A0: uart_print_string(NRF_UART0, "S132 v5.0.1"); break;
case 0x00A5: uart_print_string(NRF_UART0, "S132 v5.1.0"); break;
case 0x00A6: uart_print_string(NRF_UART0, "S112 v5.1.0"); break;
case 0x00A7: uart_print_string(NRF_UART0, "S112 v6.0.0"); break;
case 0x00A8: uart_print_string(NRF_UART0, "S132 v6.0.0"); break;
case 0x00A9: uart_print_string(NRF_UART0, "S140 v6.0.0"); break;
case 0x00Ae: uart_print_string(NRF_UART0, "S140 v6.1.0"); break;
case 0x00Af: uart_print_string(NRF_UART0, "S132 v6.1.0"); break;
case 0x00b0: uart_print_string(NRF_UART0, "S112 v6.1.0"); break;
case 0x00b6: uart_print_string(NRF_UART0, "S140 v6.1.1"); break;
case 0x00b7: uart_print_string(NRF_UART0, "S132 v6.1.1"); break;
case 0x00b8: uart_print_string(NRF_UART0, "S112 v6.1.1"); break;
case 0x00c1: uart_print_string(NRF_UART0, "S140 v7.0.0"); break;
case 0x00c2: uart_print_string(NRF_UART0, "S132 v7.0.0"); break;
case 0x00c3: uart_print_string(NRF_UART0, "S113 v7.0.0"); break;
case 0x00c4: uart_print_string(NRF_UART0, "S112 v7.0.0"); break;
case 0x00ca: uart_print_string(NRF_UART0, "S140 v7.0.1"); break;
case 0x00cb: uart_print_string(NRF_UART0, "S132 v7.0.1"); break;
case 0x00cc: uart_print_string(NRF_UART0, "S113 v7.0.1"); break;
case 0x00cd: uart_print_string(NRF_UART0, "S112 v7.0.1"); break;
case 0xFFFE: uart_print_string(NRF_UART0, "Development"); break;
		
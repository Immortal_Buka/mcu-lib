// SWI numbers and reason codes for RDI (Angel) monitors.
#define AngelSWI_ARM 			0x123456
#ifdef __thumb__
	#define AngelSWI 			0xAB
#else
	#define AngelSWI 			AngelSWI_ARM
#endif
// For thumb only architectures use the BKPT instruction instead of SWI.
#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__) || defined(__ARM_ARCH_6M__)
	#define AngelSWIInsn		"bkpt"
	#define AngelSWIAsm			bkpt
#else
	#define AngelSWIInsn		"swi"
	#define AngelSWIAsm			swi
#endif
#if !(defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__))
	#if defined(OS_USE_TRACE_ITM)
		#undef OS_USE_TRACE_ITM
		#warning "ITM unavailable"
	#endif // defined(OS_USE_TRACE_ITM)
#endif // !(defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__))
//
enum OperationNumber
{
  // Regular operations
  SEMIHOSTING_EnterSVC = 0x17,
  SEMIHOSTING_ReportException = 0x18,
  SEMIHOSTING_SYS_CLOSE = 0x02,
  SEMIHOSTING_SYS_CLOCK = 0x10,
  SEMIHOSTING_SYS_ELAPSED = 0x30,
  SEMIHOSTING_SYS_ERRNO = 0x13,
  SEMIHOSTING_SYS_FLEN = 0x0C,
  SEMIHOSTING_SYS_GET_CMDLINE = 0x15,
  SEMIHOSTING_SYS_HEAPINFO = 0x16,
  SEMIHOSTING_SYS_ISERROR = 0x08,
  SEMIHOSTING_SYS_ISTTY = 0x09,
  SEMIHOSTING_SYS_OPEN = 0x01,
  SEMIHOSTING_SYS_READ = 0x06,
  SEMIHOSTING_SYS_READC = 0x07,
  SEMIHOSTING_SYS_REMOVE = 0x0E,
  SEMIHOSTING_SYS_RENAME = 0x0F,
  SEMIHOSTING_SYS_SEEK = 0x0A,
  SEMIHOSTING_SYS_SYSTEM = 0x12,
  SEMIHOSTING_SYS_TICKFREQ = 0x31,
  SEMIHOSTING_SYS_TIME = 0x11,
  SEMIHOSTING_SYS_TMPNAM = 0x0D,
  SEMIHOSTING_SYS_WRITE = 0x05,
  SEMIHOSTING_SYS_WRITEC = 0x03,
  SEMIHOSTING_SYS_WRITE0 = 0x04,
  // Codes returned by SEMIHOSTING_ReportException
  ADP_Stopped_ApplicationExit = ((2 << 16) + 38),
  ADP_Stopped_RunTimeError = ((2 << 16) + 35)
};
//
int trace_printf(const char* format, ...)
{
	int ret;
	va_list ap;
	va_start(ap, format);
	//TODO: rewrite it to no longer use newlib, it is way too heavy
	static char buf[OS_INTEGER_TRACE_PRINTF_TMP_ARRAY_SIZE];
	// Print to the local buffer
	ret = vsnprintf(buf, sizeof(buf), format, ap);
	if (ret > 0) ret = trace_write (buf, (size_t)ret);//Transfer the buffer to the device
    va_end(ap);
	return ret;
}
int trace_puts(const char *s)
{
	trace_write(s, strlen(s));
	return trace_write("\n", 1);
}
int	trace_putchar(int c)
{
	trace_write((const char*)&c, 1);
	return c;
}
void trace_dump_args(int argc, char* argv[])
{
	trace_printf("main(argc=%d, argv=[", argc);
	for (int i = 0; i < argc; ++i)
    {
		if (i != 0) trace_printf(", ");
		trace_printf("\"%s\"", argv[i]);
    }
	trace_printf("]);\n");
}
static inline int __attribute__ ((always_inline)) call_host (int reason, void* arg)
{
	int value;
	asm volatile 
	(
		" mov r0, %[rsn]  \n"
		" mov r1, %[arg]  \n"
		" " AngelSWIInsn " %[swi] \n"
		" mov %[val], r0"
		: [val] "=r" (value) /* Outputs */
		: [rsn] "r" (reason), [arg] "r" (arg), [swi] "i" (AngelSWI) /* Inputs */
		: "r0", "r1", "r2", "r3", "ip", "lr", "memory", "cc"
		//Clobbers r0 and r1, and lr if in supervisor mode
	);
	/*Accordingly to page 13-77 of ARM DUI 0040D other registers can also be clobbered. Some memory positions may also be
	changed by a system call, so they should not be kept in registers. Note: we are assuming the manual is right and
	Angel is respecting the APCS.*/
	return value;
}
static inline void __attribute__ ((always_inline,noreturn)) report_exception (int reason)
{
	call_host (SEMIHOSTING_ReportException, (void*) reason);
	while(1){};
}
ssize_t trace_write (const char* buf __attribute__((unused)), size_t nbyte __attribute__((unused)))
{
#if defined(OS_USE_TRACE_ITM)
	return _trace_write_itm (buf, nbyte);
#elif defined(OS_USE_TRACE_SEMIHOSTING_STDOUT)
	return _trace_write_semihosting_stdout(buf, nbyte);
#elif defined(OS_USE_TRACE_SEMIHOSTING_DEBUG)
	return _trace_write_semihosting_debug(buf, nbyte);
#endif
	return -1;
}
#if defined(OS_USE_TRACE_ITM)
	#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
/*ITM is the ARM standard mechanism, running over SWD/SWO on Cortex-M3/M4 devices, and is the recommended setting, if available.
The JLink probe and the GDB server fully support SWD/SWO and the JLink Debugging plug-in enables it by default.
The current OpenOCD does not include support to parse the SWO stream, so this configuration will not work on OpenOCD (will not crash, 
but nothing will be displayed in the output console).*/
		#if !defined(OS_INTEGER_TRACE_ITM_STIMULUS_PORT)
			#define OS_INTEGER_TRACE_ITM_STIMULUS_PORT     (0)
		#endif
static ssize_t _trace_write_itm (const char* buf, size_t nbyte)
{
	for (size_t i = 0; i < nbyte; i++)
    {
		//Check if ITM or the stimulus port are not enabled
		if (((ITM->TCR & ITM_TCR_ITMENA_Msk) == 0)||((ITM->TER & (1UL << OS_INTEGER_TRACE_ITM_STIMULUS_PORT)) == 0))
		{
			return (ssize_t)i; // return the number of sent characters (may be 0)
		}
		//Wait until STIMx is ready...
		while (ITM->PORT[OS_INTEGER_TRACE_ITM_STIMULUS_PORT].u32 == 0){};
		//then send data, one byte at a time
		ITM->PORT[OS_INTEGER_TRACE_ITM_STIMULUS_PORT].u8 = (uint8_t) (*buf++);
    }
	return (ssize_t)nbyte;//all characters successfully sent
}
	#endif // defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
#endif // OS_USE_TRACE_ITM
#if defined(OS_USE_TRACE_SEMIHOSTING_STDOUT)
static ssize_t _trace_write_semihosting_stdout (const char* buf, size_t nbyte)
{
	#if (defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)) && !defined(OS_HAS_NO_CORE_DEBUG)
	//Check if the debugger is enabled. CoreDebug is available only on CM3/CM4.
	if((CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk) == 0)
    {
		//If not, pretend we wrote all bytes
		return (ssize_t) (nbyte);
    }
	#endif // defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
	static int handle;
	void* block[3];
	int ret;
	if (handle == 0)
    {
		//On the first call get the file handle from the host
		block[0] = ":tt"; // special filename to be used for stdin/out/err
		block[1] = (void*) 4; // mode "w"
		//length of ":tt", except null terminator
		block[2] = (void*) (sizeof(":tt") - 1);
		ret = call_host (SEMIHOSTING_SYS_OPEN, (void*) block);
		if (ret == -1) return -1;
		handle = ret;
    }
	block[0] = (void*) handle;
	block[1] = (void*) buf;
	block[2] = (void*) nbyte;
	//send character array to host file/device
	ret = call_host(SEMIHOSTING_SYS_WRITE, (void*) block);
	//this call returns the number of bytes NOT written (0 if all ok)
	//-1 is not a legal value, but SEGGER seems to return it
	if (ret == -1) return -1;
	//The compliant way of returning errors
	if (ret == (int) nbyte) return -1;
	//Return the number of bytes written
	return (ssize_t) (nbyte) - (ssize_t) ret;
}
#endif // OS_USE_TRACE_SEMIHOSTING_STDOUT
#if defined(OS_USE_TRACE_SEMIHOSTING_DEBUG)
	#define OS_INTEGER_TRACE_TMP_ARRAY_SIZE  (16)
static ssize_t _trace_write_semihosting_debug (const char* buf, size_t nbyte)
{
	#if (defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)) && !defined(OS_HAS_NO_CORE_DEBUG)
	//Check if the debugger is enabled. CoreDebug is available only on CM3/CM4.
	//[Contributed by SourceForge user diabolo38]
	if((CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk) == 0)
    {
		//If not, pretend we wrote all bytes
		return (ssize_t) (nbyte);
    }
	#endif // defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
	//Since the single character debug channel is quite slow, try to
	//optimise and send a null terminated string, if possible.
	if(buf[nbyte] == '\0')
    {
		//send string
		call_host(SEMIHOSTING_SYS_WRITE0, (void*) buf);
    }
	else
    {
		//If not, use a local buffer to speed things up
		char tmp[OS_INTEGER_TRACE_TMP_ARRAY_SIZE];
		size_t togo = nbyte;
		while (togo > 0)
        {
			unsigned int n = ((togo < sizeof(tmp)) ? togo : sizeof(tmp));
			unsigned int i = 0;
			for (; i < n; ++i, ++buf) tmp[i] = *buf;
			tmp[i] = '\0';
			call_host (SEMIHOSTING_SYS_WRITE0, (void*) tmp);
			togo -= n;
        }
    }
	//All bytes written
	return (ssize_t) nbyte;
}
#endif // OS_USE_TRACE_SEMIHOSTING_DEBUG
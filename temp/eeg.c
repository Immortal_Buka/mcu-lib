//ver180220
#include "eeg.h"
//
static void* uart;
//
void eeg_init(void* print_uart)
{
	uart = print_uart;
}
uint8_t eeg_init_file(FIL* file, file_header_t* header)
{
	uint8_t temp[64];
	uint8_t size;
	datetime_t datetime;
	channel_header_t ch_header;
	eeg_print("\r\nInput file name: ");
	eeg_ask_input(temp, &size);
	FRESULT status = f_open(&file, _T(temp), (FA_CREATE_ALWAYS|FA_WRITE));
	if ((status)&&(status != FR_EXIST))
	{
		edf_print("\r\nFile create error !!!");
		return 1;
	}
	else
	{
		header->eeg_file_key = EEG_KEY;
		eeg_print("\r\nInput patient_id: ");
		eeg_ask_input(temp, &size);
		string_to_int32(temp, header->patient_id);
		eeg_print("\r\nInput record_id: ");
		eeg_ask_input(temp, &size);
		string_to_int32(temp, header->record_id);
		eeg_print("\r\nInput description text: ");
		eeg_ask_input(temp, &size);
		if(size > 31) size = 31;
		for(uint8_t i=0; i<size; i++) header->text[i] = temp[i];
		eeg_print("\r\nInput number of channels: ");
		eeg_ask_input(temp, &size);
		string_to_uint8(temp, header->number_of_channels);
		for(uint8_t j=0; j<header->number_of_channels; j++)
		{
			eeg_print("\r\nInput channel name: ");
			eeg_ask_input(temp, &size);
			if(size > 7) size = 7;
			for(uint8_t i=0; i<size; i++) ch_header->name[i] = temp[i];
			eeg_print("\r\nInput channel sample rate: ");
			eeg_ask_input(temp, &size);
			string_to_uint16(temp, ch_header->sample_rate);
			eeg_print("\r\nInput channel description text: ");
			eeg_ask_input(temp, &size);
			if(size > 31) size = 31;
			for(uint8_t i=0; i<size; i++) ch_header->text[i] = temp[i];
			eeg_print("\r\nInput logical max: ");
			eeg_ask_input(temp, &size);
			string_to_int32(temp, ch_header->data_max);
			eeg_print("\r\nInput logical min: ");
			eeg_ask_input(temp, &size);
			string_to_int32(temp, ch_header->data_min);
			eeg_print("\r\nInput phisical max: ");
			eeg_ask_input(temp, &size);
			string_to_int32(temp, ch_header->real_max);
			eeg_print("\r\nInput phisical min: ");
			eeg_ask_input(temp, &size);
			string_to_int32(temp, ch_header->real_min);
			eeg_print("\r\nInput phisical dimension: ");
			eeg_ask_input(temp, &size);
			if(size > 4) size = 4;
			for(uint8_t i=0; i<size; i++) ch_header->dimension[i] = temp[i];
			eeg_wr(file, sizeof(file_header_t) + j*sizeof(channel_header_t));
		}
		return 0;
		/*eeg_get_time(datetime);
		header.start_time.date.year = datetime.date.year;
		header.start_time.date.month = datetime.date.month;
		header.start_time.date.day = datetime.date.day;
		header.start_time.date.year = datetime.date.year;
		header.start_time.date.year = datetime.date.year;*/
	}
}
void eeg_ask_input(uint8_t* data, uint8_t* lenght)
{
	uint8_t temp;
	*lenght = 0;
	do
	{
		temp = eeg_get_char();
		if(temp < 0x21)
		{
			data[(*lenght)++] = 0;
			return;
		}
		else data[(*lenght)++] = temp;
	}
	while((*lenght) < 64)
}
inline void eeg_print(uint8_t* string)
{
	uart_print_string(uart, string);
}
inline uint8_t eeg_get_char(void)
{
	return uart_get_char(uart);
}

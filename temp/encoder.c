#include "encoder.h"
#include <stm32f4xx.h>

// Энкодер цепляется на выводы PB6/PB7

void enc_Init(void)
{
	//gpio - b6,7
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
    GPIOB->AFR[0] = (2<<(6*4))|(2<<(7*4));
    GPIOB->MODER = (2<<(6*2))|(2<<(7*2));
    //tim4
    RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
    TIM4->ARR = 0xFFFF;
    TIM4->PSC = 0;
    TIM4->SMCR &= ~(TIM_SMCR_SMS);
    TIM4->SMCR |= (TIM_SMCR_SMS_0);
    TIM4->CCER &= ~(TIM_CCER_CC1E | TIM_CCER_CC2E);
    TIM4->CCMR1 &= ~(TIM_CCMR1_CC1S | TIM_CCMR1_CC2S);
    TIM4->CCMR1 |= (TIM_CCMR1_CC1S_0 | TIM_CCMR1_CC2S_0);
    TIM4->CCER |= (TIM_CCER_CC1E | TIM_CCER_CC2E);
    TIM4->CNT = 0;
    TIM4->CR1 |= TIM_CR1_CEN;
}
// Посмотреть относительное перемещение (по сравнению с прошлым вызовом)
int16_t enc_GetRelativeMove(void)
{
    static uint16_t CNT_last = 0;
    uint16_t CNT_now = TIM4->CNT;
    int16_t CNT_diff = (int16_t)(CNT_now - CNT_last);
    CNT_last = CNT_now;
    return CNT_diff;
}
// Посмотреть положение энкодера
int16_t enc_GetPosition(void)
{
    return (int16_t)TIM4->CNT;
}

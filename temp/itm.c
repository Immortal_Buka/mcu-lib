#include "itm.h"
//
void ITM_Init(void)
{
	DBGMCU->CR |= DBGMCU_CR_TRACE_IOEN;
	CoreDebug->DEMCR = CoreDebug_DEMCR_TRCENA_Msk;
	TPI->ACPR = 167;
	TPI->SPPR = 2; // Pin protocol = NRZ/USART
	TPI->FFCR = 0x100; // TPIU packet framing enabled when bit 2 is set. You can use 0x100 if you only need DWT/ITM and not ETM.
	DWT->CTRL =
		DWT_CTRL_CYCTAP_Msk|
		DWT_CTRL_POSTPRESET_Msk|
		DWT_CTRL_PCSAMPLENA_Msk|
		DWT_CTRL_EXCTRCENA_Msk|
		DWT_CTRL_CYCCNTENA_Msk;
	ITM->LAR = 0xC5ACCE55;
	ITM->TCR = ITM_TCR_TraceBusID_Msk|ITM_TCR_DWTENA_Msk|ITM_TCR_SYNCENA_Msk|ITM_TCR_ITMENA_Msk;
	ITM->TER = 0xFFFFFFFF; // Enable all stimulus ports
}
void ITM_Send(uint32_t port, uint32_t value)
{
    if((ITM->TCR & ITM_TCR_ITMENA_Msk) && (ITM->TER & (1 << port)))
    {
        while (ITM->PORT[port].u32 == 0);
        ITM->PORT[port].u32 = value;
    }
}

#ifndef __LWIPOPTS_H__
#define __LWIPOPTS_H__

//#define LWIP_NOASSERT
#define LWIP_STATS   			0
#define LWIP_STATS_DISPLAY 		0
#define LINK_STATS   			0
#define	ETHARP_STATS  			0
#define IP_STATS   				0
#define IPFRAG_STATS   			0
#define ICMP_STATS   			0
#define IGMP_STATS   			0
#define UDP_STATS   			0
#define CP_STATS   				0
#define MEM_STATS   			0
#define MEMP_STATS   			0
#define SYS_STATS   			0
#define IP6_STATS 				0
#define ICMP6_STATS  			0
#define IP6_FRAG_STATS 			0
#define MLD6_STATS  			0
#define ND6_STATS  				0
#define MIB2_STATS   			0
#define LWIP_DBG_MIN_LEVEL   	LWIP_DBG_LEVEL_ALL
#define LWIP_DBG_TYPES_ON  		LWIP_DBG_OFF
#define ETHARP_DEBUG   			LWIP_DBG_OFF
#define NETIF_DEBUG   			LWIP_DBG_OFF
#define PBUF_DEBUG   			LWIP_DBG_OFF
#define API_LIB_DEBUG   		LWIP_DBG_OFF
#define API_MSG_DEBUG   		LWIP_DBG_OFF
#define SOCKETS_DEBUG   		LWIP_DBG_OFF
#define ICMP_DEBUG   			LWIP_DBG_OFF
#define IGMP_DEBUG   			LWIP_DBG_OFF
#define INET_DEBUG   			LWIP_DBG_OFF
#define IP_DEBUG   				LWIP_DBG_OFF
#define IP_REASS_DEBUG   		LWIP_DBG_OFF
#define RAW_DEBUG   			LWIP_DBG_OFF
#define MEM_DEBUG   			LWIP_DBG_OFF
#define MEMP_DEBUG   			LWIP_DBG_OFF
#define SYS_DEBUG   			LWIP_DBG_OFF
#define TIMERS_DEBUG   			LWIP_DBG_OFF
#define TCP_DEBUG   			LWIP_DBG_OFF
#define TCP_INPUT_DEBUG   		LWIP_DBG_OFF
#define TCP_FR_DEBUG   			LWIP_DBG_OFF
#define TCP_RTO_DEBUG   		LWIP_DBG_OFF
#define TCP_CWND_DEBUG   		LWIP_DBG_OFF
#define TCP_WND_DEBUG   		LWIP_DBG_OFF
#define TCP_OUTPUT_DEBUG   		LWIP_DBG_OFF
#define TCP_RST_DEBUG   		LWIP_DBG_OFF
#define TCP_QLEN_DEBUG   		LWIP_DBG_OFF
#define UDP_DEBUG   			LWIP_DBG_OFF
#define TCPIP_DEBUG   			LWIP_DBG_OFF
#define SLIP_DEBUG   			LWIP_DBG_OFF
#define DHCP_DEBUG   			LWIP_DBG_OFF
#define AUTOIP_DEBUG   			LWIP_DBG_OFF
#define DNS_DEBUG   			LWIP_DBG_OFF
#define IP6_DEBUG   			LWIP_DBG_OFF
#define DHCP6_DEBUG   			LWIP_DBG_OFF
#define LWIP_PERF   							0
#define NO_SYS   								1
#define LWIP_TIMERS   							1
#define LWIP_TIMERS_CUSTOM   					0
#define MEMCPY(dst, src, len)   				memcpy(dst,src,len)
#define SMEMCPY(dst, src, len)  				memcpy(dst,src,len)
#define LWIP_MPU_COMPATIBLE   					0
#define LWIP_TCPIP_CORE_LOCKING   				0
#define LWIP_TCPIP_CORE_LOCKING_INPUT   		0
#define SYS_LIGHTWEIGHT_PROT   					0
#define MEM_LIBC_MALLOC   						0
#define MEMP_MEM_MALLOC   						0
#define MEM_ALIGNMENT   						4
#define MEM_SIZE   								(20*1024)
#define MEMP_OVERFLOW_CHECK   					1
#define MEMP_SANITY_CHECK   					1
#define MEM_USE_POOLS   						1
#define EM_USE_POOLS_TRY_BIGGER_POOL   			0
#define MEMP_USE_CUSTOM_POOLS   				0
#define LWIP_ALLOW_MEM_FREE_FROM_OTHER_CONTEXT	0
#define MEMP_NUM_PBUF   						20
#define MEMP_NUM_RAW_PCB   						0
#define MEMP_NUM_UDP_PCB   						0
#define MEMP_NUM_TCP_PCB   						1
#define MEMP_NUM_TCP_PCB_LISTEN   				1
#define MEMP_NUM_TCP_SEG   						16
#define MEMP_NUM_REASSDATA   					5
#define MEMP_NUM_FRAG_PBUF   					15
#define MEMP_NUM_ARP_QUEUE   					0
#define MEMP_NUM_IGMP_GROUP   					0
#define MEMP_NUM_NETBUF   						0
#define MEMP_NUM_NETCONN   						0
#define MEMP_NUM_TCPIP_MSG_API   				0
#define MEMP_NUM_TCPIP_MSG_INPKT   				0
#define MEMP_NUM_NETDB  						0
#define MEMP_NUM_LOCALHOSTLIST   				0
#define PBUF_POOL_SIZE   						30
#define MEMP_NUM_API_MSG   						MEMP_NUM_TCPIP_MSG_API
#define MEMP_NUM_DNS_API_MSG   					MEMP_NUM_TCPIP_MSG_API
#define MEMP_NUM_SOCKET_SETGETSOCKOPT_DATA   	MEMP_NUM_TCPIP_MSG_API
#define MEMP_NUM_NETIFAPI_MSG   				MEMP_NUM_TCPIP_MSG_API
#define LWIP_MIB2_CALLBACKS   					0
#define LWIP_CHECKSUM_CTRL_PER_NETIF   			0
#define CHECKSUM_GEN_IP   						1
#define CHECKSUM_GEN_UDP   						1
#define CHECKSUM_GEN_TCP   						1
#define CHECKSUM_GEN_ICMP   					1
#define CHECKSUM_GEN_ICMP6   					1
#define CHECKSUM_CHECK_IP   					1
#define CHECKSUM_CHECK_UDP   					1
#define CHECKSUM_CHECK_TCP   					1
#define CHECKSUM_CHECK_ICMP   					1
#define CHECKSUM_CHECK_ICMP6   					1
#define LWIP_CHECKSUM_ON_COPY   				0
#define LWIP_NETCONN 							0
#define LWIP_SOCKET 							0
#define LWIP_TCP   								1
#define TCP_TTL   								(IP_DEFAULT_TTL)
#define TCP_WND   								(4 * TCP_MSS)
#define TCP_MAXRTX   							12
#define TCP_SYNMAXRTX   						6
#define TCP_QUEUE_OOSEQ   						(LWIP_TCP)
#define TCP_MSS   								536
#define TCP_CALCULATE_EFF_SEND_MSS   			1
#define TCP_SND_BUF  							(4 * TCP_MSS)
#define TCP_SND_QUEUELEN   						((4 * (TCP_SND_BUF) + (TCP_MSS - 1))/(TCP_MSS))
#define TCP_SNDLOWAT   							(2 * TCP_MSS)
#define TCP_SNDQUEUELOWAT   					LWIP_MAX(((TCP_SND_QUEUELEN)/2), 5)
#define TCP_OOSEQ_MAX_BYTES   					0
#define TCP_OOSEQ_MAX_PBUFS   					0
#define TCP_LISTEN_BACKLOG   					0
#define TCP_DEFAULT_LISTEN_BACKLOG   			0xff
#define TCP_OVERSIZE   							TCP_MSS
#define LWIP_TCP_TIMESTAMPS   					0
#define TCP_WND_UPDATE_THRESHOLD   				LWIP_MIN((TCP_WND / 4), (TCP_MSS * 4))
#define LWIP_EVENT_API  						0
#define LWIP_CALLBACK_API						1
#define LWIP_WND_SCALE   						0
#define MEMP_NUM_SYS_TIMEOUT   					(LWIP_TCP + IP_REASSEMBLY + LWIP_ARP + (2*LWIP_DHCP) + LWIP_AUTOIP + LWIP_IGMP + LWIP_DNS + (PPP_SUPPORT*6*MEMP_NUM_PPP_PCB) + (LWIP_IPV6 ? (1 + LWIP_IPV6_REASS + LWIP_IPV6_MLD) : 0))
#endif /* __LWIPOPTS_H__ */

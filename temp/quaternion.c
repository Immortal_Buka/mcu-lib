//ver181130
#include "quaternion.h"
//
void quaternion_addition(quaternion_t* a, quaternion_t* b, quaternion_t* c)
{
	c->r = a->r + b->r;
	c->i = a->i + b->i;
	c->j = a->j + b->j;
	c->k = a->k + b->k;
}
void quaternion_subtraction(quaternion_t* a, quaternion_t* b, quaternion_t* c)
{
	c->r = a->r - b->r;
	c->i = a->i - b->i;
	c->j = a->j - b->j;
	c->k = a->k - b->k;
}
void quaternion_multiplication(quaternion_t* a, quaternion_t* b, quaternion_t* c)
{
	c->r = a->r * b->r - a->i * b->i - a->j * b->j - a->k * b->k;
	c->i = a->r * b->i + b->r * a->i + a->j * b->k - b->j * a->k;
	c->j = a->r * b->j + b->r * a->j + a->k * b->i - b->k * a->i;
	c->k = a->r * b->k + b->r * a->k + a->i * b->j - b->i * a->j;
}
float32_t quaternion_norm(quaternion_t* a)
{
	return sqrt(a->r*a->r + a->i*a->i + a->j*a->j + a->k*a->k)
}
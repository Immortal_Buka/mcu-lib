//ver181130
#ifndef _Quaternion_H_
#define _Quaternion_H_
//
#include <stdint.h>
//
typedef struct
{
	float32_t r;//real
	float32_t i;//imaginary
	float32_t j;//imaginary
	float32_t k;//imaginary
}
quaternion_t;
//
void quaternion_addition(quaternion_t* a, quaternion_t* b, quaternion_t* c);//c=a+b
void quaternion_subtraction(quaternion_t* a, quaternion_t* b, quaternion_t* c);//c=a-b
void quaternion_multiplication(quaternion_t* a, quaternion_t* b, quaternion_t* c);//c=a*b
float32_t quaternion_norm(quaternion_t* a);
#endif//_Quaternion_H_

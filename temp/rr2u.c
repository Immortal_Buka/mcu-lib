//ver170927
#include "rr2u.h"
//
void rr2u_programm(uint32_t bank, uint32_t addr, uint32_t data)
{
	uint32_t temp1, temp2;
	*((uint32_t*)(MEM_ADDR|bank|(0x555<<2))) = 0xaaaaaaaa;
	*((uint32_t*)(MEM_ADDR|bank|(0x2aa<<2))) = 0x55555555;
	*((uint32_t*)(MEM_ADDR|bank|(0x555<<2))) = 0xa0a0a0a0;
	*((uint32_t*)(MEM_ADDR|bank|(addr<<2))) = data;
	while(1)
	{
		temp1 = *((uint32_t*)(MEM_ADDR|bank));
		temp2 = *((uint32_t*)(MEM_ADDR|bank));
		if(temp1 == temp2) break;
		else
		{
			temp1 = 0;
			temp2 = 0;
		}
	}
}
void rr2u_unlock_bypass(uint32_t bank)
{
	*((uint32_t*)(MEM_ADDR|bank|(0x555<<2))) = 0xaaaaaaaa;
	*((uint32_t*)(MEM_ADDR|bank|(0x2aa<<2))) = 0x55555555;
	*((uint32_t*)(MEM_ADDR|bank|(0x555<<2))) = 0x20202020;
}
void rr2u_unlock_bypass_programm(uint32_t bank, uint32_t addr, uint32_t data)
{
	uint32_t temp1, temp2;
	*((uint32_t*)(MEM_ADDR|bank|(addr<<2))) = 0xa0a0a0a0;
	*((uint32_t*)(MEM_ADDR|bank|(addr<<2))) = data;
	while(1)
	{
		temp1 = *((uint32_t*)(MEM_ADDR|bank));
		temp2 = *((uint32_t*)(MEM_ADDR|bank));
		if(temp1 == temp2) break;
		else
		{
			temp1 = 0;
			temp2 = 0;
		}
	}
}
void rr2u_unlock_bypass_reset(uint32_t bank)
{
	*((uint32_t*)(MEM_ADDR|bank)) = 0x90909090;
	*((uint32_t*)(MEM_ADDR|bank)) = 0;
}

void rr2u_sector_erase(uint32_t bank, uint8_t sector)
{
	uint32_t temp1, temp2;
	*((uint32_t*)(MEM_ADDR|bank|(0x555<<2))) = 0xaaaaaaaa;
	*((uint32_t*)(MEM_ADDR|bank|(0x2aa<<2))) = 0x55555555;
	*((uint32_t*)(MEM_ADDR|bank|(0x555<<2))) = 0x80808080;
	*((uint32_t*)(MEM_ADDR|bank|(0x555<<2))) = 0xaaaaaaaa;
	*((uint32_t*)(MEM_ADDR|bank|(0x2aa<<2))) = 0x55555555;
	*((uint32_t*)(MEM_ADDR|bank|(sector<<18))) = 0x30303030;
	while(1)
	{
		temp1 = *((uint32_t*)(MEM_ADDR|bank));
		temp2 = *((uint32_t*)(MEM_ADDR|bank));
		if(temp1 == temp2) break;
		else
		{
			temp1 = 0;
			temp2 = 0;
		}
	}
}
uint32_t rr2u_read(uint32_t bank, uint32_t addr)
{
	uint32_t temp = *((uint32_t*)(MEM_ADDR|bank|(addr<<2)));
	return temp;
}

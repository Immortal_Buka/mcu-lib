typedef enum
{
	startup_tx_lines_chk,
	startup_wait_controller_req,
	
} sn_states_dev;
typedef enum
{
	chk_line = 0xde,
	init_req = 0x37,
	set_addr = 0x19
} cn_cmd;
typedef enum
{
	zero,
	tx_comleted,
	rx_completed,
	unconnected,
	connected
} sn_states_ld;
typedef struct __attribute__((packed, aligned(4)))
{
	uint8_t addr_dev;
	uint8_t addr_parent;
	uint8_t line_table_index;
	uint8_t reserved[1];
} sn_at8_record;
typedef struct __attribute__((packed, aligned(4)))
{
	uint8_t table_size;
	uint8_t table_last_index;
	uint8_t reserved[2];
} sn_at8_controller;
typedef struct __attribute__((packed, aligned(4)))
{
	uint8_t addr;
	uint8_t cmd;
	uint16_t size;
	uint8_t* data;
} sn_msg;
typedef struct __attribute__((packed, aligned(4)))
{
	void* driver_ptr;
	sn_states_ld status;
} sn_ld;
//
sn_at8_record at8_table[500];
sn_ld ld_table[10];
sn_states_dev state;
//
void sn_startup(sn_ld* ld_table, uint8_t ld_table_size, sn_states_dev* state)
{
	sn_msg msg;
	msg.addr = 0;
	msg.cmd = chk_line;
	msg.size = 0;
	for(uint8_t i=0; i<ld_table_size; i++) sn_tx_msg(&msg, &ld_table[i]);
	*state = startup_wait_controller_req;
}
//
uint8_t cn_at_add_record(addr_table* at, uint8_t addr, void* uart)
{
	if(last_num < 253)
	{
		at->record[last_num].addr = addr;
		at->record[last_num].uart = uart;
		last_num++;
		return 0;
	}
	else return 1;
}
uint8_t cn_c_init_r0(void* uart_list, uint8_t quant, addr_table* at) 
{
	cn_msg msg;
	uint8_t ret_value = 0;
	uint8_t data;
	msg.data = &data;
	for(uint8_t i=0;i<quant;i++)
	{
		msg.addr = 0;
		msg.cmd = chk_exist;
		msg.size = 0;
		if(cn_tx_msg(&msg, uart_list[i]) == answ_ok)
		{
			ret_value++;
			msg.addr = 0;
			msg.cmd = set_addr;
			msg.size = 1;
			msg.size = ret_value+2;
			if(cn_tx_msg(&msg, uart_list[i]) == answ_ok) 
			{
				msg.addr = ret_value+2;
				msg.cmd = chk_exist;
				msg.size = 0;
				if(cn_tx_msg(&msg, uart_list[i]) == answ_ok) cn_at_add_record(at, ret_value+2, uart_list[i]);
				else ret_value--;
			}
			else ret_value--;
		}
	}
	return ret_value;
}
uint8_t cn_c_init_r1(addr_table* at)
{
	cn_msg msg;
	uint8_t data, l_temp_1;
	msg.data = &data;
	uint8_t l_temp_2 = (at->last_num) + 2;
	for(uint8_t i=0; i<(at->last_num); i++)
	{
		mem.addr = at->record[i].addr;
		mem.cmd = init_r1;
		mem.size = 0;
		if(cn_tx_msg(&msg, at->record[i].uart) == answ_data)
		{
			l_temp_1 = msg.data;
			for(uint8_t j=0;j<l_temp_1;j++)
			{
				msg.cmd = set_addr_sl;
				msg.size = 1;
				data = l_temp_2 + 1;
				if(cn_tx_msg(&msg, at->record[i].uart) == answ_ok) 
				{
					cn_at_add_record(at, data, at->record[i].uart);
					l_temp_2++;
				}
			}
		}
	}
}

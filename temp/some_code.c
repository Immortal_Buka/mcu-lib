int a[M][N];
&a[i][j] == (int*)(a + i*N + j);
//
#define SUM(x,y) (a##x + a##y)
int a1 = 5, a2 = 3;
printf("%d", SUM(1, 2));//(a1 + a2)
//
.S file:
	.section .rawdata
	.global bin_start
	bin_start:
	.incbin "raw.bin"
	.global bin_end
	bin_end:
.ld file:
	.data :
	{
		*(.rawdata*)
	}
//

typedef struct
{
	modbus_rtu_header     header;
	uint16_t              regStart;
	uint16_t              regNumbers;
	uint8_t               ByteCount;      /* ByteCount = query.RegNumbers * 2 */
	uint16_t              raw[];          /* raw[query.regNumbers] - CRC */
} qPresetMultipleRegisters;
//
uint16_t* ptr_1 = 0x40001234;
qPresetMultipleRegisters* ptr_2 = (qPresetMultipleRegisters*)ptr_1;
ptr_2->raw[i];